class ErinTodd extends ScriptedPawnFemale;

#exec TEXTURE IMPORT NAME=ErinTodd_Head FILE=Textures\Skins\ErinTodd_Head.pcx GROUP=Skins FLAGS=2

defaultproperties
{
    BindName="ErinTodd"
    FamiliarName="Erin Todd"
    UnfamiliarName="Activist"
    Archetype=AT_Military
    WalkingSpeed=0.300000
    GroundSpeed=200
    bImportant=True
    bInvincible=True
    bFollowPersistently=True
    Mesh=LodMesh
    Mesh=LodMesh'DeusExCharacters.GFM_TShirtPants'
    MultiSkins(0)=Texture'MachineGod.Skins.ErinTodd_Head'
    MultiSkins(1)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(2)=Texture'MachineGod.Skins.ErinTodd_Head'
    MultiSkins(3)=Texture'DeusExItems.Skins.GrayMaskTex'
    MultiSkins(4)=Texture'DeusExItems.Skins.BlackMaskTex'
    MultiSkins(5)=None
    MultiSkins(6)=Texture'MachineGod.Skins.TacticalPants4'
    MultiSkins(7)=Texture'MachineGod.Skins.BulletProofVest4_Female_Skin4'
}
