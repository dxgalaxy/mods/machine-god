---
title: "Deus Ex: Machine God"
description: A direct continuation of the Deus Ex story through the eyes of nano-augmented field operative Tiffany Savage
image: /img/banner.jpg
---

@social
    heading: Follow us!
    links:
        - image: img/gitlab.png
          url: https://gitlab.com/dxgalaxy/mods/machine-god
          name: Machine God on GitLab
        - image: img/moddb.png
          url: https://www.moddb.com/mods/deus-ex-machine-god
          name: Machine God on ModDB
        - image: img/discord.png
          url: https://discord.gg/P8CEk5ZZpF
          name: Machine God on Discord

@admonition
    ---
    type: important
    title: Voice acting
    ---
    The voices in Deus Ex: Machine God are performed by a mix of real voice actors and text-to-speech software. In some cases, the recordings have been altered to better impersonate their respective characters. If you are the owner of one of these impersonated voices and would like us to change something, please get in touch [here](mailto:hello@dxgalaxy.org)

JC Denton has decided to merge with Helios, the all-knowing AI managing the international communications protocol "Aquinas", in order to unify mankind through mandatory nanite infusion. You assume the role of recently nano-augmented field operative Tiffany Savage and go on a mind bending journey to sabotage their authoritarian design. How (and if) you choose to do so, is entirely up to you.

Deus Ex: Machine God is a story mod for Deus Ex, the plot being a direct continuation of the main story through the eyes of Tiffany Savage.


## Downloads 

@download
    files:
        - name: "Machine God (demo) - build 0.3.0"
          description: "This is the official demo of Machine God. It takes you through chapter one of Tiffany's epic journey to save her father's lab, and perhaps humanity as a whole, from JC/Helios' authoritarian scheme."
          id: 280208"
        - name: "Machine God (testing)"
          description: "This is a testing release. We try to keep it playable, but there might be game breaking bugs and crashes."
          id: 270655"


## How to install & run

@admonition
    ---
    type: important
    title: Other story mods
    ---
    Make sure you don't have any story mods installed directly into your base game folder, as this will cause issues with conversations. Story mods should be in their own separate folders like shown below.

Unpack the `MachineGod-*.zip` file into your Deus Ex folder, like this:

- 📁 DeusEx
    - ⚙️ DXMG.exe
    - 📁 MachineGod

Run the `DXMG.exe` program to start playing!


### Found a problem?

Go to our [GitLab](https://gitlab.com/dxgalaxy/mods/machine-god) repo to submit a bug report, or just send us a quick message on [Discord](https://discord.gg/P8CEk5ZZpF).


### Playing without the launcher

Run DeusEx.exe with these parameters (change `C:\DeusEx` to your install directory):

```
INI="C:\DeusEx\MachineGod\System\DeusEx.ini" USERINI="C:\DeusEx\MachineGod\System\User.ini"
```

#### Steam

Set the above as your launch parameters.  

#### Linux

Run DeusEx.exe through Wine from a terminal and append the above to the end.  

#### Windows

Create a shortcut to DeusEx.exe and use the above in the location field after the `C:\DeusEx\System\DeusEx.exe` part.  


## Videos

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/NU8sr_YWSLk?si=78qieAJf3evToQjq" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/efnhlb3OUtM?si=LSbqw6tHX3GZPaHr" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

## Images

@gallery
    images:
        - name: "Tracer Tong concept by Alkasushi"
          filename: "Tracer_Tong_concept_by_Alkasushi.jpg"
        - name: "Cover art by Alkasuhi, Artifechs & Heartspowl"
          filename: "Machine_God_cover.jpg"
        - name: "Triad fit Paul Denton by cchr11"
          filename: "TriadPaulDenton.jpg"
        - name: "Morgan Everett concept art by Alkasushi"
          filename: "Everett.jpg"
        - name: "Julia Montes concept art by Alkasushi"
          filename: "Julia_Montes_concept.jpg"
        - name: "El Coyote concept art by Alkasushi"
          filename: "El_Coyote_concept.jpg"
        - name: "Adam and Tiffany by Alkasushi"
          filename: "Adam_and_Tiffany_art.jpg"
        - name: "Tiffany's coat concept art by Alkasushi"
          filename: IMG_20241108_212606-1.jpg"
        - name: "Tiffany's various outfits"
          filename: Tiff.1.jpg"
        - name: "Alex Jacobson working for The Illuminati"
          filename: Pasted_image.1.png"
        - name: "Tijuana cops"
          filename: image-2.png"
        - name: "Tijuana cops"
          filename: image-3.png"
        - name: "Modern conversation UI and camera angles"
          filename: image-1.png"
        - name: "Mike the driver"
          filename: Mike2.jpg"
        - name: "JC/Helios"
          filename: JC2.jpg"
        - name: "JC/Helios concept by Alkasushi"
          filename: Capture_decran_2024-08-06_203143.png"
        - name: "Tiffany and Miguel by Alkasushi"
          filename: Tiffany_and_Miguel_by_Alkasushi.jpg"
        - name: "photo 2024 05 31 19 38 29"
          filename: photo_2024-05-31_19-38-29.jpg"
        - name: "Early Mike"
          filename: Pasted_image_5.jpg"
        - name: "JC/Helios"
          filename: Pasted_image_4.jpg"
        - name: "Vandenberg"
          filename: Pasted_image_3.jpg"
        - name: "Title screen"
          filename: photo_2024-06-02_08-09-10.jpg" 


## Credits

@credits
    #### Artifechs  
    Concept  
    Dialogue  
    Level design  
    Programming  
    Project management  
    Story  
    Textures  
    Voice acting  

    #### Alkasushi  
    Promo & concept art  

    #### AmazingBodilyFluids
    Story  
    Testing  

    #### Ermacgerd
    Testing

    #### FineKone
    Character design  
    Story
    Voice acting  
    
    #### FredrikVA
    Voice acting
    
    #### Heartspowl  
    Character design
    Promo & concept art
    Story   

    #### hugelogan  
    Music  

    #### LoadLineCalibration
    Launcher  

    #### Magmadiver
    Testing  

    #### unfa
    Voice acting   
    
    #### zaiden   
    World building   

    #### Special thanks
    aizome8086  
    Defaultmom001  
    Greasel  
    Rubber Ducky WCCC  
    TheAstropath   


## Roadmap

##### ⚠️ Spoilers ahead

@roadmap
