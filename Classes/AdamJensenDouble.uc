class AdamJensenDouble extends ScriptedPawnMale;

defaultproperties
{
    bPlayIdle=False
    BarkBindName="AdamJensen"
    BindName="AdamJensen"
    FamiliarName="Adam Jensen"
    UnfamiliarName="Adam Jensen"
    WalkingSpeed=0.300000
    bImportant=True
    bInvincible=True
    bFollowPersistently=True
    BaseAssHeight=-23.000000
    walkAnimMult=1.050000
    GroundSpeed=320.000000
    Mesh=LodMesh'DeusExCharacters.GM_Trench'
    MultiSkins(0)=Texture'MachineGod.Skins.AdamJensen_Head'
    MultiSkins(1)=Texture'MachineGod.Skins.AdamJensen_Coat'
    MultiSkins(2)=Texture'MachineGod.Skins.TacticalPants4'
    MultiSkins(3)=None
    MultiSkins(4)=Texture'MachineGod.Skins.AdamJensen_ShirtFront'
    MultiSkins(5)=Texture'MachineGod.Skins.AdamJensen_Coat'
    MultiSkins(6)=Texture'DeusExItems.Skins.GrayMaskTex'
    MultiSkins(7)=Texture'DeusExItems.Skins.BlackMaskTex'
    Orders='Standing'
}

