class Mission16Dream extends Mission16;

// BEGIN UE1

function TravelToMap()
{
    StartSequence('Dream', 'Init');
}

function UpdateSequence(name Sequence, out name Step)
{
    switch(Sequence)
    {
        case 'Dream':
            switch(Step)
            {
                case 'Init':
                    if(Level.Game.GameSpeed == 0.75)
                        Step = 'Bark1';

                    else
                        Level.Game.SetGameSpeed(0.75);
                    
                    break;

                case 'Bark1': // Hurry up...
                    if(IsConversationDone('JCKidBark1')) 
                        Step = 'RunToDoor';
                    
                    else if(
                        !IsPawnConversing('JCKid') && 
                        GetActorDistance('JCKid', 'Player') < 256
                    )
                        StartConversation('JCKid', 'JCKidBark1');
                
                    break;

                case 'RunToDoor':
                    if(AreActorsOverlapping('JCKid', 'JCDoorPoint'))
                        Step = 'Bark2';

                    else if(!IsPawnFollowingOrders('JCKid', 'RunningTo', 'JCDoorPoint'))
                        OrderPawn('JCKid', 'RunningTo', 'JCDoorPoint');
        
                    break;

                case 'Bark2': // I can see the ball...
                    if(IsConversationDone('JCKidBark2')) 
                        Step = 'Bark3';
                    
                    else if(
                        !IsPawnConversing('JCKid') &&
                        GetActorDistance('JCKid', 'Player') < 256
                    )
                        StartConversation('JCKid', 'JCKidBark2');
                
                    break;
                
                case 'Bark3': // What are you doing...
                    if(
                        IsConversationDone('JCKidBark3') ||
                        AreActorsOverlapping('Player', 'DreamVent')
                    ) 
                        Step = 'Bark4';
                    
                    else if(
                        !IsPawnConversing('JCKid') &&
                        IsPlayerCarrying('BoxMedium')
                    )
                        StartConversation('JCKid', 'JCKidBark3');
                
                    break;
                
                case 'Bark4': // Wow, that's cool...
                    if(
                        IsConversationDone('JCKidBark4') ||
                        AreActorsOverlapping('Player', 'DreamOffice')
                    ) 
                        Step = 'Bark5';
                    
                    else if(
                        !IsPawnConversing('JCKid') &&
                        AreActorsOverlapping('Player', 'DreamVent')
                    )
                        StartConversation('JCKid', 'JCKidBark4', False, True);
                
                    break;
                
                case 'Bark5': // Can you open the door...
                    if(
                        IsConversationDone('JCKidBark5') &&
                        IsDoorOpen('DreamDoor')
                    )
                    {
                        DoEvent('WakeUp');
                        Step = 'End';
                    }
                    else if(
                        !IsPawnConversing('JCKid') &&
                        !IsConversationDone('JCKidBark5') &&
                        AreActorsOverlapping('Player', 'DreamOffice')
                    )
                    {
                        StartConversation('JCKid', 'JCKidBark5');
                    }

                    break;
            }
            break;
    }
}

auto state EventManager
{
WakeUp:
    StartConversation('JCKid', 'JCKidBark6');
   
    Fade(0, 3);

    Sleep(0.5);

    while(IsPawnConversing('JCKid'))
        Sleep(0.1);
   
    LoadMap("16_Vandenberg");
    
    EndEvent();
}

// END UE1

