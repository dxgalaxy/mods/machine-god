class JCHelios extends ScriptedPawnMale;

#exec TEXTURE IMPORT NAME=JCHelios_Coat FILE=Textures\Skins\JCHelios_Coat.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=JCHelios_Head FILE=Textures\Skins\JCHelios_Head.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=JCHelios_Pants FILE=Textures\Skins\JCHelios_Pants.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=JCHelios_ShirtFront FILE=Textures\Skins\JCHelios_ShirtFront.pcx GROUP=Skins

defaultproperties
{
    MultiSkins(0)=Texture'MachineGod.Skins.JCHelios_Head'
    MultiSkins(1)=Texture'MachineGod.Skins.JCHelios_Coat'
    MultiSkins(2)=Texture'MachineGod.Skins.JCHelios_Pants'
    MultiSkins(3)=None
    MultiSkins(4)=Texture'MachineGod.Skins.JCHelios_ShirtFront'
    MultiSkins(5)=Texture'MachineGod.Skins.JCHelios_Coat'
    MultiSkins(6)=Texture'DeusExItems.Skins.GrayMaskTex'
    MultiSkins(7)=Texture'DeusExItems.Skins.BlackMasktex'
    bInvincible=True
    Mesh=LodMesh'DeusExCharacters.GM_Trench'
    BindName="JCHelios"
    FamiliarName="JC/Helios"
    UnfamiliarName="JC/Helios"
}
