class TextureImport expands Object abstract; 

// -----------------------------------------------
// UI
// -----------------------------------------------
#exec TEXTURE IMPORT NAME=Black FILE=Textures\UI\Black.pcx GROUP=UI
#exec TEXTURE IMPORT NAME=Logo FILE=Textures\UI\Logo.pcx GROUP=UI MIPS=Off
#exec TEXTURE IMPORT NAME=Title FILE=Textures\UI\Title.pcx GROUP=UI MIPS=Off
#exec TEXTURE IMPORT NAME=Title_1 FILE=Textures\UI\Title_1.pcx GROUP=UI MIPS=Off
#exec TEXTURE IMPORT NAME=Title_2 FILE=Textures\UI\Title_2.pcx GROUP=UI MIPS=Off
#exec TEXTURE IMPORT NAME=AugIconPossession FILE=Textures\UI\AugIconPossession.pcx GROUP=UI MIPS=Off
#exec TEXTURE IMPORT NAME=AugIconPossession_Small FILE=Textures\UI\AugIconPossession_Small.pcx GROUP=UI MIPS=Off
#exec TEXTURE IMPORT NAME=AugIconTelepathy FILE=Textures\UI\AugIconTelepathy.pcx GROUP=UI MIPS=Off
#exec TEXTURE IMPORT NAME=AugIconTelepathy_Small FILE=Textures\UI\AugIconTelepathy_Small.pcx GROUP=UI MIPS=Off
#exec TEXTURE IMPORT NAME=FakeLoading FILE=Textures\UI\FakeLoading.pcx GROUP=UI MIPS=Off FLAGS=2
#exec TEXTURE IMPORT NAME=ComputerLogonLogoAdopter FILE=Textures\UI\ComputerLogonLogoAdopter.pcx GROUP=UI MIPS=Off
#exec TEXTURE IMPORT NAME=ComputerLogonLogoOneiryx FILE=Textures\UI\ComputerLogonLogoOneiryx.pcx GROUP=UI MIPS=Off
#exec TEXTURE IMPORT NAME=ComputerLogonLogoTijuana FILE=Textures\UI\ComputerLogonLogoTijuana.pcx GROUP=UI MIPS=Off
#exec TEXTURE IMPORT NAME=ComputerLogonLogoTracerIndustries FILE=Textures\UI\ComputerLogonLogoTracerIndustries.pcx GROUP=UI MIPS=Off
#exec TEXTURE IMPORT NAME=PlayerPortrait1 FILE=Textures\UI\PlayerPortrait1.pcx GROUP=UI MIPS=Off
#exec TEXTURE IMPORT NAME=PlayerPortrait2 FILE=Textures\UI\PlayerPortrait2.pcx GROUP=UI MIPS=Off
#exec TEXTURE IMPORT NAME=PlayerPortrait3 FILE=Textures\UI\PlayerPortrait3.pcx GROUP=UI MIPS=Off
#exec TEXTURE IMPORT NAME=PlayerPortrait4 FILE=Textures\UI\PlayerPortrait4.pcx GROUP=UI MIPS=Off
#exec TEXTURE IMPORT NAME=PlayerPortrait5 FILE=Textures\UI\PlayerPortrait5.pcx GROUP=UI MIPS=Off

#exec TEXTURE IMPORT NAME=Image_17_Evidence_1 FILE=Textures\DataVaultImages\Image_17_Evidence_1.pcx GROUP=DataVaultImages MIPS=Off
#exec TEXTURE IMPORT NAME=Image_17_Evidence_2 FILE=Textures\DataVaultImages\Image_17_Evidence_2.pcx GROUP=DataVaultImages MIPS=Off
#exec TEXTURE IMPORT NAME=Image_17_Evidence_3 FILE=Textures\DataVaultImages\Image_17_Evidence_3.pcx GROUP=DataVaultImages MIPS=Off
#exec TEXTURE IMPORT NAME=Image_17_Evidence_4 FILE=Textures\DataVaultImages\Image_17_Evidence_4.pcx GROUP=DataVaultImages MIPS=Off

// -----------------------------------------------
// Human textures
// -----------------------------------------------
#exec TEXTURE IMPORT NAME=Torso_Male_Skin1 FILE=Textures\Skins\Torso_Male_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Torso_Male_Skin2 FILE=Textures\Skins\Torso_Male_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Torso_Male_Skin3 FILE=Textures\Skins\Torso_Male_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Torso_Male_Skin4 FILE=Textures\Skins\Torso_Male_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Legs_Male_Skin1 FILE=Textures\Skins\Legs_Male_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Legs_Male_Skin2 FILE=Textures\Skins\Legs_Male_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Legs_Male_Skin3 FILE=Textures\Skins\Legs_Male_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Legs_Male_Skin4 FILE=Textures\Skins\Legs_Male_Skin4.pcx GROUP=Skins

#exec TEXTURE IMPORT NAME=Lenses2 FILE=Textures\Skins\Lenses2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Frames2 FILE=Textures\Skins\Frames2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Frames3 FILE=Textures\Skins\Frames3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Frames4 FILE=Textures\Skins\Frames4.pcx GROUP=Skins

// Carla Brown, TODO: Move to own class
#exec TEXTURE IMPORT NAME=CarlaBrown_Head FILE=Textures\Skins\CarlaBrown_Head.pcx GROUP=Skins
     
// Tim Baker, TODO: Move to own class
#exec TEXTURE IMPORT NAME=TimBaker_Head FILE=Textures\Skins\TimBaker_Head.pcx GROUP=Skins
