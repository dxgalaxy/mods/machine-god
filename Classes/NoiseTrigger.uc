class NoiseTrigger extends Trigger;

var() Sound NoiseSound;
var() Texture NoiseSprite;
var() float NoiseDuration;
var() float NoiseSpriteFlashMin;
var() float NoiseSpriteFlashMax;
var() int NoiseRoll;
var() int NoiseShake;

var DeusExPlayer Player;
var float NoiseTimer;
var float NoiseSpriteFlash;

defaultproperties
{
    NoiseDuration=0.5
}

// BEGIN UE1

function Touch(Actor Other)
{
    if (IsRelevant(Other))
    {
        Player = DeusExPlayer(Other);
        
        if (Player != None)
        {
            GoToState('Noise');
            Super.Touch(Other);
        }
    }
}

function Trigger(Actor Other, Pawn Instigator)
{
    Player = DeusExPlayer(Instigator);

    if(Player != None)
    {
        GoToState('Noise');
    }
}

state Noise
{
Begin:
    if (NoiseSprite != None)
    {
        Player.Sprite = NoiseSprite;
    }

    if (NoiseSound != None)
    {
        Player.PlaySound(NoiseSound);
    }

    if (NoiseShake > 0 || NoiseRoll > 0)
    {
        Player.ShakeView(NoiseDuration, NoiseRoll, NoiseShake);
    }

    if (NoiseSprite != None && NoiseSpriteFlashMax > NoiseSpriteFlashMin)
    {
        NoiseSpriteFlash = NoiseSpriteFlashMin + (NoiseSpriteFlashMax - NoiseSpriteFlashMin) * FRand();

        for (NoiseTimer = NoiseDuration; NoiseTimer > 0; NoiseTimer -= NoiseSpriteFlash)
        {
            Sleep(NoiseSpriteFlash);

            if(Player.Sprite == None)
            {
                Player.Sprite = NoiseSprite;
            }
            else
            {
                Player.Sprite = None;
            }
            
            NoiseSpriteFlash = NoiseSpriteFlashMin + (NoiseSpriteFlashMax - NoiseSpriteFlashMin) * FRand();
        }
    }
    else
    {
        Sleep(NoiseDuration);
    }
    
    Player.Sprite = None;
    GoToState('');
}

// END UE1
