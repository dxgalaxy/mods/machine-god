class ScriptedPawnKnowledge extends KeyPoint;

struct KnowledgeEntry
{
    var int NumSubjects;
    var name Subjects[100];
    var name Object;
    var name Id;
    var string CustomNote;
    var string Location;
};

var KnowledgeEntry Entries[100];
var int NumEntries;
var ExtensionObject EO;

// ------------------
// Adds a custom note
// ------------------
function AddCustomNote(name EntryId, string Note)
{
    Entries[NumEntries].Id = EntryId;
    Entries[NumEntries].CustomNote = Note;

    NumEntries++;
}

// -----------------------------------------------------
// Adds an object: The Tag of a computer, keypad or item
// -----------------------------------------------------
function AddObject(name Object, string Location)
{
    Entries[NumEntries].Id = Object;
    Entries[NumEntries].Object = Object;
    Entries[NumEntries].Location = Location;

    NumEntries++;
}

// ---------------------------------------------------------------
// Adds a subject: The Tag, Alliance or BindName of a ScriptedPawn
// ---------------------------------------------------------------
function AddSubject(name EntryId, name SubjectName)
{
    local int i;

    i = GetEntryIndex(EntryId);

    if(i < 0)
        return;

    Entries[i].Subjects[Entries[i].NumSubjects] = SubjectName;
    Entries[i].NumSubjects++;
}

function int GetEntryIndex(name EntryId)
{
    local int i;

    for(i = 0; i < NumEntries; i++)
        if(Entries[i].Id == EntryId)
            return i;
    
    Log("ERROR (ScriptedPawnKnowledge::GetEntryIndex): \"" $ EntryId $ "\" could not be found");

    return -1;
}

function int GetEntryIndicesFromPawn(ScriptedPawn aPawn, out int Indices[100], optional out name EntryIds[100])
{
    local int i, j, NumIndices;

    if(EO == None)
        EO = new class'ExtensionObject';

    for(i = 0; i < NumEntries; i++)
    {
        for(j = 0; j < Entries[i].NumSubjects; j++)
        {
            if(
                Entries[i].Subjects[j] == aPawn.Tag ||
                Entries[i].Subjects[j] == EO.StringToName(aPawn.BindName) ||
                Entries[i].Subjects[j] == EO.StringToName(aPawn.BarkBindName) ||
                Entries[i].Subjects[j] == aPawn.Alliance
            )
            {
                Indices[NumIndices] = i;
                EntryIds[NumIndices] = Entries[i].Id;
                NumIndices++;
                break;
            }
        }
    }

    return NumIndices;
}
    
// ----------------------------------------------
// Guesses whether a username matches the subject
// ----------------------------------------------
function bool IsUsernameMatch(string Username, ScriptedPawn aPawn)
{
    local string FirstName, FirstNameInitial, Surname, Guesses[5];
    local int i;

    FirstNameInitial = Left(aPawn.FamiliarName, 1);
    Surname = class'Shivs'.static.SplitStr(aPawn.FamiliarName, " ", True);
    FirstName = class'Shivs'.static.ReplaceStr(aPawn.FamiliarName, " " $ Surname, "");

    Guesses[0] = FirstName $ Surname; // AlexJacobson
    Guesses[1] = class'Shivs'.static.StrToLower(FirstName $ Surname); // alexjacobson
    Guesses[2] = FirstNameInitial $ Surname; // AJacobson
    Guesses[3] = class'Shivs'.static.StrToLower(FirstNameInitial $ Surname); // ajacobson
    Guesses[4] = class'Shivs'.static.StrToLower(FirstNameInitial $ "_" $ Surname); // a_jacobson

    for(i = 0; i < ArrayCount(Guesses); i++)
        if(Guesses[i] == Username)
            return True;

    return False;
}
    
// ----------------------------------------------
// Generates a note revealing account information
// ----------------------------------------------
function string GenerateAccountNote(string Username, string Password, string Location)
{
    return "Login for the computer " $ Location $ ": " $ Username $ "/" $ Password;
}
    
// ----------------------------------------
// Generates a note revealing a keypad code
// ----------------------------------------
function string GenerateKeypadNote(string Code, string Location)
{
    return "Code to the keypad " $ Location $ ": " $ Code;
}

// --------------------------------------------------------
// Generates a note about the location of an inventory item
// --------------------------------------------------------
function string GenerateInventoryNote(Inventory aItem, string Location)
{
    return "There is " $ aItem.ItemArticle $ " " $ aItem.ItemName $ " " $ Location; 
}

// ---------------------------------
// Generates a note about a location
// ---------------------------------
function string GenerateLocationNote(LocationID aLocationID, string Location)
{
    return "The " $ aLocationID.LocationName $ " is " $ Location; 
}

function string GetPawnName(ScriptedPawn aPawn)
{
    if(aPawn.FamiliarName != "" && aPawn.FamiliarName != aPawn.UnfamiliarName)
        return aPawn.FamiliarName $ " the " $ class'Shivs'.static.StrToLower(aPawn.UnfamiliarName);
    else if(aPawn.UnfamiliarName != "")
        return "The " $ class'Shivs'.static.StrToLower(aPawn.UnfamiliarName);
    else if(aPawn.BindName != "")
        return aPawn.BindName;
    else
        return string(aPawn.Tag);
}

function string GeneratePawnNote(ScriptedPawn aPawn, string Location)
{
    return GetPawnName(aPawn) $ " is " $ Location;
}

// ----------------------------------
// Gets the note for a specific entry
// ----------------------------------
function string GetEntryNote(int i, ScriptedPawn aPawn)
{
    local Computers aComputer;
    local Keypad aKeypad;
    local Inventory aInventory;
    local LocationID aLocationID;
    local ScriptedPawn aOtherPawn;

    if(i < 0)
        return "";

    // Custom notes
    if(Entries[i].CustomNote != "")
        return Entries[i].CustomNote;

    // Computers
    foreach AllActors(class'Computers', aComputer, Entries[i].Object)
    {
        for(i = 0; i < ArrayCount(aComputer.UserList); i++)
        {
            if(!IsUsernameMatch(aComputer.UserList[i].Username, aPawn))
                continue;

            return GenerateAccountNote(aComputer.UserList[i].Username, aComputer.UserList[i].Password, Entries[i].Location);
        }
    }
        
    // Keypads
    foreach AllActors(class'Keypad', aKeypad, Entries[i].Object)
    {
        return GenerateKeypadNote(aKeypad.ValidCode, Entries[i].Location);
    }

    // Inventory items
    foreach AllActors(class'Inventory', aInventory, Entries[i].Object)
    {
        return GenerateInventoryNote(aInventory, Entries[i].Location);
    }
    
    // Locations
    foreach AllActors(class'LocationID', aLocationID, Entries[i].Object)
    {
        return GenerateLocationNote(aLocationID, Entries[i].Location);
    }

    // People
    foreach AllActors(class'ScriptedPawn', aOtherPawn, Entries[i].Object)
    {
        return GeneratePawnNote(aOtherPawn, Entries[i].Location);
    }

    return "";
}

// ---------------------------------------------------------------
// Checks whether a pawn is a subject in this knowledge collection
// ---------------------------------------------------------------
function bool HasPawn(ScriptedPawn aPawn)
{
    local int Indices[100];

    return GetEntryIndicesFromPawn(aPawn, Indices) > 0;
}

// -------------------------------------------------------
// Gets notes from all entries that a pawn is a subject of
// -------------------------------------------------------
function int GetNotesFromPawn(ScriptedPawn aPawn, out string Notes[100], out name EntryIds[100])
{
    local int i, NumIndices, Indices[100];

    NumIndices = GetEntryIndicesFromPawn(aPawn, Indices, EntryIds);

    for(i = 0; i < NumIndices; i++)
    {
        Notes[i] = GetEntryNote(Indices[i], aPawn);
    }

    return NumIndices;
}

