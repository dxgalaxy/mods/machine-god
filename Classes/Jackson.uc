class Jackson extends ScriptedPawnMale;

defaultproperties
{
    Archetype=AT_Military
    FamiliarName="Jackson"
    BarkBindName="Jackson" 
    Mesh=LodMesh'DeusExCharacters.GM_Jumpsuit'
    MultiSkins(0)=Texture'MachineGod.Skins.AntonJackson_Head_Balaclava'
    MultiSkins(1)=Texture'MachineGod.Skins.TacticalPants3'
    MultiSkins(2)=Texture'MachineGod.Skins.ArmorSweater3_MaleJumpsuit'
    MultiSkins(3)=Texture'MachineGod.Skins.AntonJackson_Head_Balaclava'
    MultiSkins(4)=Texture'MachineGod.Skins.AntonJackson_Head_Balaclava'
    MultiSkins(5)=Texture'MachineGod.Skins.Goggles_2'
    MultiSkins(6)=Texture'MachineGod.Skins.Goggles'
    MultiSkins(7)=Texture'DeusExItems.Skins.PinkMaskTex'
    Texture=Texture'DeusExItems.Skins.PinkMaskTex'
}
