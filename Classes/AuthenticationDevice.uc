class AuthenticationDevice extends HackableDevices;

var() localized string ConfirmationMessage;
var() name AuthenticatedAlliance; 
var() name AuthenticatedTag; 
var() string AuthenticatedBindName; 

// BEGIN UE1

function Frob(Actor aFrobber, Inventory aFrobWith)
{
    local Pawn aPawn;
    local Actor aTriggerActor;

    aPawn = Pawn(aFrobber);

    if(aPawn == None)
        return;

    if(
        Event != '' &&
        (
            (AuthenticatedBindName != "" && aPawn.BindName == AuthenticatedBindName) ||
            (AuthenticatedTag != '' && aPawn.Tag == AuthenticatedTag) ||
            (AuthenticatedAlliance != '' && aPawn.Alliance == AuthenticatedAlliance)
        )
    )
    {
        aPawn.ClientMessage(ConfirmationMessage);

        foreach AllActors(class 'Actor', aTriggerActor, Event)
            aTriggerActor.Trigger(Self, aPawn);
    }
    else
    {
        Super.Frob(aFrobber, aFrobWith);
    }
}

function HackAction(Actor aHacker, bool bHacked)
{
    local Actor aTriggerActor;

    Super.HackAction(aHacker, bHacked);

    if(bHacked && Event != '')
    {
        if(Pawn(aHacker) != None)
            Pawn(aHacker).ClientMessage(ConfirmationMessage);

        PlaySound(Sound'Beep2');

        foreach AllActors(class 'Actor', aTriggerActor, Event)
            aTriggerActor.Trigger(Self, Pawn(aHacker));
    }
}

// END UE1

defaultproperties
{
     ConfirmationMessage="Clearance granted"
     SoundRadius=8
     SoundVolume=255
     SoundPitch=96
     AmbientSound=Sound'DeusExSounds.Generic.SecurityL'
}
