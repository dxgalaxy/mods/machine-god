class Hypnos extends Pigeon;

#exec TEXTURE IMPORT NAME=Hypnos FILE=Textures\Skins\Hypnos.pcx GROUP=Skins FLAGS=2048

defaultproperties
{
    BindName="Hypnos"
    FamiliarName="Hypnos"
    UnfamiliarName="Hypnos"
    MultiSkins(0)=Texture'MachineGod.Skins.Hypnos'
    AirSpeed=300.000000
    bInvincible=True
    bHighlight=False
    bReactPresence=False
    bUnlit=True
    Style=STY_Translucent
    Orders="Standing"
    CollisionHeight=20
    DrawScale=4.0
    bNoSmooth=True
}
