#!/bin/bash

PACKAGE_NAME="MachineGod"
THIS_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
SCRIPT_DIR="$THIS_DIR/Scripts"
DX_HOME="$HOME/.deus-ex-sdk/drive_c/DeusEx"

# Read parameters
GENERATE_SKINS=false
BUILD_FOR_UE2=false
SHOW_LOG=false

for i in "$@"
do
    case $i in
        --ue2)
            BUILD_FOR_UE2=true
            ;;
        --log)
            SHOW_LOG=true
            ;;
        --skins=*)
            GENERATE_SKINS="${i#*=}"
            ;;
    esac
done

# Generate skins
if [ "$GENERATE_SKINS" != false ]
then
    if [ -n "$GENERATE_SKINS" ]
    then
        python "$SCRIPT_DIR/generate_skins.py" -e "$GENERATE_SKINS"
    else
        python "$SCRIPT_DIR/generate_skins.py"
    fi

fi

# Convert textures
python "$SCRIPT_DIR/convert_textures.py"

# Build
if [ "$BUILD_FOR_UE2" == true ]
then
    python "$SCRIPT_DIR/build.py" "$PACKAGE_NAME" "$DX_HOME" -ue2
else
    python "$SCRIPT_DIR/build.py" "$PACKAGE_NAME" "$DX_HOME"
fi

# Show the log
if [ "$SHOW_LOG" == true ]
then
    echo ""
    echo "=======================================";
    echo "Reading log file";
    echo "=======================================";
    echo ""

    tail -f "$HOME/Documents/Deus Ex/System/deusex.log"
fi
