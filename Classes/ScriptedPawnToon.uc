class ScriptedPawnToon extends ScriptedPawnExtended;

var(Display) Texture OutlineTexture;

var ActorOutline aOutline;

// BEGIN UE1

function PostBeginPlay()
{
    Super.PostBeginPlay();
    
    CreateOutline();
}

event TravelPostAccept()
{
    Super.TravelPostAccept();
    
    CreateOutline();
}

function CreateOutline()
{
    if(aOutline == None)
    {
        aOutline = Spawn(class'MachineGod.ActorOutline');
        aOutline.SetOwner(Self);
    }
    else
    {
        aOutline.ApplyMesh();
    }

    if(OutlineTexture != None)
        aOutline.SetOutline(OutlineTexture);

    else if(bHologram)
        aOutline.SetOutline(FireTexture(DynamicLoadObject("Effects.Electricity.Nano_SFX_A", class'FireTexture')));
}

function ChangeOutfit(string Outfit)
{
    Super.ChangeOutfit(Outfit);

    CreateOutline();
}

// END UE1

defaultproperties
{
     AmbientGlow=64
     CarcassType=class'CarcassToon'
}
