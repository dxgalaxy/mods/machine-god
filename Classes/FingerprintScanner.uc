class FingerprintScanner extends AuthenticationDevice;

#exec TEXTURE IMPORT NAME=FingerprintScanner FILE=Textures\Decorations\FingerprintScanner.pcx GROUP=Decorations

defaultproperties
{
     ItemName="Fingerprint Scanner"
     Mesh=LodMesh'DeusExItems.DataCube'
     MultiSkins(0)=Texture'MachineGod.Decorations.FingerprintScanner'
     MultiSkins(1)=Texture'MachineGod.Decorations.FingerprintScanner'
     MultiSkins(2)=Texture'MachineGod.Decorations.FingerprintScanner'
     MultiSkins(3)=Texture'MachineGod.Decorations.FingerprintScanner'
     CollisionRadius=10
     CollisionHeight=10
     Mass=2
     Buoyancy=3
     DrawScale=1.5
}
