class ActorOutline extends Effects;

var() float OutlineSize;
var() Texture OutlineTexture;

// BEGIN UE1

function SetOutline(Texture Tex, optional int Size)
{
    OutlineTexture = Tex;

    if(Size > 0)
        OutlineSize = Size;

    ApplyOverlayTexture();
}

function Tick(float DeltaTime)
{
    local int i;

    Super.Tick(DeltaTime);

    if(Owner == None)
    {
        Destroy();
    }
    else
    {
        bHidden = Mesh == None || Owner.bHidden || Owner.DrawScale == 0;
        Fatness = Owner.Fatness - OutlineSize;
        PrePivot = Owner.PrePivot;
        AnimSequence = Owner.AnimSequence;
        AnimFrame = Owner.AnimFrame;
        AnimRate = Owner.AnimRate;

        for(i = 0; i < 4; i++)
        {
            BlendAnimSequence[i] = Owner.BlendAnimSequence[i];
            BlendAnimFrame[i] = Owner.BlendAnimFrame[i];
            BlendAnimRate[i] = Owner.BlendAnimRate[i];
            BlendTweenRate[i] = Owner.BlendTweenRate[i];
            BlendAnimLast[i] = Owner.BlendAnimLast[i];
            BlendAnimMinRate[i] = Owner.BlendAnimMinRate[i];
            OldBlendAnimRate[i] = Owner.OldBlendAnimRate[i];
            SimBlendAnim[i] = Owner.SimBlendAnim[i];
        }
    }

    Super.Tick(DeltaTime);
}

function ApplyOverlayTexture()
{
    local int i;
   
    if(Owner == None)
        return;

    // Don't draw masked stuff
    for(i = 0; i < ArrayCount(MultiSkins); i++)
    {
        switch(Owner.MultiSkins[i])
        {
            case None:
            case Texture'BlackMaskTex':
            case Texture'PinkMaskTex':
            case Texture'GrayMaskTex':
                MultiSkins[i] = Owner.MultiSkins[i];
            break;
            default:
                MultiSkins[i] = OutlineTexture;
            break;
        }
    }
    
    // Skip Stuff that doesn't look good outlined
    switch(Owner.Mesh)
    {
        case LODMesh'GFM_Dress':
            MultiSkins[4] = Texture'PinkMaskTex';
            MultiSkins[6] = Texture'PinkMaskTex';
            break;
        
        case LODMesh'GM_DressShirt_B':
            MultiSkins[2] = Texture'PinkMaskTex';
            MultiSkins[4] = Texture'PinkMaskTex';
            MultiSkins[5] = Texture'GrayMaskTex';
            MultiSkins[6] = Texture'BlackMaskTex';
            MultiSkins[7] = Texture'BlackMaskTex';
            break;
        
        case LODMesh'GFM_TShirtPants':
            MultiSkins[1]= Texture'PinkMaskTex';
            MultiSkins[2]= Texture'PinkMaskTex';
            MultiSkins[3]= Texture'GrayMaskTex';
            MultiSkins[4] = Texture'BlackMaskTex';
            break;
        
        case LODMesh'GM_Trench':
        case LODMesh'GM_Trench_F':
        case LODMesh'GFM_Trench':
            MultiSkins[5] = Texture'PinkMaskTex';
            MultiSkins[6] = Texture'GrayMaskTex';
            MultiSkins[7] = Texture'BlackMaskTex';
            break;
        
        case LODMesh'GMK_DressShirt':
        case LODMesh'GMK_DressShirt_F':
            MultiSkins[5] = Texture'PinkMaskTex';
            MultiSkins[6] = Texture'GrayMaskTex';
            MultiSkins[7] = Texture'BlackMaskTex';
            break;

        case LODMesh'GM_DressShirt':
        case LODMesh'GM_DressShirt_F':
        case LODMesh'GM_DressShirt_S':
            MultiSkins[6] = Texture'GrayMaskTex';
            MultiSkins[7] = Texture'BlackMaskTex';
            break;
        
        case LODMesh'GFM_SuitSkirt':
        case LODMesh'GFM_SuitSkirt_F':
            MultiSkins[1] = Texture'PinkMaskTex';
            MultiSkins[5] = Texture'PinkMaskTex';
            MultiSkins[6] = Texture'GrayMaskTex';
            MultiSkins[7] = Texture'BlackMaskTex';
            break;
        
        case LODMesh'GM_Jumpsuit':
            MultiSkins[4] = Texture'PinkMaskTex';
            MultiSkins[5] = Texture'PinkMaskTex';
            MultiSkins[6] = Texture'PinkMaskTex';
            break;
        
        case LODMesh'GM_Suit':
            MultiSkins[4] = Texture'PinkMaskTex';
            MultiSkins[5] = Texture'GrayMaskTex';
            MultiSkins[6] = Texture'BlackMaskTex';
            break;
    }
}

function PostBeginPlay()
{
    Super.PostBeginPlay();

    ApplyMesh();
}

function ApplyMesh()
{
    if(Owner == None)
        return;

    switch(Owner.Mesh)
    {
        case LodMesh'GM_DressShirt':
            Mesh = LodMesh'MachineGod.GM_DressShirt_Inverted';
            break;
        
        case LodMesh'GM_DressShirt_B':
            Mesh = LodMesh'MachineGod.GM_DressShirt_B_Inverted';
            break;
        
        case LodMesh'GM_DressShirt_F':
            Mesh = LodMesh'MachineGod.GM_DressShirt_F_Inverted';
            break;
        
        case LodMesh'GM_DressShirt_S':
            Mesh = LodMesh'MachineGod.GM_DressShirt_S_Inverted';
            break;
        
        case LodMesh'GMK_DressShirt':
            Mesh = LodMesh'MachineGod.GMK_DressShirt_Inverted';
            break;
        
        case LodMesh'GMK_DressShirt_F':
            Mesh = LodMesh'MachineGod.GMK_DressShirt_F_Inverted';
            break;
        
        case LodMesh'GM_Jumpsuit':
            Mesh = LodMesh'MachineGod.GM_Jumpsuit_Inverted';
            break;
        
        case LodMesh'GM_Trench':
            Mesh = LodMesh'MachineGod.GM_Trench_Inverted';
            break;
        
        case LodMesh'GM_Trench_F':
            Mesh = LodMesh'MachineGod.GM_Trench_F_Inverted';
            break;
        
        case LodMesh'GM_Suit':
            Mesh = LodMesh'MachineGod.GM_Suit_Inverted';
            break;
        
        case LodMesh'GFM_Dress':
            Mesh = LodMesh'MachineGod.GFM_Dress_Inverted';
            break;
        
        case LodMesh'GFM_SuitSkirt':
            Mesh = LodMesh'MachineGod.GFM_SuitSkirt_Inverted';
            break;
        
        case LodMesh'GFM_SuitSkirt_F':
            Mesh = LodMesh'MachineGod.GFM_SuitSkirt_F_Inverted';
            break;
        
        case LodMesh'GFM_Trench':
            Mesh = LodMesh'MachineGod.GFM_Trench_Inverted';
            break;
        
        case LodMesh'GFM_TShirtPants':
            Mesh = LodMesh'MachineGod.GFM_TShirtPants_Inverted';
            break;

        default:
            Mesh = None;
            break;
    }

    DrawScale = Owner.DrawScale;
   
    Fatness = Owner.Fatness + OutlineSize;
    PrePivot = Owner.PrePivot;
    
    ApplyOverlayTexture();
    
    SetBase(Owner);
}

// END UE1

defaultproperties
{
    AmbientGlow=255
    ScaleGlow=10 
    bBlockActors=False
    bBlockPlayers=False
    bCollideActors=False
    bCollideWorld=False
    CollisionHeight=0
    CollisionRadius=0
    bOwnerNoSee=True
    Physics=PHYS_Trailer
    bTravel=True
    bTrailerSameRotation=True
    bTrailerPrePivot=True
    bUnlit=True
    Texture=None
    OutlineTexture=Texture'BlackMaskTex'
    DrawType=DT_Mesh
    OutlineSize=4
}
