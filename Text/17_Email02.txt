<P>Luis,
<P>
<P>It's getting out of hand.
<P>
<P>A civilian approached me with photo evidence of your dealings with the cartel. Needless to say, if sgt. Estevez sees this, you're toast.
<P>
<P>I've put the civilian under house arrest for now, but if I were you, I'd apply for a transfer very soon. 
<P>
<P>- Clara
