class Mission20Dream extends Mission20;

// BEGIN UE1

function TravelToMap()
{
    SetActorState('Player', 'ParalyzedWithHUD');
    SetBrightness(0);
    StartSequence('FetchBall', 'Bark1');
}

function UpdateSequence(name Sequence, out name Step)
{
    switch(Sequence)
    {
        case 'FetchBall':
            switch(Step)
            {
                case 'Bark1':
                    if(IsConversationDone('JCTeenBark1'))
                    {
                        Step = 'WaitForBark2';
                    }
                    else if(!IsPawnConversing('JCTeen'))
                    {
                        StartConversation('JCTeen', 'JCTeenBark1', False, True);
                    }
                    else if(
                        IsActorInState('Player', 'ParalyzedWithHUD') &&
                        GetSequenceChangeTime(Sequence) > 4.0
                    )
                    {
                        MoveActor('Player', 'PlayerStartLocation');
                        SetActorState('Player', 'PlayerWalking');
                        Fade(1, 1);
                    }

                    break;

                case 'WaitForBark2':
                    if(AreActorsOverlapping('Player', 'GroundUnderLedge'))
                        Step = 'Bark2';

                    break;

                case 'Bark2':
                    if(IsConversationDone('JCTeenBark2'))
                        Step = 'RunToCrouch';

                    else if(!IsPawnConversing('JCTeen'))
                        StartConversation('JCTeen', 'JCTeenBark2', False, True);

                    break;

                case 'RunToCrouch':
                    if(IsPawnStandingAt('JCTeen', 'CrouchPoint'))
                        Step = 'Crouch';

                    else if(!IsPawnFollowingOrders('JCTeen', 'RunningTo', 'CrouchPoint'))
                        OrderPawn('JCTeen', 'RunningTo', 'CrouchPoint');

                    break;

                case 'Crouch':
                    if(IsActorInState('JCTeen', 'Hiding'))
                        Step = 'WaitForBall';
                    
                    else
                        SetActorState('JCTeen', 'Hiding');

                    break;

                case 'WaitForBall':
                    if(IsPlayerCarrying('Basketball'))
                        Step = 'Bark3';

                    break;
    
                case 'Bark3':
                    if(IsConversationDone('JCTeenBark3'))
                        Step = 'WaitForBark4';

                    else if(!IsPawnConversing('JCTeen'))
                        StartConversation('JCTeen', 'JCTeenBark3', False, True);


                case 'WaitForBark4':
                    if(
                        GetPlayer().IsOnFloor() &&
                        GetActorDistance('Player', 'JCTeen') < 128
                    )
                        Step = 'Bark4';

                    break;

                case 'Bark4':
                    if(IsConversationDone('JCTeenBark4'))
                        Step = 'End';

                    else if(!IsPawnConversing('JCTeen'))
                        StartConversation('JCTeen', 'JCTeenBark4');
                    
                    else if(
                        GetSequenceChangeTime(Sequence) > 1.0 &&
                        !IsActorInState('Player', 'PlayerStandingWithHUD')
                    )
                        SetActorState('Player', 'PlayerStandingWithHUD');
                
                    break;
            }
            break;
    }
}

auto state EventManager
{

Distortion1:
    PlayTransientSound('Player', "DeusExSounds.Augmentation.CloakUp", 1.0, 0.2);
    GetPlayer().ShakeView(1, 0, 8);
    GetPlayer().Sprite = FireTexture(DynamicLoadObject("Effects.Electricity.wepn_nesword", class'FireTexture'));
    Sleep(1.0);
    GetPlayer().Sprite = None;

    EndEvent();

Distortion2:
    GetPlayer().ShakeView(2.5, 0, 16);
    GetPlayer().Sprite = FireTexture(DynamicLoadObject("Effects.Electricity.wepn_nesword", class'FireTexture'));
    Sleep(2.5);
    GetPlayer().Sprite = None;
    
    EndEvent();

Distortion3:
    ModernHUD(ModernRootWindow(GetPlayer().RootWindow).HUD).FrobDisplay.Hide();
    GetPlayer().GoToState('ParalyzedWithHUD');
    MoveActor('Player', 'PlayerHeliosPoint');
    MoveActor('JCTeen', 'JCHeliosPoint');
    SetActorHidden('JCTeen', True);

    Sleep(3.0);

    GetPlayer().ShowCredits(True);

    GetPlayer().bIgnoreAllInput = False;
    
    EndEvent();

}

// END UE1
