#!/bin/sh

PACKAGE_NAME="MachineGod"
RETURN_DIR=$PWD
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cd "$SCRIPT_DIR"

VERSION=$(cat ./Package.ini | grep -o '[0-9]\+\.[0-9]\+\.[0-9]\+')

RELEASE_NAME="$PACKAGE_NAME-$VERSION"

if [ -f "$PACKAGE_NAME-$VERSION.zip" ]
then
    echo "A package for version $VERSION already exists."
else
    rm MachineGod*.zip

    mkdir ./dist
    mkdir ./dist/MachineGod
    mkdir ./dist/MachineGod/Maps
    mkdir ./dist/MachineGod/System
    mkdir ./dist/MachineGod/Textures
    
    cp -r ./Maps/*.dx ./dist/MachineGod/Maps
    cp -r ./System/*.ini ./dist/MachineGod/System
    cp -r ./System/*.u ./dist/MachineGod/System
    cp -r ./Textures/*.utx ./dist/MachineGod/Textures
    cp -r ./DXMG.exe ./dist
   
    cd ./dist

    zip -r "../$RELEASE_NAME.zip" *

    cd ..

    rm -rf ./dist

    cd $RETURN_DIR
fi
