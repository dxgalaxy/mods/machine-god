class CarcassProxy extends DeusExPickup;

var Mesh Mesh2;
var Mesh Mesh3;
var name Alliance;

// -------------------------------------
// Stores the information from a carcass
// -------------------------------------
function InitFor(CarcassExtended aCarcass)
{
    local int i;

    Alliance = aCarcass.Alliance;
    Mesh = aCarcass.Mesh; 
    Mesh2 = aCarcass.Mesh2; 
    Mesh3 = aCarcass.Mesh3;
        
    for(i = 0; i < ArrayCount(aCarcass.MultiSkins); i++)
    {
        MultiSkins[i] = aCarcass.MultiSkins[i];
    }

    Texture = aCarcass.Texture;

    DrawScale = aCarcass.DrawScale;
    Fatness = aCarcass.Fatness;
}
