class ConversationPoint extends PatrolPoint;

var() name Conversation;
var() name SpeakerTag;

function BeginPlay()
{
    Super.BeginPlay();

    // Adopt tag and event as conversation and speaker, if necessary
    if(Tag != '' && Conversation == '')
        Conversation = Tag;

    if(Event != '' && SpeakerTag == '')
        SpeakerTag = Event;
}

defaultproperties
{
    Texture=Texture'DeusEx.S_Civilian'
    bDirectional=True
}
