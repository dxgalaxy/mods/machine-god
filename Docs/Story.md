# Deus Ex: Machine God

## Currently unexplained stuff

### Julia

She was working for the NSF the whole time. 
After MJ12 collapsed, it seemed as if the NSF had gone the same way.
In reality, they were operating under the radar with only a few key members on active duty.
Julia went deep undercover, cutting all communication with the NSF for her safety, in a last ditch attempt to strike at The Illuminati before they got back on their feet.
Working as security detail for Morgan Everett, she learned of their plan to use Tiffany to acquire JC's blueprint, and schemed to intercept her once she had it. 
Unable to communicate with the NSF, she couldn't inform them of this plan, which is why when Tiffany meets them, they send her onward without much thought.
However, when Julia loses contact with Tiffany, she blows her cover and returns to the NSF to inform Iris.
Iris and Adam's new objective is to rescue Tiffany and retrieve the JC blueprint.

## Fall of Chinese government

It fell with MJ12

## Triad plot

Helios has infected JC's mind, and is coercing Paul and Tong into doing its bidding
The street drug Henosia is being developed by the triad to faciliate Helios' mind control

### Reason for ending up in the sim

The reason is that Helios planted the idea in the gang member's mind that Tiffany and Miguel were fugitives.
That gang member had been taking Henosia.
Tong helping Gary in the first place was in conflict with JC, he did it out of kinship with Gary.


## Abstract

JC Denton has decided to merge with Helios, the all-knowing AI managing the international communications protocol "Aquinas", in order to unify mankind through mandatory nanite infusion. You assume the role of recently nano-augmented field operative Tiffany Savage and go on a mind bending journey to sabotage their authoritarian design. How (and if) you choose to do so, is entirely up to you.


## Structure

The game follows a basic hero's journey with a few omissions and branches.

```
0. Dream
│
│ Tiffany and JC are children at school, wearing uniforms.
│ They're sneaking around at night, trying to steal back a basketball that was confiscated.
│ They'll come across a locked door, and Tiffany can bypass it through a vent.
│ JC can't follow, so she opens the door for him.
│ Behind the door, a pigeon will wake Tiffany up, with Gary's voice.
│
│
1. Lompoc, Vandenberg
│
│ Tiffany wakes up, and the Vandenberg lab is under siege.
│ She learns of the threat of JC/Helios.
│ The journey begins with a trip to Tijuana to solicit Everett's help.
│ Tracer Tong tips the Savages off to his location.
│
│
2. Tijuana, Slums
│
│ Julia Montes is being harassed by the city guard.
│ She turns out to be Mike's mother, and that his name is actually Miguel.
│ She helps the player contact the cartel in order to enter the city.
│
│
3. Tijuana, Simulation
│
│ Tiffany suddenly finds herself in a simulated world.
│ She learns the power of mind control and hypnonsis here.
│ She emerges from it with the help of Anton Jackson.
│
│
4. Tijuana, Illuminati
│
│ She now finds herself in the Illuminati headquarters.
│ She learns of Morgan Everett's plan to seize the Aquinas Hub.
│ She acquires a new augmentation.
│ She is sent to Hong Kong to steal a blueprint of JC.
│
│
5. Hong Kong
│
│ Tiffany must find Tong's new hidden lab.
│ Julia is feeding her intel as she uncovers it.
│ Inside, she meets Paul, who tells her that she's a Denton, there is no other way she could be receptive to nano-augmentation.
│ He reveals their whole operation with the triads, how they're testing synchronicity through gain-of-function research using the street drug Henosia.
│ She can choose to join them by helping them finish the merge with Helios.
│
│
├───────────────────────────────────────────────┐
Refuse JC/Helios                                Join JC/Helios
│                                               │                                   
│ Tiffany is incapacitated with gas.            │  Tiffany meets JC/Helios and Tong.
│ She is thrown into a cell.                    │  She merges with JC and Helios.   
│ Player view switches to Adam Jensen.          └─ End game.                        
│ Julia contacts him and tells him to save her.
│ Adam breaks into the facility and opens her cell, with help from Tong, since he felt guilty about imprisoning a friend's daughter, and he kinda owes Jensen too.
│ She is unconscious, so Adam has to carry her back to NSF HQ.
│ Tiffany wakes up in a sick bay.
│ If she saved Anton, he shows up here.
│ Julia explains that she was working undercover in Illuminati, but she's with the NSF
│ Their intention is to prevent the Illuminati from taking the Aquinas Hub, instead distributing its source code to the public.
│ When Tiffany tries to establish contact with Gary, he is unreachable.
│ She finds a message from Clara Brown saying JC has broken into Vandenberg and taken control of the UC.
│
│
6. Lompoc, Vandenberg
│
│ Tiffany and Adam return to Vandenberg, which has been reformed with new defenses and traps.
│ They encounter the Illuminati, who are executing a last ditch effort to acquire the blueprint of JC.
│ Tiffany can decide to join or fight them.
│
│
├───────────────────────────────────────────────┐
Refuse Illuminati                               Join Illuminati
│                                               │
│ Alex Jacobson deploys Hypnos on them.         │  They use the blueprint to cripple JC.                                
│ Adam and Tiffany are now in a sim again.      │  The Illuminati strike force (golden masks) overpowers Paul and the Helios agents.
│ As they break out, Tiffany has 2 choices:     └─ End game.  
│                                               
│ 1. Kill Alex and the Illuminati strike force.                        
│ 2. Wake Alex up, and incapacitate the Illuminati strike force with his help.
│
│ Alex explains how Hypnos works: Everyone has a construct in their minds, Hypnos simply performs a man-in-the-middle attack and injects its own.
│ He gives Tiffany the portable Hypnos device and suggests using it on JC.
│ Adam and Tiffany find Gary in hiding with Carla and Tim.
│ Gary tells Tiffany that he and his wife couldn't conveice, so he cloned her using Paul Denton's and Iris' genes.
│ Since Gary was under MJ12 employment, Tiffany was meant to be a UNATCO agent like JC, but Gary and his wife hid her intended purpose from her, and ultimately they all fled, after which Gary's wife was murdered by MJ12.
│ As they proceed, they find JC/Helios and Paul waiting for them.
│ Adam protects Tiffany while she fights JC inside his mind using Hypnos.
│ Tiffany explores JC's memories and discovers that they were childhood friends attending the same school. 
│ JC was often away (to Switzerland), but they'd still meet to play basketball at times.
│ This memory will be the key to waking up JC from his hypnosis.
│
│
7. Resolution
│
│ Tiffany is given the choice between 2 endings.
│
│ 1. Corrupt JC's nanite architecture so the merging fails. In order to prevent Helios falling into the wrong hands, he sacrifices himself to destroy the Aquinas Hub, plunging him into a coma and triggering the collapse.
│ 2. Discover JC's childhood and using a form of "inception", convince him to give up his plan and distribute Helios' source code, making international communications open again.
```

## Mission 20: Hong Kong

- Landing:
    - Mission starts with emergency landing.
    - Scramblers are active all over Hong Kong, so Miguel could not communicate with any landing sites.
    - Only one district had no active scramblers, North Point, so he landed there.
    - This is our "town", the starting point for the chapter.

- Mission start:
    - Julia informs Tiffany about what Tong had been up to:
        - Tong, fixated on the concept of small local governments, intends to be the ruler of Hong Kong.
            - After the Luminous Path and the Red Arrow united, Tong overthrew both former leaders, Max Chen and Gordon Quick.
            - He installed scramblers that obfuscate most communications while he's undertaking his takeover.
            - Tong managed to have Max Chen killed, but Gordon Quick escaped. He is the informant in contact with Julia.
            - When the government fell, Tong's triad confiscated all military and police equipment, so their firepower is immense.
            - This all makes sense because of one line Tong says in DX:IW: "[Billie] has signed her sold over to the luddites, the haters of technology [...] I was like her twenty years ago, and I still share some of the blame for triggering the collapse."
                - Helios' influence has changed his mind from an anarchist into a technocrat.
                - The blame he talks of could be a lack of commitment to this new mindset, he let his new ideology down.
                    - He helps Adam recover Tiffany out of guilt, and she triggers the collapse
            - Paul's unconditional faith in JC is brotherly love, amplified by nanite hypnosis
    - Julia suggests finding Gordon Quick and asking for his help in approaching Tong's lab.
    - Tiffany can find him at North Point.
    - He will give her directions and a way past the triad road block.
- World:
    - Since MJ12 was the de facto Chinese government, it collapsed after the events of DX1
    - City has been sectioned off into zones, officially as a gray death countermeasure
    - Passage between the zones is possible through MTR with a privileged pass or via hidden pathways
        - No one knows why the MTR is still running, no one seems to be maintaining it
    - Some people have started enjoying this existence, it's more communal
    - Many have fled the city because of the increasing wealth gap and scarce food, some stayed behind to rebuild
    - WanChai is upper class, those who live there almost never leave that zone
        - The Venetian canals and fun fair layout of the area is just upperclass pomp

## Adam & Iris rescue mission

- Adam goes to nightclub to get info
- Iris gets to the place first, player arrives to downed guards
- She creates havoc that player uses to progress
- The place is a military complex with insane weaponry and bots
- Moment with giant security bot
- Halfways through, Iris will blow her EMP blast, and all the power will die
  - Backup lights come on, much darker
  - Enemies are paranoid now, always searching
  - New pathways open
- Once Adam has Tiffany, he can no longer put her down, and has to escape without using any weapons

## Alex Jacobson boss fight

- A trap that snaps in the middle of a convo
- Once the convo ends, everything seems normal, but as soon as the player touches anything, it falls apart
- It's a very tight space, very oppressive
- Escape room gameplay

## JC boss fight

- It takes place inside his memories
- Time is happening in reverse
- Tiffany is trying to find JC's childhood memories
- Once she does, she can "incept" him
- She and JC spent some time together growing up
- Their school assigned them codenames, no neither knew each other's real names