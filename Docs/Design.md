# Deus Ex: Machine God - Design Document

## Characters

### Tiffany Savage (Player)

Father: Gary Savage
Mother: Evelyn Savage (deceased)

#### Description from the official strategy guide

College is a time when young people are expected to question what their parents taught them, but normally the questions aren't this big. Tiffany always thought she'd go to college for a few years, pick up an advanced degree or three or four (no big deal for her father's daughter) and then go to work for Daddy in the exciting field of nano-technology.

But then along came those darn questions. What exactly was "Majestic 12," the super-secret organization that funded her father's research? Those crazy letters from France — the ones written in the name of Silhouette by somebody calling himself the "Minister of True Lies" — what if even a little of what they were saying was true? She started to read some really crazy stuff, and then got really scared when it made more sense than the evening news.

She didn't know how her father would react to all this, but to her surprise he listened. Tiffany started working behind the scenes, trying to find out more about what was going on, and the more she found out the more scared she got... and the more determined that something had to be done.

#### Facts

- Her mother was murdered by MJ12 just a year prior to the events of DX

### Gary

Always sees advanced solutions to simple problems

### Adam Jensen

#### Gameplay

- Elude to the yellow filter being a bug in his HUD
- Icarus landing?


### JC Denton

#### Boss battle

Play as JC, as a child? Or his mum?


### Alex Jacobson

#### Boss battle

He will confidently confront Tiffany and the others, as his weapon is based on illusions.

Structure:
- Fakeout of reality
- Dream loop (x3?)
- Break out through "seam" in the sim
- Corner Alex

Dream loop:
- Player is sent to some alternate reality
- Mission is to find the others
- Thematic obstacles
- Once everyone is found, Hypnos reboots i to another scenario
- With every loop, "seams" in the sim become more apparent

Ideas:
- Fake loading screens and crashes
- Transition to an exact copy of the current space, except people are now sprites and unfrobbable
- People turned into objects like phones
- Whole scene is a retrospective to 1940s
- Everyone is children, school scenario
    - Kids sitting in a classroom
    - Being taught garbage
    - Tiffany has to find the others in kid form
- Wake up fakeout, inception like
- Everyone is animals
    - Stealth section as a cat?
    - Find the others in animal form
- Matrix transition to a mundane situation with no inventory
    - NOT an office again
    - Warehouse
    - Tranquil park
    - Buddhist temple
    - Fishing boat
    - Museum <- dis gud
- Inside the simulation, after wkaing up Julia, she will tell Tiffany about the necklace Alex is wearing, which is controlling his mind
