class TriadSoldierFemale extends ScriptedPawnFemale;

// Voice processing in Audacity:
// - ElevenLabs Rachel voice
// - Duplicate track
//     - Set one to -10% pitch
//     - Mix them back into one
// - Apply phaser with default values
// - Apply tremolo with 40hz
// - Apply limiter with 4dB input gain

defaultproperties
{
     BarkBindName="TriadSoldierFemale"
     BindName="TriadSoldierFemale"
     InitialInventory(0)=(Inventory=class'DeusEx.WeaponAssaultGun',Count=1)
     InitialInventory(1)=(Inventory=class'DeusEx.Ammo762mm',Count=100)
     InitialInventory(2)=(Inventory=class'DeusEx.WeaponNanoSword',Count=1)
     FamiliarName="Private Military"
     UnfamiliarName="Private Military"
     DrawScale=0.9
     Fatness=126
     Mesh=LodMesh'DeusExCharacters.GM_Jumpsuit'
     MultiSkins(0)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(1)=Texture'MachineGod.Skins.TacticalPants4'
     MultiSkins(2)=Texture'MachineGod.Skins.ArmorSweater1_MaleJumpsuit'
     MultiSkins(3)=Texture'MachineGod.Skins.TriadBrute_Head'
     MultiSkins(4)=Texture'MachineGod.Skins.TriadBrute_Head'
     MultiSkins(5)=Texture'DeusExItems.Skins.GrayMaskTex'
     MultiSkins(6)=Texture'MachineGod.Skins.TriadBrute_Helmet'
     MultiSkins(7)=Texture'DeusExItems.Skins.PinkMaskTex'
     Texture=Texture'DeusExCharacters.Skins.VisorTex1'
}
