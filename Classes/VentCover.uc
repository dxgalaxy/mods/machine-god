class VentCover extends BlockedMover;

function bool ShouldFixTag()
{
    return Tag == 'VentCover';
}

defaultproperties
{
    ClosedSound=Sound'MoverSFX.Other.MetalLockerClos'
    MoveAmbientSound=Sound'MoverSFX.door.MetalDoorMove'
    OpeningSound=Sound'MoverSFX.Other.MetalLockerOpen'
    MoverEncroachType=ME_IgnoreWhenEncroach
    MoveTime=0.75
    bBreakable=True
    bFrobbable=True
    bHighlight=True
    bPickable=True
    ExplodeSound1=Sound'DeusExSounds.Generic.MetalHit1'
    ExplodeSound2=Sound'DeusExSounds.Generic.MetalHit2'
    FragmentClass=Class'DeusEx.MetalFragment'
    FragmentScale=1
    NumFragments=8
    Tag='DeusExMover'
}
