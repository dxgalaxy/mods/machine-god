class LarryWinger extends ScriptedPawnMale;

#exec TEXTURE IMPORT NAME=LarryWinger_Head FILE=Textures\Skins\LarryWinger_Head.pcx GROUP=Skins

defaultproperties
{
    BindName="LarryWinger"
    FamiliarName="Larry Winger"
    UnfamiliarName="Businessman"
    bInvincible=True
    GroundSpeed=160
    Mesh=LodMesh'DeusExCharacters.GM_Trench_F'
    MultiSkins(0)=Texture'MachineGod.Skins.LarryWinger_Head'
    MultiSkins(1)=Texture'MachineGod.Skins.SuitJacket2_MaleFat'
    MultiSkins(2)=Texture'MachineGod.Skins.Jeans3'
    MultiSkins(3)=Texture'DeusExItems.Skins.BlackMaskTex'
    MultiSkins(4)=Texture'MachineGod.Skins.DressShirtFront1_Male'
    MultiSkins(5)=Texture'MachineGod.Skins.SuitJacket2_MaleFat'
    MultiSkins(6)=Texture'DeusExItems.Skins.GrayMaskTex'
    MultiSkins(7)=Texture'DeusExItems.Skins.BlackMaskTex'
    CollisionRadius=20.000000
    CollisionHeight=47.500000
}
