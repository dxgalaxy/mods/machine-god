class WeaponHypnosEnforcerRocket extends WeaponMJ12Rocket;

defaultproperties
{
     AmmoName=Class'MachineGod.AmmoHypnosEnforcerRocket'
     PickupAmmoCount=666
     ProjectileClass=Class'MachineGod.ProjectileHypnosEnforcerRocket'
}
