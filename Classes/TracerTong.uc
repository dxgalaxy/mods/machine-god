class TracerTong extends ScriptedPawnMale;

#exec TEXTURE IMPORT NAME=TracerTong_Head FILE=Textures\Skins\TracerTong_Head.pcx GROUP=Skins

defaultproperties
{
    bImportant=True
    bInvincible=True
    Texture=Texture'DeusExItems.Skins.PinkMaskTex'
    Mesh=LodMesh'DeusExCharacters.GM_Jumpsuit'
    MultiSkins(0)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(1)=Texture'DeusExCharacters.Skins.TracerTongTex2'
    MultiSkins(2)=Texture'DeusExCharacters.Skins.TracerTongTex1'
    MultiSkins(3)=Texture'MachineGod.Skins.TracerTong_Head'
    MultiSkins(4)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(5)=Texture'DeusExItems.Skins.GrayMaskTex'
    MultiSkins(6)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(7)=Texture'DeusExItems.Skins.PinkMaskTex'
    BindName="TracerTong"
    FamiliarName="Tracer Tong"
    UnfamiliarName="Tracer Tong"
}
