//=============================================================================
// Mission90 - Ending
//=============================================================================
class Mission90 extends MissionScriptExtended;

var byte DesiredSoundVolume, CurrentSoundVolume;

// BEGIN UE1

function InitMap()
{
    CurrentSoundVolume = 128;
    DesiredSoundVolume = 128;
}

function Tick(float DeltaTime)
{
    Super.Tick(DeltaTime);

    if(DesiredSoundVolume < CurrentSoundVolume)
    {
        CurrentSoundVolume--;
        Player.SetInstantSoundVolume(CurrentSoundVolume);
    }
    else if(DesiredSoundVolume > CurrentSoundVolume)
    {
        CurrentSoundVolume++;
        Player.SetInstantSoundVolume(CurrentSoundVolume);
    }
}

// END UE1
