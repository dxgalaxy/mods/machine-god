class Mike extends ScriptedPawnMale;

#exec TEXTURE IMPORT NAME=Mike_Head FILE=Textures\Skins\Mike_Head.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Mike_ShirtFront FILE=Textures\Skins\Mike_ShirtFront.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Mike_Legs FILE=Textures\Skins\Mike_Legs.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=InfoPortrait_Mike FILE=Textures\UI\InfoPortrait_Mike.pcx GROUP=UI MIPS=OFF

function DeusExLevelInfo GetLevelInfo()
{
    local DeusExLevelInfo aInfo;

    foreach AllActors(class'DeusExLevelInfo', aInfo)
        break;

    return aInfo;
}

function PostBeginPlay()
{
    local DeusExLevelInfo aInfo;

    Super.PostBeginPlay();
    
    aInfo = GetLevelInfo();

    if(aInfo.MissionNumber > 17)
    {
        FamiliarName = "Miguel";
        UnfamiliarName = "Miguel";
    }
}

defaultproperties
{
    BindName="Mike"
    FamiliarName="Mike"
    UnfamiliarName="Mike"
    WalkingSpeed=0.300000
    bImportant=True
    bInvincible=True
    bFollowPersistently=True
    BaseAssHeight=-23.000000
    walkAnimMult=1.050000
    GroundSpeed=200.000000
    Mesh=LodMesh'DeusExCharacters.GM_Trench'
    MultiSkins(0)=Texture'MachineGod.Skins.Mike_Head'
    MultiSkins(1)=Texture'MachineGod.Skins.LeatherJacket1_Male'
    MultiSkins(2)=Texture'MachineGod.Skins.Mike_Legs'
    MultiSkins(3)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(4)=Texture'MachineGod.Skins.Mike_ShirtFront'
    MultiSkins(5)=Texture'MachineGod.Skins.PinkMaskTex'
    MultiSkins(6)=Texture'DeusExItems.Skins.GrayMaskTex'
    MultiSkins(7)=Texture'DeusExItems.Skins.BlackMaskTex'
    Outfits(0)=(Name="Bar",Mesh=LodMesh'DeusExCharacters.GM_DressShirt',Tex1=Texture'DeusExItems.Skins.PinkMaskTex',Tex2=Texture'DeusExItems.Skins.PinkMaskTex',Tex3=Texture'MachineGod.Skins.Jeans2',Tex4=Texture'DeusExItems.Skins.PinkMaskTex',Tex5=Texture'MachineGod.Skins.DressShirt3_Male_Skin3',Tex6=Texture'MachineGod.Skins.Frames5',Tex7=Texture'MachineGod.Skins.Lenses6')
    PortraitTexture=Texture'MachineGod.UI.InfoPortrait_Mike'
}

