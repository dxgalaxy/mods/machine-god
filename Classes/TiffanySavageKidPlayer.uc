class TiffanySavageKidPlayer extends TiffanySavagePlayer;

#exec OBJ LOAD FILE=Effects

// BEGIN UE1

state PlayerStandingWithHUD
{
    function PlayerTick(float DeltaTime)
    {
        Super.PlayerTick(DeltaTime);

        bCrosshairVisible = False;
        ModernHUD(ModernRootWindow(RootWindow).HUD).SetMinimal(True);
    }
}

state ParalyzedWithHUD
{
    function PlayerTick(float DeltaTime)
    {
        Super.PlayerTick(DeltaTime);

        bCrosshairVisible = False;
        ModernHUD(ModernRootWindow(RootWindow).HUD).SetMinimal(True);
    }
}

state PlayerWalking
{
    function PlayerTick(float DeltaTime)
    {
        Super.PlayerTick(DeltaTime);

        bCrosshairVisible = False;
        ModernHUD(ModernRootWindow(RootWindow).HUD).SetMinimal(True);
    }
}

// Stubbed functions
function TakeDamage(int Damage, Pawn aInstigatedBy, Vector HitLocation, Vector Momentum, name DamageType) {}
exec function ActivateAugmentation(int Num) {}
exec function ActivateBelt(int Num) {}
exec function ShowGoalsWindow() {}
exec function ShowLogsWindow() {}
exec function ShowAugmentationAddWindow() {}
exec function ShowQuotesWindow() {}
exec function ShowRGBDialog() {}
exec function ShowInventoryWindow() {}
exec function ShowSkillsWindow() {}
exec function ShowHealthWindow() {}
exec function ShowImagesWindow() {}
exec function ShowConversationsWindow() {}
exec function ShowAugmentationsWindow() {}
exec function NextWeapon() {}
exec function PrevWeapon() {}
exec function NextBeltItem() {}
exec function PrevBeltItem() {}


function DoJump( optional float F )
{
    local DeusExWeapon w;
    local float scaleFactor, augLevel;

    if ((CarriedDecoration != None) && (CarriedDecoration.Mass > 20))
        return;
    else if (bForceDuck || IsLeaning())
        return;

    if (Physics == PHYS_Walking)
    {
        if ( Role == ROLE_Authority )
            PlaySound(JumpSound, SLOT_None, 1.5, true, 1200, (1.0 - 0.2*FRand()) * 1.15 );
        if ( (Level.Game != None) && (Level.Game.Difficulty > 0) )
            MakeNoise(0.1 * Level.Game.Difficulty);
        PlayInAir();

        Velocity.Z = JumpZ;

        if ( Level.NetMode != NM_Standalone )
        {
         if (AugmentationSystem == None)
            augLevel = -1.0;
         else           
            augLevel = AugmentationSystem.GetAugLevelValue(class'AugSpeed');
            w = DeusExWeapon(InHand);
            if ((augLevel != -1.0) && ( w != None ) && ( w.Mass > 30.0))
            {
                scaleFactor = 1.0 - FClamp( ((w.Mass - 30.0)/55.0), 0.0, 0.5 );
                Velocity.Z *= scaleFactor;
            }
        }
        
        if ( Base != Level )
            Velocity.Z += Base.Velocity.Z;
        SetPhysics(PHYS_Falling);
        if ( bCountJumps && (Role == ROLE_Authority) )
            Inventory.OwnerJumped();
    }
}

// END UE1

defaultproperties
{
    Mesh=LodMesh'DeusExCharacters.GMK_DressShirt'
    GroundSpeed=150.000000
    BaseEyeHeight=26.000000
    HealthHead=50
    HealthTorso=50
    HealthLegLeft=50
    HealthLegRight=50
    HealthArmLeft=50
    HealthArmRight=50
    CollisionRadius=17.000000
    CollisionHeight=32.500000
    Mass=80.000000
    Buoyancy=85.000000
    MultiSkins(0)=WetTexture'Effects.UserInterface.DrunkFX'
    MultiSkins(1)=WetTexture'Effects.UserInterface.DrunkFX'
    MultiSkins(2)=WetTexture'Effects.UserInterface.DrunkFX'
    MultiSkins(3)=Texture'PinkMaskTex'
    MultiSkins(4)=Texture'PinkMaskTex'
    MultiSkins(5)=WetTexture'Effects.UserInterface.DrunkFX'
    MultiSkins(6)=Texture'GrayMaskTex'
    MultiSkins(7)=Texture'BlackMaskTex'
}
