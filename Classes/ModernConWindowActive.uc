// ========================================
// ModernConWindowActive
//
// Changes:
// - Removed letterbox borders
// - Use the same subtitle box as first person
// - Changed up some text colours
// - Removed last line, so players focus on the choice
// ========================================
class ModernConWindowActive extends ConWindowActive;

var Color ColConButtonFocus;
var ModernHUDConWindowFirst SubtitleWindow;
var ModernHUDTileWindow ChoiceListWindow;

// BEGIN UE1

event InitWindow()
{
    local ColorTheme Theme;
   
    Super.InitWindow();

    // Set colours 
    Theme = Player.ThemeManager.GetCurrentHUDColorTheme();
    ColConButtonFocus = Theme.GetColorFromName('HUDColor_Background');
    ColConTextFocus = Theme.GetColorFromName('HUDColor_HeaderText');
    ColConTextChoice = Theme.GetColorFromName('HUDColor_HeaderText');
    ColConTextSkill = Theme.GetColorFromName('HUDColor_HeaderText');

    UpperConWindow.Hide();
    LowerConWindow.Hide();

    // The window for received items
    CreateReceivedWindow();
}

// --------------------------------------------------
// Prevent all input during forced (cinematic) convos
// --------------------------------------------------
event bool MouseButtonReleased(float pointX, float pointY, EInputKey button, int numClicks)
{
    if(bForcePlay)
        return True;

    return Super.MouseButtonReleased(pointX, pointY, button, numClicks);
}

event bool VirtualKeyPressed(EInputKey key, bool bRepeat)
{
    if(bForcePlay)
        return True;
    
    return Super.VirtualKeyPressed(key, bRepeat);
}

// ---------------
// Removes choices
// ---------------
function DestroyChildren()
{
    if(ChoiceListWindow != None)
    {
        ChoiceListWindow.Destroy();
        ChoiceListWindow = None;
    }

    if(SubtitleWindow != None)
    {
        SubtitleWindow.Destroy();
        SubtitleWindow = None;
    }
    
    NumChoices = 0;
}

// -------------
// Adds a choice
// -------------
function DisplayChoice(ConChoice Choice)
{
    local ConChoiceWindow NewButton;

    NewButton = CreateConButton(HALIGN_Center, ColConTextChoice, ColConButtonFocus);
    NewButton.SetText(Choice.ChoiceText);
    NewButton.SetUserObject(Choice);

    // Add the button to the list
    AddButton(NewButton);
}

// -------------------
// Adds a skill choice
// -------------------
function DisplaySkillChoice( ConChoice choice )
{
    local ConChoiceWindow NewButton;

    NewButton = CreateConButton( HALIGN_Center, ColConTextSkill, ColConButtonFocus);
    NewButton.SetText(Choice.ChoiceText $ "  (" $ Choice.SkillNeeded $ ":" $ Choice.SkillLevelNeeded $ ")" );
    NewButton.SetUserObject(Choice);

    // Add the button to the list
    AddButton(NewButton);
}

// ------------------------------
// Creates the button for choices
// ------------------------------
function ConChoiceWindow CreateConButton(EHAlign Halign, Color Col1, Color Col2)
{
    local ConChoiceWindow NewButton;
    local ColorTheme Theme;

    if(SubtitleWindow != None)
    {
        SubtitleWindow.Destroy();
        SubtitleWindow = None;
    }
    
    if(ChoiceListWindow == None)
    {
        ChoiceListWindow = ModernHUDTileWindow(NewChild(class'ModernHUDTileWindow'));
        ChoiceListWindow.SetOrder(ORDER_Down);
        ChoiceListWindow.SetChildAlignments(HALIGN_Full, VALIGN_Top);
        ChoiceListWindow.MakeWidthsEqual(False);
        ChoiceListWindow.MakeHeightsEqual(False);
        ChoiceListWindow.SetMargins(12, 8);
        ChoiceListWindow.SetMajorSpacing(12);
        ChoiceListWindow.SetMinorSpacing(8);
        ChoiceListWindow.FillParent(True);
        ChoiceListWindow.Show();
    }

    NewButton = ConChoiceWindow(ChoiceListWindow.NewChild(Class'ConChoiceWindow'));
    NewButton.SetTextAlignments(Halign, VALIGN_Top);
    NewButton.SetTextMargins(10, 2);
    NewButton.SetFont(Font'FontLocation');
    NewButton.SetTextColor(Col1);
    NewButton.SetTextColors(Col1, Col1, Col1, Col1, Col1, Col1);
    NewButton.SetButtonColors(, Col2, Col2, Col2, Col2, Col2);
    NewButton.SetButtonTextures(,Texture'Solid', Texture'Solid', Texture'Solid');

    return NewButton;
}

// --------------------
// Displays spoken text
// --------------------
function DisplayText(string Text, Actor SpeakingActor)
{
    if(ChoiceListWindow != None)
    {
        ChoiceListWindow.Destroy();
        ChoiceListWindow = None;
    }

    if(SubtitleWindow == None)
    {
        SubtitleWindow = ModernHUDConWindowFirst(NewChild(Class'ModernHUDConWindowFirst'));
        SubtitleWindow.bStackable = False;
        SubtitleWindow.Show();
    }

    NumChoices = 0;
    SubtitleWindow.DisplayText(Text, SpeakingActor);
}

// ------------------------------
// Adds string to the last button
// ------------------------------
function AppendText(string Text)
{
    if(SubtitleWindow == None) 
        return;

    SubtitleWindow.AppendText(Text);
}

// -------------
// Configuration
// -------------
event ConfigurationChanged()
{
    local float W, H;

    if(SubtitleWindow != None)
    {
        SubtitleWindow.QueryPreferredSize(W, H);
        
        SubtitleWindow.ConfigureChild((Width / 2) - (W / 2), Height - H - 80, W, H);
    }

    if(ChoiceListWindow != None)
    {
        ChoiceListWindow.QueryPreferredSize(W, H);
        W = Min(W, Root.Width / 2);
        H = ChoiceListWindow.QueryPreferredHeight(W);
        
        ChoiceListWindow.ConfigureChild((Width / 2) - (W / 2), Height - H - 80, W, H);
    }
}

// END UE1

defaultproperties
{
    ColConTextFocus=(R=255,G=255,B=255)
    ColConTextChoice=(R=255,G=255,B=255)
    ColConTextSkill=(R=255,G=255,B=255)
}
