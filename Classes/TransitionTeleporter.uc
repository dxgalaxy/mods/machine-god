//=============================================================================
// TransitionTeleporter - Teleports an actor with a screen effect
//=============================================================================
class TransitionTeleporter extends Teleporter;

var(Teleporter) float TransitionDuration;
var(Teleporter) float TransitionFOV;
var(Teleporter) Sound TransitionSound;
var(Teleporter) bool bPlayersOnly;

// BEGIN UE1

var float TransitionTimer;
var DeusExPlayer aPlayer;
var Actor aSource;
var bool bTransitioning;
var bool bTeleported;
var float DesiredFOV;

// Accept an actor that has teleported in.
simulated function bool Accept(Actor Incoming, Actor Source)
{
    aPlayer = DeusExPlayer(GetPlayerPawn());
   
    // If whatever entered is not the player, just revert to normal behaviour
    if(Incoming != aPlayer)
    {
        if(bPlayersOnly)
            return False;
    
        return Super.Accept(Incoming, Source);
    }

    DesiredFOV = aPlayer.DesiredFOV;
    bTransitioning = True;
    aSource = Source;

    if(TransitionSound != None)
        aPlayer.PlaySound(TransitionSound, SLOT_None);

    return True;
}

function float GetSmoothingCoefficient(float Alpha, bool bOut)
{
    // ElasticOut
    if(bOut)
    {
        if(Alpha == 0)
            return 0;

        if(Alpha == 1)
            return 1;

        return (2 ** (-10 * Alpha)) * Sin((Alpha - 0.1) * 5 * Pi) + 1;
    }

    // QuadraticInOut
    if ((Alpha *= 2.0) < 1.0)
    {
        return 0.5 * Alpha * Alpha;
    }
    
    Alpha -= 1.0;

    return - 0.5 * (Alpha * (Alpha - 2.0) - 1.0);
}

function Tick(float DeltaTime)
{
    local float Alpha, Beta;

    if(bTransitioning)
    {
        TransitionTimer += DeltaTime;
        
        if(TransitionTimer < TransitionDuration / 2)
        {
            Alpha = FClamp(TransitionTimer / (TransitionDuration / 2), 0.0, 1.0);
            Beta = GetSmoothingCoefficient(Alpha, False);
            aPlayer.DesiredFOV = DesiredFOV + Beta * (TransitionFOV - DesiredFOV); 
        }
        else
        {
            Alpha = FClamp((TransitionTimer - (TransitionDuration / 2)) / (TransitionDuration / 2), 0.0, 1.0);
            Beta = GetSmoothingCoefficient(Alpha, True);
            aPlayer.DesiredFOV = TransitionFOV + Beta * (DesiredFOV - TransitionFOV); 
        }

        aPlayer.SetFOVAngle(aPlayer.DesiredFOV);

        if(aPlayer.DesiredFOV <= DesiredFOV && !bTeleported)
        {
            bTeleported = True;

            Super.Accept(aPlayer, aSource);
        }

        if(TransitionTimer >= TransitionDuration)
        {
            if(!bTeleported)
                Super.Accept(aPlayer, aSource);

            TransitionTimer = 0;
            bTransitioning = False;
            bTeleported = False;
            aPlayer = None;
            aSource = None;
        }
    }

    Super.Tick(DeltaTime);
}

// END UE1

defaultproperties
{
    TransitionDuration=2.0
    TransitionFOV=160
    bStatic=False
    bPlayersOnly=True
}
