class MachineGodPlayer extends HumanExtended;

var string LocalURL;
var int MissionNumber;
var ActorOutline aOutline;

// BEGIN UE1 

// -----------------
// Shows the credits
// -----------------
function ShowCredits(optional bool bLoadIntro)
{
    local DeusExRootWindow Root;
    local ModernCreditsWindow WinCredits;
    local int i;

    Root = DeusExRootWindow(RootWindow);

    if(Root == None)
        return;

    WinCredits = ModernCreditsWindow(Root.InvokeMenuScreen(class'ModernCreditsWindow', bLoadIntro));
   
    // This is not the end
    if(MissionNumber < 90)
        WinCredits.SetSplashText("To be continued", 2.0);

    WinCredits.SetLoadIntro(bLoadIntro);
    WinCredits.SetBanner(Texture'MachineGod.UI.Title', 1024, 256);
   
    WinCredits.AddRole("Artifechs");
    WinCredits.AddRoleBody("Concept");
    WinCredits.AddRoleBody("Dialogue");
    WinCredits.AddRoleBody("Level design");
    WinCredits.AddRoleBody("Programming");
    WinCredits.AddRoleBody("Project management");
    WinCredits.AddRoleBody("Story");
    WinCredits.AddRoleBody("Textures");
    WinCredits.AddRoleBody("Voice acting");
    
    WinCredits.AddRole("Alkasushi");
    WinCredits.AddRoleBody("Promo & concept art");
    
    WinCredits.AddRole("AmazingBodilyFluids");
    WinCredits.AddRoleBody("Story");
    WinCredits.AddRoleBody("Testing");
    
    WinCredits.AddRole("dudithebudi");
    WinCredits.AddRoleBody("Dialogue");

    WinCredits.AddRole("Ermacgerd");
    WinCredits.AddRoleBody("Testing");

    WinCredits.AddRole("FineKone");
    WinCredits.AddRoleBody("Character design");
    WinCredits.AddRoleBody("Story");
    WinCredits.AddRoleBody("Voice acting");
    
    WinCredits.AddRole("FredrikVA");
    WinCredits.AddRoleBody("Voice acting");

    WinCredits.AddRole("Heartspowl");
    WinCredits.AddRoleBody("Character design");
    WinCredits.AddRoleBody("Promo & concept art");
    WinCredits.AddRoleBody("Story");
    
    WinCredits.AddRole("hugelogan");
    WinCredits.AddRoleBody("Music");

    WinCredits.AddRole("LoadLineCalibration");
    WinCredits.AddRoleBody("Launcher");

    WinCredits.AddRole("Magmadiver");
    WinCredits.AddRoleBody("Testing");

    WinCredits.AddRole("unfa");
    WinCredits.AddRoleBody("Voice acting");
    
    WinCredits.AddRole("zaiden");
    WinCredits.AddRoleBody("World building");

    WinCredits.AddRole("Special thanks");
    WinCredits.AddRoleBody("aizome8086");
    WinCredits.AddRoleBody("Defaultmom001");
    WinCredits.AddRoleBody("Greasel");
    WinCredits.AddRoleBody("Rubber Ducky WCCC");
    WinCredits.AddRoleBody("TheAstropath");

    WinCredits.AddLogo(Texture'MachineGod.UI.Logo', 512, 512);
}

// -----------------
// We're in the game
// -----------------
event TravelPostAccept()
{
    local DeusExLevelInfo aDeusExLevelInfo;
    local Actor aActor;
    local name StartTextFlagName;

    Super.TravelPostAccept(); 

    // Force intended ViewFlash
    ViewFlash(1.0);

    // Set swimming skill to max, 'cause screw that skill
    SkillSystem.GetSkillFromClass(class'SkillSwimming').CurrentLevel = 3; 

    // The splash screen has a fixed FOV
    if(LocalURL == "DX")
    {
        DesiredFOV = 70;
        SetFOVAngle(70);
    }
    
    // Learn what we need to know from DeusExLevelInfo
    foreach AllActors(class'DeusExLevelInfo', aDeusExLevelInfo)
    {
        LocalURL = Caps(aDeusExLevelInfo.MapName);
        MissionNumber = aDeusExLevelInfo.MissionNumber;

        // If we're displaying startup text, hide the HUD
        StartTextFlagName = RootWindow.StringToName("M" $ Caps(aDeusExLevelInfo.MapName) $ "_StartupText");

        if(
            FlagBase.GetBool('PlayerTraveling') &&
            !FlagBase.GetBool(StartTextFlagName) &&
            aDeusExLevelInfo.StartupMessage[0] != ""
        )
            ShowHUD(False);
    }
    
    // Apply outline
    if(aOutline == None)
    {
        aOutline = Spawn(class'MachineGod.ActorOutline');
        aOutline.SetOwner(Self);
    }
    
    // Set up player for intro/outro sequences
    if(FlagBase.GetBool('PlayerTraveling') || LocalURL == "DX")
    {
        foreach AllActors(class'Actor', aActor, 'StartIntroSequence')
        {
            ModernRootWindow(RootWindow).Brightness = 0;
            ModernRootWindow(RootWindow).Fade(1, 1);
            ShowHUD(False);
            UnderWaterTime = -1.0;  
            SetCollision(False, False, False);
            bCollideWorld = False;
            GoToState('CheatFlying');
            aActor.Trigger(Self, Self);
            break;
        }
    }

    // Clear inventory if we have to
    if(FlagBase.GetBool('ClearInventory'))
    {
        ClearInventory();
        FlagBase.SetBool('ClearInventory', False,, 0);
    }
}

// Trigger intro
exec function Intro()
{
    local Actor aActor;
    local DeusExLevelInfo aDXInfo;
    local int i;

    foreach AllActors(class'DeusExLevelInfo', aDXInfo)
    {
        if(aDXInfo.startupMessage[0] != "")
        {
            for(i = 0; i < ArrayCount(aDXInfo.StartupMessage); i++)
                ModernRootWindow(RootWindow).StartDisplay.AddMessage(aDXInfo.StartupMessage[i]);
            
            ModernRootWindow(RootWindow).StartDisplay.StartMessage();
        }

        break;
    }

    foreach AllActors(class'Actor', aActor, 'StartIntroSequence')
    {
        ShowHUD(False);
        UnderWaterTime = -1.0;  
        SetCollision(False, False, False);
        bCollideWorld = False;
        GoToState('CheatFlying');
        aActor.Trigger(Self, Self);
    }
}

// Trigger outro
exec function Outro()
{
    local Actor aActor;

    foreach AllActors(class'Actor', aActor, 'StartOutroSequence')
    {
        aActor.Trigger(Self, Self);
    }
}

// ----------------------------------------
// Hijack this method to load our first map
// ----------------------------------------
function ShowIntro(optional bool bStartNewGame)
{
    if (DeusExRootWindow(rootWindow) != None)
        DeusExRootWindow(rootWindow).ClearWindowStack();

    bStartNewGameAfterIntro = bStartNewGame;

    // Make sure all augmentations are OFF before going into the intro
    AugmentationSystem.DeactivateAll();

    // Reset the player
    Level.Game.SendPlayer(Self, "16_Dream");
}

// -------------------------------
// Checks whether we're spaced out
// -------------------------------
function bool IsSpaced()
{
    return bBehindView && AmbientSound == Sound'Ambient.Ambient.Underwater';
}

function CheckActiveConversationRadius()
{
    local int checkRadius;

    // Ignore if conPlay.GetForcePlay() returns True

    if ((conPlay != None) && (!conPlay.GetForcePlay()) && (conPlay.ConversationStarted()) && (conPlay.displayMode == DM_FirstPerson) && (conPlay.StartActor != None))
    {
        // If this was invoked via a radius, then check to make sure the player doesn't 
        // exceed that radius plus 

        if (conPlay.con.bInvokeRadius) 
            checkRadius = conPlay.con.radiusDistance + 100;
        else
            checkRadius = 300;

        // Add the collisioncylinder since some objects are wider than others
        checkRadius += conPlay.StartActor.CollisionRadius;

        if (VSize(conPlay.startActor.Location - Location) > checkRadius)
        {
            // Abort the conversation
            conPlay.TerminateConversation(True);
        }
    }
}

// ----------------
// Console commands
// ----------------
// Gets the current state of an actor
exec function GetState(name ActorTag)
{
    local Actor aActor;

    if(ActorTag == 'Player')
    {
        aActor = Self;
        ClientMessage(aActor.Name $ ": " $ aActor.GetStateName());
    }
    else
    {
        foreach AllActors(class'Actor', aActor, ActorTag)
        {
            ClientMessage(aActor.Name $ ": " $ aActor.GetStateName());
        }
    }
}

// Gets a property from an actor
exec function GetProp(name ActorTag, string PropName)
{
    local Actor aActor;

    if(ActorTag == 'Player')
    {
        aActor = Self;
        ClientMessage(aActor.Name $ ": " $ aActor.GetPropertyText(PropName));
    }
    else
    {
        foreach AllActors(class'Actor', aActor, ActorTag)
        {
            ClientMessage(aActor.Name $ ": " $ aActor.GetPropertyText(PropName));
        }
    }
}

// Is stealth context?
exec function IsStealthContext()
{
    local ScriptedPawn aPawn;
    local float MinDist;
    local bool bFoundEnemies, bAnyEnemiesAttacking;

    MinDist = 2048;

    foreach RadiusActors(class'ScriptedPawn', aPawn, MinDist)
    {
        if(aPawn.GetStateName() == 'Attacking')
            bAnyEnemiesAttacking = True;

        if(aPawn.GetPawnAllianceType(Self) == ALLIANCE_Hostile)
            bFoundEnemies = True;
    }

    if(bFoundEnemies && !bAnyEnemiesAttacking)
        ClientMessage("This is a stealth context");
    else
        ClientMessage("This is not a stealth context");
}

// Apply outline
exec function Outline(name ActorTag, bool bOn)
{
    local Actor aActor;
    local ActorOutline aOutline;

    if(ActorTag == '')
        return;
    
    if(bOn)
    {
        foreach AllActors(class'Actor', aActor, ActorTag)
        {
            ClientMessage("Applied outline to " $ aActor.Name);
            aOutline = Spawn(class'MachineGod.ActorOutline');
            aOutline.SetOwner(aActor);
            aOutline.ApplyMesh();
        }
    }
    else
    {
        foreach AllActors(class'Actor', aActor, ActorTag)
        {
            
            foreach AllActors(class'ActorOutline', aOutline)
            {
                if(aOutline.Owner != aActor)
                    continue;
            
                ClientMessage("Removed outline from " $ aActor.Name);
                aOutline.Destroy();
            }
        }
    }
}

// Change outfit
exec function Outfit(name ActorTag, string OutfitName)
{
    local ScriptedPawnExtended aPawn;

    if(ActorTag != 'Player')
    {
        foreach AllActors(class'ScriptedPawnExtended', aPawn, ActorTag)
        {
            aPawn.ChangeOutfit(OutfitName);
        }
    }
    else
    {
        ChangeOutfit(OutfitName);
    }
}

// Randomise random humans
exec function RandomizePawns()
{
    local RandomScriptedPawn aPawn;

    foreach AllActors(class'RandomScriptedPawn', aPawn)
        aPawn.StartGeneration();
}

// Fade in/out
exec function Fade(bool bIn)
{
    if(bIn)
        ModernRootWindow(RootWindow).Fade(1, 1);
    else
        ModernRootWindow(RootWindow).Fade(0, 1);
}

// Release from cinematic
exec function Release()
{
    local CinematicPoint aPoint;

    foreach AllActors(class'CinematicPoint', aPoint)
    {
        if(aPoint.aTarget == Self)
            aPoint.aTarget = None;

        if(aPoint.TargetTag == 'Player')
            aPoint.TargetTag = '';
    }

    Ghost();
}

// State
exec function State(name ActorTag, optional name NewState)
{
    local Actor aTarget, aActor;

    if(ActorTag == 'Player')
    {
        aTarget = Self;
    }
    else
    {
        foreach AllActors(class'Actor', aActor, ActorTag)
        {
            aTarget = aActor;
            break;
        }
    }

    if(aTarget == None)
        return;

    if(NewState != '')
        aTarget.GoToState(NewState);
    else
        ClientMessage(aTarget.GetStateName());
}

// ------------------------
// Changes the players's outfit
// ------------------------
function ChangeOutfit(string Outfit)
{
    Super.ChangeOutfit(Outfit);

    aOutline.ApplyMesh();
}

// END UE1

defaultproperties
{
    CollisionRadius=16
    AmbientGlow=64
    JumpZ=360.000000
}
