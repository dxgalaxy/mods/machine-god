class AntonJackson extends ScriptedPawnMale;

#exec TEXTURE IMPORT NAME=AntonJackson_Head FILE=Textures\Skins\AntonJackson_Head.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=AntonJackson_Head_Balaclava FILE=Textures\Skins\AntonJackson_Head_Balaclava.pcx GROUP=Skins

defaultproperties
{
    BindName="Anton"
    FamiliarName="Anton Jackson"
    UnfamiliarName="Anton Jackson"
    Archetype=AT_Military
    WalkingSpeed=0.300000
    GroundSpeed=300
    bImportant=True
    bFollowPersistently=True
    Mesh=LodMesh'DeusExCharacters.GM_Jumpsuit'
    MultiSkins(0)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(1)=Texture'MachineGod.Skins.TacticalPants3'
    MultiSkins(2)=Texture'MachineGod.Skins.ArmorSweater3_MaleJumpsuit'
    MultiSkins(3)=Texture'MachineGod.Skins.AntonJackson_Head'
    MultiSkins(4)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(5)=Texture'DeusExItems.Skins.GrayMaskTex'
    MultiSkins(6)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(7)=Texture'DeusExItems.Skins.PinkMaskTex'
    Texture=Texture'DeusExItems.Skins.PinkMaskTex'
}
