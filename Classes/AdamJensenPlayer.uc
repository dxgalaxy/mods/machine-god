class AdamJensenPlayer extends MachineGodPlayer;

#exec TEXTURE IMPORT NAME=AdamJensen_Head FILE=Textures\Skins\AdamJensen_Head.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=AdamJensen_Coat FILE=Textures\Skins\AdamJensen_Coat.pcx GROUP=Skins FLAGS=2
#exec TEXTURE IMPORT NAME=AdamJensen_ShirtFront FILE=Textures\Skins\AdamJensen_ShirtFront.pcx GROUP=Skins FLAGS=2

defaultproperties
{
    AugmentationManagerClass=class'AdamJensenAugmentationManager'
    BindName="AdamJensen"
    FamiliarName="Adam Jensen"
    UnfamiliarName="Adam Jensen"
    BaseEyeHeight=42.000000
    Mesh=LodMesh'DeusExCharacters.GM_Trench'
    MultiSkins(0)=Texture'MachineGod.Skins.AdamJensen_Head'
    MultiSkins(1)=Texture'MachineGod.Skins.AdamJensen_Coat'
    MultiSkins(2)=Texture'MachineGod.Skins.TacticalPants4'
    MultiSkins(3)=None
    MultiSkins(4)=Texture'MachineGod.Skins.AdamJensen_ShirtFront'
    MultiSkins(5)=Texture'MachineGod.Skins.AdamJensen_Coat'
    MultiSkins(6)=Texture'DeusExItems.Skins.GrayMaskTex'
    MultiSkins(7)=Texture'DeusExItems.Skins.BlackMaskTex'
}
