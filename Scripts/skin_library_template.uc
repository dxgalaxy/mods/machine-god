// ================================================================================ 
// SkinLibrary - A colleciton of textures with added metadata
// NOTE:
//      This is an automatically generated class, do not modify.
//      See /Scripts/generate_skins.py and /Scripts/generate_skins_functions.py
// ================================================================================ 
class SkinLibrary extends Object abstract;

%imports%

// -----------------------------------------------------------------------
// Gets a TextureInfo object from an index
// -----------------------------------------------------------------------
static function GetTextureInfo(int Index, out Texture Tex, out name Gender, out name Category, out name Tags[10], out int SkinTone, out name Age)
{
    local int i;

    // First reset the out variables, as they might have been assigned previously
    Tex = None;
    Gender = '';
    Category = '';
    SkinTone = 0;
    Age = '';
    
    for(i = 0; i < ArrayCount(Tags); i++)
        Tags[i] = '';

    // Then look up the info
    switch(Index)
    {
%textureinfos%
    }
}

// ---------------------
// Gets a random Texture
// ---------------------
static function Texture GetRandomTexture(name ReqStyleTag, name ReqGender, name ReqCategory, int ReqSkinTone, name ReqAge)
{
    local int i;
    local int NumValidTextures;
    local Texture ValidTextures[%numinfos%];
    local Texture Tex;
    local int SkinTone;
    local name Age, Gender, Category, Tags[10];

    // Find valid textures
    for(i = 0; i < %numinfos%; i++)
    {
        GetTextureInfo(i, Tex, Gender, Category, Tags, SkinTone, Age);

        // Skip null textures
        if(Tex == None)
            continue;

        // Skip if the gender or category doesn't match
        if(Gender != ReqGender || Category != ReqCategory)
            continue;

        // Skip if the skin tone is specified and doesn't match
        if(ReqSkinTone > 0 && SkinTone > 0 && SkinTone != ReqSkinTone)
            continue;
        
        // Skip if the age is specified and doesn't match
        if(ReqAge != '' && Age != '' && Age != ReqAge)
            continue;

        // Skip if this texture has tags and they're not enabled
        if(HasTags(Tags) && !AreAnyTagsEnabled(Tags, ReqStyleTag))
            continue;
  
        ValidTextures[NumValidTextures] = Tex; 
        NumValidTextures++; 
    }
    
    // Oops! No valid textures. Return an empty texture.
    if(NumValidTextures < 1)
        return Texture'Engine.DefaultTexture';

    return ValidTextures[Rand(NumValidTextures)];
}

// ------------------------------------------
// Gets whether a TextureInfo has a given tag
// ------------------------------------------
static function bool HasTag(name Tags[10], name TagName)
{
    local int i;

    for(i = 0; i < 10; i++)
        if(Tags[i] == TagName)
            return True;

    return False;
}

// -------------------------------------------------
// Gets whether a TextureInfo has any tags specified
// -------------------------------------------------
static function bool HasTags(name Tags[10])
{
    local int i;

    for(i = 0; i < 10; i++)
        if(Tags[i] != '')
            return True;

    return False;
}

// -----------------------------------------
// Gets whether any tag in a list is enabled
// -----------------------------------------
static function bool AreAnyTagsEnabled(name Tags[10], name StyleTag)
{
    local int i;

    for(i = 0; i < 10; i++)
        if(Tags[i] == StyleTag)
            return True;
    
    return False;
}
