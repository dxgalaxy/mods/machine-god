// ==========================================
// ModernHUDMissionStartTextDisplay
//
// Changes:
// - Beep when printing characters
// - Implemented the abandoned character delay
// - Self-updating position
// - Moved to side of the screen
// - Added line height
// ==========================================
class ModernHUDMissionStartTextDisplay extends HUDMissionStartTextDisplay;

var Sound CharacterSound;
var string CharacterSoundName;
var float CharacterSoundPitch, CharacterSoundVolume, CharacterDelayTime, HorizontalOffset, VerticalOffset;
var ModernRootWindow Root;

// BEGIN UE1

function InitWindow()
{
    Super.InitWindow();
        
    Root = ModernRootWindow(GetRootWindow());

    if(CharacterSoundName != "")
        CharacterSound = Sound(DynamicLoadObject(CharacterSoundName, class'Sound'));
}

function Tick(float DeltaTime)
{
    if (bSpewingText)
    {
        if(CharacterDelayTime < PerCharDelay)
        {
            CharacterDelayTime += DeltaTime;
        }
        else
        {
            CharacterDelayTime = 0;
            PrintNextCharacter();
        }
    }
    else
    {
        DisplayTime -= DeltaTime;

        if(DisplayTime <= 0)
        {
            bTickEnabled = False;
            HideMessage();
        }
    }
}

// ------------------------------------------------------
// Adds a message
// 
// Note: We moved the positioning here, because we
// can't rely on ConfigurationChanged() in the RootWindow
// ------------------------------------------------------
function AddMessage(string Str)
{
    local float qWidth, qHeight;
    
    Super.AddMessage(Str);
        
    QueryPreferredSize(qWidth, qHeight);
    SetPos((Root.Width * HorizontalOffset) - (qWidth / 2), (Root.Height * VerticalOffset) - (qHeight / 2));
}

function PrintNextCharacter()
{
    Super.PrintNextCharacter();

    if(CharacterSound != None)
        PlaySound(CharacterSound, CharacterSoundPitch, CharacterSoundVolume);
}

// END UE1

defaultproperties
{
    CharacterSoundName="DeusExSounds.Generic.Beep4"
    CharacterSoundPitch=0.25
    CharacterSoundVolume=1.0
    DisplayTime=3
    VerticalOffset=0.5
    HorizontalOffset=0.25
    MaxTextWidth=600
    PerCharDelay=0.05
    FontText=Font'FontMenuExtraLarge'
}
