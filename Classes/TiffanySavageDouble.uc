class TiffanySavageDouble extends ScriptedPawnFemale;

// BEGIN UE1

event Tick(float DeltaTime)
{
    Super.Tick(DeltaTime);

    if(aPlayer == None)
        aPlayer = HumanExtended(GetPlayerPawn());

    if(aPlayer.Mesh != Mesh || aPlayer.MultiSkins[1] != MultiSkins[1])
        UpdateOutfit();
}

function UpdateOutfit()
{
    local int i;

    if(aPlayer == None)
        return;

    Mesh = aPlayer.Mesh;

    for(i = 0; i < ArrayCount(MultiSkins); i++)
    {
        MultiSkins[i] = aPlayer.MultiSkins[i];
    }

    aOutline.ApplyMesh();
}

// END UE1

defaultproperties
{
    bPlayIdle=False
    AmbientGlow=64
    BindName="TiffanySavage"
    FamiliarName="Tiffany Savage"
    UnfamiliarName="Tiffany Savage"
    Orders='Standing'
}
