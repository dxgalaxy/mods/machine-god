//=============================================================================
// Mission20 - Hong Kong
//=============================================================================
class Mission20 extends MissionScriptExtended;

// BEGIN UE1

function StartMission()
{
    GetPlayer().DeleteAllGoals();
}

function string GetCurrentMTRStation()
{
    local DeusExMover aMover;
    local int KeyNum;
    
    foreach AllActors(class'DeusExMover', aMover, 'MTRStationDisplay')
    {
        KeyNum = aMover.KeyNum;
        break;
    }

    switch(LocalURL)
    {
        case "20_HONGKONG_NORTHPOINT":
            switch(KeyNum)
            {
                case 0:
                    return "MongKok";

                case 1:
                    return "TempleStreet";
            }
            break;
        
        case "20_HONGKONG_MONGKOK":
            switch(KeyNum)
            {
                case 0:
                    return "NorthPoint";

                case 1:
                    return "TempleStreet";
            }
            break;
        
        case "20_HONGKONG_TEMPLESTREET":
            switch(KeyNum)
            {
                case 0:
                    return "MongKok";

                case 1:
                    return "NorthPoint";
            }
            break;
    }

    return "";
}

function UpdateMTRStationTeleporters()
{
    local string CurrentStationName;
    local Teleporter aTeleporter;

    CurrentStationName = GetCurrentMTRStation();

    foreach AllActors(class'Teleporter', aTeleporter, 'MTRTeleporterOut')
    {
        aTeleporter.URL = "20_HongKong_" $ CurrentStationName $ "#MTRTeleporterIn";
    }
}

function SetPreviousMTRStation()
{
    local int KeyNum;
    local DeusExMover aMover;

    KeyNum = 99;

    foreach AllActors(class'DeusExMover', aMover, 'MTRStationDisplay')
    {
        if(KeyNum == 99)
        {
            KeyNum = aMover.KeyNum - 1;
            
            if(KeyNum < 0)
                KeyNum = aMover.NumKeys - 1;
        }
    
        aMover.InterpolateTo(KeyNum, 0);
    }

    UpdateMTRStationTeleporters();
}

function SetNextMTRStation()
{
    local int KeyNum;
    local DeusExMover aMover;

    KeyNum = 99;

    foreach AllActors(class'DeusExMover', aMover, 'MTRStationDisplay')
    {
        if(KeyNum == 99)
        {
            KeyNum = aMover.KeyNum + 1;
            
            if(KeyNum >= aMover.NumKeys)
                KeyNum = 0;
        }
    
        aMover.InterpolateTo(KeyNum, 0);
    }
    
    UpdateMTRStationTeleporters();
}

function bool PlayerHasMTRPass()
{
    return GetPlayer().FamiliarName == "Adam Jensen";
}

auto state EventManager
{

PlayerPressedMTRNext:
    SetNextMTRStation(); 

    EndEvent();

PlayerPressedMTRPrevious:
    SetPreviousMTRStation(); 

    EndEvent();

PlayerPressedMTRPanel:
    if(!PlayerHasMTRPass())
    {
        GetPlayer().ClientMessage("Subway pass required");
    }
    else
    {
        OpenDoor('MTRGate');
    }

    EndEvent();

}

// END UE1
