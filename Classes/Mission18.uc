//=============================================================================
// Mission18 - Simulation, Tijuana, Mexico
//=============================================================================
class Mission18 extends MissionScriptExtended;

// BEGIN UE1

function InitMap()
{
    if(!Flags.GetBool('Mike_Woke'))
    {
        ChangePawnOutfit('Player', 'Office');
        ChangePawnOutfit('Mike', 'Bar');
    }
}

function MakeEveryoneCower()
{
    local ScriptedPawn aScriptedPawn;

    foreach AllActors(class'ScriptedPawn', aScriptedPawn)
    {
        if(aScriptedPawn.Health <= 0 || aScriptedPawn.IsA('Hypnos') || aScriptedPawn.IsA('HypnosEnforcer'))
            continue;

        aScriptedPawn.PlayCowering();
    }
}

function Timer()
{
    Super.Timer();

    // Go to reboot state if needed
    if (!IsInState('Rebooting') && Flags.GetBool('Rebooting'))
        GoToState('Rebooting');
}

// ---------
// Rebooting
// ---------
state Rebooting
{
Begin:
    Player.GoToState('TransitionOut');

    MakeEveryoneCower();
    
    Sleep(TiffanySavagePlayer(Player).TransitionDuration);

    Flags.SetBool('Rebooting', False,, 18); 
    
    Flags.SetInt('NumReboots', Flags.GetInt('NumReboots') + 1,, 20);
    Flags.SetName('NextMap', Player.RootWindow.StringToName(LocalURL),, 20);
    Level.Game.SendPlayer(Player, "19_Tijuana_Capsule");  
}

// END UE1
