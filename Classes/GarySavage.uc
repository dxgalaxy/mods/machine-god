class GarySavage extends ScriptedPawnMale;

#exec TEXTURE IMPORT NAME=GarySavage_Head FILE=Textures\Skins\GarySavage_Head.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=GarySavage_ShirtFront FILE=Textures\Skins\GarySavage_ShirtFront.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=InfoPortrait_GarySavage FILE=Textures\UI\InfoPortrait_GarySavage.pcx GROUP=UI MIPS=OFF

defaultproperties
{
    AmbientGlow=64
    WalkingSpeed=0.213333
    bImportant=True
    bInvincible=True
    BaseAssHeight=-23.000000
    GroundSpeed=180.000000
    Mesh=LodMesh'DeusExCharacters.GM_Trench'
    MultiSkins(0)=Texture'MachineGod.Skins.GarySavage_Head'
    MultiSkins(1)=Texture'MachineGod.Skins.PlainCoat1_Male'
    MultiSkins(2)=Texture'MachineGod.Skins.Jeans2'
    MultiSkins(3)=None
    MultiSkins(4)=Texture'MachineGod.Skins.GarySavage_ShirtFront'
    MultiSkins(5)=Texture'MachineGod.Skins.PlainCoat1_Male'
    MultiSkins(6)=Texture'MachineGod.Skins.Frames5'
    MultiSkins(7)=Texture'MachineGod.Skins.Lenses1'
    CollisionRadius=20.000000
    CollisionHeight=47.500000
    BindName="GarySavage"
    FamiliarName="Gary Savage"
    UnfamiliarName="Gary Savage"
    PortraitTexture=Texture'MachineGod.UI.InfoPortrait_GarySavage'
}
