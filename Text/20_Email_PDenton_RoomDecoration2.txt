<P>Mr. Denton,
<P>
<P>I see, I will then ensure that your room receives a traditional touch.
<P>
<P>- Lee
<P>
<P>Paul Denton wrote:
<P>
<P>> Hello Kwok-yam,
<P>>
<P>> I must admit decoration is not my strong suit.
<P>> My last apartment in New York had a strictly
<P>> functional layout, not much to look at.
<P>>
<P>> You can do with it as you like, something grounded
<P>> and not too high tech will suit me just fine.
<P>>
<P>> - Paul
