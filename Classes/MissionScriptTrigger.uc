//=============================================================================
// MissionScriptTrigger - Triggers events in the MissionScript
//=============================================================================
class MissionScriptTrigger extends Trigger;

var Actor aOther;

// BEGIN UE1

function Touch(Actor Other)
{
    if(!IsRelevant(Other))
        return;

    Trigger(Other, Pawn(Other));
    
    if(bTriggerOnceOnly)
        SetCollision(False);
}

function Trigger(Actor Other, Pawn Instigator)
{
    local MissionScript aMissionScript;
    local MissionScriptExtended aMissionScriptExtended;

    aOther = Other;
    
    foreach AllActors(class'MissionScript', aMissionScript)
    {
        aMissionScriptExtended = MissionScriptExtended(aMissionScript);

        if(aMissionScriptExtended != None)
        {
            aMissionScriptExtended.QueueEvent(Event, Other, Instigator);
        }
        else
        {
            aMissionScript.Trigger(Self, Instigator);
        }
    }

    Super.Trigger(Other, Instigator);
}

// END UE1
