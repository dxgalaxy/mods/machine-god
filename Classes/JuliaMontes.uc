class JuliaMontes extends ScriptedPawnFemale;

#exec TEXTURE IMPORT NAME=JuliaMontes_Head FILE=Textures\Skins\JuliaMontes_Head.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=JuliaMontes_Torso FILE=Textures\Skins\JuliaMontes_Torso.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=InfoPortrait_JuliaMontes FILE=Textures\UI\InfoPortrait_JuliaMontes.pcx GROUP=UI MIPS=OFF

defaultproperties
{
    ArcheType=AT_Military
    BindName="JuliaMontes"
    FamiliarName="Julia Montes"
    GroundSpeed=200.000000
    Mesh=LodMesh'DeusExCharacters.GFM_TShirtPants'
    MultiSkins(0)=Texture'MachineGod.Skins.JuliaMontes_Head'
    MultiSkins(1)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(2)=Texture'MachineGod.Skins.JuliaMontes_Head'
    MultiSkins(3)=Texture'DeusExItems.Skins.GrayMaskTex'
    MultiSkins(4)=Texture'DeusExItems.Skins.BlackMaskTex'
    MultiSkins(5)=None
    MultiSkins(6)=Texture'MachineGod.Skins.TacticalPants3'
    MultiSkins(7)=Texture'MachineGod.Skins.JuliaMontes_Torso'
    UnfamiliarName="Some badass"
    bImportant=True
    bInvincible=True
    CombatStyle=1
    bAimForHead=True
    BaseAccuracy=-0.1
    PortraitTexture=Texture'MachineGod.UI.InfoPortrait_JuliaMontes'
}
