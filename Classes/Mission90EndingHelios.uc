//=============================================================================
// Mission90 - Ending - Helios
//=============================================================================
class Mission90EndingHelios extends Mission90;

// BEGIN UE1

var float ClawSpeed, MaxClawSpeed, ClawSpeedMultiplier, HeadsTimer;
var bool bSpinClaw, bSpinSparks, bMoveHeads;
var Mover aClaw;
var AmbientSound aClawSound;
var ElectricityEmitter aClawSparks[4];
var Actor aHeliosHead, aJCHead, aTiffanyHead;
var Vector StartTiffLoc, StartJCLoc;
var Light aHeadsLight;

function InitMap()
{
    local int i;
    local Actor aActor;

    Super.InitMap();

    // Claw
    aClaw = Mover(GetActor('Claw'));
    aClawSound = AmbientSound(GetActor('ClawSound'));
   
    i = 0;
    
    foreach AllActors(class'Actor', aActor, 'ClawSparks')
    {
        aClawSparks[i] = ElectricityEmitter(aActor);
        aClawSparks[i].LastFlickerTime = i * 0.25;
        i++;
    }

    // Heads
    aHeliosHead = GetActor('HeliosHead');
    aJCHead = GetActor('JCHead');
    aTiffanyHead = GetActor('TiffanyHead');

    StartTiffLoc = aTiffanyHead.Location;
    StartJCLoc = aJCHead.Location;
    
    aHeadsLight = Light(GetActor('HeadsLight'));
}

function SetHeliosFace()
{
    local int i;
    local Actor aDouble;

    aDouble = GetActor('TiffanySavageDouble'); 

    for(i = 0; i < ArrayCount(Player.MultiSkins); i++)
    {
        if(Player.MultiSkins[i] != Texture'MachineGod.Skins.TiffanySavage_Head')
            continue;

        Player.MultiSkins[i] = Texture'MachineGod.Skins.TiffanySavage_HeadHelios';
        aDouble.MultiSkins[i] = Player.MultiSkins[i];
    }
}

function ConfigureClaw(bool bOn, optional float NewMultiplier, optional float NewMaxSpeed)
{
    MaxClawSpeed = NewMaxSpeed;
    ClawSpeedMultiplier = NewMultiplier;
    bSpinClaw = bOn; 
}

function SpinClaw(float DeltaTime)
{
    local Rotator NewClawRotation;
    local int i;

    if(!bSpinClaw)
        return;

    // Rotate claw
    if(aClaw != None)
        NewClawRotation.Yaw = aClaw.Rotation.Yaw + ClawSpeed;

    if(NewClawRotation.Yaw > 65536)
        NewClawRotation.Yaw -= 65536;
    
    aClaw.SetRotation(NewClawRotation);
    
    // Increment sound pitch
    if(aClawSound != None)
        aClawSound.SoundPitch = ClawSpeed / 32;
  
    // Change rotation speed
    if(MaxClawSpeed == 0)
    {
        ClawSpeed -= DeltaTime * ClawSpeedMultiplier;

        if(ClawSpeed < 0)
            ClawSpeed = 0;
    }
    else if(ClawSpeed < MaxClawSpeed)
    {
        ClawSpeed += DeltaTime * ClawSpeedMultiplier;
    }
}

function SpinSparks()
{
    local int i;

    for(i = 0; i < ArrayCount(aClawSparks); i++)
    {
        if(aClawSparks[i] == None)
            continue;

        // Turn the emitters off
        if(!bSpinSparks && aClawSparks[i].bIsOn)
        {
            aClawSparks[i].TurnOff();
        }
        // Turn the emitters on one by one
        else if(bSpinSparks && ClawSpeed / 512 >= (1.0 / ArrayCount(aClawSparks)) * i)
        {
            if(!aClawSparks[i].bIsOn)
                aClawSparks[i].TurnOn();

            // Increment spark frequency
            aClawSparks[i].FlickerTime = 1.0 - (ClawSpeed / 1024);
        }
    }   
}

function MoveHeads(float DeltaTime)
{
    local float Delta, HalfDelta, QuarterDelta;
    local Vector CurTiffLoc, CurJCLoc;
    local int i;

    if(!bMoveHeads)
        return;

    HeadsTimer += DeltaTime;
    Delta = HeadsTimer / 3.0;
    HalfDelta = (Delta - 0.5) / 0.5;

    if(Delta < 1.0)
    {
        CurTiffLoc = aTiffanyHead.Location;
        CurTiffLoc.X = Smerp(Delta, StartTiffLoc.X, 0);
        aTiffanyHead.SetLocation(CurTiffLoc);
        
        CurJCLoc = aJCHead.Location;
        CurJCLoc.X = Smerp(Delta, StartJCLoc.X, 0);
        aJCHead.SetLocation(CurJCLoc);
    }
    else
    {
        Delta = 1.0;
        bMoveHeads = False;
    }

    aHeadsLight.LightBrightness = Smerp(Delta, 128, 0);

    if(HalfDelta > 0)
    {
        aHeliosHead.ScaleGlow = Smerp(HalfDelta, 0, 255);
        aHeliosHead.AmbientGlow = Smerp(HalfDelta, 0, 255);
    }
}

function Tick(float DeltaTime)
{
    Super.Tick(DeltaTime);

    SpinClaw(DeltaTime);
    SpinSparks();
    MoveHeads(DeltaTime);
}

auto state EventManager
{

PlayerEnteredChamber:
    GetPlayer().bIgnoreAllInput = True;

    PlayMusic("Endgame3_Music.Endgame3_Music", 0, 255, MTRAN_FastFade);
    TriggerActor('Camera0');
    
    Sleep(1.0);
    
    OpenDoor('Claw');
    
    Sleep(1.0);
    
    StartConversation('JCDenton', 'HeliosEnding1', False, True);
    
    Sleep(2.0);

    Player.PlaySound(Sound'Pneumatic3Close',,64);
    
    Sleep(2.0);

ChamberStarted:
    ConfigureClaw(True, 64, 512);

    Sleep(3.0);
   
    // Camera1 -> Camera2 (orbit Tiffany)

    SetActorHidden('TiffanySavageDouble', False);
    OpenDoor('SideDoor');

    Sleep(3.0);

    bSpinSparks = True; 

    Sleep(7.0);

    // Camera3 -> Camera4 (zoom on Tong)

    Sleep(1.0);

ChamberSpeedUp:
    ConfigureClaw(True, 128, 1024);
    
    Sleep(3.0);

    // Camera5 (Tiffany headshot)

    Sleep(2.0);
  
HeliosActivate:
    Player.PlaySound(Sound'PickupActivate');
    SetHeliosFace();

    Sleep(2.0);

ChamberSlowDown:
    // Camera6 (Tiffany above)
    
    ConfigureClaw(True, 256, 0);

    Sleep(2.0);

    bSpinSparks = False;

    Sleep(2.0);

ChamberDone:
    ConfigureClaw(False);
    
    Sleep(1.0);
    
    CloseDoor('Claw');

    Sleep(2.0);

    DesiredSoundVolume = 0;
    OrderPawn('TiffanySavageDouble', 'GoingTo', 'TiffanyChamberExit');

    Sleep(2.0);
    
    // Camera7 (Paul waiting)

    Sleep(1.0);

MeetPaul:
    MoveActor('TiffanySavageDouble', 'TiffanySideDoor1');
    OrderPawn('TiffanySavageDouble', 'GoingTo', 'TiffanySideDoor2');

    Sleep(3.0);

    OrderPawn('PaulDenton', 'GoingTo', 'PaulSideDoor');

    Sleep(5.0);
   
MeetTong:
    // Camera8 (Tong headshot)

    Sleep(1.0);

    MoveActor('TiffanySavageDouble', 'TiffanyLab1');
    OrderPawn('TiffanySavageDouble', 'GoingTo', 'TiffanyLab2');
    
    MoveActor('PaulDenton', 'PaulLab1');
    OrderPawn('PaulDenton', 'GoingTo', 'PaulLab2');

    TurnActorTowards('TracerTong', 'TiffanyLab2');

    Sleep(2.0);

    // Camera9 -> Camera10 (Follow Paul and Tiffany)

    Sleep(6.0);

    StartConversation('TiffanySavage', 'HeliosEnding2', False, True);

    EndEvent();

PaulSpeaking:
    TriggerActor('Camera11'); 

    MakePawnLookAt('TiffanySavageDouble', 'PaulDenton');
    MakePawnLookAt('PaulDenton', 'TiffanySavageDouble');

    EndEvent();

TiffanySpeaking:
    TriggerActor('Camera12'); 
    
    OrderPawn('Zombie', 'WalkingForward');

    EndEvent();

TongSpeaking:
    TriggerActor('Camera13'); 
    
    MakePawnLookAt('TiffanySavageDouble', 'TracerTong');
    MakePawnLookAt('PaulDenton', 'TracerTong');

    EndEvent();

Outside:
    TriggerActor('Camera14');

    EndEvent();

Heads:
    TriggerActor('Camera16');

    bMoveHeads = True;

    Sleep(2.5);

    Player.SetInstantSoundVolume(128);
    StopMusic();

    aHeliosHead.PlayAnim('MouthO', 1.0, 0.1);
    
    Sleep(0.3);
    
    aHeliosHead.PlayAnim('MouthClosed', 1.0, 0.2);

    Sleep(1.0);

    Player.ShowCredits(True);

    GetPlayer().bIgnoreAllInput = False;
}

// END UE1
