//=============================================================================
// FollowStealthTrigger
// ---
// Makes pawns follow the player and crouch at this spot
//=============================================================================
class FollowStealthTrigger extends FollowTrigger;

// BEGIN UE1

function StartPawn(ScriptedPawn aPawn, name OrderTag)
{
    if(
        aPawn == None || 
        (
            aPawn.IsInState('Hiding') &&
            aPawn.OrderTag == OrderTag
        )
    )
        return;

    aPawn.SetOrders('RunningTo', OrderTag, True);

    aPawn.bIgnore = True;
    aPawn.bDetectable = False;
}

function StopPawn(ScriptedPawn aPawn)
{
    if(aPawn == None)
        return;
        
    aPawn.SetOrders('Hiding', Tag, True);
}

// END UE1
