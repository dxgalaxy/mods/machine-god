---
title: Timeline
description: This is a unified timeline of the Deus Ex canon and intercompliant fan fiction
---

Fan fiction accounted for:  

- Deus Ex: Machine God  
- Code: 1R15  

@timeline
    '2030':
    - Name: MJ12 coup
      Location: USA
      Factions:
      - Illuminati
      - MJ12
      Description: |
        Bob Page forms MJ12 and overthrows the Illuminati
        MJ12 extends its reach and entirely takes over several governments, including the Chinese

    '2051':
    - Name: Segregation of Hong Kong
      Location: Hong Kong
      Factions:
      - MJ12
      Description: |
        Most of the population have emigrated due to extreme food shortage and wealth gap
        Only Wan Chai and a few other districts are still well off, living as if it's the 1990's
        Districts are physically sectioned off by the government, officially as a Gray Death countermeasure

    '2052':
    - Name: Fall of the NSF
      Location: New York, USA
      Factions:
      - UNATCO
      - NSF
      Characters:
      - JC Denton
      - Juan Ivanovic Lebedev
      - Julia Montes
      - Morgan Everett
      - Paul Denton
      Description: |
        Paul reveals to JC that he's working with the NSF
        JC finds Lebedev inside his Boeing 747
        Lebedev is taken away for questioning by UNATCO troops and murdered afterwards
        Lebedev's assets are confiscated, and the NSF lost its main benefactor and coordinator
        The Juggernaut Collective is reactivated, and some remaining NSF members join them
        The Juggernaut Collective decide to establish a work relationship with Morgan Everet through the NSF, lending him foot soliders
        Julia Montes infiltrates the Illuminati as a double agent for the Juggernaut Collective

    - Name: Unification of the triads
      Location: Hong Kong
      Characters:
      - JC Denton
      - Tracer Tong
      - Max Chen
      - Gordon Quick
      Description: |
        With JC's help, Tong retrieves the blueprint for the Dragon's Tooth sword and gives it to both rivalling triads
        Max and Gordon celebrate and decide to unify the triads.

    - Location: Paris, France
      Characters:
      - Alex Jacobson
      - Morgan Everett
      - Tracer Tong
      Description: |
        Tracer Tong sends Alex Jacobson to spy on Morgan Everett

    - Name: Deus Ex 1 ending
      Location: Area 51, USA
      Factions:
      - MJ12
      Characters:
      - Helios
      - JC Denton
      Description: |
        JC Denton agrees to merge with Helios inside the Aquinas Hub
        Bob Page dies, and MJ12 loses their monolothic leadership structure
        Their goal is to infuse all of humanity with nanites in order to grant them unity through global telepathy
        They decide that they need Gary Savage's UC to produce enough nanites

    - Name: Triad takeover
      Location: Hong Kong
      Characters:
      - Tracer Tong
      - Paul Denton
      - JC/Helios
      - Max Chen
      - Gordon Quick
      Description: |
        As MJ12 was until now puppeteering the Chinese governemnt, it collapsed with the death of Bob Page
        The triad leaders want to run Hong Kong like in the 1960's, with drugs and human trafficking as their main business
        Out of disagreement and strong commitment to his vision of city states, Tong decides to overthrow Max and Gordon, becoming the ultimate triad head
        Max gets assassinated, but Gordon manages to flee
        At the behest of Helios, Tong develops the street drug Henosia, which allows Helios to interface with the user
        The new triad sets up scramblers all over the city, to prevent outside communication until they've secured their grip on the population

    '2053':
    - Name: Illuminati comeback
      Location: Tijuana, Mexico
      Factions:
      - Illuminati
      - The NSF
      Characters:
      - Alex Jacobson
      - Julia Montes
      - Morgan Everett
      - Stanton Dowd
      Description: |
        Morgan Everett and Stanton Dowd reform the Illuminati, reintegrating MJ12 members
        They set up a base in Tijuana, which is within reasonable distance of Area 51, in order to take over the Aquinas Hub
        Alex Jacobson is brought along and has been subject to gradual hypnosis, now under Everett's control
        They develop two new augmentations, telepathy and possession, with the ultimate goal of using them to take the Aquinas Hub from JC
        Julia, Dowd and Everett hatch a plan to weaken JC/Helios, which involves soliciting the help of Tiffany, who is compatible with the new augs
        Julia regularly reports to NSF in order to eventually prevent the Illuminati from carrying out their plan

    - Name: Machine God beginning
      Location: Lompoc, USA
      Characters:
      - JC/Helios
      - Tiffany Savage
      - Gary Savage
      Description: |
        Gary refuses JC/Helios, and they attack X51
        The attack is held off by X51 lockdown protocol, and JC/Helios retreat
        Tiffany Savage takes on the task of contacting Morgan Everett for help with stopping JC/Helios
        Gary contacts Tracer Tong for help locating Everett
        Tong is officially loyal to Helios, but decides to help Gary out of friendship
        Tong lost contact with Alex Jacobson when Everett moved him to his new project in Tijuana
        Tiffany and Miguel drive to Tijuana to find Everett

    - Location: Tijuana, Mexico
      Characters:
      - Alex Jacobson
      - Anton Jackson
      - Gary Savage
      - JC/Helios
      - Julia Montes
      - Miguel Montes
      - Morgan Everett
      - Tiffany Savage
      Description: |
        Tijuana is segregated, with the inner city only being accessible by the upper class
        Tiffany meets Julia Montes, Miguel's mother, who is an NSF spy pretending to work for the Illuminati
        Julia helps Tiffany contact the cartel, who can smuggle people into the inner city
        The gang member in charge of the smuggling has been taking Henosia
        Helios influences the gang member to classify Tiffany and Miguel as convicts and send them to the Illuminati's private prison instead of giving them city passes
        Tiffany and Miguel find themselves in a simulated reality, where they live in ignorance for a month
        Inside they meet Anton Jackson, an NSF engineer, who helps them escape
        As Tiffany wakes up, she is met by Julia, who apologizes for the misunderstanding
        She takes Tiffany to Morgan Everett, who reveals their plot to acquire a blueprint of JC's nanite architecture, in order to weaken his defenses
        He sends her and Miguel on a mission to Hong Kong, to infiltrate Tong's lab and retrieve the blueprint
        Julia is sent to a separate location in Hong Kong, to provide intel to Tiffany

    - Location: Hong Kong
      Characters:
      - Adam Jensen
      - Decker Parkes
      - Erin Todd
      - Gordon Quick
      - Iris
      - Janus
      - Julia Montes
      - Miguel Montes
      - Tiffany Savage
      - Tracer Tong
      - Wayne Young 
      Description: |
        Tiffany and Miguel land in North Point, as the signal scramblers prevented them from landing anywhere else
        There is a minor resistance to the triad takeover established in North Point, where Gordon Quick is now hiding
        Tiffany finds Gordon, and he helps her locate Tong's new triad compound
        She infiltrates the compound and meets Paul Denton, who reveals that she is a Denton
        She confirms this postulate with Helios in Tong's lab, but Helios adds that she is only half Denton, a spliced clone
        Tiffany declines the offer to merge with Helios and is incapacitated
        Julia calls Adam & Iris asking for their help saving her
        Adam and Iris save Tiffany, bringing her back to the NSF HQ
        As Tiffany awakens, Julia explains she's a spy for the NSF
        When Tiffany tries to establish contact with Gary, he is unreachable.
        She finds a message from Clara Brown saying JC has taken over Vandenberg

    - Name: Machine God ending
      Location: Lompoc, USA
      Description: |
        Tiffany and Adam return to Vandenberg, which has been reformed with new defenses and traps.
        They encounter the Illuminati, who are executing a last ditch effort to acquire the blueprint of JC.
        Tiffany can decide to join or fight them.
        Alex Jacobson deploys Hypnos on them. 
        Adam and Tiffany are now in a sim again.
        They break out, wake up Alex and incapacitate the Illuminati strike force with his help.
        Adam and Tiffany find Gary in hiding with Carla and Tim.
        Gary tells Tiffany that he and his wife couldn't conveice, so he cloned her using Paul Denton's and Iris' genes.
        Since Gary was under MJ12 employment, Tiffany was meant to be a UNATCO agent like JC, but Gary and his wife hid her intended purpose from her, and ultimately they all fled, after which Gary's wife was murdered by MJ12.
        As they proceed, they find JC/Helios and Paul waiting for them.
        Adam fights Paul, Tiffany fights JC inside his mind.
        Tiffany corrupts JC's nanite architecture so the merging fails.
        Wanting to ensure that Helios doesn't fall into the wrong hands, JC sacrifices himself to destroy the Aquinas Hub, which plunges him into a coma and triggers the collapse.
