// ================================================================================ 
// SkinLibrary - A colleciton of textures with added metadata
// NOTE:
//      This is an automatically generated class, do not modify.
//      See /Scripts/generate_skins.py and /Scripts/generate_skins_functions.py
// ================================================================================ 
class SkinLibrary extends Object abstract;

// ----------------------------------------
// Texture imports
// ----------------------------------------
#exec TEXTURE IMPORT NAME=Head_Female_Young1_Skin1 FILE=Textures\Skins\Head_Female_Young1_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Head_Female_Young2_Skin1 FILE=Textures\Skins\Head_Female_Young2_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Head_Female_Old1_Skin1 FILE=Textures\Skins\Head_Female_Old1_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Head_Female_Young1_Skin2 FILE=Textures\Skins\Head_Female_Young1_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Head_Female_Young2_Skin2 FILE=Textures\Skins\Head_Female_Young2_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Head_Female_Old1_Skin2 FILE=Textures\Skins\Head_Female_Old1_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Head_Female_Young1_Skin3 FILE=Textures\Skins\Head_Female_Young1_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Head_Female_Young2_Skin3 FILE=Textures\Skins\Head_Female_Young2_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Head_Female_Old1_Skin3 FILE=Textures\Skins\Head_Female_Old1_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Head_Female_Young1_Skin4 FILE=Textures\Skins\Head_Female_Young1_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Head_Female_Young2_Skin4 FILE=Textures\Skins\Head_Female_Young2_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Head_Female_Old1_Skin4 FILE=Textures\Skins\Head_Female_Old1_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Sleeveless1_Female_Skin1 FILE=Textures\Skins\Sleeveless1_Female_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TShirt1_Female_Skin1 FILE=Textures\Skins\TShirt1_Female_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TankTop1_Female_Skin1 FILE=Textures\Skins\TankTop1_Female_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=GunHolster1_Female_Skin1 FILE=Textures\Skins\GunHolster1_Female_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=BulletProofVest1_Female_Skin1 FILE=Textures\Skins\BulletProofVest1_Female_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=CropTop1_Female_Skin1 FILE=Textures\Skins\CropTop1_Female_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Sleeveless2_Female_Skin2 FILE=Textures\Skins\Sleeveless2_Female_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TShirt2_Female_Skin2 FILE=Textures\Skins\TShirt2_Female_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TankTop2_Female_Skin2 FILE=Textures\Skins\TankTop2_Female_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=GunHolster2_Female_Skin2 FILE=Textures\Skins\GunHolster2_Female_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=BulletProofVest2_Female_Skin2 FILE=Textures\Skins\BulletProofVest2_Female_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=CropTop2_Female_Skin2 FILE=Textures\Skins\CropTop2_Female_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Sleeveless3_Female_Skin3 FILE=Textures\Skins\Sleeveless3_Female_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TShirt3_Female_Skin3 FILE=Textures\Skins\TShirt3_Female_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TankTop3_Female_Skin3 FILE=Textures\Skins\TankTop3_Female_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=GunHolster3_Female_Skin3 FILE=Textures\Skins\GunHolster3_Female_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=BulletProofVest3_Female_Skin3 FILE=Textures\Skins\BulletProofVest3_Female_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=CropTop3_Female_Skin3 FILE=Textures\Skins\CropTop3_Female_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Sleeveless4_Female_Skin4 FILE=Textures\Skins\Sleeveless4_Female_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TShirt4_Female_Skin4 FILE=Textures\Skins\TShirt4_Female_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TankTop4_Female_Skin4 FILE=Textures\Skins\TankTop4_Female_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=GunHolster4_Female_Skin4 FILE=Textures\Skins\GunHolster4_Female_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=BulletProofVest4_Female_Skin4 FILE=Textures\Skins\BulletProofVest4_Female_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=CropTop4_Female_Skin4 FILE=Textures\Skins\CropTop4_Female_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TacticalShirt1_Female FILE=Textures\Skins\TacticalShirt1_Female.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TacticalShirt2_Female FILE=Textures\Skins\TacticalShirt2_Female.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TacticalShirt3_Female FILE=Textures\Skins\TacticalShirt3_Female.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TacticalShirt4_Female FILE=Textures\Skins\TacticalShirt4_Female.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=PlainCoat1_Female FILE=Textures\Skins\PlainCoat1_Female.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=PlainCoat2_Female FILE=Textures\Skins\PlainCoat2_Female.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=PlainCoat3_Female FILE=Textures\Skins\PlainCoat3_Female.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=SegmentedCoat1_Female FILE=Textures\Skins\SegmentedCoat1_Female.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=SegmentedCoat2_Female FILE=Textures\Skins\SegmentedCoat2_Female.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=SegmentedCoat3_Female FILE=Textures\Skins\SegmentedCoat3_Female.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=LeatherJacket1_Female FILE=Textures\Skins\LeatherJacket1_Female.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=LeatherJacket2_Female FILE=Textures\Skins\LeatherJacket2_Female.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=LeatherJacket3_Female FILE=Textures\Skins\LeatherJacket3_Female.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=OfficeDress1_Female_Skin1 FILE=Textures\Skins\OfficeDress1_Female_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=OfficeDress2_Female_Skin2 FILE=Textures\Skins\OfficeDress2_Female_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=OfficeDress3_Female_Skin3 FILE=Textures\Skins\OfficeDress3_Female_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=OfficeDress4_Female_Skin4 FILE=Textures\Skins\OfficeDress4_Female_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Legs_Female_Skin1 FILE=Textures\Skins\Legs_Female_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Legs_Female_Skin2 FILE=Textures\Skins\Legs_Female_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Legs_Female_Skin3 FILE=Textures\Skins\Legs_Female_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Legs_Female_Skin4 FILE=Textures\Skins\Legs_Female_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Hotpants1_Female_Skin1 FILE=Textures\Skins\Hotpants1_Female_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Hotpants2_Female_Skin2 FILE=Textures\Skins\Hotpants2_Female_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Hotpants3_Female_Skin3 FILE=Textures\Skins\Hotpants3_Female_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Hotpants4_Female_Skin4 FILE=Textures\Skins\Hotpants4_Female_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=SkinnyJeans1_Female FILE=Textures\Skins\SkinnyJeans1_Female.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=SkinnyJeans2_Female FILE=Textures\Skins\SkinnyJeans2_Female.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=SkinnyJeans3_Female FILE=Textures\Skins\SkinnyJeans3_Female.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=SkinnyJeans4_Female FILE=Textures\Skins\SkinnyJeans4_Female.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TShirtFront1_Female_Skin1 FILE=Textures\Skins\TShirtFront1_Female_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TShirtFront2_Female_Skin2 FILE=Textures\Skins\TShirtFront2_Female_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TShirtFront3_Female_Skin3 FILE=Textures\Skins\TShirtFront3_Female_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TShirtFront4_Female_Skin4 FILE=Textures\Skins\TShirtFront4_Female_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Skirt1_Female FILE=Textures\Skins\Skirt1_Female.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Skirt2_Female FILE=Textures\Skins\Skirt2_Female.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Skirt3_Female FILE=Textures\Skins\Skirt3_Female.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Skirt4_Female FILE=Textures\Skins\Skirt4_Female.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Head_Male_Young1_Skin1 FILE=Textures\Skins\Head_Male_Young1_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Head_Male_Young2_Skin1 FILE=Textures\Skins\Head_Male_Young2_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Head_Male_Old1_Skin1 FILE=Textures\Skins\Head_Male_Old1_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Head_Male_Young1_Skin2 FILE=Textures\Skins\Head_Male_Young1_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Head_Male_Young2_Skin2 FILE=Textures\Skins\Head_Male_Young2_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Head_Male_Old1_Skin2 FILE=Textures\Skins\Head_Male_Old1_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Head_Male_Young1_Skin3 FILE=Textures\Skins\Head_Male_Young1_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Head_Male_Young2_Skin3 FILE=Textures\Skins\Head_Male_Young2_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Head_Male_Old1_Skin3 FILE=Textures\Skins\Head_Male_Old1_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Head_Male_Young1_Skin4 FILE=Textures\Skins\Head_Male_Young1_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Head_Male_Young2_Skin4 FILE=Textures\Skins\Head_Male_Young2_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Head_Male_Old1_Skin4 FILE=Textures\Skins\Head_Male_Old1_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Jacket1_MaleSuit_Skin1 FILE=Textures\Skins\Jacket1_MaleSuit_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Jacket2_MaleSuit_Skin2 FILE=Textures\Skins\Jacket2_MaleSuit_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Jacket3_MaleSuit_Skin3 FILE=Textures\Skins\Jacket3_MaleSuit_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Jacket4_MaleSuit_Skin4 FILE=Textures\Skins\Jacket4_MaleSuit_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TankTop1_MaleJumpsuit_Skin1 FILE=Textures\Skins\TankTop1_MaleJumpsuit_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=GunHolster1_MaleJumpsuit_Skin1 FILE=Textures\Skins\GunHolster1_MaleJumpsuit_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Sweater1_MaleJumpsuit_Skin1 FILE=Textures\Skins\Sweater1_MaleJumpsuit_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=BulletProofVest1_MaleJumpsuit_Skin1 FILE=Textures\Skins\BulletProofVest1_MaleJumpsuit_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TankTop2_MaleJumpsuit_Skin2 FILE=Textures\Skins\TankTop2_MaleJumpsuit_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=GunHolster2_MaleJumpsuit_Skin2 FILE=Textures\Skins\GunHolster2_MaleJumpsuit_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Sweater2_MaleJumpsuit_Skin2 FILE=Textures\Skins\Sweater2_MaleJumpsuit_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=BulletProofVest2_MaleJumpsuit_Skin2 FILE=Textures\Skins\BulletProofVest2_MaleJumpsuit_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TankTop3_MaleJumpsuit_Skin3 FILE=Textures\Skins\TankTop3_MaleJumpsuit_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=GunHolster3_MaleJumpsuit_Skin3 FILE=Textures\Skins\GunHolster3_MaleJumpsuit_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Sweater3_MaleJumpsuit_Skin3 FILE=Textures\Skins\Sweater3_MaleJumpsuit_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=BulletProofVest3_MaleJumpsuit_Skin3 FILE=Textures\Skins\BulletProofVest3_MaleJumpsuit_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TankTop4_MaleJumpsuit_Skin4 FILE=Textures\Skins\TankTop4_MaleJumpsuit_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=GunHolster4_MaleJumpsuit_Skin4 FILE=Textures\Skins\GunHolster4_MaleJumpsuit_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Sweater4_MaleJumpsuit_Skin4 FILE=Textures\Skins\Sweater4_MaleJumpsuit_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=BulletProofVest4_MaleJumpsuit_Skin4 FILE=Textures\Skins\BulletProofVest4_MaleJumpsuit_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=ArmorSweater1_MaleJumpsuit FILE=Textures\Skins\ArmorSweater1_MaleJumpsuit.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=ArmorSweater2_MaleJumpsuit FILE=Textures\Skins\ArmorSweater2_MaleJumpsuit.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=ArmorSweater3_MaleJumpsuit FILE=Textures\Skins\ArmorSweater3_MaleJumpsuit.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=ArmorSweater4_MaleJumpsuit FILE=Textures\Skins\ArmorSweater4_MaleJumpsuit.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TankTop1_MaleFat_Skin1 FILE=Textures\Skins\TankTop1_MaleFat_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=DressShirt1_MaleFat_Skin1 FILE=Textures\Skins\DressShirt1_MaleFat_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TankTop2_MaleFat_Skin2 FILE=Textures\Skins\TankTop2_MaleFat_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=DressShirt2_MaleFat_Skin2 FILE=Textures\Skins\DressShirt2_MaleFat_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TankTop3_MaleFat_Skin3 FILE=Textures\Skins\TankTop3_MaleFat_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=DressShirt3_MaleFat_Skin3 FILE=Textures\Skins\DressShirt3_MaleFat_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TankTop4_MaleFat_Skin4 FILE=Textures\Skins\TankTop4_MaleFat_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=DressShirt4_MaleFat_Skin4 FILE=Textures\Skins\DressShirt4_MaleFat_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TankTop1_Male_Skin1 FILE=Textures\Skins\TankTop1_Male_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=DressShirt1_Male_Skin1 FILE=Textures\Skins\DressShirt1_Male_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TankTop2_Male_Skin2 FILE=Textures\Skins\TankTop2_Male_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=DressShirt2_Male_Skin2 FILE=Textures\Skins\DressShirt2_Male_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TankTop3_Male_Skin3 FILE=Textures\Skins\TankTop3_Male_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=DressShirt3_Male_Skin3 FILE=Textures\Skins\DressShirt3_Male_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TankTop4_Male_Skin4 FILE=Textures\Skins\TankTop4_Male_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=DressShirt4_Male_Skin4 FILE=Textures\Skins\DressShirt4_Male_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=PlainCoat1_Male FILE=Textures\Skins\PlainCoat1_Male.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=PlainCoat2_Male FILE=Textures\Skins\PlainCoat2_Male.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=LeatherJacket1_Male FILE=Textures\Skins\LeatherJacket1_Male.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=LeatherJacket2_Male FILE=Textures\Skins\LeatherJacket2_Male.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=PlainCoat1_MaleFat FILE=Textures\Skins\PlainCoat1_MaleFat.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=PlainCoat2_MaleFat FILE=Textures\Skins\PlainCoat2_MaleFat.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=LeatherJacket1_MaleFat FILE=Textures\Skins\LeatherJacket1_MaleFat.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=LeatherJacket2_MaleFat FILE=Textures\Skins\LeatherJacket2_MaleFat.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=SuitJacket1_MaleFat FILE=Textures\Skins\SuitJacket1_MaleFat.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=SuitJacket2_MaleFat FILE=Textures\Skins\SuitJacket2_MaleFat.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TShirtFront1_Male_Skin1 FILE=Textures\Skins\TShirtFront1_Male_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TShirtFront2_Male_Skin2 FILE=Textures\Skins\TShirtFront2_Male_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TShirtFront3_Male_Skin3 FILE=Textures\Skins\TShirtFront3_Male_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TShirtFront4_Male_Skin4 FILE=Textures\Skins\TShirtFront4_Male_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TornTShirtFront1_Male_Skin1 FILE=Textures\Skins\TornTShirtFront1_Male_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TornTShirtFront2_Male_Skin2 FILE=Textures\Skins\TornTShirtFront2_Male_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TornTShirtFront3_Male_Skin3 FILE=Textures\Skins\TornTShirtFront3_Male_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TornTShirtFront4_Male_Skin4 FILE=Textures\Skins\TornTShirtFront4_Male_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=DressShirtFront1_Male FILE=Textures\Skins\DressShirtFront1_Male.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=DressShirtFront2_Male FILE=Textures\Skins\DressShirtFront2_Male.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=DressShirtFront3_Male FILE=Textures\Skins\DressShirtFront3_Male.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=DressShirtFront4_Male FILE=Textures\Skins\DressShirtFront4_Male.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=DressShirtTieFront1_Male FILE=Textures\Skins\DressShirtTieFront1_Male.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=DressShirtTieFront2_Male FILE=Textures\Skins\DressShirtTieFront2_Male.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=DressShirtTieFront3_Male FILE=Textures\Skins\DressShirtTieFront3_Male.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=DressShirtTieFront4_Male FILE=Textures\Skins\DressShirtTieFront4_Male.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Goggles FILE=Textures\Skins\Goggles.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Goggles_1 FILE=Textures\Skins\Goggles_1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Goggles_2 FILE=Textures\Skins\Goggles_2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Head_Child1_Skin1 FILE=Textures\Skins\Head_Child1_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Head_Child1_Skin2 FILE=Textures\Skins\Head_Child1_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Head_Child1_Skin3 FILE=Textures\Skins\Head_Child1_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Head_Child1_Skin4 FILE=Textures\Skins\Head_Child1_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TankTop1_Child_Skin1 FILE=Textures\Skins\TankTop1_Child_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TankTop2_Child_Skin2 FILE=Textures\Skins\TankTop2_Child_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TankTop3_Child_Skin3 FILE=Textures\Skins\TankTop3_Child_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TankTop4_Child_Skin4 FILE=Textures\Skins\TankTop4_Child_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Jeans1 FILE=Textures\Skins\Jeans1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Jeans2 FILE=Textures\Skins\Jeans2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Jeans3 FILE=Textures\Skins\Jeans3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Jeans4 FILE=Textures\Skins\Jeans4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TacticalPants1 FILE=Textures\Skins\TacticalPants1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TacticalPants2 FILE=Textures\Skins\TacticalPants2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TacticalPants3 FILE=Textures\Skins\TacticalPants3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TacticalPants4 FILE=Textures\Skins\TacticalPants4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TechPants FILE=Textures\Skins\TechPants.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TornJeans1_Skin1 FILE=Textures\Skins\TornJeans1_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=SuitPants1_Skin1 FILE=Textures\Skins\SuitPants1_Skin1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TornJeans2_Skin2 FILE=Textures\Skins\TornJeans2_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=SuitPants2_Skin2 FILE=Textures\Skins\SuitPants2_Skin2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TornJeans3_Skin3 FILE=Textures\Skins\TornJeans3_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=SuitPants3_Skin3 FILE=Textures\Skins\SuitPants3_Skin3.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TornJeans4_Skin4 FILE=Textures\Skins\TornJeans4_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=SuitPants4_Skin4 FILE=Textures\Skins\SuitPants4_Skin4.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Lenses1 FILE=Textures\Skins\Lenses1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Lenses2 FILE=Textures\Skins\Lenses2.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Frames1 FILE=Textures\Skins\Frames1.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=Frames5 FILE=Textures\Skins\Frames5.pcx GROUP=Skins


// -----------------------------------------------------------------------
// Gets a TextureInfo object from an index
// -----------------------------------------------------------------------
static function GetTextureInfo(int Index, out Texture Tex, out name Gender, out name Category, out name Tags[10], out int SkinTone, out name Age)
{
    local int i;

    // First reset the out variables, as they might have been assigned previously
    Tex = None;
    Gender = '';
    Category = '';
    SkinTone = 0;
    Age = '';
    
    for(i = 0; i < ArrayCount(Tags); i++)
        Tags[i] = '';

    // Then look up the info
    switch(Index)
    {
        // ----------------------------------------
        // Female
        // ----------------------------------------
        // Heads
        case 0:
            Tex = Texture'Head_Female_Young1_Skin1';
            Gender = 'Female';
            Age = 'Young';
            Category = 'Heads';
            SkinTone = 1;
            break;

        case 1:
            Tex = Texture'Head_Female_Young2_Skin1';
            Gender = 'Female';
            Age = 'Young';
            Category = 'Heads';
            SkinTone = 1;
            break;

        case 2:
            Tex = Texture'Head_Female_Old1_Skin1';
            Gender = 'Female';
            Age = 'Old';
            Category = 'Heads';
            SkinTone = 1;
            break;

        case 3:
            Tex = Texture'Head_Female_Young1_Skin2';
            Gender = 'Female';
            Age = 'Young';
            Category = 'Heads';
            SkinTone = 2;
            break;

        case 4:
            Tex = Texture'Head_Female_Young2_Skin2';
            Gender = 'Female';
            Age = 'Young';
            Category = 'Heads';
            SkinTone = 2;
            break;

        case 5:
            Tex = Texture'Head_Female_Old1_Skin2';
            Gender = 'Female';
            Age = 'Old';
            Category = 'Heads';
            SkinTone = 2;
            break;

        case 6:
            Tex = Texture'Head_Female_Young1_Skin3';
            Gender = 'Female';
            Age = 'Young';
            Category = 'Heads';
            SkinTone = 3;
            break;

        case 7:
            Tex = Texture'Head_Female_Young2_Skin3';
            Gender = 'Female';
            Age = 'Young';
            Category = 'Heads';
            SkinTone = 3;
            break;

        case 8:
            Tex = Texture'Head_Female_Old1_Skin3';
            Gender = 'Female';
            Age = 'Old';
            Category = 'Heads';
            SkinTone = 3;
            break;

        case 9:
            Tex = Texture'Head_Female_Young1_Skin4';
            Gender = 'Female';
            Age = 'Young';
            Category = 'Heads';
            SkinTone = 4;
            break;

        case 10:
            Tex = Texture'Head_Female_Young2_Skin4';
            Gender = 'Female';
            Age = 'Young';
            Category = 'Heads';
            SkinTone = 4;
            break;

        case 11:
            Tex = Texture'Head_Female_Old1_Skin4';
            Gender = 'Female';
            Age = 'Old';
            Category = 'Heads';
            SkinTone = 4;
            break;


        // Torsos
        case 12:
            Tex = Texture'Sleeveless1_Female_Skin1';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 1;
            Tags[0] = 'Normal';
            Tags[1] = 'Bum';
            break;

        case 13:
            Tex = Texture'TShirt1_Female_Skin1';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 1;
            Tags[0] = 'Normal';
            break;

        case 14:
            Tex = Texture'TankTop1_Female_Skin1';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 1;
            Tags[0] = 'Normal';
            Tags[1] = 'Bum';
            Tags[2] = 'Tech';
            break;

        case 15:
            Tex = Texture'GunHolster1_Female_Skin1';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 1;
            Tags[0] = 'Guerilla';
            break;

        case 16:
            Tex = Texture'BulletProofVest1_Female_Skin1';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 1;
            Tags[0] = 'Police';
            break;

        case 17:
            Tex = Texture'CropTop1_Female_Skin1';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 1;
            Tags[0] = 'Party';
            break;

        case 18:
            Tex = Texture'Sleeveless2_Female_Skin2';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 2;
            Tags[0] = 'Normal';
            Tags[1] = 'Bum';
            break;

        case 19:
            Tex = Texture'TShirt2_Female_Skin2';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 2;
            Tags[0] = 'Normal';
            break;

        case 20:
            Tex = Texture'TankTop2_Female_Skin2';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 2;
            Tags[0] = 'Normal';
            Tags[1] = 'Bum';
            Tags[2] = 'Tech';
            break;

        case 21:
            Tex = Texture'GunHolster2_Female_Skin2';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 2;
            Tags[0] = 'Guerilla';
            break;

        case 22:
            Tex = Texture'BulletProofVest2_Female_Skin2';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 2;
            Tags[0] = 'Police';
            break;

        case 23:
            Tex = Texture'CropTop2_Female_Skin2';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 2;
            Tags[0] = 'Party';
            break;

        case 24:
            Tex = Texture'Sleeveless3_Female_Skin3';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 3;
            Tags[0] = 'Normal';
            Tags[1] = 'Bum';
            break;

        case 25:
            Tex = Texture'TShirt3_Female_Skin3';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 3;
            Tags[0] = 'Normal';
            break;

        case 26:
            Tex = Texture'TankTop3_Female_Skin3';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 3;
            Tags[0] = 'Normal';
            Tags[1] = 'Bum';
            Tags[2] = 'Tech';
            break;

        case 27:
            Tex = Texture'GunHolster3_Female_Skin3';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 3;
            Tags[0] = 'Guerilla';
            break;

        case 28:
            Tex = Texture'BulletProofVest3_Female_Skin3';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 3;
            Tags[0] = 'Police';
            break;

        case 29:
            Tex = Texture'CropTop3_Female_Skin3';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 3;
            Tags[0] = 'Party';
            break;

        case 30:
            Tex = Texture'Sleeveless4_Female_Skin4';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 4;
            Tags[0] = 'Normal';
            Tags[1] = 'Bum';
            break;

        case 31:
            Tex = Texture'TShirt4_Female_Skin4';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 4;
            Tags[0] = 'Normal';
            break;

        case 32:
            Tex = Texture'TankTop4_Female_Skin4';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 4;
            Tags[0] = 'Normal';
            Tags[1] = 'Bum';
            Tags[2] = 'Tech';
            break;

        case 33:
            Tex = Texture'GunHolster4_Female_Skin4';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 4;
            Tags[0] = 'Guerilla';
            break;

        case 34:
            Tex = Texture'BulletProofVest4_Female_Skin4';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 4;
            Tags[0] = 'Police';
            break;

        case 35:
            Tex = Texture'CropTop4_Female_Skin4';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 4;
            Tags[0] = 'Party';
            break;

        case 36:
            Tex = Texture'TacticalShirt1_Female';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 4;
            Tags[0] = 'Military';
            Tags[1] = 'Guerilla';
            break;

        case 37:
            Tex = Texture'TacticalShirt2_Female';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 4;
            Tags[0] = 'Military';
            Tags[1] = 'Guerilla';
            break;

        case 38:
            Tex = Texture'TacticalShirt3_Female';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 4;
            Tags[0] = 'Military';
            Tags[1] = 'Guerilla';
            break;

        case 39:
            Tex = Texture'TacticalShirt4_Female';
            Gender = 'Female';
            Age = '';
            Category = 'Torsos';
            SkinTone = 4;
            Tags[0] = 'Military';
            Tags[1] = 'Guerilla';
            break;


        // Coats
        case 40:
            Tex = Texture'PlainCoat1_Female';
            Gender = 'Female';
            Age = '';
            Category = 'Coats';
            Tags[0] = 'Science';
            break;

        case 41:
            Tex = Texture'PlainCoat2_Female';
            Gender = 'Female';
            Age = '';
            Category = 'Coats';
            Tags[0] = 'Normal';
            break;

        case 42:
            Tex = Texture'PlainCoat3_Female';
            Gender = 'Female';
            Age = '';
            Category = 'Coats';
            Tags[0] = 'Normal';
            break;

        case 43:
            Tex = Texture'SegmentedCoat1_Female';
            Gender = 'Female';
            Age = '';
            Category = 'Coats';
            Tags[0] = 'Normal';
            break;

        case 44:
            Tex = Texture'SegmentedCoat2_Female';
            Gender = 'Female';
            Age = '';
            Category = 'Coats';
            Tags[0] = 'Normal';
            break;

        case 45:
            Tex = Texture'SegmentedCoat3_Female';
            Gender = 'Female';
            Age = '';
            Category = 'Coats';
            Tags[0] = 'Party';
            break;

        case 46:
            Tex = Texture'LeatherJacket1_Female';
            Gender = 'Female';
            Age = '';
            Category = 'Coats';
            Tags[0] = 'Normal';
            break;

        case 47:
            Tex = Texture'LeatherJacket2_Female';
            Gender = 'Female';
            Age = '';
            Category = 'Coats';
            Tags[0] = 'Normal';
            break;

        case 48:
            Tex = Texture'LeatherJacket3_Female';
            Gender = 'Female';
            Age = '';
            Category = 'Coats';
            Tags[0] = 'Normal';
            break;


        // Dresses
        case 49:
            Tex = Texture'OfficeDress1_Female_Skin1';
            Gender = 'Female';
            Age = '';
            Category = 'Dresses';
            SkinTone = 1;
            Tags[0] = 'Formal';
            Tags[1] = 'Normal';
            break;

        case 50:
            Tex = Texture'OfficeDress2_Female_Skin2';
            Gender = 'Female';
            Age = '';
            Category = 'Dresses';
            SkinTone = 2;
            Tags[0] = 'Formal';
            Tags[1] = 'Normal';
            break;

        case 51:
            Tex = Texture'OfficeDress3_Female_Skin3';
            Gender = 'Female';
            Age = '';
            Category = 'Dresses';
            SkinTone = 3;
            Tags[0] = 'Formal';
            Tags[1] = 'Normal';
            break;

        case 52:
            Tex = Texture'OfficeDress4_Female_Skin4';
            Gender = 'Female';
            Age = '';
            Category = 'Dresses';
            SkinTone = 4;
            Tags[0] = 'Formal';
            Tags[1] = 'Normal';
            break;


        // BareLegs
        case 53:
            Tex = Texture'Legs_Female_Skin1';
            Gender = 'Female';
            Age = '';
            Category = 'BareLegs';
            SkinTone = 1;
            Tags[0] = 'Normal';
            Tags[1] = 'Formal';
            break;

        case 54:
            Tex = Texture'Legs_Female_Skin2';
            Gender = 'Female';
            Age = '';
            Category = 'BareLegs';
            SkinTone = 2;
            Tags[0] = 'Normal';
            Tags[1] = 'Formal';
            break;

        case 55:
            Tex = Texture'Legs_Female_Skin3';
            Gender = 'Female';
            Age = '';
            Category = 'BareLegs';
            SkinTone = 3;
            Tags[0] = 'Normal';
            Tags[1] = 'Formal';
            break;

        case 56:
            Tex = Texture'Legs_Female_Skin4';
            Gender = 'Female';
            Age = '';
            Category = 'BareLegs';
            SkinTone = 4;
            Tags[0] = 'Normal';
            Tags[1] = 'Formal';
            break;


        // ClothedLegs
        case 57:
            Tex = Texture'Hotpants1_Female_Skin1';
            Gender = 'Female';
            Age = '';
            Category = 'ClothedLegs';
            SkinTone = 1;
            Tags[0] = 'Party';
            break;

        case 58:
            Tex = Texture'Hotpants2_Female_Skin2';
            Gender = 'Female';
            Age = '';
            Category = 'ClothedLegs';
            SkinTone = 2;
            Tags[0] = 'Party';
            break;

        case 59:
            Tex = Texture'Hotpants3_Female_Skin3';
            Gender = 'Female';
            Age = '';
            Category = 'ClothedLegs';
            SkinTone = 3;
            Tags[0] = 'Party';
            break;

        case 60:
            Tex = Texture'Hotpants4_Female_Skin4';
            Gender = 'Female';
            Age = '';
            Category = 'ClothedLegs';
            SkinTone = 4;
            Tags[0] = 'Party';
            break;

        case 61:
            Tex = Texture'SkinnyJeans1_Female';
            Gender = 'Female';
            Age = '';
            Category = 'ClothedLegs';
            Tags[0] = 'Normal';
            Tags[1] = 'Science';
            break;

        case 62:
            Tex = Texture'SkinnyJeans2_Female';
            Gender = 'Female';
            Age = '';
            Category = 'ClothedLegs';
            Tags[0] = 'Normal';
            Tags[1] = 'Science';
            break;

        case 63:
            Tex = Texture'SkinnyJeans3_Female';
            Gender = 'Female';
            Age = '';
            Category = 'ClothedLegs';
            Tags[0] = 'Normal';
            Tags[1] = 'Science';
            break;

        case 64:
            Tex = Texture'SkinnyJeans4_Female';
            Gender = 'Female';
            Age = '';
            Category = 'ClothedLegs';
            Tags[0] = 'Normal';
            Tags[1] = 'Science';
            break;


        // ShirtFronts
        case 65:
            Tex = Texture'TShirtFront1_Female_Skin1';
            Gender = 'Female';
            Age = '';
            Category = 'ShirtFronts';
            SkinTone = 1;
            Tags[0] = 'Normal';
            Tags[1] = 'Science';
            break;

        case 66:
            Tex = Texture'TShirtFront2_Female_Skin2';
            Gender = 'Female';
            Age = '';
            Category = 'ShirtFronts';
            SkinTone = 2;
            Tags[0] = 'Normal';
            Tags[1] = 'Science';
            break;

        case 67:
            Tex = Texture'TShirtFront3_Female_Skin3';
            Gender = 'Female';
            Age = '';
            Category = 'ShirtFronts';
            SkinTone = 3;
            Tags[0] = 'Normal';
            Tags[1] = 'Science';
            break;

        case 68:
            Tex = Texture'TShirtFront4_Female_Skin4';
            Gender = 'Female';
            Age = '';
            Category = 'ShirtFronts';
            SkinTone = 4;
            Tags[0] = 'Normal';
            Tags[1] = 'Science';
            break;


        // Skirts
        case 69:
            Tex = Texture'Skirt1_Female';
            Gender = 'Female';
            Age = '';
            Category = 'Skirts';
            Tags[0] = 'Normal';
            break;

        case 70:
            Tex = Texture'Skirt2_Female';
            Gender = 'Female';
            Age = '';
            Category = 'Skirts';
            Tags[0] = 'Normal';
            break;

        case 71:
            Tex = Texture'Skirt3_Female';
            Gender = 'Female';
            Age = '';
            Category = 'Skirts';
            Tags[0] = 'Normal';
            break;

        case 72:
            Tex = Texture'Skirt4_Female';
            Gender = 'Female';
            Age = '';
            Category = 'Skirts';
            Tags[0] = 'Normal';
            break;

        // ----------------------------------------
        // Male
        // ----------------------------------------
        // Heads
        case 73:
            Tex = Texture'Head_Male_Young1_Skin1';
            Gender = 'Male';
            Age = 'Young';
            Category = 'Heads';
            SkinTone = 1;
            break;

        case 74:
            Tex = Texture'Head_Male_Young2_Skin1';
            Gender = 'Male';
            Age = 'Young';
            Category = 'Heads';
            SkinTone = 1;
            break;

        case 75:
            Tex = Texture'Head_Male_Old1_Skin1';
            Gender = 'Male';
            Age = 'Old';
            Category = 'Heads';
            SkinTone = 1;
            break;

        case 76:
            Tex = Texture'Head_Male_Young1_Skin2';
            Gender = 'Male';
            Age = 'Young';
            Category = 'Heads';
            SkinTone = 2;
            break;

        case 77:
            Tex = Texture'Head_Male_Young2_Skin2';
            Gender = 'Male';
            Age = 'Young';
            Category = 'Heads';
            SkinTone = 2;
            break;

        case 78:
            Tex = Texture'Head_Male_Old1_Skin2';
            Gender = 'Male';
            Age = 'Old';
            Category = 'Heads';
            SkinTone = 2;
            break;

        case 79:
            Tex = Texture'Head_Male_Young1_Skin3';
            Gender = 'Male';
            Age = 'Young';
            Category = 'Heads';
            SkinTone = 3;
            break;

        case 80:
            Tex = Texture'Head_Male_Young2_Skin3';
            Gender = 'Male';
            Age = 'Young';
            Category = 'Heads';
            SkinTone = 3;
            break;

        case 81:
            Tex = Texture'Head_Male_Old1_Skin3';
            Gender = 'Male';
            Age = 'Old';
            Category = 'Heads';
            SkinTone = 3;
            break;

        case 82:
            Tex = Texture'Head_Male_Young1_Skin4';
            Gender = 'Male';
            Age = 'Young';
            Category = 'Heads';
            SkinTone = 4;
            break;

        case 83:
            Tex = Texture'Head_Male_Young2_Skin4';
            Gender = 'Male';
            Age = 'Young';
            Category = 'Heads';
            SkinTone = 4;
            break;

        case 84:
            Tex = Texture'Head_Male_Old1_Skin4';
            Gender = 'Male';
            Age = 'Old';
            Category = 'Heads';
            SkinTone = 4;
            break;


        // SuitTorsos
        case 85:
            Tex = Texture'Jacket1_MaleSuit_Skin1';
            Gender = 'Male';
            Age = '';
            Category = 'SuitTorsos';
            SkinTone = 1;
            Tags[0] = 'Formal';
            break;

        case 86:
            Tex = Texture'Jacket2_MaleSuit_Skin2';
            Gender = 'Male';
            Age = '';
            Category = 'SuitTorsos';
            SkinTone = 2;
            Tags[0] = 'Formal';
            break;

        case 87:
            Tex = Texture'Jacket3_MaleSuit_Skin3';
            Gender = 'Male';
            Age = '';
            Category = 'SuitTorsos';
            SkinTone = 3;
            Tags[0] = 'Formal';
            break;

        case 88:
            Tex = Texture'Jacket4_MaleSuit_Skin4';
            Gender = 'Male';
            Age = '';
            Category = 'SuitTorsos';
            SkinTone = 4;
            Tags[0] = 'Formal';
            break;


        // JumpsuitTorsos
        case 89:
            Tex = Texture'TankTop1_MaleJumpsuit_Skin1';
            Gender = 'Male';
            Age = '';
            Category = 'JumpsuitTorsos';
            SkinTone = 1;
            Tags[0] = 'Guerilla';
            Tags[1] = 'Tech';
            break;

        case 90:
            Tex = Texture'GunHolster1_MaleJumpsuit_Skin1';
            Gender = 'Male';
            Age = '';
            Category = 'JumpsuitTorsos';
            SkinTone = 1;
            Tags[0] = 'Guerilla';
            break;

        case 91:
            Tex = Texture'Sweater1_MaleJumpsuit_Skin1';
            Gender = 'Male';
            Age = '';
            Category = 'JumpsuitTorsos';
            SkinTone = 1;
            Tags[0] = 'Military';
            Tags[1] = 'Guerilla';
            break;

        case 92:
            Tex = Texture'BulletProofVest1_MaleJumpsuit_Skin1';
            Gender = 'Male';
            Age = '';
            Category = 'JumpsuitTorsos';
            SkinTone = 1;
            Tags[0] = 'Police';
            break;

        case 93:
            Tex = Texture'TankTop2_MaleJumpsuit_Skin2';
            Gender = 'Male';
            Age = '';
            Category = 'JumpsuitTorsos';
            SkinTone = 2;
            Tags[0] = 'Guerilla';
            Tags[1] = 'Tech';
            break;

        case 94:
            Tex = Texture'GunHolster2_MaleJumpsuit_Skin2';
            Gender = 'Male';
            Age = '';
            Category = 'JumpsuitTorsos';
            SkinTone = 2;
            Tags[0] = 'Guerilla';
            break;

        case 95:
            Tex = Texture'Sweater2_MaleJumpsuit_Skin2';
            Gender = 'Male';
            Age = '';
            Category = 'JumpsuitTorsos';
            SkinTone = 2;
            Tags[0] = 'Military';
            Tags[1] = 'Guerilla';
            break;

        case 96:
            Tex = Texture'BulletProofVest2_MaleJumpsuit_Skin2';
            Gender = 'Male';
            Age = '';
            Category = 'JumpsuitTorsos';
            SkinTone = 2;
            Tags[0] = 'Police';
            break;

        case 97:
            Tex = Texture'TankTop3_MaleJumpsuit_Skin3';
            Gender = 'Male';
            Age = '';
            Category = 'JumpsuitTorsos';
            SkinTone = 3;
            Tags[0] = 'Guerilla';
            Tags[1] = 'Tech';
            break;

        case 98:
            Tex = Texture'GunHolster3_MaleJumpsuit_Skin3';
            Gender = 'Male';
            Age = '';
            Category = 'JumpsuitTorsos';
            SkinTone = 3;
            Tags[0] = 'Guerilla';
            break;

        case 99:
            Tex = Texture'Sweater3_MaleJumpsuit_Skin3';
            Gender = 'Male';
            Age = '';
            Category = 'JumpsuitTorsos';
            SkinTone = 3;
            Tags[0] = 'Military';
            Tags[1] = 'Guerilla';
            break;

        case 100:
            Tex = Texture'BulletProofVest3_MaleJumpsuit_Skin3';
            Gender = 'Male';
            Age = '';
            Category = 'JumpsuitTorsos';
            SkinTone = 3;
            Tags[0] = 'Police';
            break;

        case 101:
            Tex = Texture'TankTop4_MaleJumpsuit_Skin4';
            Gender = 'Male';
            Age = '';
            Category = 'JumpsuitTorsos';
            SkinTone = 4;
            Tags[0] = 'Guerilla';
            Tags[1] = 'Tech';
            break;

        case 102:
            Tex = Texture'GunHolster4_MaleJumpsuit_Skin4';
            Gender = 'Male';
            Age = '';
            Category = 'JumpsuitTorsos';
            SkinTone = 4;
            Tags[0] = 'Guerilla';
            break;

        case 103:
            Tex = Texture'Sweater4_MaleJumpsuit_Skin4';
            Gender = 'Male';
            Age = '';
            Category = 'JumpsuitTorsos';
            SkinTone = 4;
            Tags[0] = 'Military';
            Tags[1] = 'Guerilla';
            break;

        case 104:
            Tex = Texture'BulletProofVest4_MaleJumpsuit_Skin4';
            Gender = 'Male';
            Age = '';
            Category = 'JumpsuitTorsos';
            SkinTone = 4;
            Tags[0] = 'Police';
            break;

        case 105:
            Tex = Texture'ArmorSweater1_MaleJumpsuit';
            Gender = 'Male';
            Age = '';
            Category = 'JumpsuitTorsos';
            Tags[0] = 'Military';
            Tags[1] = 'Guerilla';
            break;

        case 106:
            Tex = Texture'ArmorSweater2_MaleJumpsuit';
            Gender = 'Male';
            Age = '';
            Category = 'JumpsuitTorsos';
            Tags[0] = 'Military';
            Tags[1] = 'Guerilla';
            break;

        case 107:
            Tex = Texture'ArmorSweater3_MaleJumpsuit';
            Gender = 'Male';
            Age = '';
            Category = 'JumpsuitTorsos';
            Tags[0] = 'Military';
            Tags[1] = 'Guerilla';
            break;

        case 108:
            Tex = Texture'ArmorSweater4_MaleJumpsuit';
            Gender = 'Male';
            Age = '';
            Category = 'JumpsuitTorsos';
            Tags[0] = 'Military';
            Tags[1] = 'Guerilla';
            break;


        // FatTorsos
        case 109:
            Tex = Texture'TankTop1_MaleFat_Skin1';
            Gender = 'Male';
            Age = '';
            Category = 'FatTorsos';
            SkinTone = 1;
            Tags[0] = 'Normal';
            Tags[1] = 'Bum';
            break;

        case 110:
            Tex = Texture'DressShirt1_MaleFat_Skin1';
            Gender = 'Male';
            Age = '';
            Category = 'FatTorsos';
            SkinTone = 1;
            Tags[0] = 'Formal';
            break;

        case 111:
            Tex = Texture'TankTop2_MaleFat_Skin2';
            Gender = 'Male';
            Age = '';
            Category = 'FatTorsos';
            SkinTone = 2;
            Tags[0] = 'Normal';
            Tags[1] = 'Bum';
            break;

        case 112:
            Tex = Texture'DressShirt2_MaleFat_Skin2';
            Gender = 'Male';
            Age = '';
            Category = 'FatTorsos';
            SkinTone = 2;
            Tags[0] = 'Formal';
            break;

        case 113:
            Tex = Texture'TankTop3_MaleFat_Skin3';
            Gender = 'Male';
            Age = '';
            Category = 'FatTorsos';
            SkinTone = 3;
            Tags[0] = 'Normal';
            Tags[1] = 'Bum';
            break;

        case 114:
            Tex = Texture'DressShirt3_MaleFat_Skin3';
            Gender = 'Male';
            Age = '';
            Category = 'FatTorsos';
            SkinTone = 3;
            Tags[0] = 'Formal';
            break;

        case 115:
            Tex = Texture'TankTop4_MaleFat_Skin4';
            Gender = 'Male';
            Age = '';
            Category = 'FatTorsos';
            SkinTone = 4;
            Tags[0] = 'Normal';
            Tags[1] = 'Bum';
            break;

        case 116:
            Tex = Texture'DressShirt4_MaleFat_Skin4';
            Gender = 'Male';
            Age = '';
            Category = 'FatTorsos';
            SkinTone = 4;
            Tags[0] = 'Formal';
            break;


        // Torsos
        case 117:
            Tex = Texture'TankTop1_Male_Skin1';
            Gender = 'Male';
            Age = '';
            Category = 'Torsos';
            SkinTone = 1;
            Tags[0] = 'Normal';
            Tags[1] = 'Bum';
            break;

        case 118:
            Tex = Texture'DressShirt1_Male_Skin1';
            Gender = 'Male';
            Age = '';
            Category = 'Torsos';
            SkinTone = 1;
            Tags[0] = 'Normal';
            break;

        case 119:
            Tex = Texture'TankTop2_Male_Skin2';
            Gender = 'Male';
            Age = '';
            Category = 'Torsos';
            SkinTone = 2;
            Tags[0] = 'Normal';
            Tags[1] = 'Bum';
            break;

        case 120:
            Tex = Texture'DressShirt2_Male_Skin2';
            Gender = 'Male';
            Age = '';
            Category = 'Torsos';
            SkinTone = 2;
            Tags[0] = 'Normal';
            break;

        case 121:
            Tex = Texture'TankTop3_Male_Skin3';
            Gender = 'Male';
            Age = '';
            Category = 'Torsos';
            SkinTone = 3;
            Tags[0] = 'Normal';
            Tags[1] = 'Bum';
            break;

        case 122:
            Tex = Texture'DressShirt3_Male_Skin3';
            Gender = 'Male';
            Age = '';
            Category = 'Torsos';
            SkinTone = 3;
            Tags[0] = 'Normal';
            break;

        case 123:
            Tex = Texture'TankTop4_Male_Skin4';
            Gender = 'Male';
            Age = '';
            Category = 'Torsos';
            SkinTone = 4;
            Tags[0] = 'Normal';
            Tags[1] = 'Bum';
            break;

        case 124:
            Tex = Texture'DressShirt4_Male_Skin4';
            Gender = 'Male';
            Age = '';
            Category = 'Torsos';
            SkinTone = 4;
            Tags[0] = 'Normal';
            break;


        // Coats
        case 125:
            Tex = Texture'PlainCoat1_Male';
            Gender = 'Male';
            Age = '';
            Category = 'Coats';
            Tags[0] = 'Science';
            break;

        case 126:
            Tex = Texture'PlainCoat2_Male';
            Gender = 'Male';
            Age = '';
            Category = 'Coats';
            Tags[0] = 'Bum';
            break;

        case 127:
            Tex = Texture'LeatherJacket1_Male';
            Gender = 'Male';
            Age = '';
            Category = 'Coats';
            Tags[0] = 'Normal';
            break;

        case 128:
            Tex = Texture'LeatherJacket2_Male';
            Gender = 'Male';
            Age = '';
            Category = 'Coats';
            Tags[0] = 'Normal';
            break;


        // FatCoats
        case 129:
            Tex = Texture'PlainCoat1_MaleFat';
            Gender = 'Male';
            Age = '';
            Category = 'FatCoats';
            Tags[0] = 'Science';
            break;

        case 130:
            Tex = Texture'PlainCoat2_MaleFat';
            Gender = 'Male';
            Age = '';
            Category = 'FatCoats';
            Tags[0] = 'Bum';
            break;

        case 131:
            Tex = Texture'LeatherJacket1_MaleFat';
            Gender = 'Male';
            Age = '';
            Category = 'FatCoats';
            Tags[0] = 'Normal';
            break;

        case 132:
            Tex = Texture'LeatherJacket2_MaleFat';
            Gender = 'Male';
            Age = '';
            Category = 'FatCoats';
            Tags[0] = 'Normal';
            break;

        case 133:
            Tex = Texture'SuitJacket1_MaleFat';
            Gender = 'Male';
            Age = '';
            Category = 'FatCoats';
            Tags[0] = 'Formal';
            break;

        case 134:
            Tex = Texture'SuitJacket2_MaleFat';
            Gender = 'Male';
            Age = '';
            Category = 'FatCoats';
            Tags[0] = 'Formal';
            break;


        // ShirtFronts
        case 135:
            Tex = Texture'TShirtFront1_Male_Skin1';
            Gender = 'Male';
            Age = '';
            Category = 'ShirtFronts';
            SkinTone = 1;
            Tags[0] = 'Normal';
            break;

        case 136:
            Tex = Texture'TShirtFront2_Male_Skin2';
            Gender = 'Male';
            Age = '';
            Category = 'ShirtFronts';
            SkinTone = 2;
            Tags[0] = 'Normal';
            break;

        case 137:
            Tex = Texture'TShirtFront3_Male_Skin3';
            Gender = 'Male';
            Age = '';
            Category = 'ShirtFronts';
            SkinTone = 3;
            Tags[0] = 'Normal';
            break;

        case 138:
            Tex = Texture'TShirtFront4_Male_Skin4';
            Gender = 'Male';
            Age = '';
            Category = 'ShirtFronts';
            SkinTone = 4;
            Tags[0] = 'Normal';
            break;

        case 139:
            Tex = Texture'TornTShirtFront1_Male_Skin1';
            Gender = 'Male';
            Age = '';
            Category = 'ShirtFronts';
            SkinTone = 1;
            Tags[0] = 'Bum';
            break;

        case 140:
            Tex = Texture'TornTShirtFront2_Male_Skin2';
            Gender = 'Male';
            Age = '';
            Category = 'ShirtFronts';
            SkinTone = 2;
            Tags[0] = 'Bum';
            break;

        case 141:
            Tex = Texture'TornTShirtFront3_Male_Skin3';
            Gender = 'Male';
            Age = '';
            Category = 'ShirtFronts';
            SkinTone = 3;
            Tags[0] = 'Bum';
            break;

        case 142:
            Tex = Texture'TornTShirtFront4_Male_Skin4';
            Gender = 'Male';
            Age = '';
            Category = 'ShirtFronts';
            SkinTone = 4;
            Tags[0] = 'Bum';
            break;

        case 143:
            Tex = Texture'DressShirtFront1_Male';
            Gender = 'Male';
            Age = '';
            Category = 'ShirtFronts';
            Tags[0] = 'Normal';
            break;

        case 144:
            Tex = Texture'DressShirtFront2_Male';
            Gender = 'Male';
            Age = '';
            Category = 'ShirtFronts';
            Tags[0] = 'Normal';
            break;

        case 145:
            Tex = Texture'DressShirtFront3_Male';
            Gender = 'Male';
            Age = '';
            Category = 'ShirtFronts';
            Tags[0] = 'Normal';
            break;

        case 146:
            Tex = Texture'DressShirtFront4_Male';
            Gender = 'Male';
            Age = '';
            Category = 'ShirtFronts';
            Tags[0] = 'Normal';
            break;

        case 147:
            Tex = Texture'DressShirtTieFront1_Male';
            Gender = 'Male';
            Age = '';
            Category = 'ShirtFronts';
            Tags[0] = 'Science';
            break;

        case 148:
            Tex = Texture'DressShirtTieFront2_Male';
            Gender = 'Male';
            Age = '';
            Category = 'ShirtFronts';
            Tags[0] = 'Science';
            break;

        case 149:
            Tex = Texture'DressShirtTieFront3_Male';
            Gender = 'Male';
            Age = '';
            Category = 'ShirtFronts';
            Tags[0] = 'Science';
            break;

        case 150:
            Tex = Texture'DressShirtTieFront4_Male';
            Gender = 'Male';
            Age = '';
            Category = 'ShirtFronts';
            Tags[0] = 'Science';
            break;


        // Helmets
        case 151:
            Tex = Texture'Goggles';
            Gender = 'Male';
            Age = '';
            Category = 'Helmets';
            break;

        case 152:
            Tex = Texture'Goggles_1';
            Gender = 'Male';
            Age = '';
            Category = 'Helmets';
            break;

        case 153:
            Tex = Texture'Goggles_2';
            Gender = 'Male';
            Age = '';
            Category = 'Helmets';
            break;

        // ----------------------------------------
        // Child
        // ----------------------------------------
        // Heads
        case 154:
            Tex = Texture'Head_Child1_Skin1';
            Gender = 'Child';
            Age = '';
            Category = 'Heads';
            SkinTone = 1;
            break;

        case 155:
            Tex = Texture'Head_Child1_Skin2';
            Gender = 'Child';
            Age = '';
            Category = 'Heads';
            SkinTone = 2;
            break;

        case 156:
            Tex = Texture'Head_Child1_Skin3';
            Gender = 'Child';
            Age = '';
            Category = 'Heads';
            SkinTone = 3;
            break;

        case 157:
            Tex = Texture'Head_Child1_Skin4';
            Gender = 'Child';
            Age = '';
            Category = 'Heads';
            SkinTone = 4;
            break;


        // Torsos
        case 158:
            Tex = Texture'TankTop1_Child_Skin1';
            Gender = 'Child';
            Age = '';
            Category = 'Torsos';
            SkinTone = 1;
            Tags[0] = 'Normal';
            Tags[1] = 'Bum';
            break;

        case 159:
            Tex = Texture'TankTop2_Child_Skin2';
            Gender = 'Child';
            Age = '';
            Category = 'Torsos';
            SkinTone = 2;
            Tags[0] = 'Normal';
            Tags[1] = 'Bum';
            break;

        case 160:
            Tex = Texture'TankTop3_Child_Skin3';
            Gender = 'Child';
            Age = '';
            Category = 'Torsos';
            SkinTone = 3;
            Tags[0] = 'Normal';
            Tags[1] = 'Bum';
            break;

        case 161:
            Tex = Texture'TankTop4_Child_Skin4';
            Gender = 'Child';
            Age = '';
            Category = 'Torsos';
            SkinTone = 4;
            Tags[0] = 'Normal';
            Tags[1] = 'Bum';
            break;

        // ----------------------------------------
        // Common
        // ----------------------------------------
        // Pants
        case 162:
            Tex = Texture'Jeans1';
            Gender = 'Common';
            Age = '';
            Category = 'Pants';
            Tags[0] = 'Normal';
            Tags[1] = 'Science';
            break;

        case 163:
            Tex = Texture'Jeans2';
            Gender = 'Common';
            Age = '';
            Category = 'Pants';
            Tags[0] = 'Normal';
            Tags[1] = 'Science';
            break;

        case 164:
            Tex = Texture'Jeans3';
            Gender = 'Common';
            Age = '';
            Category = 'Pants';
            Tags[0] = 'Normal';
            Tags[1] = 'Science';
            break;

        case 165:
            Tex = Texture'Jeans4';
            Gender = 'Common';
            Age = '';
            Category = 'Pants';
            Tags[0] = 'Normal';
            Tags[1] = 'Science';
            break;

        case 166:
            Tex = Texture'TacticalPants1';
            Gender = 'Common';
            Age = '';
            Category = 'Pants';
            Tags[0] = 'Guerilla';
            Tags[1] = 'Military';
            Tags[2] = 'Police';
            break;

        case 167:
            Tex = Texture'TacticalPants2';
            Gender = 'Common';
            Age = '';
            Category = 'Pants';
            Tags[0] = 'Guerilla';
            Tags[1] = 'Military';
            Tags[2] = 'Police';
            break;

        case 168:
            Tex = Texture'TacticalPants3';
            Gender = 'Common';
            Age = '';
            Category = 'Pants';
            Tags[0] = 'Guerilla';
            Tags[1] = 'Military';
            Tags[2] = 'Police';
            break;

        case 169:
            Tex = Texture'TacticalPants4';
            Gender = 'Common';
            Age = '';
            Category = 'Pants';
            Tags[0] = 'Guerilla';
            Tags[1] = 'Military';
            Tags[2] = 'Police';
            break;

        case 170:
            Tex = Texture'TechPants';
            Gender = 'Common';
            Age = '';
            Category = 'Pants';
            Tags[0] = 'Tech';
            break;

        case 171:
            Tex = Texture'TornJeans1_Skin1';
            Gender = 'Common';
            Age = '';
            Category = 'Pants';
            SkinTone = 1;
            Tags[0] = 'Bum';
            break;

        case 172:
            Tex = Texture'SuitPants1_Skin1';
            Gender = 'Common';
            Age = '';
            Category = 'Pants';
            SkinTone = 1;
            Tags[0] = 'Formal';
            break;

        case 173:
            Tex = Texture'TornJeans2_Skin2';
            Gender = 'Common';
            Age = '';
            Category = 'Pants';
            SkinTone = 2;
            Tags[0] = 'Bum';
            break;

        case 174:
            Tex = Texture'SuitPants2_Skin2';
            Gender = 'Common';
            Age = '';
            Category = 'Pants';
            SkinTone = 2;
            Tags[0] = 'Formal';
            break;

        case 175:
            Tex = Texture'TornJeans3_Skin3';
            Gender = 'Common';
            Age = '';
            Category = 'Pants';
            SkinTone = 3;
            Tags[0] = 'Bum';
            break;

        case 176:
            Tex = Texture'SuitPants3_Skin3';
            Gender = 'Common';
            Age = '';
            Category = 'Pants';
            SkinTone = 3;
            Tags[0] = 'Formal';
            break;

        case 177:
            Tex = Texture'TornJeans4_Skin4';
            Gender = 'Common';
            Age = '';
            Category = 'Pants';
            SkinTone = 4;
            Tags[0] = 'Bum';
            break;

        case 178:
            Tex = Texture'SuitPants4_Skin4';
            Gender = 'Common';
            Age = '';
            Category = 'Pants';
            SkinTone = 4;
            Tags[0] = 'Formal';
            break;


        // Lenses
        case 179:
            Tex = Texture'Lenses1';
            Gender = 'Common';
            Age = '';
            Category = 'Lenses';
            break;

        case 180:
            Tex = Texture'Lenses2';
            Gender = 'Common';
            Age = '';
            Category = 'Lenses';
            break;


        // Frames
        case 181:
            Tex = Texture'Frames1';
            Gender = 'Common';
            Age = '';
            Category = 'Frames';
            break;

        case 182:
            Tex = Texture'Frames5';
            Gender = 'Common';
            Age = '';
            Category = 'Frames';
            break;


    }
}

// ---------------------
// Gets a random Texture
// ---------------------
static function Texture GetRandomTexture(name ReqStyleTag, name ReqGender, name ReqCategory, int ReqSkinTone, name ReqAge)
{
    local int i;
    local int NumValidTextures;
    local Texture ValidTextures[183];
    local Texture Tex;
    local int SkinTone;
    local name Age, Gender, Category, Tags[10];

    // Find valid textures
    for(i = 0; i < 183; i++)
    {
        GetTextureInfo(i, Tex, Gender, Category, Tags, SkinTone, Age);

        // Skip null textures
        if(Tex == None)
            continue;

        // Skip if the gender or category doesn't match
        if(Gender != ReqGender || Category != ReqCategory)
            continue;

        // Skip if the skin tone is specified and doesn't match
        if(ReqSkinTone > 0 && SkinTone > 0 && SkinTone != ReqSkinTone)
            continue;
        
        // Skip if the age is specified and doesn't match
        if(ReqAge != '' && Age != '' && Age != ReqAge)
            continue;

        // Skip if this texture has tags and they're not enabled
        if(HasTags(Tags) && !AreAnyTagsEnabled(Tags, ReqStyleTag))
            continue;
  
        ValidTextures[NumValidTextures] = Tex; 
        NumValidTextures++; 
    }
    
    // Oops! No valid textures. Return an empty texture.
    if(NumValidTextures < 1)
        return Texture'Engine.DefaultTexture';

    return ValidTextures[Rand(NumValidTextures)];
}

// ------------------------------------------
// Gets whether a TextureInfo has a given tag
// ------------------------------------------
static function bool HasTag(name Tags[10], name TagName)
{
    local int i;

    for(i = 0; i < 10; i++)
        if(Tags[i] == TagName)
            return True;

    return False;
}

// -------------------------------------------------
// Gets whether a TextureInfo has any tags specified
// -------------------------------------------------
static function bool HasTags(name Tags[10])
{
    local int i;

    for(i = 0; i < 10; i++)
        if(Tags[i] != '')
            return True;

    return False;
}

// -----------------------------------------
// Gets whether any tag in a list is enabled
// -----------------------------------------
static function bool AreAnyTagsEnabled(name Tags[10], name StyleTag)
{
    local int i;

    for(i = 0; i < 10; i++)
        if(Tags[i] == StyleTag)
            return True;
    
    return False;
}
