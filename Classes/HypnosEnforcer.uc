class HypnosEnforcer extends ScriptedPawnExtended;

// BEGIN UE1

function Absorb(ScriptedPawn aScriptedPawn)
{
    local int i;
    local Vector SpawnLocation;

    InitializePawn();

    if(aScriptedPawn == None)
        return;

    SpawnLocation = aScriptedPawn.Location;    
    
    if(aScriptedPawn.Orders == 'Crouching' || aScriptedPawn.Orders == 'Sitting')
        SetOrders('Standing', '', True);
    else
        SetOrders(aScriptedPawn.Orders, aScriptedPawn.OrderTag, True);

    SetRotation(aScriptedPawn.Rotation);
    DesiredRotation = aScriptedPawn.DesiredRotation;

    ChangeAlly('Player', -1.0, True);
  
    aScriptedPawn.Destroy();
    
    while(i < 100 && !SetLocation(SpawnLocation))
    {
        i++;
        SpawnLocation += Vect(0, 0, 10);
    }
   
    SpawnEffect();
}

function SpawnEffect()
{
    local SphereEffect aSphereEffect;

    aSphereEffect = Spawn(class'SphereEffect',,, Location);
    
    if(aSphereEffect != None)
        aSphereEffect.Size = 6.0;
                
    PlaySound(Sound'DeusExSounds.Weapons.EMPGrenadeExplode', SLOT_Misc);
}

function Carcass SpawnCarcass()
{
    SpawnEffect();

    return None;
}



// END UE1

defaultproperties
{
    bFearHacking=False
    bFearWeapon=False
    bCanCrouch=False
    bFearShot=False
    bFearInjury=False
    bFearIndirectInjury=False
    bFearCarcass=False
    bFearDistress=False
    bFearAlarm=False
    bFearProjectiles=False 
    Alliance='Hypnos'
    AmbientGlow=255
    BindName="HypnosEnforcer"
    bUnlit=True
    FamiliarName="Hypnos Enforcer"
    UnfamiliarName="Hypnos Enforcer"
    WalkingSpeed=0.180000
    GroundSpeed=300.000000
    Mesh=LodMesh'DeusExCharacters.GM_ScaryTroop'
    MultiSkins(0)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(1)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(2)=FireTexture'Effects.Electricity.Nano_SFX'
    MultiSkins(3)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(4)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(5)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(6)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(7)=Texture'DeusExItems.Skins.PinkMaskTex'
    InitialInventory(0)=(Inventory=Class'MachineGod.WeaponHypnosEnforcerRocket')
    InitialInventory(1)=(Inventory=Class'MachineGod.AmmoHypnosEnforcerRocket',Count=666)
    InitialInventory(2)=(Inventory=None)
    InitialInventory(3)=(Inventory=None)
    HitSound1=Sound'MachineGod.NPC.HypnosEnforcerPainSmall'
    HitSound2=Sound'MachineGod.NPC.HypnosEnforcerPainMedium'
    Die=Sound'MachineGod.NPC.HypnosEnforcerDeath'
}
