//=============================================================================
// MoveTrigger - Moves an actor to a new location
//=============================================================================
class MoveTrigger extends Trigger;

var() bool bThisLocation;
var() Name LocationTag;
var() Vector NewLocation;
var() Rotator NewRotation;

defaultproperties
{
    bDirectional=True
}

// BEGIN UE1

function Touch(Actor Other)
{
    if(!IsRelevant(Other))
        return;

    Trigger(Other, Pawn(Other));
    
    if(bTriggerOnceOnly)
        SetCollision(False);
}

function Trigger(Actor Other, Pawn Instigator)
{
    local Actor A;
    local Actor Target;

    if (Event != '')
    {
        foreach AllActors(class 'Actor', A, Event)
        {
            if(bThisLocation)
            {
                MoveActor(A, Location, Rotation);
            }
            else if(LocationTag != '')
            {
                foreach AllActors(class 'Actor', Target, LocationTag)
                {
                    MoveActor(A, Target.Location, Target.Rotation);
                }
            }
            else
            {
                MoveActor(A, NewLocation, NewRotation);
            }
        }
    }

    Super.Trigger(Other, Instigator);
}

// --------------------
// Moves the actor, and tries to place it nearby if it fails
// --------------------
function MoveActor(Actor A, Vector Loc, Rotator Rot)
{
    local Rotator HelpRotator;
    local int HelpDistance;
    local int LoopCount;

    HelpRotator = Rot;
    HelpDistance = 16;

    if(!A.SetLocation(Loc))
    {
        while (LoopCount < 16 && !A.SetLocation(Loc + Normal(Vector(HelpRotator)) * HelpDistance))
        {
            HelpRotator.Yaw += 8192;
            HelpDistance += 16;
            LoopCount++;
        }
    }
    
    A.SetRotation(Rot);
}

// END UE1
