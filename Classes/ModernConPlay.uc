class ModernConPlay extends ConPlay;

var ConEventMoveCamera LastCameraEvent;
var bool bPlayerWasHidden;
var float MinPawnDistance, MinPlayerDistance;
var int LastCameraPosition;

// BEGIN UE1

// ---------------------
// Starts a conversation
//
// Changes:
// - Fade before play
// ---------------------
function bool StartConversation(DeusExPlayer NewPlayer, optional Actor NewInvokeActor, optional bool bNewForcePlay)
{
    if(Con == None || NewPlayer == None)
        return False;

    if(NewPlayer.IsInState('Dying'))
        return False;

    // Keep a pointer to the player and invoking actor
    Player = NewPlayer;
    bPlayerWasHidden = Player.bHidden;

    if (NewInvokeActor != None) 
        InvokeActor = NewInvokeActor;
    else
        InvokeActor = StartActor;

    // Bind the conversation events
    Con.BindEvents(ConActorsBound, InvokeActor);

    // Check to see if the conversation has multiple owners, in which case we 
    // want to rebind all the events with this owner.  This allows conversations
    // to be shared by more than one owner.
    if ((Con.ownerRefCount > 1) && (InvokeActor != None))
        Con.BindActorEvents(InvokeActor);

    // Check to see if all the actors are on the level.
    // Don't check this for InfoLink conversations, since oftentimes
    // the person speaking via InfoLink *won't* be on the map.
    if ((!bForcePlay) && (!con.bDataLinkCon) && (!con.CheckActors()))
        return False;

    // Cinematics and first person convos are started right away
    if(Con.bFirstPerson || bForcePlay || bPlayerWasHidden)
    {
        PostStartConversation();
        GotoState('PlayEvent');
    }
    // Third person convos get a fade in
    else
    {
        GoToState('FadeBeforePlay');
    }
    
    return True;
}

// ----------------------------------------
// Ends the actor states
//
// Changes:
// - Include bFirstPerson+bForcePlay convos
// ----------------------------------------
function EndConActorStates()
{
    local int conActorIndex;

    // Originally, this skipped bForcePlay, but we want to
    // keep it in if it's a bFirstPerson convo
    if (!bForcePlay || Con.bFirstPerson)
    {
        for(conActorIndex=conActorCount-1; conActorIndex>=0; conActorIndex--)
        {
            if (ConActors[conActorIndex] != None)
            {
                ConActors[conActorIndex].EndConversation();
                ConActors[conActorIndex] = None;
                conActorCount--;
            }
        }

        // Make sure the invoking actor, if a DeusExDecoration or ScriptedPawn,
        // is not stuck in the conversation state
        if (ScriptedPawn(invokeActor) != None )
            ScriptedPawn(invokeActor).EndConversation();
        else if (DeusExDecoration(invokeActor) != None )
            DeusExDecoration(invokeActor).EndConversation();
    }
}

// --------------------------------------
// Turns actors towards each other
//
// Changes:
// - Make all characters face the speaker
// --------------------------------------
function TurnSpeakingActors(Actor aSpeaker, Actor aSpeakingTo)
{
    local int i;

    if(bForcePlay)
    {
        Super.TurnSpeakingActors(aSpeaker, aSpeakingTo);
    }
    else
    {
        TurnActor(aSpeaker, aSpeakingTo);
        TurnActor(aSpeakingTo, aSpeaker);

        // Try to turn other actors in the convo towards the speaker
        for(i = 0; i < ConActorCount; i++)
        {
            if(ConActors[i] == None || ConActors[i] == aSpeaker)
                continue;

            TurnActor(ConActors[i], aSpeaker);
        }
    }
}

// ----------------------------------------------------------
// Displays the speech and awaits input
// 
// Changes:
// - Allow text with no audio to display before choice events
// ----------------------------------------------------------
function EEventAction SetupEventSpeechPost(ConEventSpeech Event, out string NextLabel)
{
    local EEventAction NextAction;
    local bool bHaveSpeechAudio, bIsPassiveConvo;

    NextAction = Super.SetupEventSpeechPost(Event, NextLabel); 

    bHaveSpeechAudio = Event.ConSpeech.SoundID != -1;
    bIsPassiveConvo = PlayMode == PM_PASSIVE || DisplayMode == DM_FirstPerson;

    if(!bHaveSpeechAudio && !bIsPassiveConvo)
        NextAction = EA_WaitForInput;

    return NextAction;
}

// ---------------------------------------------------------------------
// Fires a specified trigger (or the mission script, if none were found)
// ---------------------------------------------------------------------
function EEventAction SetupEventTrigger( ConEventTrigger Event, out string NextLabel)
{
    local Actor aActor;
    local bool bFound;
    local MissionScriptExtended aMissionScript;

    // Loop through all the actors, firing a trigger for each
    foreach AllActors(class'Actor', aActor, Event.TriggerTag)
    {
        aActor.Trigger(LastActor, Pawn(LastActor));
        bFound = True;
    }

    // If not found, look for the mission script to trigger an event
    if(!bFound)
    {
        foreach AllActors(class'MissionScriptExtended', aMissionScript)
        {
            aMissionScript.QueueEvent(Event.TriggerTag, LastActor, Pawn(LastActor));
        }
    }

    return EA_NextEvent;
}

// -----------------------------------------------
// Teleports required actors into the conversation
// -----------------------------------------------
function FixActorDistances()
{
    local ConversationPoint aConvoSpot;
    local ConEvent Event;
    local ConEventSpeech EventSpeech;
    local ScriptedPawn aScriptedPawn;
    local bool bFoundConvoSpot;

    // First look for designated spots
    foreach AllActors(class'ConversationPoint', aConvoSpot)
    {
        if(
            aConvoSpot.Conversation != Con.ConName ||
            aConvoSpot.SpeakerTag == ''
        )
            continue;
        
        if(aConvoSpot.SpeakerTag == 'Player' || aConvoSpot.SpeakerTag == 'JCDenton')
        {
            MoveActor(Player, aConvoSpot); 
        }
        else 
        {
            foreach AllActors(class'ScriptedPawn', aScriptedPawn, aConvoSpot.SpeakerTag)
            {
                MoveActor(aScriptedPawn, aConvoSpot);
            }
        }

        bFoundConvoSpot = True;
    }

    if(bFoundConvoSpot)
        return;

    // Didn't find designated spots, proceed with automated solution
    Event = Con.EventList;
    
    while(Event != None)
    {
        EventSpeech = ConEventSpeech(Event);
        Event = Event.NextEvent;

        if(EventSpeech == None)
            continue;

        foreach AllActors(class'ScriptedPawn', aScriptedPawn)
        {
            // Skip this pawn if:
            // - They don't belong in this convo, or...
            // - ...they're the StartActor, or...
            // - ...they're translucent (hologram), or...
            // - ...they're within range already
            if(
                (
                    aScriptedPawn.BindName != EventSpeech.SpeakerName &&
                    aScriptedPawn.BindName != EventSpeech.SpeakingToName
                ) ||
                aScriptedPawn == StartActor ||
                aScriptedPawn.BindName == StartActor.BindName ||
                aScriptedPawn.Style == STY_Translucent ||
                VSize(aScriptedPawn.Location - Player.Location) < MinPawnDistance
            )
                continue;

            // Move the actor next to the player
            MoveActor(aScriptedPawn, Player);
        }
    }
}

// ----------------------------------------------
// Gets called after the conversation has started
// ----------------------------------------------
function PostStartConversation()
{
    local DeusExLevelInfo aDeusExLevelInfo;
    local int i;

    // Save the conversation radius
    SaveRadiusDistance = Con.RadiusDistance;

    // Some preparation for third person convos
    if(!Con.bFirstPerson && !bForcePlay)
    {
        Player.AugmentationSystem.DeactivateAll();
   
        // Make sure player is in a normal state.
        // This is relevant if a conversation is started
        // right after an interpolation sequence.
        if(bPlayerWasHidden)
        {
            Player.bHidden = False;
            Player.bDetectable = True;
            Player.Visibility = 1;
            Player.BaseEyeHeight = Player.Default.BaseEyeHeight;
            Player.bCollideWorld = True;
            Player.SetCollision(True, True, True);
            
            if(Player.Shadow == None)
                Player.CreateShadow();

        }
        // Player invoked this convo manually, do some sanity adjustments
        else
        {
            // Player is too close, back off a bit
            if(VSize(Player.Location - StartActor.Location) < MinPlayerDistance)
                Player.SetLocation(StartActor.Location + Normal(Player.Location - StartActor.Location) * MinPlayerDistance);
        }
    }

    // Save the mission number and location
    foreach AllActors(class'DeusExLevelInfo', aDeusExLevelInfo)
    {
        if (aDeusExLevelInfo != None)
        {
            missionNumber   = aDeusExLevelInfo.missionNumber;
            missionLocation = aDeusExLevelInfo.MissionLocation;
            break;
        }
    }

    // Initialize Windowing System
    rootWindow = DeusExRootWindow(player.rootWindow);

    bConversationStarted = True;

    bEndingConversation = False;

    // Check to see if this conversation uses random camera placement
    randomCamera = con.bRandomCamera;

    // Based on whether we're in first-person or third-person mode, 
    // we need to derive our conversation window differently.  First-person
    // conversation mode is non-interactive and only allows speech, for
    // the most part.

    if ( con.bFirstPerson )
    {
        displayMode = DM_FirstPerson;

        conWinFirst = rootWindow.hud.CreateConWindowFirst();

        // Initialize Windowing System
        conWinFirst.conPlay = Self;
    }
    else
    {
        displayMode = DM_ThirdPerson;

        // Hide the hud display if this is a third-person convo
        rootWindow.hud.Hide();

        conWinThird = ModernConWindowActive(rootWindow.NewChild(Class'ModernConWindowActive', False));
        conWinThird.SetForcePlay(bForcePlay);

        // Initialize Windowing System
        conWinThird.conPlay = Self;

        // Align the conversation window
        conWinThird.SetWindowAlignments(HALIGN_Full, VALIGN_Full);
        conWinThird.Show();
        
        // Set up the camera
        if(!bForcePlay)
        {
            CameraInfo = Con.CreateConCamera();
           
            CameraInfo.InitializeCameraValues(Self);

            if(RandomCamera)
                CameraInfo.SetupRandomCameraPosition();
        }
    }

    // Check to see if this is a passive or interactive conversation
    // Passive conversations are beyond the PC's control and do not have
    // things like Choices, etc. in them.

    if ( con.bNonInteractive )
        playMode = PM_Passive;
    else
        playMode = PM_Active;

    // Grab the first event!
    currentEvent = con.eventList;

    // Create a ConHistory object
    if (!bForcePlay)
        SetupHistory(player.GetDisplayName(startActor, True));
}

// ----------------------------------------------------------------------------------------
// Adds/deducts credits from the player
// We're hooking into this to make sure the InvokeActor gets them if the amount is negative
// ----------------------------------------------------------------------------------------
function EEventAction SetupEventAddCredits(ConEventAddCredits Event, out string NextLabel )
{
    local Credits aCredits;

    if(Event.CreditsToAdd < 0 && Pawn(InvokeActor) != None)
    {
        aCredits = Spawn(class'Credits');
        aCredits.NumCredits = -Event.CreditsToAdd;
        aCredits.GiveTo(Pawn(InvokeActor));
    }

    return Super.SetupEventAddCredits(Event, NextLabel);
}

// -------------------------------------------------
// Defines the actors in the shot
//
// Changes:
// - Default to shoulder cam instead of a random one
// -------------------------------------------------
function SetCameraActors()
{
    // Totally ignore this if "bForcePlay" is set to true
    if (bForcePlay)
        return;

    if ((currentspeaker != None) && (currentSpeakingTo != None))
    {
        // Update the Actors used by the camera
        if(CameraInfo != None)
        {
            CameraInfo.SetActors(CurrentSpeaker, CurrentSpeakingTo);
            bSetupInitialCamera = True;
           
            // We have a camera event, handle it
            if(PendingCameraEvent != LastCameraEvent)
            {
                CameraInfo.SetupCameraFromEvent(PendingCameraEvent);
                LastCameraEvent = PendingCameraEvent;
            } 
            // No camera event speficied, default to shoulder cam
            else
            {
                CameraInfo.ResetFallbackPosition();
                CameraInfo.CameraPosition = CP_ShoulderLeft;
            }
               
            CameraInfo.SetCameraValues();
        }

        // If we haven't setup an initial camera then do so, 
        // because we don't want to play a speech or choice
        // event without the camera first having been placed

        if ((CameraInfo != None) && (!bSetupInitialCamera))
        {
            CameraInfo.SetActors(currentSpeaker, currentSpeakingTo);
            CameraInfo.SetupFallbackCamera();
            bSetupInitialCamera = True;
        }
    }
}

// ---------------------------------------
// Terminates the conversation
//
// Changes:
// - Fade out/in after third person convos
// ---------------------------------------
function TerminateConversation(optional bool bContinueSpeech, optional bool bNoPlayedFlag)
{
    // Save this for when we call Super.TerminateConversation() later
    bSaveContinueSpeech = bContinueSpeech;
    bSaveNoPlayedFlag   = bNoPlayedFlag;

    // Some protection to make sure this doesn't happen 
    // more than once.

    if (!bEndingConversation)
    {
        bEndingConversation = True;

        // Tell the Conversation Window to shut down
        if ((conWinThird != None) && (!bForcePlay))
            GotoState('FadeBeforePostTerminate');
        else
            ConWinFinished();
    }
}

// ------------------------------------------------------------------
// Calculates the camera position
//
// NOTE: This is necessary for using custom camera angle computation.
//       Subclassing ConCamera itself results in crashes.
// ------------------------------------------------------------------
function bool CalculateCameraPosition
(
    out Actor ViewActor, 
    out Vector CameraLocation, 
    out Rotator CameraRotation,
    out float FOV
)
{
    // First check if we can set up a shoulder cam
    if(CalculateShoulderCameraPosition(ViewActor, CameraLocation, CameraRotation, FOV))
        return True;

    // Never mind, just use default cameras
    return CameraInfo.CalculateCameraPosition(ViewActor, CameraLocation, CameraRotation);
}

// ---------------------------------------
// Sets up shoulder cams without fallbacks
// ---------------------------------------
function bool CalculateShoulderCameraPosition
(
    out Actor ViewActor, 
    out Vector CameraLocation, 
    out Rotator CameraRotation,
    out float FOV
)
{
    local Vector Center1, Center2, Focus, Direction; 
    local float HeightModifier, DistanceBehindModifier, DistanceNextToModifier, DistanceBehindSpeaker, DistanceNextToSpeaker;
    local Rotator HelpRotator;
    
    if(
        CameraInfo.FirstActor == None ||
        CameraInfo.SecondActor == None ||
        CameraInfo.FirstActor == CameraInfo.SecondActor ||
        CameraInfo.CameraMode != CT_Speakers || 
        (
            CameraInfo.CameraPosition != CP_ShoulderLeft &&
            CameraInfo.CameraPosition != CP_ShoulderRight
        )
    )
        return False;

    // Modifiers
    HeightModifier = -4;
    DistanceBehindModifier = 12;
    DistanceNextToModifier = 6;

    // Establish focal point
    Center1 = CameraInfo.FirstActor.Location;
    
    if(Pawn(CameraInfo.FirstActor) != None)
        Center1.Z += Pawn(CameraInfo.FirstActor).BaseEyeHeight;
    else if(Decoration(CameraInfo.FirstActor) != None)
        Center1.Z += Decoration(CameraInfo.FirstActor).BaseEyeHeight;
    
    Center1.Z += HeightModifier;

    Center2 = CameraInfo.SecondActor.Location; 
    
    if(Pawn(CameraInfo.SecondActor) != None)
        Center2.Z += Pawn(CameraInfo.SecondActor).BaseEyeHeight;
    else if(Decoration(CameraInfo.SecondActor) != None)
        Center2.Z += Decoration(CameraInfo.SecondActor).BaseEyeHeight;
    
    Center2.Z += HeightModifier;

    Focus = (Center1 + Center2) / 2.0;
    
    // The direction between the actors
    Direction = Normal(Center1 - Center2);

    // The camera's distance from the speaker
    DistanceBehindSpeaker = (CameraInfo.SecondActor.CollisionRadius + DistanceBehindModifier);
    DistanceNextToSpeaker = (CameraInfo.SecondActor.CollisionRadius + DistanceNextToModifier);

    // Place the camera behind the speaker
    CameraLocation = Center2 - Direction * DistanceBehindSpeaker;

    // Shift the camera left/right of the actor being spoken to 
    HelpRotator = Rotator(Direction);
    
    if(CameraInfo.CameraPosition == CP_ShoulderLeft)
        HelpRotator.Yaw += 16384;

    else if(CameraInfo.CameraPosition == CP_ShoulderRight)
        HelpRotator.Yaw -= 16384;

    CameraLocation += Vector(HelpRotator) * DistanceNextToSpeaker;

    // Update the light positions
    CameraInfo.ConLightSpeaker.UpdateLocation(CameraInfo.FirstActor);
    CameraInfo.ConLightSpeakingTo.UpdateLocation(CameraInfo.SecondActor);
   
    // Position the camera
    CameraRotation = Rotator(Focus - CameraLocation);

    // Set the FOV
    FOV = 60;

    return True;
}

// --------------------------------------------------------
// Moves an actor, and tries to place it nearby if it fails
// --------------------------------------------------------
function MoveActor(Actor aFromActor, Actor aToActor, optional bool bQuitOnFail)
{
    local Rotator HelpRotator;
    local float HelpDistance;
    local int LoopCount, MaxLoops;
    local Vector HelpVector;
    local ConCamera TempConCamera;
    local ScriptedPawn aFromPawn;

    if(aToActor.IsA('DeusExPlayer'))
        HelpRotator = DeusExPlayer(aToActor).ViewRotation + Rot(0, 16384, 0);
    else
        HelpRotator = aToActor.Rotation + Rot(0, 16384, 0);

    // If "to" is a conversation point, get the "from" as close as possible
    if(aToActor.IsA('ConversationPoint'))
        HelpDistance = 0;
    else
        HelpDistance = MinPawnDistance * 0.25;

    // Limit to 1 attempt if requested
    if(bQuitOnFail)
        MaxLoops = 1;
    else
        MaxLoops = 100;

    aFromPawn = ScriptedPawn(aFromActor);
    
    // Make the pawn stand
    if(aFromPawn != None)
    {
        if(aFromPawn.bCrouching)
        {
            aFromPawn.bCrouching = False;
            aFromPawn.ResetBasedPawnSize();
        }
        
        aFromPawn.SetOrders('Standing', '', True);
    }

    // Rotate around the "to" actor, starting at its right facing direction,
    // until a valid location has been found
    while(LoopCount < MaxLoops && !aFromActor.SetLocation(aToActor.Location + Normal(Vector(HelpRotator)) * HelpDistance))
    {
        HelpRotator.Yaw += 8192;
        LoopCount++;

        if(HelpRotator.Yaw > aToActor.Rotation.Yaw + 65536)
        {
            HelpRotator.Yaw = aToActor.Rotation.Yaw;
            HelpDistance += 32;
        }
    }

    // Instantly turn the "from" actor according to the "to" actor
    if(aToActor.IsA('ConversationPoint'))
    {
        HelpRotator = aToActor.Rotation;
    }
    else
    {
        HelpVector = aToActor.Location;
        HelpVector.Z = aFromActor.Location.Z;
        HelpVector = Normal(HelpVector - aFromActor.Location);
        HelpRotator = Rotator(HelpVector);
    }
    
    aFromActor.SetRotation(HelpRotator);
    aFromActor.DesiredRotation = HelpRotator;

    if(aFromActor.IsA('DeusExPlayer'))
        DeusExPlayer(aFromActor).ViewRotation = HelpRotator;
}

// ------------------
// State: Fade out/in
// ------------------
state FadeBeforePlay
{

Begin: 
    ModernRootWindow(Player.RootWindow).Fade(0, 0.5);
    Sleep(0.5);
    FixActorDistances();
    PostStartConversation(); 
    Player.bBehindView = True;
    ModernRootWindow(Player.RootWindow).Fade(1, 0.5);
    GotoState('PlayEvent');

}

// ---------------
// State: Fade out
// ---------------
state FadeBeforePostTerminate
{

Begin:
    ModernRootWindow(Player.RootWindow).Fade(0, 0.5);
    Sleep(0.5);
    ModernRootWindow(Player.RootWindow).Fade(1, 0.5);
    
    // Allow input
    if (conWinThird != None)
        conWinThird.RestrictInput(False);

    // Reset FOV
    Player.DesiredFOV = Player.DefaultFOV;
    Player.SetFOVAngle(Player.DefaultFOV);

    ConWinFinished();

}
  
// ------------------------------------
// State: Wait for speech
//
// Changes:
// - Multiplied durations by game speed
// ------------------------------------
state WaitForSpeech
{
Begin:
    // No audio
    if(ConEventSpeech(CurrentEvent).ConSpeech.soundID == -1)
    {
        SetTimer(FMax(LastSpeechTextLength * PerCharDelay, MinimumTextPause) * Level.Game.GameSpeed, False);
    }

    // Audio
    else
    {
        PlaySpeech(ConEventSpeech(currentEvent).ConSpeech.SoundID, ConEventSpeech(CurrentEvent).Speaker);

        SetTimer(Con.GetSpeechLength(ConEventSpeech(CurrentEvent).ConSpeech.SoundID) * Level.Game.GameSpeed, False );
    }

    GoTo('Idle');
}

// END UE1

defaultproperties
{
    bStatic=False
    MinPawnDistance=256
    MinPlayerDistance=64
}
