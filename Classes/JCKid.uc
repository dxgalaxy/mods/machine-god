class JCKid extends ScriptedPawnChild;

#exec OBJ LOAD FILE=Effects

// Voice: ElevenLabs Young JC
//
// Audacity:
// - Reverb, wet only
// - Delay
//   - Delay per echo: -12
//   - Delay time: 0.3
//   - Pitch change: -1.0
//   - Number of echoes: 2
// - Limiter
//   - +4dB input gain

function SupportActor(Actor aStandingActor) {}
function TakeDamage(int Damage, Pawn aInstigatedBy, Vector HitLocation, Vector Momentum, name DamageType) {}

defaultproperties
{
    bInvincible=True
    UnfamiliarName="Tantalus"
    GroundSpeed=150.000000
    FamiliarName="Tantalus"
    BindName="JCKid"
    MultiSkins(0)=WetTexture'Effects.UserInterface.DrunkFX'
    MultiSkins(1)=WetTexture'Effects.UserInterface.DrunkFX'
    MultiSkins(2)=WetTexture'Effects.UserInterface.DrunkFX'
    MultiSkins(3)=Texture'PinkMaskTex'
    MultiSkins(4)=Texture'PinkMaskTex'
    MultiSkins(5)=WetTexture'Effects.UserInterface.DrunkFX'
    MultiSkins(6)=Texture'GrayMaskTex'
    MultiSkins(7)=Texture'BlackMaskTex'
    OutlineTexture=FireTexture'Effects.Electricity.wepn_nesword'
}
