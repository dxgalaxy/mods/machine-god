//=============================================================================
// CrouchTrigger
// ---
// Forces the player to crouch
// Useful for odd transitions into crawl spaces
//=============================================================================
class CrouchTrigger extends Trigger;

var() bool bCrouch;

// BEGIN UE1

function Trigger(Actor Other, Pawn Instigator)
{
    Super.Trigger(Other, Instigator);
    
    if(DeusExPlayer(Instigator) == None)
        return;

    DeusExPlayer(Instigator).bCrouchOn = bCrouch;
}

function Touch(Actor Other)
{
    Super.Touch(Other);
  
    if(!IsRelevant(Other))
        return;
    
    if(DeusExPlayer(Other) == None)
        return;
    
    DeusExPlayer(Other).bCrouchOn = bCrouch;
}

// END UE1
