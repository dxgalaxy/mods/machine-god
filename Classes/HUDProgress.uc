class HUDProgress extends HUDSharedBorderWindow;

var TileWindow WinTile;
var TextWindow Label;
var Font FontLabel;

// BEGIN UE1

event InitWindow()
{
    Super.InitWindow();

    WinTile = TileWindow(NewChild(Class'TileWindow'));
    WinTile.SetOrder(ORDER_RightThenDown);
    WinTile.SetChildAlignments(HALIGN_Left, VALIGN_Center);
    WinTile.SetPos(0, 0);
    WinTile.SetMargins(10, 10);
    WinTile.SetMinorSpacing(4);
    WinTile.MakeWidthsEqual(False);
    WinTile.MakeHeightsEqual(True);
    WinTile.SetChildAlignments(HALIGN_Center, VALIGN_Top);

    Label = TextWindow(WinTile.NewChild(Class'TextWindow'));
    Label.SetFont(FontLabel);
    Label.SetTextAlignments(HALIGN_Center, VALIGN_Center);

    StyleChanged();
}

function SetValue(string Text, int Progress)
{
    local float PrefWidth, PrefHeight;

    if(Label == None)
        return;

    WinTile.QueryPreferredSize(PrefWidth, PrefHeight);
    
    PrefHeight = Max(PrefHeight, MinHeight);
    PrefWidth = Max(PrefWidth, 200);

    Label.SetText(Text $ " (" $ Progress $ "%)");
    
    ConfigureChild((Player.RootWindow.Width / 2) - (PrefWidth / 2), (Player.RootWindow.Height / 2) - (PrefHeight / 2), PrefWidth, PrefHeight);
    Label.ConfigureChild(0, 0, PrefWidth - 20, PrefHeight); 
}

event ParentRequestedPreferredSize(bool bWidthSpecified, out float PrefWidth, bool bHeightSpecified, out float PrefHeight)
{
    WinTile.QueryPreferredSize(PrefWidth, PrefHeight);
    PrefHeight = Max(PrefHeight, MinHeight);
    PrefWidth = Max(PrefWidth, 200);
}

function ConfigurationChanged()
{
    WinTile.ConfigureChild(0, 0, Width, Height);
}

function bool ChildRequestedReconfiguration(Window Child)
{
    ConfigurationChanged();

    return True;
}

event StyleChanged()
{
    Super.StyleChanged();

    if(Label != None)
        Label.SetTextColor(ColText);
}

// END UE1

defaultproperties
{
     FontLabel=Font'DeusExUI.FontMenuHeaders_DS'
}
