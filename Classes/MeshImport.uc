class MeshImport expands Object abstract; 

// BEGIN UE1

// --------------------------------------
// GM_Jumpsuit
// --------------------------------------
#exec MESH IMPORT MESH=GM_Jumpsuit_Inverted ANIVFILE=Models\GM_Jumpsuit_a.3d DATAFILE=Models\GM_Jumpsuit_d.3d UNMIRROR=1
#exec MESH ORIGIN MESH=GM_Jumpsuit_Inverted X=0 Y=0 Z=12200 YAW=64
#exec MESH LODPARAMS MESH=GM_Jumpsuit_Inverted STRENGTH=0.5

#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=All                STARTFRAME=0   NUMFRAMES=372         
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=Still              STARTFRAME=0   NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=Walk               STARTFRAME=1   NUMFRAMES=10  RATE=10 
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=Run                STARTFRAME=11  NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=Crouch             STARTFRAME=21  NUMFRAMES=3   RATE=6     GROUP=Ducking
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=Stand              STARTFRAME=24  NUMFRAMES=3   RATE=6     GROUP=Ducking
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=CrouchShoot        STARTFRAME=27  NUMFRAMES=5   RATE=10    GROUP=Ducking
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=BreatheLight       STARTFRAME=32  NUMFRAMES=5   RATE=2     GROUP=Waiting
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=DeathFront         STARTFRAME=37  NUMFRAMES=13  RATE=10 
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=DeathBack          STARTFRAME=50  NUMFRAMES=8   RATE=10 
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=HitHead            STARTFRAME=58  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=HitTorso           STARTFRAME=62  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=HitArmLeft         STARTFRAME=66  NUMFRAMES=4   RATE=12    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=HitLegLeft         STARTFRAME=70  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=HitArmRight        STARTFRAME=74  NUMFRAMES=4   RATE=12    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=HitLegRight        STARTFRAME=78  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=HitHeadBack        STARTFRAME=82  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=HitTorsoBack       STARTFRAME=86  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=MouthClosed        STARTFRAME=90  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=MouthA             STARTFRAME=91  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=MouthE             STARTFRAME=92  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=MouthF             STARTFRAME=93  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=MouthM             STARTFRAME=94  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=MouthO             STARTFRAME=95  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=MouthT             STARTFRAME=96  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=MouthU             STARTFRAME=97  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=Blink              STARTFRAME=98  NUMFRAMES=2   RATE=10 
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=Shoot              STARTFRAME=100 NUMFRAMES=6   RATE=15    GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=Attack             STARTFRAME=106 NUMFRAMES=7   RATE=10    GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=HeadUp             STARTFRAME=113 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=HeadDown           STARTFRAME=114 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=HeadLeft           STARTFRAME=115 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=HeadRight          STARTFRAME=116 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=RunShoot           STARTFRAME=117 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=GestureLeft        STARTFRAME=127 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=GestureRight       STARTFRAME=136 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=GestureBoth        STARTFRAME=145 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=RubEyesStart       STARTFRAME=154 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=RubEyes            STARTFRAME=158 NUMFRAMES=4   RATE=7.5
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=RubEyesStop        STARTFRAME=162 NUMFRAMES=4   RATE=15 
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=SitBegin           STARTFRAME=166 NUMFRAMES=5   RATE=3  
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=SitStill           STARTFRAME=171 NUMFRAMES=1   RATE=15 
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=SitStand           STARTFRAME=172 NUMFRAMES=5   RATE=3  
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=Pickup             STARTFRAME=177 NUMFRAMES=7   RATE=10 
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=Jump               STARTFRAME=184 NUMFRAMES=3   RATE=10 
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=Land               STARTFRAME=187 NUMFRAMES=3   RATE=10    GROUP=Landing
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=Panic              STARTFRAME=190 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=PushButton         STARTFRAME=200 NUMFRAMES=6   RATE=5     GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=ReloadBegin        STARTFRAME=206 NUMFRAMES=4   RATE=8     GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=Reload             STARTFRAME=210 NUMFRAMES=4   RATE=8     GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=ReloadEnd          STARTFRAME=214 NUMFRAMES=7   RATE=10    GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=Tread              STARTFRAME=221 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=TreadShoot         STARTFRAME=233 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=WaterHitTorso      STARTFRAME=245 NUMFRAMES=3   RATE=20    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=WaterHitTorsoBack  STARTFRAME=248 NUMFRAMES=3   RATE=20    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=WaterDeath         STARTFRAME=251 NUMFRAMES=8   RATE=10 
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=CowerBegin         STARTFRAME=259 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=CowerStill         STARTFRAME=263 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=CowerEnd           STARTFRAME=264 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=AttackSide         STARTFRAME=268 NUMFRAMES=13  RATE=20 
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=Strafe             STARTFRAME=281 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=Walk2H             STARTFRAME=291 NUMFRAMES=10  RATE=10 
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=Strafe2H           STARTFRAME=301 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=RunShoot2H         STARTFRAME=311 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=Shoot2H            STARTFRAME=321 NUMFRAMES=7   RATE=20    GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=BreatheLight2H     STARTFRAME=328 NUMFRAMES=5   RATE=2     GROUP=Waiting
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=Idle1              STARTFRAME=333 NUMFRAMES=15  RATE=6     GROUP=Waiting
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=Idle12H            STARTFRAME=348 NUMFRAMES=15  RATE=6     GROUP=Waiting
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=Shocked            STARTFRAME=363 NUMFRAMES=4   RATE=18    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Jumpsuit_Inverted SEQ=SitBreathe         STARTFRAME=367 NUMFRAMES=5   RATE=2     GROUP=Waiting

#exec MESHMAP SCALE MESHMAP=GM_Jumpsuit_Inverted X=-0.00390625 Y=0.00390625 Z=0.00390625

#exec MESH NOTIFY MESH=GM_Jumpsuit_Inverted SEQ=Walk         TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Jumpsuit_Inverted SEQ=Walk         TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Jumpsuit_Inverted SEQ=Run          TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Jumpsuit_Inverted SEQ=Run          TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Jumpsuit_Inverted SEQ=RunShoot     TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Jumpsuit_Inverted SEQ=RunShoot     TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Jumpsuit_Inverted SEQ=Panic        TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Jumpsuit_Inverted SEQ=Panic        TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Jumpsuit_Inverted SEQ=Strafe       TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Jumpsuit_Inverted SEQ=Strafe       TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Jumpsuit_Inverted SEQ=Walk2H       TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Jumpsuit_Inverted SEQ=Walk2H       TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Jumpsuit_Inverted SEQ=Strafe2H     TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Jumpsuit_Inverted SEQ=Strafe2H     TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Jumpsuit_Inverted SEQ=RunShoot2H   TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Jumpsuit_Inverted SEQ=RunShoot2H   TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Jumpsuit_Inverted SEQ=DeathFront   TIME=0.3    FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GM_Jumpsuit_Inverted SEQ=DeathBack    TIME=0.5    FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GM_Jumpsuit_Inverted SEQ=Tread        TIME=0.3    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Jumpsuit_Inverted SEQ=TreadShoot   TIME=0.3    FUNCTION=PlayFootStep

// --------------------------------------
// GM_DressShirt
// --------------------------------------
#exec MESH IMPORT MESH=GM_DressShirt_Inverted ANIVFILE=Models\GM_DressShirt_a.3d DATAFILE=Models\GM_DressShirt_d.3d UNMIRROR=1
#exec MESH ORIGIN MESH=GM_DressShirt_Inverted X=0 Y=0 Z=12200 YAW=64
#exec MESH LODPARAMS MESH=GM_DressShirt_Inverted STRENGTH=0.5

#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=All                 STARTFRAME=0   NUMFRAMES=358         
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=Still               STARTFRAME=0   NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=Walk                    STARTFRAME=1   NUMFRAMES=10  RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=Run                 STARTFRAME=11  NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=Crouch              STARTFRAME=21  NUMFRAMES=3   RATE=6     GROUP=Ducking
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=Stand               STARTFRAME=24  NUMFRAMES=3   RATE=6     GROUP=Ducking
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=CrouchShoot         STARTFRAME=27  NUMFRAMES=5   RATE=10    GROUP=Ducking
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=BreatheLight            STARTFRAME=32  NUMFRAMES=5   RATE=2.5   GROUP=Waiting
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=DeathFront          STARTFRAME=37  NUMFRAMES=13  RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=DeathBack           STARTFRAME=50  NUMFRAMES=8   RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=HitHead             STARTFRAME=58  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=HitTorso                STARTFRAME=62  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=HitArmLeft          STARTFRAME=66  NUMFRAMES=4   RATE=12    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=HitLegLeft          STARTFRAME=70  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=HitArmRight         STARTFRAME=74  NUMFRAMES=4   RATE=12    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=HitLegRight         STARTFRAME=78  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=HitHeadBack         STARTFRAME=82  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=HitTorsoBack            STARTFRAME=86  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=MouthClosed         STARTFRAME=90  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=MouthA              STARTFRAME=91  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=MouthE              STARTFRAME=92  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=MouthF              STARTFRAME=93  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=MouthM              STARTFRAME=94  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=MouthO              STARTFRAME=95  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=MouthT              STARTFRAME=96  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=MouthU              STARTFRAME=97  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=Blink               STARTFRAME=98  NUMFRAMES=2   RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=Shoot               STARTFRAME=100 NUMFRAMES=6   RATE=15    GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=Attack              STARTFRAME=106 NUMFRAMES=7   RATE=10    GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=HeadUp              STARTFRAME=113 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=HeadDown                STARTFRAME=114 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=HeadLeft                STARTFRAME=115 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=HeadRight           STARTFRAME=116 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=RunShoot                STARTFRAME=117 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=GestureLeft         STARTFRAME=127 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=GestureRight            STARTFRAME=136 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=GestureBoth         STARTFRAME=145 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=RubEyesStart            STARTFRAME=154 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=RubEyes             STARTFRAME=158 NUMFRAMES=4   RATE=7.5
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=RubEyesStop         STARTFRAME=162 NUMFRAMES=4   RATE=15 
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=SitBegin                STARTFRAME=166 NUMFRAMES=5   RATE=3  
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=SitStill                STARTFRAME=171 NUMFRAMES=1   RATE=15 
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=SitStand                STARTFRAME=172 NUMFRAMES=5   RATE=3  
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=Pickup              STARTFRAME=177 NUMFRAMES=7   RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=Jump                    STARTFRAME=184 NUMFRAMES=3   RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=Land                    STARTFRAME=187 NUMFRAMES=3   RATE=10    GROUP=Landing
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=Panic               STARTFRAME=190 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=PushButton          STARTFRAME=200 NUMFRAMES=6   RATE=5     GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=ReloadBegin         STARTFRAME=206 NUMFRAMES=4   RATE=8     GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=Reload              STARTFRAME=210 NUMFRAMES=4   RATE=8     GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=ReloadEnd           STARTFRAME=214 NUMFRAMES=7   RATE=10    GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=Tread               STARTFRAME=221 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=TreadShoot          STARTFRAME=233 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=WaterHitTorso       STARTFRAME=245 NUMFRAMES=3   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=WaterHitTorsoBack   STARTFRAME=248 NUMFRAMES=3   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=WaterDeath          STARTFRAME=251 NUMFRAMES=8   RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=CowerBegin          STARTFRAME=259 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=CowerStill          STARTFRAME=263 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=CowerEnd                STARTFRAME=264 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=AttackSide          STARTFRAME=268 NUMFRAMES=13  RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=Strafe              STARTFRAME=281 NUMFRAMES=10  RATE=24 
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=Walk2H              STARTFRAME=291 NUMFRAMES=10  RATE=10
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=Strafe2H                STARTFRAME=301 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=RunShoot2H          STARTFRAME=311 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=Shoot2H             STARTFRAME=321 NUMFRAMES=7   RATE=20    GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=BreatheLight2H      STARTFRAME=328 NUMFRAMES=5   RATE=2.5   GROUP=Waiting
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=Idle1               STARTFRAME=333 NUMFRAMES=8   RATE=3     GROUP=Waiting
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=Idle12H             STARTFRAME=341 NUMFRAMES=8   RATE=3     GROUP=Waiting
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=Shocked             STARTFRAME=349 NUMFRAMES=4   RATE=18    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_Inverted SEQ=SitBreathe          STARTFRAME=353 NUMFRAMES=5   RATE=2.5   GROUP=Waiting

#exec MESHMAP SCALE MESHMAP=GM_DressShirt_Inverted X=-0.00390625 Y=0.00390625 Z=0.00390625

#exec MESH NOTIFY MESH=GM_DressShirt_Inverted SEQ=Walk      TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_Inverted SEQ=Walk      TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_Inverted SEQ=Run       TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_Inverted SEQ=Run       TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_Inverted SEQ=RunShoot  TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_Inverted SEQ=RunShoot  TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_Inverted SEQ=Panic     TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_Inverted SEQ=Panic     TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_Inverted SEQ=Strafe        TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_Inverted SEQ=Strafe        TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_Inverted SEQ=Walk2H        TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_Inverted SEQ=Walk2H        TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_Inverted SEQ=Strafe2H  TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_Inverted SEQ=Strafe2H  TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_Inverted SEQ=RunShoot2H    TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_Inverted SEQ=RunShoot2H    TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_Inverted SEQ=DeathFront    TIME=0.3    FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GM_DressShirt_Inverted SEQ=DeathBack TIME=0.5    FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GM_DressShirt_Inverted SEQ=Tread     TIME=0.3    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_Inverted SEQ=TreadShoot    TIME=0.3    FUNCTION=PlayFootStep

// ---------------------------------------- 
// GM_DressShirt_S
// ---------------------------------------- 
#exec MESH IMPORT MESH=GM_DressShirt_S_Inverted ANIVFILE=Models\GM_DressShirt_S_a.3d DATAFILE=Models\GM_DressShirt_S_d.3d UNMIRROR=1
#exec MESH ORIGIN MESH=GM_DressShirt_S_Inverted X=0 Y=0 Z=12200 YAW=64
#exec MESH LODPARAMS MESH=GM_DressShirt_S_Inverted STRENGTH=0.5

#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=All                STARTFRAME=0   NUMFRAMES=312         
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=Still              STARTFRAME=0   NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=Walk               STARTFRAME=1   NUMFRAMES=10  RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=Run                STARTFRAME=11  NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=Crouch             STARTFRAME=21  NUMFRAMES=3   RATE=6     GROUP=Ducking
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=Stand              STARTFRAME=24  NUMFRAMES=3   RATE=6     GROUP=Ducking
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=CrouchShoot        STARTFRAME=27  NUMFRAMES=5   RATE=10    GROUP=Ducking
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=BreatheLight       STARTFRAME=32  NUMFRAMES=5   RATE=2.5   GROUP=Waiting
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=DeathFront         STARTFRAME=37  NUMFRAMES=13  RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=DeathBack          STARTFRAME=50  NUMFRAMES=8   RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=HitHead            STARTFRAME=58  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=HitTorso           STARTFRAME=62  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=HitArmLeft         STARTFRAME=66  NUMFRAMES=4   RATE=12    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=HitLegLeft         STARTFRAME=70  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=HitArmRight        STARTFRAME=74  NUMFRAMES=4   RATE=12    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=HitLegRight        STARTFRAME=78  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=HitHeadBack        STARTFRAME=82  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=HitTorsoBack       STARTFRAME=86  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=MouthClosed        STARTFRAME=90  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=MouthA             STARTFRAME=91  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=MouthE             STARTFRAME=92  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=MouthF             STARTFRAME=93  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=MouthM             STARTFRAME=94  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=MouthO             STARTFRAME=95  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=MouthT             STARTFRAME=96  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=MouthU             STARTFRAME=97  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=Blink              STARTFRAME=98  NUMFRAMES=2   RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=Shoot              STARTFRAME=100 NUMFRAMES=6   RATE=15    GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=Attack             STARTFRAME=106 NUMFRAMES=7   RATE=10    GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=HeadUp             STARTFRAME=113 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=HeadDown           STARTFRAME=114 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=HeadLeft           STARTFRAME=115 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=HeadRight          STARTFRAME=116 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=RunShoot           STARTFRAME=117 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=GestureLeft        STARTFRAME=127 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=GestureRight       STARTFRAME=136 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=GestureBoth        STARTFRAME=145 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=RubEyesStart       STARTFRAME=154 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=RubEyes            STARTFRAME=158 NUMFRAMES=4   RATE=7.5
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=RubEyesStop        STARTFRAME=162 NUMFRAMES=4   RATE=15 
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=SitBegin           STARTFRAME=166 NUMFRAMES=5   RATE=3  
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=SitStill           STARTFRAME=171 NUMFRAMES=1   RATE=15 
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=SitStand           STARTFRAME=172 NUMFRAMES=5   RATE=3  
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=Pickup             STARTFRAME=177 NUMFRAMES=7   RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=Jump               STARTFRAME=184 NUMFRAMES=3   RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=Land               STARTFRAME=187 NUMFRAMES=3   RATE=10    GROUP=Landing
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=Panic              STARTFRAME=190 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=PushButton         STARTFRAME=200 NUMFRAMES=6   RATE=5     GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=ReloadBegin        STARTFRAME=206 NUMFRAMES=4   RATE=8     GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=Reload             STARTFRAME=210 NUMFRAMES=4   RATE=8     GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=ReloadEnd          STARTFRAME=214 NUMFRAMES=7   RATE=10    GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=Tread              STARTFRAME=221 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=TreadShoot         STARTFRAME=233 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=WaterHitTorso      STARTFRAME=245 NUMFRAMES=3   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=WaterHitTorsoBack  STARTFRAME=248 NUMFRAMES=3   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=WaterDeath         STARTFRAME=251 NUMFRAMES=8   RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=CowerBegin         STARTFRAME=259 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=CowerStill         STARTFRAME=263 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=CowerEnd           STARTFRAME=264 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=AttackSide         STARTFRAME=268 NUMFRAMES=13  RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=Strafe             STARTFRAME=281 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=Idle1              STARTFRAME=291 NUMFRAMES=12  RATE=8     GROUP=Waiting
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=Shocked            STARTFRAME=303 NUMFRAMES=4   RATE=18    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_S_Inverted SEQ=SitBreathe         STARTFRAME=307 NUMFRAMES=5   RATE=2.5   GROUP=Waiting

#exec MESHMAP SCALE MESHMAP=GM_DressShirt_S_Inverted X=-0.00390625 Y=0.00390625 Z=0.00390625

#exec MESH NOTIFY MESH=GM_DressShirt_S_Inverted SEQ=Walk         TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_S_Inverted SEQ=Walk         TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_S_Inverted SEQ=Run          TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_S_Inverted SEQ=Run          TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_S_Inverted SEQ=RunShoot     TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_S_Inverted SEQ=RunShoot     TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_S_Inverted SEQ=Panic        TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_S_Inverted SEQ=Panic        TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_S_Inverted SEQ=Strafe       TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_S_Inverted SEQ=Strafe       TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_S_Inverted SEQ=DeathFront   TIME=0.3    FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GM_DressShirt_S_Inverted SEQ=DeathBack    TIME=0.5    FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GM_DressShirt_S_Inverted SEQ=Tread        TIME=0.3    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_S_Inverted SEQ=TreadShoot   TIME=0.3    FUNCTION=PlayFootStep

// ---------------------------------------- 
// GM_DressShirt_F
// ---------------------------------------- 
#exec MESH IMPORT MESH=GM_DressShirt_F_Inverted ANIVFILE=Models\GM_DressShirt_F_a.3d DATAFILE=Models\GM_DressShirt_F_d.3d UNMIRROR=1
#exec MESH ORIGIN MESH=GM_DressShirt_F_Inverted X=0 Y=0 Z=12200 YAW=64
#exec MESH LODPARAMS MESH=GM_DressShirt_F_Inverted STRENGTH=0.5

#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=All                STARTFRAME=0   NUMFRAMES=315         
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=Still              STARTFRAME=0   NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=Walk               STARTFRAME=1   NUMFRAMES=10  RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=Run                STARTFRAME=11  NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=Crouch             STARTFRAME=21  NUMFRAMES=3   RATE=6     GROUP=Ducking
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=Stand              STARTFRAME=24  NUMFRAMES=3   RATE=6     GROUP=Ducking
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=CrouchShoot        STARTFRAME=27  NUMFRAMES=5   RATE=10    GROUP=Ducking
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=BreatheLight       STARTFRAME=32  NUMFRAMES=5   RATE=2.5   GROUP=Waiting
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=DeathFront         STARTFRAME=37  NUMFRAMES=13  RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=DeathBack          STARTFRAME=50  NUMFRAMES=8   RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=HitHead            STARTFRAME=58  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=HitTorso           STARTFRAME=62  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=HitArmLeft         STARTFRAME=66  NUMFRAMES=4   RATE=12    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=HitLegLeft         STARTFRAME=70  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=HitArmRight        STARTFRAME=74  NUMFRAMES=4   RATE=12    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=HitLegRight        STARTFRAME=78  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=HitHeadBack        STARTFRAME=82  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=HitTorsoBack       STARTFRAME=86  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=MouthClosed        STARTFRAME=90  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=MouthA             STARTFRAME=91  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=MouthE             STARTFRAME=92  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=MouthF             STARTFRAME=93  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=MouthM             STARTFRAME=94  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=MouthO             STARTFRAME=95  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=MouthT             STARTFRAME=96  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=MouthU             STARTFRAME=97  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=Blink              STARTFRAME=98  NUMFRAMES=2   RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=Shoot              STARTFRAME=100 NUMFRAMES=6   RATE=15    GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=Attack             STARTFRAME=106 NUMFRAMES=7   RATE=10    GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=HeadUp             STARTFRAME=113 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=HeadDown           STARTFRAME=114 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=HeadLeft           STARTFRAME=115 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=HeadRight          STARTFRAME=116 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=RunShoot           STARTFRAME=117 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=GestureLeft        STARTFRAME=127 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=GestureRight       STARTFRAME=136 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=GestureBoth        STARTFRAME=145 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=RubEyesStart       STARTFRAME=154 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=RubEyes            STARTFRAME=158 NUMFRAMES=4   RATE=7.5
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=RubEyesStop        STARTFRAME=162 NUMFRAMES=4   RATE=15 
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=SitBegin           STARTFRAME=166 NUMFRAMES=5   RATE=3  
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=SitStill           STARTFRAME=171 NUMFRAMES=1   RATE=15 
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=SitStand           STARTFRAME=172 NUMFRAMES=5   RATE=3  
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=Pickup             STARTFRAME=177 NUMFRAMES=7   RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=Jump               STARTFRAME=184 NUMFRAMES=3   RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=Land               STARTFRAME=187 NUMFRAMES=3   RATE=10    GROUP=Landing
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=Panic              STARTFRAME=190 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=PushButton         STARTFRAME=200 NUMFRAMES=6   RATE=5     GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=ReloadBegin        STARTFRAME=206 NUMFRAMES=4   RATE=8     GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=Reload             STARTFRAME=210 NUMFRAMES=4   RATE=8     GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=ReloadEnd          STARTFRAME=214 NUMFRAMES=7   RATE=10    GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=Tread              STARTFRAME=221 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=TreadShoot         STARTFRAME=233 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=WaterHitTorso      STARTFRAME=245 NUMFRAMES=3   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=WaterHitTorsoBack  STARTFRAME=248 NUMFRAMES=3   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=WaterDeath         STARTFRAME=251 NUMFRAMES=8   RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=CowerBegin         STARTFRAME=259 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=CowerStill         STARTFRAME=263 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=CowerEnd           STARTFRAME=264 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=AttackSide         STARTFRAME=268 NUMFRAMES=13  RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=Strafe             STARTFRAME=281 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=Idle1              STARTFRAME=291 NUMFRAMES=15  RATE=6     GROUP=Waiting
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=Shocked            STARTFRAME=306 NUMFRAMES=4   RATE=18    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_F_Inverted SEQ=SitBreathe         STARTFRAME=310 NUMFRAMES=5   RATE=2.5   GROUP=Waiting

#exec MESHMAP SCALE MESHMAP=GM_DressShirt_F_Inverted X=-0.00390625 Y=0.00390625 Z=0.00390625

#exec MESH NOTIFY MESH=GM_DressShirt_F_Inverted SEQ=Walk         TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_F_Inverted SEQ=Walk         TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_F_Inverted SEQ=Run          TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_F_Inverted SEQ=Run          TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_F_Inverted SEQ=RunShoot     TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_F_Inverted SEQ=RunShoot     TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_F_Inverted SEQ=Panic        TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_F_Inverted SEQ=Panic        TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_F_Inverted SEQ=Strafe       TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_F_Inverted SEQ=Strafe       TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_F_Inverted SEQ=DeathFront   TIME=0.3    FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GM_DressShirt_F_Inverted SEQ=DeathBack    TIME=0.5    FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GM_DressShirt_F_Inverted SEQ=Tread        TIME=0.3    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_F_Inverted SEQ=TreadShoot   TIME=0.3    FUNCTION=PlayFootStep

// --------------------------------------
// GM_DressShirt_B
// --------------------------------------
#exec MESH IMPORT MESH=GM_DressShirt_B_Inverted ANIVFILE=Models\GM_DressShirt_B_a.3d DATAFILE=Models\GM_DressShirt_B_d.3d UNMIRROR=1
#exec MESH ORIGIN MESH=GM_DressShirt_B_Inverted X=0 Y=0 Z=12200 YAW=64
#exec MESH LODPARAMS MESH=GM_DressShirt_B_Inverted STRENGTH=0.5

#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=All                STARTFRAME=0   NUMFRAMES=358         
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=Still              STARTFRAME=0   NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=Walk               STARTFRAME=1   NUMFRAMES=10  RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=Run                STARTFRAME=11  NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=Crouch             STARTFRAME=21  NUMFRAMES=3   RATE=6     GROUP=Ducking
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=Stand              STARTFRAME=24  NUMFRAMES=3   RATE=6     GROUP=Ducking
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=CrouchShoot        STARTFRAME=27  NUMFRAMES=5   RATE=10    GROUP=Ducking
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=BreatheLight       STARTFRAME=32  NUMFRAMES=5   RATE=2.5   GROUP=Waiting
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=DeathFront         STARTFRAME=37  NUMFRAMES=13  RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=DeathBack          STARTFRAME=50  NUMFRAMES=8   RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=HitHead            STARTFRAME=58  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=HitTorso           STARTFRAME=62  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=HitArmLeft         STARTFRAME=66  NUMFRAMES=4   RATE=12    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=HitLegLeft         STARTFRAME=70  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=HitArmRight        STARTFRAME=74  NUMFRAMES=4   RATE=12    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=HitLegRight        STARTFRAME=78  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=HitHeadBack        STARTFRAME=82  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=HitTorsoBack       STARTFRAME=86  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=MouthClosed        STARTFRAME=90  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=MouthA             STARTFRAME=91  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=MouthE             STARTFRAME=92  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=MouthF             STARTFRAME=93  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=MouthM             STARTFRAME=94  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=MouthO             STARTFRAME=95  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=MouthT             STARTFRAME=96  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=MouthU             STARTFRAME=97  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=Blink              STARTFRAME=98  NUMFRAMES=2   RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=Shoot              STARTFRAME=100 NUMFRAMES=6   RATE=15    GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=Attack             STARTFRAME=106 NUMFRAMES=7   RATE=10    GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=HeadUp             STARTFRAME=113 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=HeadDown           STARTFRAME=114 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=HeadLeft           STARTFRAME=115 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=HeadRight          STARTFRAME=116 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=RunShoot           STARTFRAME=117 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=GestureLeft        STARTFRAME=127 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=GestureRight       STARTFRAME=136 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=GestureBoth        STARTFRAME=145 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=RubEyesStart       STARTFRAME=154 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=RubEyes            STARTFRAME=158 NUMFRAMES=4   RATE=7.5
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=RubEyesStop        STARTFRAME=162 NUMFRAMES=4   RATE=15 
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=SitBegin           STARTFRAME=166 NUMFRAMES=5   RATE=3  
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=SitStill           STARTFRAME=171 NUMFRAMES=1   RATE=15 
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=SitStand           STARTFRAME=172 NUMFRAMES=5   RATE=3  
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=Pickup             STARTFRAME=177 NUMFRAMES=7   RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=Jump               STARTFRAME=184 NUMFRAMES=3   RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=Land               STARTFRAME=187 NUMFRAMES=3   RATE=10    GROUP=Landing
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=Panic              STARTFRAME=190 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=PushButton         STARTFRAME=200 NUMFRAMES=6   RATE=5     GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=ReloadBegin        STARTFRAME=206 NUMFRAMES=4   RATE=8     GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=Reload             STARTFRAME=210 NUMFRAMES=4   RATE=8     GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=ReloadEnd          STARTFRAME=214 NUMFRAMES=7   RATE=10    GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=Tread              STARTFRAME=221 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=TreadShoot         STARTFRAME=233 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=WaterHitTorso      STARTFRAME=245 NUMFRAMES=3   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=WaterHitTorsoBack  STARTFRAME=248 NUMFRAMES=3   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=WaterDeath         STARTFRAME=251 NUMFRAMES=8   RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=CowerBegin         STARTFRAME=259 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=CowerStill         STARTFRAME=263 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=CowerEnd           STARTFRAME=264 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=AttackSide         STARTFRAME=268 NUMFRAMES=13  RATE=20 
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=Strafe             STARTFRAME=281 NUMFRAMES=10  RATE=24 
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=Walk2H             STARTFRAME=291 NUMFRAMES=10  RATE=10 
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=Strafe2H           STARTFRAME=301 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=RunShoot2H         STARTFRAME=311 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=Shoot2H            STARTFRAME=321 NUMFRAMES=7   RATE=20    GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=BreatheLight2H     STARTFRAME=328 NUMFRAMES=5   RATE=2.5   GROUP=Waiting
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=Idle1              STARTFRAME=333 NUMFRAMES=8   RATE=3     GROUP=Waiting
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=Idle12H            STARTFRAME=341 NUMFRAMES=8   RATE=3     GROUP=Waiting
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=Shocked            STARTFRAME=349 NUMFRAMES=4   RATE=18    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_DressShirt_B_Inverted SEQ=SitBreathe         STARTFRAME=353 NUMFRAMES=5   RATE=2.5   GROUP=Waiting

#exec MESHMAP SCALE MESHMAP=GM_DressShirt_B_Inverted X=-0.00390625 Y=0.00390625 Z=0.00390625

#exec MESH NOTIFY MESH=GM_DressShirt_B_Inverted SEQ=Walk         TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_B_Inverted SEQ=Walk         TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_B_Inverted SEQ=Run          TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_B_Inverted SEQ=Run          TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_B_Inverted SEQ=RunShoot     TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_B_Inverted SEQ=RunShoot     TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_B_Inverted SEQ=Panic        TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_B_Inverted SEQ=Panic        TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_B_Inverted SEQ=Strafe       TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_B_Inverted SEQ=Strafe       TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_B_Inverted SEQ=Walk2H       TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_B_Inverted SEQ=Walk2H       TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_B_Inverted SEQ=Strafe2H     TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_B_Inverted SEQ=Strafe2H     TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_B_Inverted SEQ=RunShoot2H   TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_B_Inverted SEQ=RunShoot2H   TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_B_Inverted SEQ=DeathFront   TIME=0.3    FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GM_DressShirt_B_Inverted SEQ=DeathBack    TIME=0.5    FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GM_DressShirt_B_Inverted SEQ=Tread        TIME=0.3    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_DressShirt_B_Inverted SEQ=TreadShoot   TIME=0.3    FUNCTION=PlayFootStep

// ---------------------------------------- 
// GMK_DressShirt
// ---------------------------------------- 
#exec MESH IMPORT MESH=GMK_DressShirt_Inverted ANIVFILE=Models\GMK_DressShirt_a.3d DATAFILE=Models\GMK_DressShirt_d.3d UNMIRROR=1
#exec MESH ORIGIN MESH=GMK_DressShirt_Inverted X=0 Y=0 Z=12700 YAW=64
#exec MESH LODPARAMS MESH=GMK_DressShirt_Inverted STRENGTH=0.5

#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=All                 STARTFRAME=0   NUMFRAMES=234         
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=Still               STARTFRAME=0   NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=Walk                STARTFRAME=1   NUMFRAMES=10  RATE=10 
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=Run                 STARTFRAME=11  NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=Crouch              STARTFRAME=21  NUMFRAMES=3   RATE=6     GROUP=Ducking
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=Stand               STARTFRAME=24  NUMFRAMES=3   RATE=6     GROUP=Ducking
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=BreatheLight        STARTFRAME=27  NUMFRAMES=5   RATE=2.5   GROUP=Waiting
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=DeathFront          STARTFRAME=32  NUMFRAMES=13  RATE=10 
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=DeathBack           STARTFRAME=45  NUMFRAMES=8   RATE=10 
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=HitHead             STARTFRAME=53  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=HitTorso            STARTFRAME=57  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=HitArmLeft          STARTFRAME=61  NUMFRAMES=4   RATE=12    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=HitLegLeft          STARTFRAME=65  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=HitArmRight         STARTFRAME=69  NUMFRAMES=4   RATE=12    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=HitLegRight         STARTFRAME=73  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=HitHeadBack         STARTFRAME=77  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=HitTorsoBack        STARTFRAME=81  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=MouthClosed         STARTFRAME=85  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=MouthA              STARTFRAME=86  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=MouthE              STARTFRAME=87  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=MouthF              STARTFRAME=88  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=MouthM              STARTFRAME=89  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=MouthO              STARTFRAME=90  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=MouthT              STARTFRAME=91  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=MouthU              STARTFRAME=92  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=Blink               STARTFRAME=93  NUMFRAMES=2   RATE=10 
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=Attack              STARTFRAME=95  NUMFRAMES=7   RATE=10 
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=HeadUp              STARTFRAME=102 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=HeadDown            STARTFRAME=103 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=HeadLeft            STARTFRAME=104 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=HeadRight           STARTFRAME=105 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=GestureLeft         STARTFRAME=106 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=GestureRight        STARTFRAME=115 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=GestureBoth         STARTFRAME=124 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=RubEyesStart        STARTFRAME=133 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=RubEyes             STARTFRAME=137 NUMFRAMES=4   RATE=7.5
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=RubEyesStop         STARTFRAME=141 NUMFRAMES=4   RATE=15 
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=Pickup              STARTFRAME=145 NUMFRAMES=7   RATE=10    GROUP=Gesture
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=Jump                STARTFRAME=152 NUMFRAMES=3   RATE=10 
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=Land                STARTFRAME=155 NUMFRAMES=3   RATE=10    GROUP=Landing
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=Panic               STARTFRAME=158 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=PushButton          STARTFRAME=168 NUMFRAMES=6   RATE=5     GROUP=Gesture
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=Tread               STARTFRAME=174 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=WaterHitTorso       STARTFRAME=186 NUMFRAMES=3   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=WaterHitTorsoBack   STARTFRAME=189 NUMFRAMES=3   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=WaterDeath          STARTFRAME=192 NUMFRAMES=8   RATE=10 
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=CowerBegin          STARTFRAME=200 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=CowerStill          STARTFRAME=204 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=CowerEnd            STARTFRAME=205 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=AttackSide          STARTFRAME=209 NUMFRAMES=13  RATE=20 
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=Idle1               STARTFRAME=222 NUMFRAMES=8   RATE=3     GROUP=Waiting
#exec MESH SEQUENCE MESH=GMK_DressShirt_Inverted SEQ=Shocked             STARTFRAME=230 NUMFRAMES=4   RATE=18    GROUP=TakeHit

#exec MESHMAP SCALE MESHMAP=GMK_DressShirt_Inverted X=-0.00257813 Y=0.00257813 Z=0.00257813

#exec MESH NOTIFY MESH=GMK_DressShirt_Inverted SEQ=Walk          TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GMK_DressShirt_Inverted SEQ=Walk          TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GMK_DressShirt_Inverted SEQ=Run           TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GMK_DressShirt_Inverted SEQ=Run           TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GMK_DressShirt_Inverted SEQ=Panic         TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GMK_DressShirt_Inverted SEQ=Panic         TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GMK_DressShirt_Inverted SEQ=DeathFront    TIME=0.3    FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GMK_DressShirt_Inverted SEQ=DeathBack     TIME=0.5    FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GMK_DressShirt_Inverted SEQ=Tread         TIME=0.3    FUNCTION=PlayFootStep

// ---------------------------------------- 
// GMK_DressShirt_F
// ---------------------------------------- 
#exec MESH IMPORT MESH=GMK_DressShirt_F_Inverted ANIVFILE=Models\GMK_DressShirt_F_a.3d DATAFILE=Models\GMK_DressShirt_F_d.3d UNMIRROR=1
#exec MESH ORIGIN MESH=GMK_DressShirt_F_Inverted X=0 Y=0 Z=12700 YAW=64
#exec MESH LODPARAMS MESH=GMK_DressShirt_F_Inverted STRENGTH=0.5

#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=All               STARTFRAME=0   NUMFRAMES=234         
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=Still             STARTFRAME=0   NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=Walk              STARTFRAME=1   NUMFRAMES=10  RATE=10 
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=Run               STARTFRAME=11  NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=Crouch            STARTFRAME=21  NUMFRAMES=3   RATE=6     GROUP=Ducking
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=Stand             STARTFRAME=24  NUMFRAMES=3   RATE=6     GROUP=Ducking
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=BreatheLight      STARTFRAME=27  NUMFRAMES=5   RATE=2.5   GROUP=Waiting
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=DeathFront        STARTFRAME=32  NUMFRAMES=13  RATE=10 
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=DeathBack         STARTFRAME=45  NUMFRAMES=8   RATE=10 
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=HitHead           STARTFRAME=53  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=HitTorso          STARTFRAME=57  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=HitArmLeft        STARTFRAME=61  NUMFRAMES=4   RATE=12    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=HitLegLeft        STARTFRAME=65  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=HitArmRight       STARTFRAME=69  NUMFRAMES=4   RATE=12    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=HitLegRight       STARTFRAME=73  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=HitHeadBack       STARTFRAME=77  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=HitTorsoBack      STARTFRAME=81  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=MouthClosed       STARTFRAME=85  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=MouthA            STARTFRAME=86  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=MouthE            STARTFRAME=87  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=MouthF            STARTFRAME=88  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=MouthM            STARTFRAME=89  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=MouthO            STARTFRAME=90  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=MouthT            STARTFRAME=91  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=MouthU            STARTFRAME=92  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=Blink             STARTFRAME=93  NUMFRAMES=2   RATE=10 
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=Attack            STARTFRAME=95  NUMFRAMES=7   RATE=10 
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=HeadUp            STARTFRAME=102 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=HeadDown          STARTFRAME=103 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=HeadLeft          STARTFRAME=104 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=HeadRight         STARTFRAME=105 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=GestureLeft       STARTFRAME=106 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=GestureRight      STARTFRAME=115 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=GestureBoth       STARTFRAME=124 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=RubEyesStart      STARTFRAME=133 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=RubEyes           STARTFRAME=137 NUMFRAMES=4   RATE=7.5
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=RubEyesStop       STARTFRAME=141 NUMFRAMES=4   RATE=15 
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=Pickup            STARTFRAME=145 NUMFRAMES=7   RATE=10    GROUP=Gesture
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=Jump              STARTFRAME=152 NUMFRAMES=3   RATE=10 
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=Land              STARTFRAME=155 NUMFRAMES=3   RATE=10    GROUP=Landing
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=Panic             STARTFRAME=158 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=PushButton        STARTFRAME=168 NUMFRAMES=6   RATE=5     GROUP=Gesture
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=Tread             STARTFRAME=174 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=WaterHitTorso     STARTFRAME=186 NUMFRAMES=3   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=WaterHitTorsoBack STARTFRAME=189 NUMFRAMES=3   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=WaterDeath        STARTFRAME=192 NUMFRAMES=8   RATE=10 
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=CowerBegin        STARTFRAME=200 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=CowerStill        STARTFRAME=204 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=CowerEnd          STARTFRAME=205 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=AttackSide        STARTFRAME=209 NUMFRAMES=13  RATE=20 
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=Idle1             STARTFRAME=222 NUMFRAMES=8   RATE=3     GROUP=Waiting
#exec MESH SEQUENCE MESH=GMK_DressShirt_F_Inverted SEQ=Shocked           STARTFRAME=230 NUMFRAMES=4   RATE=18    GROUP=TakeHit

#exec MESHMAP SCALE MESHMAP=GMK_DressShirt_F_Inverted X=-0.00257813 Y=0.00257813 Z=0.00257813

#exec MESH NOTIFY MESH=GMK_DressShirt_F_Inverted SEQ=Walk        TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GMK_DressShirt_F_Inverted SEQ=Walk        TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GMK_DressShirt_F_Inverted SEQ=Run         TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GMK_DressShirt_F_Inverted SEQ=Run         TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GMK_DressShirt_F_Inverted SEQ=Panic       TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GMK_DressShirt_F_Inverted SEQ=Panic       TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GMK_DressShirt_F_Inverted SEQ=DeathFront  TIME=0.3    FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GMK_DressShirt_F_Inverted SEQ=DeathBack   TIME=0.5    FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GMK_DressShirt_F_Inverted SEQ=Tread       TIME=0.3    FUNCTION=PlayFootStep

// --------------------------------------
// GFM_Trench
// --------------------------------------
#exec MESH IMPORT MESH=GFM_Trench_Inverted ANIVFILE=Models\GFM_Trench_a.3d DATAFILE=Models\GFM_Trench_d.3d UNMIRROR=1
#exec MESH ORIGIN MESH=GFM_Trench_Inverted X=0 Y=0 Z=11000 YAW=64
#exec MESH LODPARAMS MESH=GFM_Trench_Inverted STRENGTH=0.5

#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=All                STARTFRAME=0   NUMFRAMES=321         
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=Still              STARTFRAME=0   NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=Walk               STARTFRAME=1   NUMFRAMES=10  RATE=10 
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=Run                STARTFRAME=11  NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=Crouch             STARTFRAME=21  NUMFRAMES=3   RATE=6     GROUP=Ducking
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=Stand              STARTFRAME=24  NUMFRAMES=3   RATE=6     GROUP=Ducking
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=CrouchShoot        STARTFRAME=27  NUMFRAMES=5   RATE=10    GROUP=Ducking
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=BreatheLight       STARTFRAME=32  NUMFRAMES=5   RATE=2.5   GROUP=Waiting
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=DeathFront         STARTFRAME=37  NUMFRAMES=11  RATE=10 
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=DeathBack          STARTFRAME=48  NUMFRAMES=13  RATE=15 
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=HitHead            STARTFRAME=61  NUMFRAMES=3   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=HitTorso           STARTFRAME=64  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=HitArmLeft         STARTFRAME=68  NUMFRAMES=3   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=HitLegLeft         STARTFRAME=71  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=HitArmRight        STARTFRAME=75  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=HitLegRight        STARTFRAME=79  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=HitHeadBack        STARTFRAME=83  NUMFRAMES=3   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=HitTorsoBack       STARTFRAME=86  NUMFRAMES=5   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=MouthClosed        STARTFRAME=91  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=MouthA             STARTFRAME=92  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=MouthE             STARTFRAME=93  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=MouthF             STARTFRAME=94  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=MouthM             STARTFRAME=95  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=MouthO             STARTFRAME=96  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=MouthT             STARTFRAME=97  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=MouthU             STARTFRAME=98  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=Blink              STARTFRAME=99  NUMFRAMES=2   RATE=10 
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=Shoot              STARTFRAME=101 NUMFRAMES=6   RATE=14    GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=Attack             STARTFRAME=107 NUMFRAMES=7   RATE=12    GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=HeadUp             STARTFRAME=114 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=HeadDown           STARTFRAME=115 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=HeadLeft           STARTFRAME=116 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=HeadRight          STARTFRAME=117 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=RunShoot           STARTFRAME=118 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=GestureLeft        STARTFRAME=128 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=GestureRight       STARTFRAME=137 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=GestureBoth        STARTFRAME=146 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=RubEyesStart       STARTFRAME=155 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=RubEyes            STARTFRAME=159 NUMFRAMES=4   RATE=7.5
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=RubEyesStop        STARTFRAME=163 NUMFRAMES=4   RATE=15 
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=SitBegin           STARTFRAME=167 NUMFRAMES=7   RATE=10 
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=SitStill           STARTFRAME=174 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=SitStand           STARTFRAME=175 NUMFRAMES=7   RATE=12 
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=Pickup             STARTFRAME=182 NUMFRAMES=6   RATE=10    GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=Jump               STARTFRAME=188 NUMFRAMES=3   RATE=10 
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=Land               STARTFRAME=191 NUMFRAMES=3   RATE=10    GROUP=Landing
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=Panic              STARTFRAME=194 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=PushButton         STARTFRAME=204 NUMFRAMES=6   RATE=5     GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=ReloadBegin        STARTFRAME=210 NUMFRAMES=8   RATE=16    GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=Reload             STARTFRAME=218 NUMFRAMES=4   RATE=8     GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=ReloadEnd          STARTFRAME=222 NUMFRAMES=4   RATE=6     GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=Tread              STARTFRAME=226 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=TreadShoot         STARTFRAME=238 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=WaterHitTorso      STARTFRAME=250 NUMFRAMES=3   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=WaterHitTorsoBack  STARTFRAME=253 NUMFRAMES=3   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=WaterDeath         STARTFRAME=256 NUMFRAMES=6   RATE=10 
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=CowerBegin         STARTFRAME=262 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=CowerStill         STARTFRAME=266 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=CowerEnd           STARTFRAME=267 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=AttackSide         STARTFRAME=271 NUMFRAMES=13  RATE=20 
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=Strafe             STARTFRAME=284 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=Idle1              STARTFRAME=294 NUMFRAMES=8   RATE=3     GROUP=Waiting
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=Dance              STARTFRAME=302 NUMFRAMES=10  RATE=10    GROUP=Waiting
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=Shocked            STARTFRAME=312 NUMFRAMES=4   RATE=18    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_Trench_Inverted SEQ=SitBreathe         STARTFRAME=316 NUMFRAMES=5   RATE=2.5   GROUP=Waiting

#exec MESHMAP SCALE MESHMAP=GFM_Trench_Inverted X=-0.00390625 Y=0.00390625 Z=0.00390625

#exec MESH NOTIFY MESH=GFM_Trench_Inverted SEQ=Walk                 TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_Trench_Inverted SEQ=Walk                 TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_Trench_Inverted SEQ=Run                  TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_Trench_Inverted SEQ=Run                  TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_Trench_Inverted SEQ=RunShoot             TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_Trench_Inverted SEQ=RunShoot             TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_Trench_Inverted SEQ=Panic                TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_Trench_Inverted SEQ=Panic                TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_Trench_Inverted SEQ=Strafe               TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_Trench_Inverted SEQ=Strafe               TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_Trench_Inverted SEQ=DeathFront           TIME=0.3    FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GFM_Trench_Inverted SEQ=DeathBack            TIME=0.5    FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GFM_Trench_Inverted SEQ=Tread                TIME=0.3    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_Trench_Inverted SEQ=TreadShoot           TIME=0.3    FUNCTION=PlayFootStep

// --------------------------------------
// GM_Trench
// --------------------------------------

#exec MESH IMPORT MESH=GM_Trench_Inverted ANIVFILE=Models\GM_Trench_a.3d DATAFILE=Models\GM_Trench_d.3d UNMIRROR=1
#exec MESH ORIGIN MESH=GM_Trench_Inverted X=0 Y=0 Z=12200 YAW=64
#exec MESH LODPARAMS MESH=GM_Trench_Inverted STRENGTH=0.5

#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=All                 STARTFRAME=0   NUMFRAMES=366         
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=Still               STARTFRAME=0   NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=Walk                STARTFRAME=1   NUMFRAMES=10  RATE=10 
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=Run                 STARTFRAME=11  NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=CrouchWalk          STARTFRAME=21  NUMFRAMES=9   RATE=5     GROUP=Ducking
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=Crouch              STARTFRAME=30  NUMFRAMES=3   RATE=6     GROUP=Ducking
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=Stand               STARTFRAME=33  NUMFRAMES=3   RATE=6     GROUP=Ducking
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=CrouchShoot         STARTFRAME=36  NUMFRAMES=5   RATE=10    GROUP=Ducking
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=BreatheLight        STARTFRAME=41  NUMFRAMES=5   RATE=2.5   GROUP=Waiting
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=DeathFront          STARTFRAME=46  NUMFRAMES=13  RATE=10 
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=DeathBack           STARTFRAME=59  NUMFRAMES=8   RATE=10 
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=HitHead             STARTFRAME=67  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=HitTorso            STARTFRAME=71  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=HitArmLeft          STARTFRAME=75  NUMFRAMES=4   RATE=12    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=HitLegLeft          STARTFRAME=79  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=HitArmRight         STARTFRAME=83  NUMFRAMES=4   RATE=12    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=HitLegRight         STARTFRAME=87  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=HitHeadBack         STARTFRAME=91  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=HitTorsoBack        STARTFRAME=95  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=MouthClosed         STARTFRAME=99  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=MouthA              STARTFRAME=100 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=MouthE              STARTFRAME=101 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=MouthF              STARTFRAME=102 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=MouthM              STARTFRAME=103 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=MouthO              STARTFRAME=104 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=MouthT              STARTFRAME=105 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=MouthU              STARTFRAME=106 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=Blink               STARTFRAME=107 NUMFRAMES=2   RATE=10 
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=Shoot               STARTFRAME=109 NUMFRAMES=6   RATE=15    GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=Attack              STARTFRAME=115 NUMFRAMES=7   RATE=10    GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=HeadUp              STARTFRAME=122 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=HeadDown            STARTFRAME=123 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=HeadLeft            STARTFRAME=124 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=HeadRight           STARTFRAME=125 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=RunShoot            STARTFRAME=126 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=GestureLeft         STARTFRAME=136 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=GestureRight        STARTFRAME=145 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=GestureBoth         STARTFRAME=154 NUMFRAMES=9   RATE=8.5
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=RubEyesStart        STARTFRAME=163 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=RubEyes             STARTFRAME=167 NUMFRAMES=4   RATE=7.5
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=RubEyesStop         STARTFRAME=171 NUMFRAMES=4   RATE=15 
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=SitBegin            STARTFRAME=175 NUMFRAMES=5   RATE=3  
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=SitStill            STARTFRAME=180 NUMFRAMES=1   RATE=15 
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=SitStand            STARTFRAME=181 NUMFRAMES=5   RATE=3  
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=Pickup              STARTFRAME=186 NUMFRAMES=7   RATE=10 
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=Jump                STARTFRAME=193 NUMFRAMES=3   RATE=10 
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=Land                STARTFRAME=196 NUMFRAMES=3   RATE=10    GROUP=Landing
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=Panic               STARTFRAME=199 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=PushButton          STARTFRAME=209 NUMFRAMES=6   RATE=5     GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=ReloadBegin         STARTFRAME=215 NUMFRAMES=4   RATE=8     GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=Reload              STARTFRAME=219 NUMFRAMES=4   RATE=8     GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=ReloadEnd           STARTFRAME=223 NUMFRAMES=7   RATE=10    GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=Tread               STARTFRAME=230 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=TreadShoot          STARTFRAME=242 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=WaterHitTorso       STARTFRAME=254 NUMFRAMES=3   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=WaterHitTorsoBack   STARTFRAME=257 NUMFRAMES=3   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=WaterDeath          STARTFRAME=260 NUMFRAMES=8   RATE=10 
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=CowerBegin          STARTFRAME=268 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=CowerStill          STARTFRAME=272 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=CowerEnd            STARTFRAME=273 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=AttackSide          STARTFRAME=277 NUMFRAMES=13  RATE=20 
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=Strafe              STARTFRAME=290 NUMFRAMES=9   RATE=18 
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=Walk2H              STARTFRAME=299 NUMFRAMES=10  RATE=10 
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=Strafe2H            STARTFRAME=309 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=RunShoot2H          STARTFRAME=319 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=Shoot2H             STARTFRAME=329 NUMFRAMES=7   RATE=20    GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=BreatheLight2H      STARTFRAME=336 NUMFRAMES=5   RATE=2.5   GROUP=Waiting
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=Idle1               STARTFRAME=341 NUMFRAMES=8   RATE=3     GROUP=Waiting
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=Idle12H             STARTFRAME=349 NUMFRAMES=8   RATE=3     GROUP=Waiting
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=Shocked             STARTFRAME=357 NUMFRAMES=4   RATE=18    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Trench_Inverted SEQ=SitBreathe          STARTFRAME=361 NUMFRAMES=5   RATE=2.5   GROUP=Waiting

#exec MESHMAP SCALE MESHMAP=GM_Trench_Inverted X=-0.00390625 Y=0.00390625 Z=0.00390625

#exec MESH NOTIFY MESH=GM_Trench_Inverted SEQ=Walk          TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_Inverted SEQ=Walk          TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_Inverted SEQ=Run           TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_Inverted SEQ=Run           TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_Inverted SEQ=RunShoot      TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_Inverted SEQ=RunShoot      TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_Inverted SEQ=Panic         TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_Inverted SEQ=Panic         TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_Inverted SEQ=Strafe        TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_Inverted SEQ=Strafe        TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_Inverted SEQ=Walk2H        TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_Inverted SEQ=Walk2H        TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_Inverted SEQ=Strafe2H      TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_Inverted SEQ=Strafe2H      TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_Inverted SEQ=RunShoot2H    TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_Inverted SEQ=RunShoot2H    TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_Inverted SEQ=DeathFront    TIME=0.3    FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GM_Trench_Inverted SEQ=DeathBack     TIME=0.5    FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GM_Trench_Inverted SEQ=Tread         TIME=0.3    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_Inverted SEQ=TreadShoot    TIME=0.3    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_Inverted SEQ=Jump          TIME=0.05   FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_Inverted SEQ=Land          TIME=0.05   FUNCTION=PlayFootStep

// ---------------------------------------- 
// GM_Trench_F
// ---------------------------------------- 
#exec MESH IMPORT MESH=GM_Trench_F_Inverted ANIVFILE=Models\GM_Trench_F_a.3d DATAFILE=Models\GM_Trench_F_d.3d UNMIRROR=1
#exec MESH ORIGIN MESH=GM_Trench_F_Inverted X=0 Y=0 Z=12200 YAW=64
#exec MESH LODPARAMS MESH=GM_Trench_F_Inverted STRENGTH=0.5

#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=All                STARTFRAME=0   NUMFRAMES=315         
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=Still              STARTFRAME=0   NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=Walk               STARTFRAME=1   NUMFRAMES=10  RATE=10 
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=Run                STARTFRAME=11  NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=Crouch             STARTFRAME=21  NUMFRAMES=3   RATE=6     GROUP=Ducking
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=Stand              STARTFRAME=24  NUMFRAMES=3   RATE=6     GROUP=Ducking
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=CrouchShoot        STARTFRAME=27  NUMFRAMES=5   RATE=10    GROUP=Ducking
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=BreatheLight       STARTFRAME=32  NUMFRAMES=5   RATE=2.5   GROUP=Waiting
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=DeathFront         STARTFRAME=37  NUMFRAMES=13  RATE=10 
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=DeathBack          STARTFRAME=50  NUMFRAMES=8   RATE=10 
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=HitHead            STARTFRAME=58  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=HitTorso           STARTFRAME=62  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=HitArmLeft         STARTFRAME=66  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=HitLegLeft         STARTFRAME=70  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=HitArmRight        STARTFRAME=74  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=HitLegRight        STARTFRAME=78  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=HitHeadBack        STARTFRAME=82  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=HitTorsoBack       STARTFRAME=86  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=MouthClosed        STARTFRAME=90  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=MouthA             STARTFRAME=91  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=MouthE             STARTFRAME=92  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=MouthF             STARTFRAME=93  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=MouthM             STARTFRAME=94  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=MouthO             STARTFRAME=95  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=MouthT             STARTFRAME=96  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=MouthU             STARTFRAME=97  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=Blink              STARTFRAME=98  NUMFRAMES=2   RATE=10 
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=Shoot              STARTFRAME=100 NUMFRAMES=6   RATE=15    GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=Attack             STARTFRAME=106 NUMFRAMES=7   RATE=10    GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=HeadUp             STARTFRAME=113 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=HeadDown           STARTFRAME=114 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=HeadLeft           STARTFRAME=115 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=HeadRight          STARTFRAME=116 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=RunShoot           STARTFRAME=117 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=GestureLeft        STARTFRAME=127 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=GestureRight       STARTFRAME=136 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=GestureBoth        STARTFRAME=145 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=RubEyesStart       STARTFRAME=154 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=RubEyes            STARTFRAME=158 NUMFRAMES=4   RATE=7.5
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=RubEyesStop        STARTFRAME=162 NUMFRAMES=4   RATE=15 
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=SitBegin           STARTFRAME=166 NUMFRAMES=5   RATE=3  
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=SitStill           STARTFRAME=171 NUMFRAMES=1   RATE=15 
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=SitStand           STARTFRAME=172 NUMFRAMES=5   RATE=3  
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=Pickup             STARTFRAME=177 NUMFRAMES=7   RATE=10 
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=Jump               STARTFRAME=184 NUMFRAMES=3   RATE=10 
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=Land               STARTFRAME=187 NUMFRAMES=3   RATE=10    GROUP=Landing
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=Panic              STARTFRAME=190 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=PushButton         STARTFRAME=200 NUMFRAMES=6   RATE=5     GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=ReloadBegin        STARTFRAME=206 NUMFRAMES=4   RATE=8     GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=Reload             STARTFRAME=210 NUMFRAMES=4   RATE=8     GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=ReloadEnd          STARTFRAME=214 NUMFRAMES=7   RATE=10    GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=Tread              STARTFRAME=221 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=TreadShoot         STARTFRAME=233 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=WaterHitTorso      STARTFRAME=245 NUMFRAMES=3   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=WaterHitTorsoBack  STARTFRAME=248 NUMFRAMES=3   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=WaterDeath         STARTFRAME=251 NUMFRAMES=8   RATE=10 
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=CowerBegin         STARTFRAME=259 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=CowerStill         STARTFRAME=263 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=CowerEnd           STARTFRAME=264 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=AttackSide         STARTFRAME=268 NUMFRAMES=13  RATE=20 
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=Strafe             STARTFRAME=281 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=Idle1              STARTFRAME=291 NUMFRAMES=15  RATE=6     GROUP=Waiting
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=Shocked            STARTFRAME=306 NUMFRAMES=4   RATE=18    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Trench_F_Inverted SEQ=SitBreathe         STARTFRAME=310 NUMFRAMES=5   RATE=2.5   GROUP=Waiting

#exec MESHMAP SCALE MESHMAP=GM_Trench_F_Inverted X=-0.00390625 Y=0.00390625 Z=0.00390625

#exec MESH NOTIFY MESH=GM_Trench_F_Inverted SEQ=Walk         TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_F_Inverted SEQ=Walk         TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_F_Inverted SEQ=Run          TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_F_Inverted SEQ=Run          TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_F_Inverted SEQ=RunShoot     TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_F_Inverted SEQ=RunShoot     TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_F_Inverted SEQ=Panic        TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_F_Inverted SEQ=Panic        TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_F_Inverted SEQ=Strafe       TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_F_Inverted SEQ=Strafe       TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_F_Inverted SEQ=DeathFront   TIME=0.3    FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GM_Trench_F_Inverted SEQ=DeathBack    TIME=0.5    FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GM_Trench_F_Inverted SEQ=Tread        TIME=0.3    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Trench_F_Inverted SEQ=TreadShoot   TIME=0.3    FUNCTION=PlayFootStep

// --------------------------------------
// GFM_TShirtPants
// --------------------------------------

#exec MESH IMPORT MESH=GFM_TShirtPants_Inverted ANIVFILE=Models\GFM_TShirtPants_a.3d DATAFILE=Models\GFM_TShirtPants_d.3d UNMIRROR=1
#exec MESH ORIGIN MESH=GFM_TShirtPants_Inverted X=0 Y=0 Z=11000 YAW=64
#exec MESH LODPARAMS MESH=GFM_TShirtPants_Inverted STRENGTH=0.5

#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=All                STARTFRAME=0   NUMFRAMES=310         
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=Still              STARTFRAME=0   NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=Walk               STARTFRAME=1   NUMFRAMES=10  RATE=10 
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=Run                STARTFRAME=11  NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=Crouch             STARTFRAME=21  NUMFRAMES=3   RATE=6     GROUP=Ducking
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=Stand              STARTFRAME=24  NUMFRAMES=3   RATE=6     GROUP=Ducking
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=CrouchShoot        STARTFRAME=27  NUMFRAMES=5   RATE=10    GROUP=Ducking
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=BreatheLight       STARTFRAME=32  NUMFRAMES=5   RATE=2.5   GROUP=Waiting
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=DeathFront         STARTFRAME=37  NUMFRAMES=11  RATE=10 
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=DeathBack          STARTFRAME=48  NUMFRAMES=13  RATE=15 
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=HitHead            STARTFRAME=61  NUMFRAMES=3   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=HitTorso           STARTFRAME=64  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=HitArmLeft         STARTFRAME=68  NUMFRAMES=3   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=HitLegLeft         STARTFRAME=71  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=HitArmRight        STARTFRAME=75  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=HitLegRight        STARTFRAME=79  NUMFRAMES=4   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=HitHeadBack        STARTFRAME=83  NUMFRAMES=3   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=HitTorsoBack       STARTFRAME=86  NUMFRAMES=5   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=MouthClosed        STARTFRAME=91  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=MouthA             STARTFRAME=92  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=MouthE             STARTFRAME=93  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=MouthF             STARTFRAME=94  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=MouthM             STARTFRAME=95  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=MouthO             STARTFRAME=96  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=MouthT             STARTFRAME=97  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=MouthU             STARTFRAME=98  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=Blink              STARTFRAME=99  NUMFRAMES=2   RATE=10 
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=Shoot              STARTFRAME=101 NUMFRAMES=6   RATE=14    GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=Attack             STARTFRAME=107 NUMFRAMES=7   RATE=12    GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=HeadUp             STARTFRAME=114 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=HeadDown           STARTFRAME=115 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=HeadLeft           STARTFRAME=116 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=HeadRight          STARTFRAME=117 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=RunShoot           STARTFRAME=118 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=GestureLeft        STARTFRAME=128 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=GestureRight       STARTFRAME=137 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=GestureBoth        STARTFRAME=146 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=RubEyesStart       STARTFRAME=155 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=RubEyes            STARTFRAME=159 NUMFRAMES=4   RATE=7.5
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=RubEyesStop        STARTFRAME=163 NUMFRAMES=4   RATE=15 
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=SitBegin           STARTFRAME=167 NUMFRAMES=7   RATE=10 
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=SitStill           STARTFRAME=174 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=SitStand           STARTFRAME=175 NUMFRAMES=7   RATE=12 
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=Pickup             STARTFRAME=182 NUMFRAMES=6   RATE=10    GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=Jump               STARTFRAME=188 NUMFRAMES=3   RATE=10 
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=Land               STARTFRAME=191 NUMFRAMES=3   RATE=10    GROUP=Landing
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=Panic              STARTFRAME=194 NUMFRAMES=9   RATE=18 
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=PushButton         STARTFRAME=203 NUMFRAMES=6   RATE=5     GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=ReloadBegin        STARTFRAME=209 NUMFRAMES=8   RATE=16    GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=Reload             STARTFRAME=217 NUMFRAMES=4   RATE=8     GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=ReloadEnd          STARTFRAME=221 NUMFRAMES=4   RATE=6     GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=Tread              STARTFRAME=225 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=TreadShoot         STARTFRAME=237 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=WaterHitTorso      STARTFRAME=249 NUMFRAMES=3   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=WaterHitTorsoBack  STARTFRAME=252 NUMFRAMES=3   RATE=10    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=WaterDeath         STARTFRAME=255 NUMFRAMES=6   RATE=10 
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=CowerBegin         STARTFRAME=261 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=CowerStill         STARTFRAME=265 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=CowerEnd           STARTFRAME=266 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=AttackSide         STARTFRAME=270 NUMFRAMES=13  RATE=20 
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=Strafe             STARTFRAME=283 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=Idle1              STARTFRAME=293 NUMFRAMES=8   RATE=3     GROUP=Waiting
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=Shocked            STARTFRAME=301 NUMFRAMES=4   RATE=18    GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_TShirtPants_Inverted SEQ=SitBreathe         STARTFRAME=305 NUMFRAMES=5   RATE=2.5   GROUP=Waiting

#exec MESHMAP SCALE MESHMAP=GFM_TShirtPants_Inverted X=-0.00390625 Y=0.00390625 Z=0.00390625

#exec MESH NOTIFY MESH=GFM_TShirtPants_Inverted SEQ=Walk         TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_TShirtPants_Inverted SEQ=Walk         TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_TShirtPants_Inverted SEQ=Run          TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_TShirtPants_Inverted SEQ=Run          TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_TShirtPants_Inverted SEQ=RunShoot     TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_TShirtPants_Inverted SEQ=RunShoot     TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_TShirtPants_Inverted SEQ=Panic        TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_TShirtPants_Inverted SEQ=Panic        TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_TShirtPants_Inverted SEQ=Strafe       TIME=0.1    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_TShirtPants_Inverted SEQ=Strafe       TIME=0.6    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_TShirtPants_Inverted SEQ=DeathFront   TIME=0.3    FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GFM_TShirtPants_Inverted SEQ=DeathBack    TIME=0.5    FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GFM_TShirtPants_Inverted SEQ=Tread        TIME=0.3    FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_TShirtPants_Inverted SEQ=TreadShoot   TIME=0.3    FUNCTION=PlayFootStep

// ---------------------------------------- 
// GFM_Dress
// ---------------------------------------- 
#exec MESH IMPORT MESH=GFM_Dress_Inverted ANIVFILE=Models\GFM_Dress_a.3d DATAFILE=Models\GFM_Dress_d.3d UNMIRROR=1
#exec MESH ORIGIN MESH=GFM_Dress_Inverted X=0 Y=0 Z=11000 YAW=64
#exec MESH LODPARAMS MESH=GFM_Dress_Inverted STRENGTH=0.5

#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=All					STARTFRAME=0   NUMFRAMES=327
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=Still				STARTFRAME=0   NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=Walk					STARTFRAME=1   NUMFRAMES=10  RATE=10 
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=Run					STARTFRAME=11  NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=Crouch				STARTFRAME=21  NUMFRAMES=3   RATE=6  	GROUP=Ducking
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=Stand				STARTFRAME=24  NUMFRAMES=3   RATE=6  	GROUP=Ducking
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=CrouchShoot			STARTFRAME=27  NUMFRAMES=5   RATE=10 	GROUP=Ducking
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=BreatheLight			STARTFRAME=32  NUMFRAMES=5   RATE=2.5	GROUP=Waiting
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=DeathFront			STARTFRAME=37  NUMFRAMES=11  RATE=10 
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=DeathBack			STARTFRAME=48  NUMFRAMES=13  RATE=15 
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=HitHead				STARTFRAME=61  NUMFRAMES=3   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=HitTorso				STARTFRAME=64  NUMFRAMES=4   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=HitArmLeft			STARTFRAME=68  NUMFRAMES=3   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=HitLegLeft			STARTFRAME=71  NUMFRAMES=4   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=HitArmRight			STARTFRAME=75  NUMFRAMES=4   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=HitLegRight			STARTFRAME=79  NUMFRAMES=4   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=HitHeadBack			STARTFRAME=83  NUMFRAMES=3   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=HitTorsoBack			STARTFRAME=86  NUMFRAMES=5   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=MouthClosed			STARTFRAME=91  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=MouthA				STARTFRAME=92  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=MouthE				STARTFRAME=93  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=MouthF				STARTFRAME=94  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=MouthM				STARTFRAME=95  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=MouthO				STARTFRAME=96  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=MouthT				STARTFRAME=97  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=MouthU				STARTFRAME=98  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=Blink				STARTFRAME=99  NUMFRAMES=2   RATE=10 
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=Shoot				STARTFRAME=101 NUMFRAMES=6   RATE=14 	GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=Attack				STARTFRAME=107 NUMFRAMES=7   RATE=12 	GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=HeadUp				STARTFRAME=114 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=HeadDown				STARTFRAME=115 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=HeadLeft				STARTFRAME=116 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=HeadRight			STARTFRAME=117 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=RunShoot				STARTFRAME=118 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=GestureLeft			STARTFRAME=128 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=GestureRight			STARTFRAME=137 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=GestureBoth			STARTFRAME=146 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=RubEyesStart			STARTFRAME=155 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=RubEyes				STARTFRAME=159 NUMFRAMES=4   RATE=7.5
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=RubEyesStop			STARTFRAME=163 NUMFRAMES=4   RATE=15 
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=SitBegin				STARTFRAME=167 NUMFRAMES=9   RATE=10 
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=SitStill				STARTFRAME=176 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=SitStand				STARTFRAME=177 NUMFRAMES=9   RATE=12 
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=Pickup				STARTFRAME=186 NUMFRAMES=6   RATE=10 	GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=Jump					STARTFRAME=192 NUMFRAMES=3   RATE=10 
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=Land					STARTFRAME=195 NUMFRAMES=3   RATE=10 	GROUP=Landing
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=Panic				STARTFRAME=198 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=PushButton			STARTFRAME=208 NUMFRAMES=6   RATE=5  	GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=ReloadBegin			STARTFRAME=214 NUMFRAMES=8   RATE=16 	GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=Reload				STARTFRAME=222 NUMFRAMES=4   RATE=8  	GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=ReloadEnd			STARTFRAME=226 NUMFRAMES=4   RATE=6  	GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=Tread				STARTFRAME=230 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=TreadShoot			STARTFRAME=242 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=WaterHitTorso		STARTFRAME=254 NUMFRAMES=3   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=WaterHitTorsoBack	STARTFRAME=257 NUMFRAMES=3   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=WaterDeath			STARTFRAME=260 NUMFRAMES=6   RATE=10 
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=CowerBegin			STARTFRAME=266 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=CowerStill			STARTFRAME=270 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=CowerEnd				STARTFRAME=271 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=AttackSide			STARTFRAME=275 NUMFRAMES=13  RATE=20 
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=Strafe				STARTFRAME=288 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=Idle1				STARTFRAME=298 NUMFRAMES=8   RATE=3  	GROUP=Waiting
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=Dance				STARTFRAME=306 NUMFRAMES=12  RATE=10 	GROUP=Waiting
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=Shocked				STARTFRAME=318 NUMFRAMES=4   RATE=18 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_Dress_Inverted SEQ=SitBreathe			STARTFRAME=322 NUMFRAMES=5   RATE=2.5	GROUP=Waiting

#exec MESHMAP SCALE MESHMAP=GFM_Dress_Inverted X=-0.00390625 Y=0.00390625 Z=0.00390625

#exec MESH NOTIFY MESH=GFM_Dress_Inverted SEQ=Walk		TIME=0.1	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_Dress_Inverted SEQ=Walk		TIME=0.6	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_Dress_Inverted SEQ=Run		TIME=0.1	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_Dress_Inverted SEQ=Run		TIME=0.6	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_Dress_Inverted SEQ=RunShoot	TIME=0.1	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_Dress_Inverted SEQ=RunShoot	TIME=0.6	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_Dress_Inverted SEQ=Panic		TIME=0.1	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_Dress_Inverted SEQ=Panic		TIME=0.6	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_Dress_Inverted SEQ=Strafe		TIME=0.1	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_Dress_Inverted SEQ=Strafe		TIME=0.6	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_Dress_Inverted SEQ=DeathFront	TIME=0.3	FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GFM_Dress_Inverted SEQ=DeathBack	TIME=0.5	FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GFM_Dress_Inverted SEQ=Tread		TIME=0.3	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_Dress_Inverted SEQ=TreadShoot	TIME=0.3	FUNCTION=PlayFootStep

// ---------------------------------------- 
// GFM_SuitSkirt
// ---------------------------------------- 
#exec MESH IMPORT MESH=GFM_SuitSkirt_Inverted ANIVFILE=Models\GFM_SuitSkirt_a.3d DATAFILE=Models\GFM_SuitSkirt_d.3d UNMIRROR=1
#exec MESH ORIGIN MESH=GFM_SuitSkirt_Inverted X=0 Y=0 Z=11000 YAW=64
#exec MESH LODPARAMS MESH=GFM_SuitSkirt_Inverted STRENGTH=0.5

#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=All					STARTFRAME=0   NUMFRAMES=327
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=Still				STARTFRAME=0   NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=Walk					STARTFRAME=1   NUMFRAMES=10  RATE=10 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=Run					STARTFRAME=11  NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=Crouch				STARTFRAME=21  NUMFRAMES=3   RATE=6  	GROUP=Ducking
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=Stand				STARTFRAME=24  NUMFRAMES=3   RATE=6  	GROUP=Ducking
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=CrouchShoot			STARTFRAME=27  NUMFRAMES=5   RATE=10 	GROUP=Ducking
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=BreatheLight			STARTFRAME=32  NUMFRAMES=5   RATE=2.5	GROUP=Waiting
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=DeathFront			STARTFRAME=37  NUMFRAMES=11  RATE=10 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=DeathBack			STARTFRAME=48  NUMFRAMES=13  RATE=15 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=HitHead				STARTFRAME=61  NUMFRAMES=3   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=HitTorso				STARTFRAME=64  NUMFRAMES=4   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=HitArmLeft			STARTFRAME=68  NUMFRAMES=3   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=HitLegLeft			STARTFRAME=71  NUMFRAMES=4   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=HitArmRight			STARTFRAME=75  NUMFRAMES=4   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=HitLegRight			STARTFRAME=79  NUMFRAMES=4   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=HitHeadBack			STARTFRAME=83  NUMFRAMES=3   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=HitTorsoBack			STARTFRAME=86  NUMFRAMES=5   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=MouthClosed			STARTFRAME=91  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=MouthA				STARTFRAME=92  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=MouthE				STARTFRAME=93  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=MouthF				STARTFRAME=94  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=MouthM				STARTFRAME=95  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=MouthO				STARTFRAME=96  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=MouthT				STARTFRAME=97  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=MouthU				STARTFRAME=98  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=Blink				STARTFRAME=99  NUMFRAMES=2   RATE=10 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=Shoot				STARTFRAME=101 NUMFRAMES=6   RATE=14 	GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=Attack				STARTFRAME=107 NUMFRAMES=7   RATE=12 	GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=HeadUp				STARTFRAME=114 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=HeadDown				STARTFRAME=115 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=HeadLeft				STARTFRAME=116 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=HeadRight			STARTFRAME=117 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=RunShoot				STARTFRAME=118 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=GestureLeft			STARTFRAME=128 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=GestureRight			STARTFRAME=137 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=GestureBoth			STARTFRAME=146 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=RubEyesStart			STARTFRAME=155 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=RubEyes				STARTFRAME=159 NUMFRAMES=4   RATE=7.5
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=RubEyesStop			STARTFRAME=163 NUMFRAMES=4   RATE=15 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=SitBegin				STARTFRAME=167 NUMFRAMES=9   RATE=10 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=SitStill				STARTFRAME=176 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=SitStand				STARTFRAME=177 NUMFRAMES=9   RATE=12 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=Pickup				STARTFRAME=186 NUMFRAMES=6   RATE=10 	GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=Jump					STARTFRAME=192 NUMFRAMES=3   RATE=10 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=Land					STARTFRAME=195 NUMFRAMES=3   RATE=10 	GROUP=Landing
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=Panic				STARTFRAME=198 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=PushButton			STARTFRAME=208 NUMFRAMES=6   RATE=5  	GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=ReloadBegin			STARTFRAME=214 NUMFRAMES=8   RATE=16 	GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=Reload				STARTFRAME=222 NUMFRAMES=4   RATE=8  	GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=ReloadEnd			STARTFRAME=226 NUMFRAMES=4   RATE=6  	GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=Tread				STARTFRAME=230 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=TreadShoot			STARTFRAME=242 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=WaterHitTorso		STARTFRAME=254 NUMFRAMES=3   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=WaterHitTorsoBack	STARTFRAME=257 NUMFRAMES=3   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=WaterDeath			STARTFRAME=260 NUMFRAMES=6   RATE=10 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=CowerBegin			STARTFRAME=266 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=CowerStill			STARTFRAME=270 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=CowerEnd				STARTFRAME=271 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=AttackSide			STARTFRAME=275 NUMFRAMES=13  RATE=20 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=Strafe				STARTFRAME=288 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=Idle1				STARTFRAME=298 NUMFRAMES=8   RATE=3  	GROUP=Waiting
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=Dance				STARTFRAME=306 NUMFRAMES=12  RATE=10 	GROUP=Waiting
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=Shocked				STARTFRAME=318 NUMFRAMES=4   RATE=18 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_Inverted SEQ=SitBreathe			STARTFRAME=322 NUMFRAMES=5   RATE=2.5	GROUP=Waiting

#exec MESHMAP SCALE MESHMAP=GFM_SuitSkirt_Inverted X=-0.00390625 Y=0.00390625 Z=0.00390625

#exec MESH NOTIFY MESH=GFM_SuitSkirt_Inverted SEQ=Walk		TIME=0.1	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_SuitSkirt_Inverted SEQ=Walk		TIME=0.6	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_SuitSkirt_Inverted SEQ=Run		TIME=0.1	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_SuitSkirt_Inverted SEQ=Run		TIME=0.6	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_SuitSkirt_Inverted SEQ=RunShoot	TIME=0.1	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_SuitSkirt_Inverted SEQ=RunShoot	TIME=0.6	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_SuitSkirt_Inverted SEQ=Panic		TIME=0.1	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_SuitSkirt_Inverted SEQ=Panic		TIME=0.6	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_SuitSkirt_Inverted SEQ=Strafe		TIME=0.1	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_SuitSkirt_Inverted SEQ=Strafe		TIME=0.6	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_SuitSkirt_Inverted SEQ=DeathFront	TIME=0.3	FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GFM_SuitSkirt_Inverted SEQ=DeathBack	TIME=0.5	FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GFM_SuitSkirt_Inverted SEQ=Tread		TIME=0.3	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_SuitSkirt_Inverted SEQ=TreadShoot	TIME=0.3	FUNCTION=PlayFootStep

// ---------------------------------------- 
// GFM_SuitSkirt_F
// ---------------------------------------- 
#exec MESH IMPORT MESH=GFM_SuitSkirt_F_Inverted ANIVFILE=Models\GFM_SuitSkirt_F_a.3d DATAFILE=Models\GFM_SuitSkirt_F_d.3d UNMIRROR=1
#exec MESH ORIGIN MESH=GFM_SuitSkirt_F_Inverted X=0 Y=0 Z=11000 YAW=64
#exec MESH LODPARAMS MESH=GFM_SuitSkirt_F_Inverted STRENGTH=0.5

#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=All				STARTFRAME=0   NUMFRAMES=318         
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=Still				STARTFRAME=0   NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=Walk				STARTFRAME=1   NUMFRAMES=10  RATE=10 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=Run				STARTFRAME=11  NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=Crouch				STARTFRAME=21  NUMFRAMES=3   RATE=6  	GROUP=Ducking
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=Stand				STARTFRAME=24  NUMFRAMES=3   RATE=6  	GROUP=Ducking
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=CrouchShoot		STARTFRAME=27  NUMFRAMES=5   RATE=10 	GROUP=Ducking
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=BreatheLight		STARTFRAME=32  NUMFRAMES=5   RATE=2.5	GROUP=Waiting
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=DeathFront			STARTFRAME=37  NUMFRAMES=11  RATE=10 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=DeathBack			STARTFRAME=48  NUMFRAMES=13  RATE=15 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=HitHead			STARTFRAME=61  NUMFRAMES=3   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=HitTorso			STARTFRAME=64  NUMFRAMES=4   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=HitArmLeft			STARTFRAME=68  NUMFRAMES=3   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=HitLegLeft			STARTFRAME=71  NUMFRAMES=4   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=HitArmRight		STARTFRAME=75  NUMFRAMES=4   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=HitLegRight		STARTFRAME=79  NUMFRAMES=4   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=HitHeadBack		STARTFRAME=83  NUMFRAMES=3   RATE=14 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=HitTorsoBack		STARTFRAME=86  NUMFRAMES=5   RATE=18 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=MouthClosed		STARTFRAME=91  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=MouthA				STARTFRAME=92  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=MouthE				STARTFRAME=93  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=MouthF				STARTFRAME=94  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=MouthM				STARTFRAME=95  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=MouthO				STARTFRAME=96  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=MouthT				STARTFRAME=97  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=MouthU				STARTFRAME=98  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=Blink				STARTFRAME=99  NUMFRAMES=2   RATE=10 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=Shoot				STARTFRAME=101 NUMFRAMES=6   RATE=14 	GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=Attack				STARTFRAME=107 NUMFRAMES=7   RATE=12 	GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=HeadUp				STARTFRAME=114 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=HeadDown			STARTFRAME=115 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=HeadLeft			STARTFRAME=116 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=HeadRight			STARTFRAME=117 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=RunShoot			STARTFRAME=118 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=GestureLeft		STARTFRAME=128 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=GestureRight		STARTFRAME=137 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=GestureBoth		STARTFRAME=146 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=RubEyesStart		STARTFRAME=155 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=RubEyes			STARTFRAME=159 NUMFRAMES=4   RATE=7.5
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=RubEyesStop		STARTFRAME=163 NUMFRAMES=4   RATE=15 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=SitBegin			STARTFRAME=167 NUMFRAMES=7   RATE=10 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=SitStill			STARTFRAME=174 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=SitStand			STARTFRAME=175 NUMFRAMES=7   RATE=12 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=Pickup				STARTFRAME=182 NUMFRAMES=6   RATE=10 	GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=Jump				STARTFRAME=188 NUMFRAMES=3   RATE=10 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=Land				STARTFRAME=191 NUMFRAMES=3   RATE=10 	GROUP=Landing
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=Panic				STARTFRAME=194 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=PushButton			STARTFRAME=204 NUMFRAMES=6   RATE=5  	GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=ReloadBegin		STARTFRAME=210 NUMFRAMES=8   RATE=16 	GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=Reload				STARTFRAME=218 NUMFRAMES=4   RATE=8  	GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=ReloadEnd			STARTFRAME=222 NUMFRAMES=4   RATE=6  	GROUP=Gesture
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=Tread				STARTFRAME=226 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=TreadShoot			STARTFRAME=238 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=WaterHitTorso		STARTFRAME=250 NUMFRAMES=3   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=WaterHitTorsoBack	STARTFRAME=253 NUMFRAMES=3   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=WaterDeath			STARTFRAME=256 NUMFRAMES=6   RATE=10 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=CowerBegin			STARTFRAME=262 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=CowerStill			STARTFRAME=266 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=CowerEnd			STARTFRAME=267 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=AttackSide			STARTFRAME=271 NUMFRAMES=13  RATE=20 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=Strafe				STARTFRAME=284 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=Idle1				STARTFRAME=294 NUMFRAMES=15  RATE=6  	GROUP=Waiting
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=Shocked			STARTFRAME=309 NUMFRAMES=4   RATE=18 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GFM_SuitSkirt_F_Inverted SEQ=SitBreathe			STARTFRAME=313 NUMFRAMES=5   RATE=2.5	GROUP=Waiting

#exec MESHMAP SCALE MESHMAP=GFM_SuitSkirt_F_Inverted X=-0.00390625 Y=0.00390625 Z=0.00390625

#exec MESH NOTIFY MESH=GFM_SuitSkirt_F_Inverted SEQ=Walk			TIME=0.1	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_SuitSkirt_F_Inverted SEQ=Walk			TIME=0.6	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_SuitSkirt_F_Inverted SEQ=Run			TIME=0.1	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_SuitSkirt_F_Inverted SEQ=Run			TIME=0.6	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_SuitSkirt_F_Inverted SEQ=RunShoot		TIME=0.1	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_SuitSkirt_F_Inverted SEQ=RunShoot		TIME=0.6	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_SuitSkirt_F_Inverted SEQ=Panic		TIME=0.1	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_SuitSkirt_F_Inverted SEQ=Panic		TIME=0.6	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_SuitSkirt_F_Inverted SEQ=Strafe		TIME=0.1	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_SuitSkirt_F_Inverted SEQ=Strafe		TIME=0.6	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_SuitSkirt_F_Inverted SEQ=DeathFront	TIME=0.3	FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GFM_SuitSkirt_F_Inverted SEQ=DeathBack	TIME=0.5	FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GFM_SuitSkirt_F_Inverted SEQ=Tread		TIME=0.3	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GFM_SuitSkirt_F_Inverted SEQ=TreadShoot	TIME=0.3	FUNCTION=PlayFootStep

// ---------------------------------------- 
// GM_Suit
// ---------------------------------------- 
#exec MESH IMPORT MESH=GM_Suit_Inverted ANIVFILE=Models\GM_Suit_a.3d DATAFILE=Models\GM_Suit_d.3d UNMIRROR=1
#exec MESH ORIGIN MESH=GM_Suit_Inverted X=0 Y=0 Z=12200 YAW=64
#exec MESH LODPARAMS MESH=GM_Suit_Inverted STRENGTH=0.5

#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=All				STARTFRAME=0   NUMFRAMES=373         
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=Still				STARTFRAME=0   NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=Walk				STARTFRAME=1   NUMFRAMES=10  RATE=10 
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=Run				STARTFRAME=11  NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=Crouch				STARTFRAME=21  NUMFRAMES=3   RATE=6  	GROUP=Ducking
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=Stand				STARTFRAME=24  NUMFRAMES=3   RATE=6  	GROUP=Ducking
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=CrouchShoot		STARTFRAME=27  NUMFRAMES=5   RATE=10 	GROUP=Ducking
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=BreatheLight		STARTFRAME=32  NUMFRAMES=5   RATE=2.5	GROUP=Waiting
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=DeathFront			STARTFRAME=37  NUMFRAMES=13  RATE=10 
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=DeathBack			STARTFRAME=50  NUMFRAMES=8   RATE=10 
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=HitHead			STARTFRAME=58  NUMFRAMES=4   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=HitTorso			STARTFRAME=62  NUMFRAMES=4   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=HitArmLeft			STARTFRAME=66  NUMFRAMES=4   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=HitLegLeft			STARTFRAME=70  NUMFRAMES=4   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=HitArmRight		STARTFRAME=74  NUMFRAMES=4   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=HitLegRight		STARTFRAME=78  NUMFRAMES=4   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=HitHeadBack		STARTFRAME=82  NUMFRAMES=4   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=HitTorsoBack		STARTFRAME=86  NUMFRAMES=4   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=MouthClosed		STARTFRAME=90  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=MouthA				STARTFRAME=91  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=MouthE				STARTFRAME=92  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=MouthF				STARTFRAME=93  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=MouthM				STARTFRAME=94  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=MouthO				STARTFRAME=95  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=MouthT				STARTFRAME=96  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=MouthU				STARTFRAME=97  NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=Blink				STARTFRAME=98  NUMFRAMES=2   RATE=10 
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=Shoot				STARTFRAME=100 NUMFRAMES=6   RATE=15 	GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=Attack				STARTFRAME=106 NUMFRAMES=7   RATE=10 	GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=HeadUp				STARTFRAME=113 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=HeadDown			STARTFRAME=114 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=HeadLeft			STARTFRAME=115 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=HeadRight			STARTFRAME=116 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=RunShoot			STARTFRAME=117 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=GestureLeft		STARTFRAME=127 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=GestureRight		STARTFRAME=136 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=GestureBoth		STARTFRAME=145 NUMFRAMES=9   RATE=20 
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=RubEyesStart		STARTFRAME=154 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=RubEyes			STARTFRAME=158 NUMFRAMES=4   RATE=7.5
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=RubEyesStop		STARTFRAME=162 NUMFRAMES=4   RATE=15 
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=SitBegin			STARTFRAME=166 NUMFRAMES=5   RATE=3  
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=SitStill			STARTFRAME=171 NUMFRAMES=1   RATE=15 
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=SitStand			STARTFRAME=172 NUMFRAMES=5   RATE=3  
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=Pickup				STARTFRAME=177 NUMFRAMES=4   RATE=5  
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=Jump				STARTFRAME=181 NUMFRAMES=3   RATE=10 
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=Land				STARTFRAME=184 NUMFRAMES=3   RATE=10 	GROUP=Landing
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=Panic				STARTFRAME=187 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=PushButton			STARTFRAME=197 NUMFRAMES=6   RATE=5  	GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=ReloadBegin		STARTFRAME=203 NUMFRAMES=4   RATE=8  	GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=Reload				STARTFRAME=207 NUMFRAMES=4   RATE=8  	GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=ReloadEnd			STARTFRAME=211 NUMFRAMES=7   RATE=10 	GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=Tread				STARTFRAME=218 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=TreadShoot			STARTFRAME=230 NUMFRAMES=12  RATE=15 
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=WaterHitTorso		STARTFRAME=242 NUMFRAMES=3   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=WaterHitTorsoBack	STARTFRAME=245 NUMFRAMES=3   RATE=10 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=WaterDeath			STARTFRAME=248 NUMFRAMES=8   RATE=10 
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=CowerBegin			STARTFRAME=256 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=CowerStill			STARTFRAME=260 NUMFRAMES=1           
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=CowerEnd			STARTFRAME=261 NUMFRAMES=4   RATE=20 
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=AttackSide			STARTFRAME=265 NUMFRAMES=13  RATE=20 
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=Strafe				STARTFRAME=278 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=Walk2H				STARTFRAME=288 NUMFRAMES=10  RATE=10 
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=Strafe2H			STARTFRAME=298 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=RunShoot2H			STARTFRAME=308 NUMFRAMES=10  RATE=18 
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=Shoot2H			STARTFRAME=318 NUMFRAMES=7   RATE=20 	GROUP=Gesture
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=BreatheLight2H		STARTFRAME=325 NUMFRAMES=5   RATE=2.5	GROUP=Waiting
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=Idle1				STARTFRAME=330 NUMFRAMES=7   RATE=3  	GROUP=Waiting
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=Idle12H			STARTFRAME=337 NUMFRAMES=15  RATE=6  	GROUP=Waiting
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=Shocked			STARTFRAME=352 NUMFRAMES=4   RATE=18 	GROUP=TakeHit
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=Dance				STARTFRAME=356 NUMFRAMES=12  RATE=18 	GROUP=Waiting
#exec MESH SEQUENCE MESH=GM_Suit_Inverted SEQ=SitBreathe			STARTFRAME=368 NUMFRAMES=5   RATE=2.5	GROUP=Waiting

#exec MESHMAP SCALE MESHMAP=GM_Suit_Inverted X=-0.00390625 Y=0.00390625 Z=0.00390625

#exec MESH NOTIFY MESH=GM_Suit_Inverted SEQ=Walk			TIME=0.1	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Suit_Inverted SEQ=Walk			TIME=0.6	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Suit_Inverted SEQ=Run			TIME=0.1	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Suit_Inverted SEQ=Run			TIME=0.6	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Suit_Inverted SEQ=RunShoot		TIME=0.1	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Suit_Inverted SEQ=RunShoot		TIME=0.6	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Suit_Inverted SEQ=Panic		TIME=0.1	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Suit_Inverted SEQ=Panic		TIME=0.6	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Suit_Inverted SEQ=Strafe		TIME=0.1	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Suit_Inverted SEQ=Strafe		TIME=0.6	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Suit_Inverted SEQ=Walk2H		TIME=0.1	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Suit_Inverted SEQ=Walk2H		TIME=0.6	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Suit_Inverted SEQ=Strafe2H		TIME=0.1	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Suit_Inverted SEQ=Strafe2H		TIME=0.6	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Suit_Inverted SEQ=RunShoot2H	TIME=0.1	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Suit_Inverted SEQ=RunShoot2H	TIME=0.6	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Suit_Inverted SEQ=DeathFront	TIME=0.3	FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GM_Suit_Inverted SEQ=DeathBack	TIME=0.5	FUNCTION=PlayBodyThud
#exec MESH NOTIFY MESH=GM_Suit_Inverted SEQ=Tread		TIME=0.3	FUNCTION=PlayFootStep
#exec MESH NOTIFY MESH=GM_Suit_Inverted SEQ=TreadShoot	TIME=0.3	FUNCTION=PlayFootStep

// END UE1
