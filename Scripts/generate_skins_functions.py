import os, glob, time, subprocess, argparse, re

# Parse arguments
parser = argparse.ArgumentParser("Generate Skins")
parser.add_argument('-e', '--export', type = str, required = False, help = 'Only export the specified file')
args = parser.parse_args()

# Set up directories
script_dir = os.path.dirname(os.path.abspath(__file__))
root_dir = os.path.dirname(script_dir)
skins_dir = os.path.join(root_dir, 'Textures', 'Skins')
return_dir = os.getcwd()
os.chdir(skins_dir)
printed_hello = False

# Inkscape export strings
objects = ''
base_objects = ''
mask_objects = ''
svg_filename = ''

# Library strings
library = {}
gender = ''
age = ''
skin_category = ''
skin_tone = 0
tags = []
category = ''

# Prints a hello message
def print_hello():
    global printed_hello
        
    if printed_hello:
        return
    
    print('')
    print('=======================================')
    print('Generating skins')
    print('=======================================')
    print('')
    
    printed_hello = True

# Export an object with identical id and filename
def export_id(png_filename, scale = 1):
    global base_objects
    global objects

    base_object = ''
    objects = png_filename

    export(png_filename, scale)

# The export function
def export(png_filename, scale = 1):
    global base_objects
    global mask_objects
    global objects
    global library
    global svg_filename
    global age

    png_filename = png_filename.replace('+', '')

    if scale < 1:
        scale = 1

    # Add to library
    if gender and skin_category:
        if not gender in library:
            library[gender] = {}

        if not skin_category in library[gender]:
            library[gender][skin_category] = []

        library_entry = {
            'filename': png_filename,
            'tags': tags,
            'skin_tone': skin_tone,
            'age': age
        }

        library[gender][skin_category].append(library_entry)
    
    # Get .svg file's mtime
    svg_file_mtime = os.path.getmtime(os.path.join('Source', svg_filename + '.svg'))
    
    is_export_name_match = args.export and re.match(re.compile(args.export.replace('*', '.*')), png_filename)

    if not args.export or is_export_name_match:
        # Export mask, if specified and .svg is newer than the .png
        if mask_objects:
            png_file_exists = os.path.isfile(os.path.join(skins_dir, png_filename + '_Mask.png'))
            png_file_mtime = 0

            if png_file_exists:
                png_file_mtime = os.path.getmtime(os.path.join(skins_dir, png_filename + '_Mask.png'))
               
            if is_export_name_match or png_file_mtime < svg_file_mtime:
                print('> ' + png_filename + '_Mask')
                subprocess.run(['flatpak', 'run', 'org.inkscape.Inkscape', '--export-id=' + mask_objects , '--actions=unhide-all;export-dpi:' + str(96 * scale) + ';export-id-only;export-area-page;export-type:png;export-filename:' + png_filename + '_Mask.png;export-do', os.path.join('Source', svg_filename + '.svg')], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        
        # Export file, if .svg is newer than the .png
        png_file_exists = os.path.isfile(png_filename + '.png')
        png_file_mtime = 0

        if png_file_exists:
            png_file_mtime = os.path.getmtime(os.path.join(skins_dir, png_filename + '.png'))
        
        if is_export_name_match or png_file_mtime < svg_file_mtime:
            print('> ' + png_filename)

            export_objects = base_objects

            if export_objects != '':
                export_objects += ';'

            export_objects += objects

            subprocess.run(['flatpak', 'run', 'org.inkscape.Inkscape', '--export-id=' + export_objects, '--actions=unhide-all;export-dpi:' + str(96 * scale) + ';export-id-only;export-area-page;export-type:png;export-filename:' + png_filename + '.png;export-do', os.path.join('Source', svg_filename + '.svg')], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)

# Sets the SVG filename and resets the objects
def set_svg(name):
    global svg_filename
    global base_objects
    global objects
    global mask_objects
    global skin_tone
    global tags
    global category
    global gender
    global age
    global skin_category 

    svg_filename = name
    base_objects = ''
    objects = ''
    mask_objects = ''
    skin_tone  = 0
    tags = []
    category = ''
    gender = ''
    age = ''
    skin_category = ''

# Prints all objects currently in memory
def print_objects():
    global base_objects
    global objects
    global mask_objects

    print('Base objects: ' + base_objects)
    print('Objects: ' + objects)
    print('Mask objects: ' + mask_objects)

# Sets the next mask object to be exported
def set_mask_objects(*objs):
    global mask_objects

    mask_objects = ''
    
    for obj in objs:
        mask_objects += ';' + obj

    mask_objects = mask_objects.lower()

# Sets base objects for the current operation
def set_base_objects(*objs):
    global base_objects
    global objects
    global mask_objects

    base_objects = ''
    objects = ''
    mask_objects = ''

    for obj in objs:
        base_objects += ';' + obj

    base_objects = base_objects.lower()

# Sets objects with a type(s)/variant composition
def set_composed_objects(types, variant, objs, type_suffix = ''):
    global objects
    global mask_objects
    
    mask_objects = ''
    objects = ''

    for t in types.split('+'):
        if type_suffix:
            t += '-' + type_suffix
        
        objects += ';' + t + '-' + variant

        for obj in objs:
            objects += ';' + t + '-' + obj
    
    objects = objects.lower()

# Adds objects with a type(s)/variant composition
def add_composed_objects(types, variant, objs, type_suffix = ''):
    global objects
    
    for t in types.split('+'):
        if type_suffix:
            t += '-' + type_suffix

        objects += ';' + t + '-' + variant

        for obj in objs:
            objects += ';' + t + '-' + obj
    
    objects = objects.lower()

# Sets objects for the current operation
def set_objects(*objs):
    global objects
    global mask_objects

    mask_objects = ''
    objects = ''
    
    for obj in objs:
        objects += ';' + obj
    
    objects = objects.lower()

# Adds objects for the current operation
def add_objects(*objs):
    global objects

    for obj in objs:
        objects += ';' + obj
    
    objects = objects.lower()

# Sets the library group
def set_group(g, sc):
    global gender
    global skin_category

    gender = g
    skin_category = sc

# Sets the library age
def set_age(a):
    global age

    age = a

# Unsets the library age
def unset_age():
    global age

    age = ''

# Unsets the library group, ending the library entries
def unset_group():
    global gender
    global skin_category
    global age

    gender = ''
    skin_category = ''
    age = ''

# Sets the library skin tone
def set_skin_tone(tone):
    global skin_tone

    skin_tone = int(tone)

# Unsets the library skin tone
def unset_skin_tone():
    global skin_tone

    skin_tone = 0

# Sets the library tags
def set_tags(*tag_array):
    global tags

    tags = []

    for tag in tag_array:
        if isinstance(tag, list):
            for t in tag:
                tags.append(t)
        else:
            tags.append(tag)

# Unsets the library tags
def unset_tags():
    global tags

    tags = []

# Sets the library style tags as a set to be interpreted
def set_tag_set(tag_set, variant=0):
    variant = int(variant)

    if isinstance(tag_set, dict):
        if variant == 0:
            set_tags(tag_set.values())
        else:
            set_tags(tag_set[variant])
    else:
        set_tags(tag_set)        

# Builds the library
def build_library():
    # Init imports
    imports = ''
    imports += '// ----------------------------------------\n'
    imports += '// Texture imports\n'
    imports += '// ----------------------------------------\n'
  
    # Init count
    count = 0
    
    # Init texture infos
    textureinfos = ''

    # Populate texture infos and imports
    for g in library:
        textureinfos += '        // ----------------------------------------\n'
        textureinfos += '        // ' + g + '\n'
        textureinfos += '        // ----------------------------------------'

        for sc in library[g]:
            textureinfos += '\n'
            textureinfos += '        // ' + sc + '\n'
            
            for i in range(len(library[g][sc])):
                entry = library[g][sc][i]

                imports += '#exec TEXTURE IMPORT NAME=' + entry['filename'] + ' FILE=Textures\\Skins\\' + entry['filename'] + '.pcx GROUP=Skins\n'
                textureinfos += '        case ' + str(count) + ':\n'
                textureinfos += '            Tex = Texture\'' + entry['filename'] + '\';\n'
                textureinfos += '            Gender = \'' + g + '\';\n'
                textureinfos += '            Age = \'' + entry['age'] + '\';\n'
                textureinfos += '            Category = \'' + sc + '\';\n'

                if entry['skin_tone'] > 0:
                    textureinfos += '            SkinTone = ' + str(entry['skin_tone']) + ';\n'

                for i in range(len(entry['tags'])):
                    textureinfos += '            Tags[' + str(i) + '] = \'' + str(entry['tags'][i]) + '\';\n'
                
                textureinfos += '            break;\n\n'

                count += 1

    content = open(os.path.join(script_dir, 'skin_library_template.uc'), 'r').read()
    content = content.replace('%textureinfos%', textureinfos)
    content = content.replace('%imports%', imports)
    content = content.replace('%numinfos%', str(count))

    file = open(os.path.join(root_dir, 'Classes', 'SkinLibrary.uc'), 'w')
    file.write(content)
    file.close()
