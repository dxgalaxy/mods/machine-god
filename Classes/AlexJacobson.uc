class AlexJacobson extends ScriptedPawnMale;

#exec TEXTURE IMPORT NAME=AlexJacobson_Head FILE=Textures\Skins\AlexJacobson_Head.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=AlexJacobson_Torso FILE=Textures\Skins\AlexJacobson_Torso.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=InfoPortrait_AlexJacobson FILE=Textures\UI\InfoPortrait_AlexJacobson.pcx GROUP=UI MIPS=OFF

defaultproperties
{
    WalkingSpeed=0.296000
    bImportant=True
    bInvincible=True
    Mesh=LodMesh'DeusExCharacters.GM_DressShirt_S'
    MultiSkins(0)=Texture'MachineGod.Skins.AlexJacobson_Head'
    MultiSkins(1)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(2)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(3)=Texture'MachineGod.Skins.Jeans2'
    MultiSkins(4)=None
    MultiSkins(5)=Texture'MachineGod.Skins.AlexJacobson_Torso'
    MultiSkins(6)=Texture'MachineGod.Skins.Frames5'
    MultiSkins(7)=Texture'MachineGod.Skins.Lenses1'
    CollisionRadius=20.000000
    CollisionHeight=47.500000
    BindName="AlexJacobson"
    FamiliarName="Alex Jacobson"
    UnfamiliarName="Hacker"
    PortraitTexture=Texture'MachineGod.UI.InfoPortrait_AlexJacobson'
}
