//=============================================================================
// WarpZoneInfoExtended
// ---
// Ensures that whenever the OtherSideURL is changed,
// the other side is actually connected.
// (Why the hell is this not default behaviour?)
//=============================================================================
class WarpZoneInfoExtended extends WarpZoneInfo;

var(Sound) Sound TransientSound;

// BEGIN UE1

event ActorLeaving(Actor aOther)
{
    Super.ActorLeaving(aOther);

    if(TransientSound != None && OtherSideActor != None && aOther.IsA('Pawn'))
        aOther.PlaySound(TransientSound, SLOT_Pain);
        
}

function Trigger(Actor aOther, Pawn aEventInstigator)
{
    local WarpZoneInfo aOtherSide;
    local DeusExPlayer aPlayer;

    Super.Trigger(aOther, aEventInstigator);

    if(OtherSideActor != None)
    {
        OtherSideActor.OtherSideURL = string(ThisTag);
        OtherSideActor.ForceGenerate();
    }
    
    ForceGenerate();
}

// END UE1
