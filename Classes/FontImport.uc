class FontImport expands Object abstract;

#exec new TrueTypeFontFactory Name=FontHeading1 FontName="Eurostile-Bold" Height=48 CharactersPerPage=16 AntiAlias=1
#exec new TrueTypeFontFactory Name=FontHeading2 FontName="Eurostile-Bold" Height=36 CharactersPerPage=32 AntiAlias=1
#exec new TrueTypeFontFactory Name=FontHeading3 FontName="Eurostile-Bold" Height=24 CharactersPerPage=32 AntiAlias=1
#exec new TrueTypeFontFactory Name=FontBody FontName="Eurostile-Bold" Height=20 CharactersPerPage=32 AntiAlias=1
