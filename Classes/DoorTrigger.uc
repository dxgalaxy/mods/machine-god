//=============================================================================
// DoorTrigger - Opens a door when a pawn is close, closes it again when they leave
//=============================================================================
class DoorTrigger extends Trigger;

function Touch(Actor aOther)
{
    local Mover aMover;
    
    if(Event == '' || !IsRelevant(aOther))
        return;

    if(ReTriggerDelay > 0)
    {
        if(Level.TimeSeconds - TriggerTime < ReTriggerDelay)
            return;

        TriggerTime = Level.TimeSeconds;
    }
    
    foreach AllActors(class'Mover', aMover, Event)
    {
        if(aMover.bOpening)
            continue;

        aMover.DoOpen();
    }

    if(aOther.IsA('Pawn') && Pawn(aOther).SpecialGoal == Self)
        Pawn(aOther).SpecialGoal = None;
            
    if(Message != "")
        aOther.Instigator.ClientMessage(Message);
    
    if(bTriggerOnceOnly)
        SetCollision(False);
    else if(RepeatTriggerTime > 0)
        SetTimer(RepeatTriggerTime, False);
}

function UnTouch(Actor aOther)
{
    local Mover aMover;
    
    if(Event == '' || !IsRelevant(aOther))
        return;

    foreach AllActors(class'Mover', aMover, Event)
        aMover.DoClose();
}
