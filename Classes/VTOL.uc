class VTOL extends Vehicles;

var() float LiftAmount;
var() float DestinationTravelDuration;
var() float LiftTravelDuration;
var() name TriggerState;

var Sound AccelerateSound, StartLoopSound, FlyLoopSound;
var Rotator RotationFrom, RotationTo;
var Vector LocationFrom, LocationTo;
var float SoundPitchFrom, SoundPitchTo, FrameRateFrom, FrameRateTo;
var float SoundPitchDelta, RotationDelta, LocationDelta, FrameRateDelta;
var float SoundPitchTweenDuration, RotationTweenDuration, LocationTweenDuration, FrameRateTweenDuration;
var float SoundPitchTweenTime, RotationTweenTime, LocationTweenTime, FrameRateTweenTime;

// BEGIN UE1

#exec MESH IMPORT MESH=VTOL ANIVFILE=Models\VTOL_a.3d DATAFILE=Models\VTOL_d.3d X=0 Y=0 Z=0 LODSTYLE=10 LODFRAME=0 
#exec MESH ORIGIN MESH=VTOL X=0 Y=0 Z=0 YAW=-64 PITCH=0 ROLL=0

#exec MESHMAP NEW MESHMAP=VTOL MESH=VTOL
#exec MESHMAP SCALE MESHMAP=VTOL X=0.003125 Y=0.003125 Z=0.003125

// END UE1

// BEGIN UE2

#exec MESH IMPORT MESH=VTOL ANIVFILE=Models\VTOL_UNR_a.3d DATAFILE=Models\VTOL_UNR_d.3d X=0 Y=0 Z=0 LODSTYLE=10 LODFRAME=0 
#exec MESH ORIGIN MESH=VTOL X=0 Y=0 Z=0 YAW=-64 PITCH=0 ROLL=0

#exec MESHMAP NEW MESHMAP=VTOL MESH=VTOL
#exec MESHMAP SCALE MESHMAP=VTOL X=0.1 Y=0.1 Z=0.2

// END UE2

#exec MESH SEQUENCE MESH=VTOL SEQ=ALL    STARTFRAME=0 NUMFRAMES=236 RATE=10
#exec MESH SEQUENCE MESH=VTOL SEQ=Still    STARTFRAME=0 NUMFRAMES=1 RATE=10
#exec MESH SEQUENCE MESH=VTOL SEQ=Flying    STARTFRAME=2 NUMFRAMES=1 RATE=10
#exec MESH SEQUENCE MESH=VTOL SEQ=MotorsVertical    STARTFRAME=3 NUMFRAMES=38 RATE=10
#exec MESH SEQUENCE MESH=VTOL SEQ=MotorsHorizontal    STARTFRAME=41 NUMFRAMES=38 RATE=10
#exec MESH SEQUENCE MESH=VTOL SEQ=WheelsDown    STARTFRAME=81 NUMFRAMES=19 RATE=10
#exec MESH SEQUENCE MESH=VTOL SEQ=WheelsUp    STARTFRAME=100 NUMFRAMES=19 RATE=10
#exec MESH SEQUENCE MESH=VTOL SEQ=MotorsTurnLeft    STARTFRAME=118 NUMFRAMES=19 RATE=10
#exec MESH SEQUENCE MESH=VTOL SEQ=MotorsTurnRight    STARTFRAME=138 NUMFRAMES=19 RATE=10
#exec MESH SEQUENCE MESH=VTOL SEQ=MotorsHorizontalFromLeft    STARTFRAME=158 NUMFRAMES=38 RATE=10
#exec MESH SEQUENCE MESH=VTOL SEQ=MotorsHorizontalFromRight    STARTFRAME=198 NUMFRAMES=38 RATE=10

#exec TEXTURE IMPORT NAME=VTOL1 FILE=Textures\Decorations\VTOL1.pcx GROUP=Decorations FLAGS=2
#exec MESHMAP SETTEXTURE MESHMAP=VTOL NUM=0 TEXTURE=VTOL1

#exec TEXTURE IMPORT NAME=VTOL2 FILE=Textures\Decorations\VTOL2.pcx GROUP=Decorations FLAGS=2
#exec MESHMAP SETTEXTURE MESHMAP=VTOL NUM=1 TEXTURE=VTOL2

#exec TEXTURE IMPORT NAME=VTOL3 FILE=Textures\Decorations\VTOL3.pcx GROUP=Decorations FLAGS=2
#exec MESHMAP SETTEXTURE MESHMAP=VTOL NUM=2 TEXTURE=VTOL3

#exec TEXTURE IMPORT NAME=VTOL4_A01 FILE=Textures\Decorations\VTOL4_A01.pcx GROUP=Decorations FLAGS=2
#exec TEXTURE IMPORT NAME=VTOL4_A02 FILE=Textures\Decorations\VTOL4_A02.pcx GROUP=Decorations FLAGS=2

function TweenLocation(Vector From, Vector To, float TweenDuration)
{
    LocationFrom = From;
    LocationTo = To;
    LocationTweenDuration = TweenDuration;
    LocationTweenTime = 0;
    LocationDelta = 0;
}

function TweenRotation(Rotator From, Rotator To, float TweenDuration)
{
    RotationFrom = From;
    RotationTo = To;
    RotationTweenDuration = TweenDuration;
    RotationTweenTime = 0;
    RotationDelta = 0;
}

function TweenFrameRate(float From, float To, float TweenDuration)
{
    FrameRateFrom = From;
    FrameRateTo = To;
    FrameRateTweenDuration = TweenDuration;
    FrameRateTweenTime = 0;
    FrameRateDelta = 0;
}

function SetFrameRate(float To)
{
    TweenFrameRate(To, To, 0);
}

function TweenSoundPitch(float From, float To, float TweenDuration)
{
    SoundPitchFrom = From;
    SoundPitchTo = To;
    SoundPitchTweenDuration = TweenDuration;
    SoundPitchTweenTime = 0;
    SoundPitchDelta = 0;
}

function SetSoundPitch(float To)
{
    TweenSoundPitch(To, To, 0);
}

function Rotator GetRotationToTarget()
{
    local Actor aTarget;
    local Vector Direction;

    foreach AllActors(class'Actor', aTarget, Event)
    {
        Direction = Location - aTarget.Location;
        Direction.Z = 0;

        return Rotator(Direction);
    }

    return Rotation;
}

function Rotator GetTargetRotation()
{
    local Actor aTarget;

    if(Event != '')
    {
        foreach AllActors(class'Actor', aTarget, Event)
        {
            return aTarget.Rotation;
        }
    }

    return Rotation;
}

function Vector GetTargetLocation()
{
    local Actor aTarget;

    if(Event != '')
    {
        foreach AllActors(class'Actor', aTarget, Event)
        {
            return aTarget.Location;
        }
    }

    return Location + Vector(Rotation) * 4096;
}

function float GetSmoothingCoefficient(float Delta)
{
    if ((Delta *= 2.0) < 1.0)
        return 0.5 * Delta * Delta;
    
    Delta -= 1.0;

    return - 0.5 * (Delta * (Delta - 2.0) - 1.0);
}

function Rotator SmerpRotation(float Delta, Rotator From, Rotator To)
{
    local Rotator DiffRot;
    
    DiffRot = To - From;

    if (DiffRot.Pitch >= 32768)
        DiffRot.Pitch = DiffRot.Pitch - 65536;
    else if (DiffRot.Pitch <= -32768)
        DiffRot.Pitch = DiffRot.Pitch + 65536;

    if (DiffRot.Yaw >= 32768)
        DiffRot.Yaw = DiffRot.Yaw - 65536;
    else if (DiffRot.Yaw <= -32768)
        DiffRot.Yaw = DiffRot.Yaw + 65536;

    if (DiffRot.Roll >= 32768)
        DiffRot.Roll = DiffRot.Roll - 65536;
    else if (DiffRot.Roll <= -32768)
        DiffRot.Roll = DiffRot.Roll + 65536;

    return From + GetSmoothingCoefficient(Delta) * DiffRot;
}

function Vector SmerpLocation(float Delta, Vector From, Vector To)
{
    local Vector DiffLoc;
    
    DiffLoc = To - From; 

    return From + GetSmoothingCoefficient(Delta) * DiffLoc;
}

event Tick(float DeltaTime)
{
    Super.Tick(DeltaTime);
    
    if(FrameRateDelta < 1)
    {
        FrameRateTweenTime += DeltaTime;
        FrameRateDelta = FClamp(FrameRateTweenTime / FrameRateTweenDuration, 0.0, 1.0);
        
        if(MultiSkins[2] != None)
        {
            MultiSkins[2].MinFrameRate = Smerp(FrameRateDelta, FrameRateFrom, FrameRateTo);
            MultiSkins[2].MaxFrameRate = MultiSkins[2].MinFrameRate;
        }
    }

    if(SoundPitchDelta < 1)
    {
        SoundPitchTweenTime += DeltaTime;
        SoundPitchDelta = FClamp(SoundPitchTweenTime / SoundPitchTweenDuration, 0.0, 1.0);
        SoundPitch = Smerp(SoundPitchDelta, SoundPitchFrom, SoundPitchTo);
    }
    
    if(RotationDelta < 1)
    {
        RotationTweenTime += DeltaTime;
        RotationDelta = FClamp(RotationTweenTime / RotationTweenDuration, 0.0, 1.0);
        DesiredRotation = SmerpRotation(RotationDelta, RotationFrom, RotationTo);
        SetRotation(DesiredRotation);
    }
    
    if(LocationDelta < 1)
    {
        LocationTweenTime += DeltaTime;
        LocationDelta = FClamp(LocationTweenTime / LocationTweenDuration, 0.0, 1.0);
        SetLocation(SmerpLocation(LocationDelta, LocationFrom, LocationTo));
    }
}

state() Airborne
{
    function Trigger(Actor aOther, Pawn aEventInstigator)
    {
        GoToState('Landing');
    }
}

state() Grounded
{
    function Trigger(Actor aOther, Pawn aEventInstigator)
    {
        GoToState('Liftoff');
    }
}

// TODO: Automatically detect whether we're turning right or left
state Landing
{
    
Begin:
    // Start engines on high rev, facing the target
    AmbientSound = FlyLoopSound;
    MultiSkins[2] = Texture'MachineGod.Decorations.VTOL4_A01';
    SetFrameRate(40);
    SetSoundPitch(32);
    DesiredRotation = GetRotationToTarget();
    SetRotation(DesiredRotation);
    PlayAnim('Flying');

    // Fly towards the target and hover above it
    TweenLocation(Location, GetTargetLocation() + Vect(0, 0, 1) * LiftAmount, DestinationTravelDuration);
    
    Sleep(DestinationTravelDuration - 3.0);
    
    PlayAnim('MotorsVertical');

    Sleep(3.0);
    
    PlayAnim('WheelsDown');
    
    // Rev down engines
    AmbientSound = StartLoopSound;
    TweenFrameRate(40, 20, 4);
    TweenSoundPitch(96, 64, 4);
    
    // Descend and rotate towards final position, wheels down
    TweenRotation(Rotation, GetTargetRotation(), LiftTravelDuration);
    TweenLocation(Location, GetTargetLocation(), LiftTravelDuration);

    Sleep(LiftTravelDuration);
    
    // Stop engines
    TweenFrameRate(20, 0, 6);
    TweenSoundPitch(SoundPitch, 0, 6);
    
    Sleep(6.0);

    MultiSkins[2] = Texture'MachineGod.Decorations.VTOL3';
}

state Liftoff
{

Begin:
    // Rev up engines
    AmbientSound = StartLoopSound;
    TweenSoundPitch(16, 64, 4);
    MultiSkins[2] = Texture'MachineGod.Decorations.VTOL4_A01';
   
    TweenFrameRate(0, 20, 4);

    Sleep(4);

    // Start lift
    TweenLocation(Location, Location + Vect(0, 0, 1) * LiftAmount, LiftTravelDuration);

    Sleep(2);

    // Wheels up
    PlayAnim('WheelsUp');

    Sleep(1);

    // Start turning
    PlayAnim('MotorsTurnRight');
    TweenRotation(Rotation, GetRotationToTarget(), LiftTravelDuration - 2);

    Sleep(LiftTravelDuration - 6);

    // Accelerate
    PlayAnim('MotorsHorizontalFromRight');
    TweenSoundPitch(SoundPitch, 96, 3);
    TweenFrameRate(20, 40, 2);

    Sleep(2);

    PlaySound(AccelerateSound,,,,,1.4);

    TweenLocation(Location, GetTargetLocation(), DestinationTravelDuration);
}

defaultproperties
{
    DestinationTravelDuration=10
    LiftTravelDuration=10
    LiftAmount=384
    
    StartLoopSound=Sound'Ambient.Ambient.Helicopter'
    AccelerateSound=Sound'DeusExSounds.Special.RocketLaunch'
    FlyLoopSound=Sound'DeusExSounds.Special.RocketLoop'
    
    LocationDelta=1
    RotationDelta=1
    SoundPitchDelta=1
   
    InitialState=Airborne
    ItemName="VTOL"
    Mesh=LodMesh'MachineGod.VTOL'
    Texture=Texture'DeusExDeco.Skins.ReflectionMapTex1'
    SoundRadius=160
    SoundVolume=192
    CollisionRadius=0
    CollisionHeight=0
    bCollideActors=False
    bCollideWorld=False
    bBlockPlayers=False
    bBlockActors=False
    Mass=6000.000000
    Buoyancy=1000.000000
    DrawScale=10
    Physics=PHYS_Flying
}
