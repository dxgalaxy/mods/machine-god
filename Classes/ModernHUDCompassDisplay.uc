// ========================================
// ModernHUDConWindowFirst
// ========================================
class ModernHUDCompassDisplay extends HUDCompassDisplay;

var ModernRootWindow Root;
var ModernHUD HUD;
var float MarginTop, TickSize;

// BEGIN UE1

// --------------------------
// Init
// --------------------------
event InitWindow()
{
    Player = DeusExPlayer(DeusExRootWindow(GetRootWindow()).ParentPawn);
    
    StyleChanged();
    
    SetSize(200, TickSize + MarginTop);

    GetMapTrueNorth();
    UnitsPerPixel = 65536 / Width;
}

// ----------------
// Draws the window
// ----------------
event DrawWindow(GC gc)
{
    local float Delta, Center;

    if(Root == None)
        Root = ModernRootWindow(GetRootWindow());

    if(HUD == None)
        HUD = ModernHUD(Root.HUD);
    
    // Draw a shadow
    HUD.DrawShadow(gc, 32, -32, Width - 64, Height + 32, 0.1, 1);

    // Calculate position delta
    if(Player != None)
        Delta = ((Player.Rotation.Yaw - MapNorth) & 65535) / 65535;

    if(Delta > 0.5)
        Delta -= 1.0;
    
    // Draw the letters and lines
    gc.SetFont(Font'FontMenuSmall');
    gc.SetHorizontalAlignment(HALIGN_Center);
    gc.SetVerticalAlignment(VALIGN_Bottom);
    gc.SetStyle(DSTY_Translucent);
    gc.EnableTranslucentText(True);

    DrawTicks(gc, Delta, -4); 

    DrawTick(gc, Delta, -3, "E");
    DrawTicks(gc, Delta, -3); 

    DrawTick(gc, Delta, -2, "S");
    DrawTicks(gc, Delta, -2); 
    
    DrawTick(gc, Delta, -1, "W");
    DrawTicks(gc, Delta, -1); 
    
    DrawTick(gc, Delta, 0, "N");
    DrawTicks(gc, Delta, 0); 
    
    DrawTick(gc, Delta, 1, "E");
    DrawTicks(gc, Delta, 1); 
    
    DrawTick(gc, Delta, 2, "S");
    DrawTicks(gc, Delta, 2); 
    
    DrawTick(gc, Delta, 3, "W");
    DrawTicks(gc, Delta, 3); 
}

// -------------------------------------------
// Draws minor ticks starting from a major one
// -------------------------------------------
function DrawTicks(GC gc, float Delta, float Offset)
{
    local int i;

    for(i = 0; i < 7; i++)
    {
        Offset += 0.125;

        if(Offset % 0.5 == 0)
            DrawTick(gc, Delta, Offset, "|");

        else
            DrawTick(gc, Delta, Offset, "'");
    }

}

// ------------
// Draws a tick
// ------------
function DrawTick(GC gc, float Delta, float Offset, string Text)
{
    local float Pos, ColDelta;
    local Color ColTick;

    // Calculate position
    Pos = (Width / 2) - (Width * Delta);
    Pos += (Width / 4) * Offset;

    // Calculate colour
    ColDelta = 1.0 - (Abs(Pos - (Width / 2)) / (Width / 3));

    if(ColDelta < 0)
        ColDelta = 0;

    else if(ColDelta > 1)
        ColDelta = 1;

    // Fade between text colour and black 
    ColTick = HUD.ColText;
    ColTick.R = Lerp(ColDelta, 0, ColTick.R);
    ColTick.G = Lerp(ColDelta, 0, ColTick.G);
    ColTick.B = Lerp(ColDelta, 0, ColTick.B);
    gc.SetTileColor(ColTick);
    gc.SetTextColor(ColTick);

    if(Text == "|")
    {
        gc.DrawStretchedTexture(Pos - 1, MarginTop, 1, 6, 0, 0, 1, 1, Texture'Solid');
    }
    else if(Text == "'")
    {
        gc.DrawStretchedTexture(Pos - 1, MarginTop, 1, 4, 0, 0, 1, 1, Texture'Solid');
    }
    else
    {
        gc.DrawStretchedTexture(Pos - 1, MarginTop, 1, 8, 0, 0, 1, 1, Texture'Solid');
        gc.DrawText(Pos - (TickSize / 2), MarginTop, TickSize, TickSize, Text);
    }
}

// -----------------
// Stubbed functions
// -----------------
event Tick(float DeltaSeconds) {}
function PostDrawWindow(GC gc) {}

// END UE1

defaultproperties
{
    MarginTop=10
    TickSize=22
}
