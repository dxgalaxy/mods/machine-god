class ModernDataLinkPlay extends DataLinkPlay
    transient;

// BEGIN UE1

// ---------------------
// TerminateConversation
//
// Changes:
// - Left WinName alone
// ---------------------
function TerminateConversation(optional bool bContinueSpeech, optional bool bNoPlayedFlag)
{
    // Make sure sound is no longer playing
    player.StopSound(playingSoundId);

    // Save the DataLink history
    if ((history != None) && (player != None))
    {
        history.next = player.conHistory;
        player.conHistory = history;
        history = None;     // in case we get called more than once!!
    }

    SetTimer(blinkRate, True);

    bEndTransmission = True;

    // Notify the trigger that we've finished
    NotifyDatalinkTrigger();

    Super.TerminateConversation(bContinueSpeech, bNoPlayedFlag);
}

// --------------------
// Timer
//
// Changes:
// - Left WinName alone
// --------------------
function Timer()
{
    eventTimer += blinkRate;

    if ((!bEndTransmission) && (bStartTransmission))
    {
        datalink.ShowDatalinkIcon(!datalink.winPortrait.IsVisible());

        if ( eventTimer > startDelay )
        {
            bStartTransmission = False;
            eventTimer = 0.0;
            SetTimer(0.0, False);

            datalink.ShowTextCursor(True);

            // Play this event!
            GotoState('PlayEvent');
        }
    }
    else if (bEndTransmission)
    {
        if ( eventTimer > endDelay )
        {
            SetTimer(0.0, False);
            bEndTransmission = False;
            rootWindow.hud.DestroyInfoLinkWindow();
            dataLink = None;

            // Check to see if there's another DataLink to trigger
            if ( FireNextDataLink() == False )
            {
                player.dataLinkPlay = None;
                Destroy();
            }
        }
    }
}

// END UE1
