class CarcassExtended extends DeusExCarcass;

var bool bRestore;

// BEGIN UE1

// -------------------------------
// The player frobbed this carcass
// -------------------------------
function Frob(Actor aFrobber, Inventory FrobWith)
{
    local DeusExPlayer aPlayer;
    local CarcassProxy aProxy;

    Super.Frob(aFrobber, FrobWith);
    
    aPlayer = DeusExPlayer(aFrobber);
    
    if(aPlayer == None || aPlayer.InHand != None)
        return;

    aProxy = Spawn(class'CarcassProxy');

    if(aProxy == None)
        return;

    aProxy.InitFor(Self);
    aProxy.GiveTo(aPlayer);
}

// ------------------------------
// Restores the mesh and textures
// ------------------------------
function Restore()
{
    local DeusExPlayer aPlayer;
    local CarcassProxy aProxy;
    local int i;
    
    aPlayer = DeusExPlayer(GetPlayerPawn());
    aProxy = CarcassProxy(aPlayer.FindInventoryType(class'CarcassProxy'));
        
    if(aProxy != None)
    {
        Fatness = aProxy.Fatness;
        DrawScale = aProxy.DrawScale;
        Alliance = aProxy.Alliance;
        Mesh = aProxy.Mesh2; // Dropped corpses are always facing down 
        Mesh2 = aProxy.Mesh2; 
        Mesh3 = aProxy.Mesh3;

        for(i = 0; i < ArrayCount(aProxy.MultiSkins); i++)
        {
            MultiSkins[i] = aProxy.MultiSkins[i];
        }

        Texture = aProxy.Texture;

        aPlayer.RemoveInventoryType(class'CarcassProxy');
    }
}

// ------------------------------------------------
// Adopts values from an actor to match their looks
// ------------------------------------------------
function InitFor(Actor aOther)
{
    local int i;

    bRestore = False;

    if(aOther != None)
    {
        Mesh = LodMesh(DynamicLoadObject("DeusExCharacters." $ aOther.Mesh.Name $ "_Carcass", class'LodMesh'));
        Mesh2 = LodMesh(DynamicLoadObject("DeusExCharacters." $ aOther.Mesh.Name $ "_CarcassB", class'LodMesh'));
        Mesh3 = LodMesh(DynamicLoadObject("DeusExCharacters." $ aOther.Mesh.Name $ "_CarcassC", class'LodMesh'));

        for(i = 0; i < ArrayCount(MultiSkins); i++)
        {
            MultiSkins[i] = aOther.MultiSkins[i];
        }
    
        Texture = aOther.Texture;
    }

    DrawScale = aOther.DrawScale;
    Fatness = aOther.Fatness;

    Super.InitFor(aOther);
}

auto state Dead
{
Begin:
    if(bRestore)
        Restore();

    while(Physics == PHYS_Falling)
    {
        Sleep(1.0);
    }
    
    HandleLanding();
}

// END UE1

defaultproperties
{
     bRestore=True
     CollisionRadius=40.000000
}
