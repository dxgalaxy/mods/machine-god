// ======================================================================================================
// Subclass of Faucet that can be destroyed
// ======================================================================================================
class BreakableFaucet extends Faucet;

var(Events) name DestroyedEvent;
var(Faucet) bool bDestroyOnFrob;
var(Faucet) bool bDisabled;

defaultproperties
{
    bInvincible=False
}

// BEGIN UE1

function Frob(Actor Frobber, Inventory FrobWith)
{
    Super.Frob(Frobber, FrobWith);

    if(bDisabled && waterGen != None)
        waterGen.UnTrigger(Frobber, Pawn(Frobber));

    if(bDestroyOnFrob)
        TakeDamage(HitPoints + 10, Pawn(Frobber), Vect(0, 0, 0), Vect(0, 0, 0), 'Exploded');
}

function Destroyed()
{
    local Actor aActor;
    
    Super.Destroyed(); 

    if(!bDisabled)
        if(DestroyedEvent != '')
            foreach AllActors(class 'Actor', aActor, DestroyedEvent)
                aActor.Trigger(Self, None);
}

// END UE1
