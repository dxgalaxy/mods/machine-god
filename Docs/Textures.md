# Textures

## Naming scheme

To work with a wide variety of generic textures, we will be using a naming scheme

- Heads: Head\_`<Gender><Type>`\_`<AgeGroup><Variant>`\_Skin`<SkinTone>`
- Generic bodies: `<Type><Variant>`\_`<Gender><Model>`\_Skin`<SkinTone>`
- Named bodies: `<Name>`\_`<SkinCategory>`

### Examples

Name                                        | Description
------------------------------------------- | -----------
Head\_Child1\_Skin1                         | Skin tone 1, child variant 1, no gender
Head\_Female\_Young1\_Skin2                 | Skin tone 2, young variant 1, female
Head\_Female\_Mature1\_Skin3                | Skin tone 3, mature variant 1, female
Head\_MaleBalaclava\_Old1\_Skin4            | Skin tone 4, old variant 1, male wearing a balaclava
Jeans2                                      | Any skin tone, jeans variant 2, any model
TankTop4\_MaleJumpsuit\_Skin4               | Skin tone 4, tank top variant 3, male jumpsuit
GarySavage\_Shirt                           | Gary Savage's shirt

## Skin tones

1. #9e735e (Caucasian) - deprecated: #be8869 
2. #835b49 (South east asian)
3. #734f3b (Hispanic/Latino)
4. #35201f (African/Indian/Tamil)


## Texture slots

### GFM\_TShirtPants

0. Head
1. Hair sides
2. Ponytail
3. Frames
4. Lenses
5. ???
6. Legs
7. Torso

### GFM\_Trench

0. Head
1. Torso 
2. Legs
3. ???
4. Shirt
5. Coattail and collar
6. Frames
7. Lenses

### GFM\_SuitSkirt

0. Head
1. Bun
2. ???
3. Legs 
4. Torso
5. Skirt 
6. Frames
7. Lenses

### GFM\_Dress

0. ???
1. Legs
2. Waist
3. Torso
4. Skirt
5. Hair sides
6. Ponytail
7. Head

### GM\_Suit

0. Head
1. Legs
2. ???
3. Torso
4. Skirt
5. Frames
6. Lenses
7. Hat and ponytail

### GM\_DressShirt

0. Head
1. Shade back
2. Shade front
3. Legs
4. ???
5. Torso
6. Frames
7. Lenses

### GM\_Trench

0. Head
1. Torso 
2. Legs
3. ???
4. Shirt
5. Coattail and collar
6. Frames
7. Lenses

### GM\_Jumpsuit

0. ???
1. Legs
2. Torso
3. Head
4. Mask
5. Gogggles
6. Helmet
7. Visor

### GMK\_DressShirt

0. Head
1. Torso
2. Pants
3. ???
4. Cap forward
5. Lower shirt
6. Frames
7. Lenses
