class ElCoyote extends ScriptedPawnMale;

#exec TEXTURE IMPORT NAME=ElCoyote_Head FILE=Textures\Skins\ElCoyote_Head.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=ElCoyote_Torso FILE=Textures\Skins\ElCoyote_Torso.pcx GROUP=Skins

defaultproperties
{
     BindName="ElCoyote"
     FamiliarName="El Coyote"
     UnfamiliarName="Big guy"
     WalkingSpeed=0.300000
     bImportant=True
     bInvincible=False
     BaseAssHeight=-23.000000
     WalkAnimMult=1.050000
     GroundSpeed=200.000000
     Texture=None
     DrawScale=1.2
     Mesh=LodMesh'DeusExCharacters.GM_DressShirt_B'
     MultiSkins(0)=Texture'MachineGod.Skins.ElCoyote_Torso'
     MultiSkins(1)=Texture'MachineGod.Skins.Jeans3'
     MultiSkins(2)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(3)=Texture'MachineGod.Skins.ElCoyote_Head'
     MultiSkins(4)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(5)=Texture'DeusExItems.Skins.GrayMaskTex'
     MultiSkins(6)=Texture'DeusExItems.Skins.BlackMaskTex'
     MultiSkins(7)=Texture'DeusExItems.Skins.BlackMaskTex'
     CollisionRadius=20.000000
     CollisionHeight=57.000000
}
