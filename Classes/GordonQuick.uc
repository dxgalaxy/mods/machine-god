class GordonQuick extends ScriptedPawnMale;

#exec TEXTURE IMPORT NAME=GordonQuick_Head FILE=Textures\Skins\GordonQuick_Head.pcx GROUP=Skins FLAGS=2

defaultproperties
{
    BindName="GordonQuick"
    FamiliarName="Gordon Quick"
    UnfamiliarName="Activist"
    Archetype=AT_Military
    WalkingSpeed=0.300000
    GroundSpeed=200
    bImportant=True
    bInvincible=True
    bFollowPersistently=True
    Mesh=LodMesh'DeusExCharacters.GM_Jumpsuit'
    MultiSkins(0)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(1)=Texture'MachineGod.Skins.TacticalPants3'
    MultiSkins(2)=Texture'MachineGod.Skins.ArmorSweater3_MaleJumpsuit'
    MultiSkins(3)=Texture'MachineGod.Skins.GordonQuick_Head'
    MultiSkins(4)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(5)=Texture'MachineGod.Skins.Goggles_1'
    MultiSkins(6)=Texture'MachineGod.Skins.Goggles'
    MultiSkins(7)=Texture'DeusExItems.Skins.PinkMaskTex'
    Texture=Texture'DeusExItems.Skins.PinkMaskTex'
}
