class PaulDenton extends ScriptedPawnMale;

#exec TEXTURE IMPORT NAME=PaulDenton_Coat FILE=Textures\Skins\PaulDenton_Coat.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=PaulDenton_Head FILE=Textures\Skins\PaulDenton_Head.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=PaulDenton_Legs FILE=Textures\Skins\PaulDenton_Legs.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=PaulDenton_ShirtFront FILE=Textures\Skins\PaulDenton_ShirtFront.pcx GROUP=Skins

defaultproperties
{
    MultiSkins(0)=Texture'MachineGod.Skins.PaulDenton_Head'
    MultiSkins(1)=Texture'MachineGod.Skins.PaulDenton_Coat'
    MultiSkins(2)=Texture'MachineGod.Skins.PaulDenton_Legs'
    MultiSkins(3)=None
    MultiSkins(4)=Texture'MachineGod.Skins.PaulDenton_ShirtFront'
    MultiSkins(5)=Texture'MachineGod.Skins.PaulDenton_Coat'
    MultiSkins(6)=Texture'DeusExItems.Skins.GrayMaskTex'
    MultiSkins(7)=Texture'DeusExItems.Skins.BlackMasktex'
    bInvincible=True
    Mesh=LodMesh'DeusExCharacters.GM_Trench'
    BindName="PaulDenton"
    FamiliarName="Paul Denton"
    UnfamiliarName="Paul Denton"
}
