class RandomScriptedPawnSpawner extends KeyPoint;

struct PawnProperties
{
    var() name Orders;                      // Orders to give the pawn
    var() name OrderTag;                    // OrderTag to give the pawn
    var() int OrderTagSuffixMin;            // Minimum number to append to the OrderTag
    var() int OrderTagSuffixMax;            // Maximum number to append to the OrderTag
    var() int Amount;                       // Amount of pawns to receive these properties
    var() name StyleTag;                    // Clothing style
    var() float HomeExtent;                 // Extent of home base
    var() int SkinTones[3];                 // Allowed skin tones
    var() string BindName;                  // Conversation bind name
    var() bool bAppendCountToBindName;      // Whether to append the pawn count to the bind name
    var int Count;
    var int LastOrderTagSuffix;
};

// Adjustable parameters
var() PawnProperties Groups[10];            // Properties to assign to groups of pawns

// Internal variables
var Vector ValidLocations[100];
var int NumValidLocations;
var ExtensionObject EO;

// BEGIN UE1

// ------------------
// Init all locations
// ------------------
function PostBeginPlay()
{
    local int i;
    local PathNode aPathNode;
    
    // For converting strings to names
    EO = new class'ExtensionObject';

    // Gather up all valid locations
    if(Event != '')
    {
        foreach AllActors(class'PathNode', aPathNode, Event)
        {
            ValidLocations[NumValidLocations] = aPathNode.Location;
            NumValidLocations++;
        }
    }

    // Spawn all the pawns
    for(i = 0; i < ArrayCount(Groups); i++)
    {
        if(Groups[i].Amount < 1)
            continue;

        while(Groups[i].Count < Groups[i].Amount)
        {
            Groups[i].Count++;
            SpawnPawn(Groups[i]);
        }
    }

    Super.PostBeginPlay();
}

// ------------------------------------------------------
// Moves a pawn, and tries to place it nearby if it fails
// ------------------------------------------------------
function MovePawn(RandomScriptedPawn aPawn, Vector Loc)
{
    local Rotator HelpRotator;
    local int HelpDistance;
    local int LoopCount;
    local RandomScriptedPawn aNeighbor;

    HelpRotator = aPawn.Rotation;
    HelpDistance = 8;

    while(LoopCount < 32 && !aPawn.SetLocation(Loc + Normal(Vector(HelpRotator)) * HelpDistance))
    {
        HelpRotator.Yaw += 8192;
        HelpDistance += 8;
        LoopCount++;
    }
   
    aPawn.SetHomeBase(aPawn.Location);

    // If the pawn is standing, make it face a nearby pawn
    // to create group formations
    if(aPawn.Orders == 'Standing')
    {
        foreach AllActors(class'RandomScriptedPawn', aNeighbor)
        {
            if(aPawn.Orders != 'Standing' || VSize(aNeighbor.Location - aPawn.Location) > 256)
                continue;

            aPawn.TurnTowardInstant(aNeighbor);
            aNeighbor.TurnTowardInstant(aPawn);
            break;
        }
    }
}

// -----------------------------------------------
// Issues orders to a pawn and place it in the map
// -----------------------------------------------
function OrderAndPlacePawn(RandomScriptedPawn aPawn, out PawnProperties Properties)
{
    local int i;
    local string OrderTag;
    local int OrderTagSuffix;
    local Vector NewLocation;
    local PatrolPoint aPatrolPoint;
    local Actor aActor;

    if(aPawn == None)
        return;

    // Determine location
    if(NumValidLocations < 1)
        NewLocation = Location;
    else
        NewLocation = ValidLocations[Rand(NumValidLocations)];

    // Assign style tag
    aPawn.StyleTag = Properties.StyleTag;
    
    // Assign orders
    OrderTag = string(Properties.OrderTag);

    // If specified, add a numerical suffix to the order tag
    if(Properties.OrderTagSuffixMin > 0 && Properties.OrderTagSuffixMax > Properties.OrderTagSuffixMin)
    {
        // Increment one since the last suffix
        OrderTagSuffix = Properties.LastOrderTagSuffix + 1; 

        // If we're below minimum or above maximum, reset to minimum
        if(OrderTagSuffix < Properties.OrderTagSuffixMin || OrderTagSuffix > Properties.OrderTagSuffixMax)
            OrderTagSuffix = Properties.OrderTagSuffixMin;

        Properties.LastOrderTagSuffix = OrderTagSuffix;
        
        OrderTag = OrderTag $ OrderTagSuffix;
    }

    aPawn.HomeExtent = Properties.HomeExtent;
    aPawn.Orders = Properties.Orders;
    aPawn.OrderTag = EO.StringToName(OrderTag);
    
    // If the pawn is patrolling, move it to the starting PatrolPoint
    if(aPawn.Orders == 'Patrolling' && aPawn.OrderTag != '')
    {
        foreach AllActors(class'PatrolPoint', aPatrolPoint, aPawn.OrderTag)
        {
            NewLocation = aPatrolPoint.Location;
            break;
        }
    }
    // If the pawn is standing, and an OrderTag has been specified, move it there
    else if(aPawn.Orders == 'Standing' && aPawn.OrderTag != '')
    {
        foreach AllActors(class'Actor', aActor, aPawn.OrderTag)
        {
            NewLocation = aActor.Location;
            break;
        }
    }

    // Move the pawn
    MovePawn(aPawn, NewLocation);
}

// ---------------------------------------------
// Gets a random skin tone from the allowed list
// ---------------------------------------------
function int GetRandomSkinTone(out PawnProperties Properties)
{
    local int i, ValidTones[3], NumValidTones;
    
    for(i = 0; i < 3; i++)
    {
        if(Properties.SkinTones[i] < 1)
            continue;

        ValidTones[NumValidTones] = Properties.SkinTones[i];
        NumValidTones++;       
    }

    if(NumValidTones < 1)
        return 0;

    return ValidTones[Rand(NumValidTones)];
}

// -----------------
// Spawns a new pawn
// -----------------
function SpawnPawn(out PawnProperties Properties)
{
    local int i, GroundSpeedModifier;
    local RandomScriptedPawn aPawn;
    local class<RandomScriptedPawn> SpawnClass;

    // Determine the class
    if(Rand(2) > 0)
        SpawnClass = class'MachineGod.RandomMale';
    else
        SpawnClass = class'MachineGod.RandomFemale';

    // Spawn the pawn
    aPawn = Spawn(SpawnClass);

    // Issue orders and place the pawn
    OrderAndPlacePawn(aPawn, Properties);

    // Generate the pawn's appearance
    aPawn.SkinTone = GetRandomSkinTone(Properties);
    aPawn.StartGeneration();

    // Set the bind name
    if(Properties.BindName != "")
    {
        aPawn.BindName = Properties.BindName;
    }
    else
    {
        aPawn.BindName = string(aPawn.StyleTag);

        if(aPawn.IsA('RandomFemale'))
            aPawn.BindName = aPawn.BindName $ "Female";
        else if(aPawn.IsA('RandomMale'))
            aPawn.BindName = aPawn.BindName $ "Male";
        else if(aPawn.IsA('RandomChild'))
            aPawn.BindName = aPawn.BindName $ "Child";
    }

    if(Properties.bAppendCountToBindName)
        aPawn.BindName = aPawn.BindName $ string(Properties.Count);

    // Mess with the GroundSpeed a little, so everyone is not moving at the same speed
    GroundSpeedModifier = Rand(6);

    if(Rand(2) > 0)
        GroundSpeedModifier *= -1;

    aPawn.GroundSpeed += GroundSpeedModifier;
}

// END UE1
