class MorganEverett extends ScriptedPawnMale;

#exec TEXTURE IMPORT NAME=MorganEverett_Head FILE=Textures\Skins\MorganEverett_Head.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=MorganEverett_ShirtFront FILE=Textures\Skins\MorganEverett_ShirtFront.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=MorganEverett_Coat FILE=Textures\Skins\MorganEverett_Coat.pcx GROUP=Skins

defaultproperties
{
    WalkingSpeed=0.296000
    bImportant=True
    bInvincible=True
    walkAnimMult=0.750000
    GroundSpeed=200.000000
    Mesh=LodMesh'DeusExCharacters.GM_Trench_F'
    MultiSkins(0)=Texture'MachineGod.Skins.MorganEverett_Head'
    MultiSkins(1)=Texture'MachineGod.Skins.MorganEverett_Coat'
    MultiSkins(2)=Texture'MachineGod.Skins.SuitPants2_Skin2'
    MultiSkins(3)=Texture'DeusExItems.Skins.BlackMaskTex'
    MultiSkins(4)=Texture'MachineGod.Skins.MorganEverett_ShirtFront'
    MultiSkins(5)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(6)=Texture'DeusExItems.Skins.GrayMaskTex'
    MultiSkins(7)=Texture'DeusExItems.Skins.BlackMaskTex'
    CollisionRadius=20.000000
    CollisionHeight=47.500000
    BindName="MorganEverett"
    FamiliarName="Morgan Everett"
    UnfamiliarName="Morgan Everett"
}
