# Deus Ex: Machine God

## How to install
Extract/copy the files like this:

    Deus Ex
    └─ MachineGod
       ├─ Maps
       └─ System


## How to play

Run DeusEx.exe with these parameters:
    
    INI="(Deus Ex install dir)/MachineGod/System/DeusEx.ini" USERINI="(Deus Ex install dir)/MachineGod/System/User.ini"


### With Steam:

Set the above as your launch parameters.  


### Without Steam

#### Linux

Run DeusEx.exe through Wine from a terminal and append the above to the end.  


#### Windows

Create a shortcut to DeusEx.exe and use the above in the location field after the "..\System\DeusEx.exe" part.
