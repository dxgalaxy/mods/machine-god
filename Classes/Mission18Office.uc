//=============================================================================
// Mission18 - Office (Simulation), Tijuana, Mexico
//=============================================================================
class Mission18Office extends Mission18;

var Sound WaterRushingSound;

// BEGIN UE1

// -----------------------------------------------------------------------------
// Initialisation
// -----------------------------------------------------------------------------
function StartMission()
{
    Player.HealthHead = 100;
    Player.HealthTorso = 100;
    Player.HealthLegLeft = 100;
    Player.HealthLegRight = 100;
    Player.HealthArmLeft = 100;
    Player.HealthArmRight = 100;
    Player.Health = 100;

    GetPlayer().ClearInventory();
}

function TravelToMap()
{
    Player.HealthHead = 100;
    Player.HealthTorso = 100;
    Player.HealthLegLeft = 100;
    Player.HealthLegRight = 100;
    Player.HealthArmLeft = 100;
    Player.HealthArmRight = 100;
    Player.Health = 100;

    Player.ShowHUD(False);
    SetBrightness(0); 

    MoveActor('Player', 'MeetLarryPlayerConvoPoint');

    if(!Flags.GetBool('Player_Met_Hypnos'))
        StartSequence('Escape1', 'MeetLarry');

    else
        StartSequence('Escape2', 'MeetLarry');
}

function InitMap()
{
    local Faucet aFaucet;
    local Keypad aKeypad;
    local WaterPool aPool;
    local ElectricityEmitter aElectricityEmitter;
    local Actor aActor;
    local DeusExMover aDeusExMover;

    Super.InitMap();

    // Adjust level depending on which iteration we're on
    if(Flags.GetBool('Player_Met_Hypnos'))
    {
        foreach AllActors(class'Actor', aActor, 'FloodValve')
        {
            aActor.bHidden = True;
        }

        foreach AllActors(class'DeusExMover', aDeusExMover, 'Vent')
        {
            aDeusExMover.bBreakable = False;
            aDeusExMover.bHighlight = False;
            aDeusExMover.bFrobbable = False;
        }
        
        foreach AllActors(class'DeusExMover', aDeusExMover, 'MaintenanceDoor')
        {
            aDeusExMover.bBreakable = False;
            aDeusExMover.bHighlight = False;
            aDeusExMover.bFrobbable = False;
        }
        
        foreach AllActors(class'Faucet', aFaucet)
        {
            aFaucet.bHidden = True;
        }

        foreach AllActors(class'Keypad', aKeypad)
        {
            aKeypad.bHidden = True;
        }
    }
    else
    {
        foreach AllActors(class'DeusExMover', aDeusExMover, 'HypnosBlock')
        {
            aDeusExMover.DoOpen();
        }
        
        foreach AllActors(class'ElectricityEmitter', aElectricityEmitter, 'HypnosBlock')
        {
            aElectricityEmitter.TurnOff();
        }
    }
}

// -----------------------------------------------------------------------------
// Recurring functions
// -----------------------------------------------------------------------------
function Tick(float DeltaTime)
{
    Super.Tick(DeltaTime);
    
    if(Flags.GetBool('Flooding') && !Flags.GetBool('Underwater'))
        CheckWaterSurface();
}

function Timer()
{
    local DeusExCarcass aCarcass;

    Super.Timer();

    // Check if Larry Winger was killed
    foreach AllActors(class'DeusExCarcass', aCarcass, 'LarryWinger')
        Flags.SetBool('Player_Killed_LarryWinger', True,, 0);
}

// -----------------------------------------------------------------------------
// Ad hoc functions
// -----------------------------------------------------------------------------
function CheckWaterSurface()
{
    local ZoneInfo aZoneInfo;
    local ParticleGenerator aParticleGenerator;
    local Mover aMover;
    local ScriptedPawn aScriptedPawn;
    local Actor aActor;

    // Check if the flood surface is above the player
    foreach AllActors(class'Mover', aMover, 'FloodSurface')
    {
        if(aMover.Location.Z < Player.Location.Z)
            continue;
        
        Flags.SetBool('Underwater', True);
            
        // Stop and hide all particle generators
        foreach AllActors(class'ParticleGenerator', aParticleGenerator)
        {
            aParticleGenerator.bSpewing = False;
            aParticleGenerator.SoundVolume = 0;
            
            if(aParticleGenerator.Proxy != None)
                aParticleGenerator.Proxy.bHidden = True;
        }

        // Change the ZoneInfo properties to make them water zones
        foreach AllActors(class'ZoneInfo', aZoneInfo, 'FloodZoneInfo')
        {
            aZoneInfo.bWaterZone = True;
            aZoneInfo.ViewFog.X = 0.00;
            aZoneInfo.ViewFog.Y = 0.05;
            aZoneInfo.ViewFog.Z = 0.10;
            aZoneInfo.EntrySound = Sound(DynamicLoadObject("DeusExSounds.Generic.SplashMedium", class'Sound'));
            aZoneInfo.ExitSound = Sound(DynamicLoadObject("DeusExSounds.Generic.WaterOut", class'Sound'));
            aZoneInfo.EntryActor = Class'DeusEx.WaterRing';
            aZoneInfo.ExitActor = Class'DeusEx.WaterRing';
            aZoneInfo.AmbientSound = Sound(DynamicLoadObject("Ambient.Ambient.Underwater", class'Sound'));

            // Notify all actors that they're under water now
            foreach AllActors(class'Actor', aActor)
            {
                aZoneInfo.ActorEntered(aActor);
                aActor.ZoneChange(aZoneInfo);

                aScriptedPawn = ScriptedPawn(aActor);

                if(aScriptedPawn != None)
                {
                    aScriptedPawn.HeadZoneChange(aZoneInfo);
                    aScriptedPawn.FootZoneChange(aZoneInfo);
                    aScriptedPawn.PainTime = aScriptedPawn.UnderWaterTime;
                    aScriptedPawn.SetPhysics(PHYS_Swimming);
                    aScriptedPawn.SetLocation(aScriptedPawn.Location + Vect(0, 0, 32));
                }

                if(aActor.IsA('DeusExPlayer'))
                {
                    Player.HeadZoneChange(aZoneInfo);
                    Player.FootZoneChange(aZoneInfo);
                    Player.PainTime = aScriptedPawn.UnderWaterTime;
                    Player.SetPhysics(PHYS_Swimming);
                }
            }
        }
    }
}

function StartFlooding()
{
    local ParticleGenerator aParticleGenerator;
    local Mover aMover;
    local ScriptedPawn aScriptedPawn;
    local Actor aActor;
                    
    if(Flags.GetBool('Flooding'))
        return;

    Flags.SetBool('Flooding', True);

    foreach AllActors(class'ParticleGenerator', aParticleGenerator, 'FloodEffect')
        aParticleGenerator.Trigger(Player, Player);

    foreach AllActors(class'Actor', aActor, 'FloodValve')
        aActor.SetPhysics(PHYS_Falling);
    
    foreach AllActors(class'Mover', aMover, 'FloodSurface')
        aMover.DoOpen();

    foreach AllActors(class'Actor', aActor, 'WaterFlowSound')
    {
        aActor.AmbientSound = WaterRushingSound;
        aActor.SoundVolume = 64;
    }
}

function bool IsValveDestroyed()
{
    local Actor aActor;

    foreach AllActors(class'Actor', aActor, 'FloodValve')
        return False;

    return True;
}

function StopSpewingWater()
{
    local ParticleGenerator aParticleGenerator;
    local BreakableFaucet aFaucet;
    local Actor aActor;

    foreach AllActors(class'ParticleGenerator', aParticleGenerator)
    {
        aParticleGenerator.bSpewing = False;
        aParticleGenerator.SoundVolume = 0;
        
        if(aParticleGenerator.Proxy != None)
            aParticleGenerator.Proxy.bHidden = True;
    }

    foreach AllActors(class'BreakableFaucet', aFaucet)
    {
        aFaucet.bDisabled = True;
        aFaucet.bOpen = False;
        
        if(aFaucet.WaterGen != None)
            aFaucet.WaterGen.Destroy();
    }
    
    foreach AllActors(class'Actor', aActor, 'WaterFlowSound')
    {
        aActor.AmbientSound = None;
    }
}

// -----------------------------------------------------------------------------
// Sequences
// -----------------------------------------------------------------------------
function UpdateSequence(name Sequence, out name Step)
{
    switch(Sequence)
    {
        case 'Escape1':
            switch(Step)
            {
                case 'MeetLarry':
                    if(
                        IsConversationDone('MeetLarryWinger1') &&
                        !IsActorInState('LarryWinger', 'Conversation')
                    )
                        Step = 'Sabotage';
                    
                    else if(!IsActorInState('LarryWinger', 'Conversation'))
                        StartConversation('LarryWinger', 'MeetLarryWinger1');
                    
                    break;

                case 'Sabotage':
                    if(IsValveDestroyed())
                        Step = 'Flood';

                    break;

                case 'Flood':
                    if(
                        Flags.GetBool('Player_Met_Hypnos') &&
                        !IsActorInState('Hypnos', 'Conversation') && 
                        !IsInState('Rebooting')
                    )
                    {
                        GoToState('Rebooting');
                        Step = 'End';
                    }
                    else if(!Flags.GetBool('Flooding'))      
                    {    
                        StartFlooding();
                    }

                    break;

            }
            break;
        
        case 'Escape2':
            switch(Step)
            {
                case 'MeetLarry':
                    if(
                        IsConversationDone('MeetLarryWinger2') &&
                        !IsActorInState('LarryWinger', 'Conversation')
                    )
                        Step = 'End';
                    
                    else if(
                        !IsActorInState('LarryWinger', 'Conversation') &&
                        !IsActorInState('Player', 'TransitionIn')
                    )
                        StartConversation('LarryWinger', 'MeetLarryWinger2');
                    
                    break;
            }

            break;
    }
}

// -----------------------------------------------------------------------------
// Events
// -----------------------------------------------------------------------------
auto state EventManager
{
FaucetDestroyed:
    Flags.SetBool('Player_Destroyed_Faucets', True,, 18);
    Flags.SetBool('LarryWinger_Intro_Done', False,, 18);

    TriggerActor(StringToName(EventQueue[0].aEventActor.Tag $ "Effect"));
    
    EndEvent();

StopWater:
    Flags.SetBool('Player_Stopped_Water', True,, 18);
    StopSpewingWater();
    
    EndEvent();

OpenElevatorDoor:
    if(Flags.GetBool('Player_Met_Hypnos'))
    {
        Player.TakeDamage(1, Player, Player.Location, Player.Velocity, 'EMP');
        OpenDoor('ElevatorDoor');
    }
    
    EndEvent();
}

// END UE1

defaultproperties
{
    WaterRushingSound=Sound'Ambient.Ambient.WaterRushing'
}
