class StatueGuardianLion extends HongKongDecoration;

// BEGIN UE1

#exec MESH IMPORT MESH=StatueGuardianLion ANIVFILE=Models\StatueGuardianLion_a.3d DATAFILE=Models\StatueGuardianLion_d.3d X=0 Y=0 Z=0 LODSTYLE=10 LODFRAME=0 
#exec MESH ORIGIN MESH=StatueGuardianLion X=0 Y=0 Z=0 YAW=-64 PITCH=0 ROLL=0

#exec MESHMAP NEW MESHMAP=StatueGuardianLion MESH=StatueGuardianLion
#exec MESHMAP SCALE MESHMAP=StatueGuardianLion X=0.003125 Y=0.003125 Z=0.003125

// END UE1

// BEGIN UE2

#exec MESH IMPORT MESH=StatueGuardianLion ANIVFILE=Models\StatueGuardianLion_UNR_a.3d DATAFILE=Models\StatueGuardianLion_UNR_d.3d X=0 Y=0 Z=0 LODSTYLE=10 LODFRAME=0 
#exec MESH ORIGIN MESH=StatueGuardianLion X=0 Y=0 Z=0 YAW=-64 PITCH=0 ROLL=0

#exec MESHMAP NEW MESHMAP=StatueGuardianLion MESH=StatueGuardianLion
#exec MESHMAP SCALE MESHMAP=StatueGuardianLion X=0.1 Y=0.1 Z=0.2

// END UE2

#exec TEXTURE IMPORT NAME=StatueGuardianLion1 FILE=Textures\Decorations\StatueGuardianLion1.pcx GROUP=Decorations
#exec MESHMAP SETTEXTURE MESHMAP=StatueGuardianLion NUM=0 TEXTURE=StatueGuardianLion1

#exec TEXTURE IMPORT NAME=StatueGuardianLion2 FILE=Textures\Decorations\StatueGuardianLion2.pcx GROUP=Decorations
#exec MESHMAP SETTEXTURE MESHMAP=StatueGuardianLion NUM=1 TEXTURE=StatueGuardianLion2

#exec MESH SEQUENCE MESH=StatueGuardianLion SEQ=ALL                   STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=StatueGuardianLion SEQ=Still                 STARTFRAME=0    NUMFRAMES=1     RATE=10

defaultproperties
{
    Mesh=LodMesh'MachineGod.StatueGuardianLion'
    CollisionHeight=90
    CollisionRadius=80
    bPushable=False
    bInvincible=True
}
