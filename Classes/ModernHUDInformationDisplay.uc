// ========================================
// ModernHUDConWindowFirst
//
// Changes:
// - Larger font
// - Black translucent background
// - Use theme font colour
// ========================================
class ModernHUDInformationDisplay extends HUDInformationDisplay;

var ModernRootWindow Root;
var ModernHUD HUD;

// BEGIN UE1

function TextWindow AddTextWindow()
{
    local TextWindow winText;

    // Create the Text window containing the message text
    winText = TextWindow(winTile.NewChild(Class'TextWindow'));
    winText.SetFont(fontInfo);
    winText.SetTextColor(colText);
    winText.SetWordWrap(True);
    winText.SetTextAlignments(HALIGN_Left, VALIGN_Top); 
    winText.SetTextMargins(12, 0);

    AskParentForReconfigure();

    return winText;
}

event DrawWindow(GC gc)
{
    if(Root == None)
        Root = ModernRootWindow(GetRootWindow());
    
    if(HUD == None)
        HUD = ModernHUD(Root.HUD);
   
    HUD.DrawHeavyDialog(gc, 0, 0, Width, Height);
}

event ParentRequestedPreferredSize(bool bWidthSpecified, out float preferredWidth, bool bHeightSpecified, out float preferredHeight)
{
    if(preferredWidth > 800)
    {
        preferredWidth = 800;
        bWidthSpecified = True;
    }

    Super.ParentRequestedPreferredSize(bWidthSpecified, preferredWidth, bHeightSpecified, preferredHeight);
}

// END UE1

defaultproperties
{
    FontInfo=Font'FontLocation'
    TopMargin=24
    BottomMargin=24
}
