//=============================================================================
// MissionScriptExtended - Useful helper functions for mission scripts
//=============================================================================
class MissionScriptExtended extends MissionScript;

struct FlagEvent
{
    var name FlagName;
    var name EventName;
    var Actor aEventActor;
    var Pawn aEventInstigator;
};

struct QueuedEvent
{
    var name EventName;
    var Actor aEventActor;
    var Pawn aEventInstigator;
};

var QueuedEvent EventQueue[100];
var FlagEvent FlagEvents[100];
var HumanExtended aPlayerExtended;
var name Sequences[100];

// BEGIN UE1

// --------------
// Initialisation
// --------------
function InitStateMachine()
{
    Super.InitStateMachine();

    InitMap();

    InitSequences();
}

function InitSequences()
{
    local name FlagName;
    local name LastFlagName;
    local string FlagStringName;
    local int FlagIterator;

    FlagIterator = Flags.CreateIterator();
    
    do 
    {
        Flags.GetNextFlagName(FlagIterator, FlagName);

        if(LastFlagName != '')
        {
            FlagStringName = "" $ LastFlagName;

            if(InStr(FlagStringName, "Sequence_") > -1 && InStr(FlagStringName, "_Step") > -1)
                StartSequence(StringToName(ReplaceString(ReplaceString(FlagStringName, "Sequence_", ""), "_Step", "")));
        }

        LastFlagName = FlagName;

    } until(LastFlagName == '')
}

// ----------------------
// Tick, runs every frame
// ----------------------
function Tick(float DeltaTime)
{
    local int i;
    local name NewSequenceStep, OldSequenceStep;

    Super.Tick(DeltaTime);

    if(Flags != None)
    {
        // Update flag events
        for(i = 0; i < ArrayCount(FlagEvents); i++)
        {
            if(
                FlagEvents[i].FlagName == '' ||
                FlagEvents[i].EventName == '' ||
                !Flags.GetBool(FlagEvents[i].FlagName)
            )
                continue;

            QueueEvent(FlagEvents[i].EventName);
            FlagEvents[i].FlagName = '';
            FlagEvents[i].EventName = '';
        }

        // Update sequences    
        for(i = 0; i < ArrayCount(Sequences); i++)
        {
            if(Sequences[i] == '')
                break;
           
            NewSequenceStep = GetCurrentSequenceStep(Sequences[i]);
            OldSequenceStep = NewSequenceStep;

            UpdateSequence(Sequences[i], NewSequenceStep);
           
            // Step was changed
            if(NewSequenceStep != OldSequenceStep)
            {
                // If the new step is "End", stop updating
                if(NewSequenceStep == 'End')
                {
                    StopSequence(Sequences[i]);
                    UpdateSequence(Sequences[i], NewSequenceStep); 
                }

                // Otherwise, just change the step and fire again
                else
                {
                    SetCurrentSequenceStep(Sequences[i], NewSequenceStep);
                    UpdateSequence(Sequences[i], NewSequenceStep); 
                }
            }
        }
    }
}

// ------------------------------------------------------------
// Updates a sequence, the current step being subject to change
// ------------------------------------------------------------
function UpdateSequence(name Sequence, out name Step) {}

// -------------------------
// Stops tracking a sequence
// -------------------------
function StopSequence(name SequenceName)
{
    local int i, NumSequences;
    local name NewSequences[100];

    // Populate a new array with all sequence names except for
    // the one we want to stop tracking
    for(i = 0; i < ArrayCount(Sequences); i++)
    {
        if(Sequences[i] == '' || Sequences[i] == SequenceName)
            continue;

        NewSequences[NumSequences] = Sequences[i];
        NumSequences++;
    }
   
    // Overwrite the sequences array with the new one 
    for(i = 0; i < ArrayCount(NewSequences); i++)
    {
        Sequences[i] = NewSequences[i];
    }

    // Delete the step flag
    Flags.DeleteFlag(StringToName("Sequence_" $ SequenceName $ "_Step"), FLAG_Name);

    // Set a boolean flag
    Flags.SetBool(StringToName("Sequence_" $ SequenceName $ "_Ended"), True);
}

// --------------------------
// Starts tracking a sequence
// --------------------------
function StartSequence(name SequenceName, optional name SequenceStepName)
{
    local int i;

    for(i = 0; i < ArrayCount(Sequences); i++)
    {
        if(Sequences[i] != '')
            continue;

        Sequences[i] = SequenceName;
        break;
    }

    if(SequenceStepName != '')
        SetCurrentSequenceStep(SequenceName, SequenceStepName);
}

// -----------------------------------
// Gets the current step in a sequence
// -----------------------------------
function name GetCurrentSequenceStep(name SequenceName)
{
    local name StepName;

    StepName = Flags.GetName(StringToName("Sequence_" $ SequenceName $ "_Step"));

    if(StepName == '')
        StepName = 'Begin';

    return StepName;
}

// ---------------------------------------------------------
// Gets the amount of seconds since last current step change
// ---------------------------------------------------------
function float GetSequenceChangeTime(name SequenceName)
{
    return FMax(0, Level.TimeSeconds - Flags.GetFloat(StringToName("Sequence_" $ SequenceName $ "_StepChanged")));
}

// -----------------------------------
// Sets the current step in a sequence
// -----------------------------------
function SetCurrentSequenceStep(name SequenceName, name SequenceStepName)
{
    if(SequenceName == '' || SequenceStepName == '' || GetCurrentSequenceStep(SequenceName) == SequenceStepName)
        return;

    Flags.SetName(StringToName("Sequence_" $ SequenceName $ "_Step"), SequenceStepName);    
    Flags.SetFloat(StringToName("Sequence_" $ SequenceName $ "_StepChanged"), Level.TimeSeconds);    
}

// --------------------------
// Hook an event up to a flag
// --------------------------
function BindEventToFlag(name EventName, name FlagName, optional Actor aEventActor, optional Pawn aEventInstigator)
{
    local int i;
    
    for(i = 0; i < ArrayCount(FlagEvents); i++)
    {
        if(
            FlagEvents[i].FlagName != '' &&
            FlagEvents[i].EventName != ''
        )
            continue;

        FlagEvents[i].FlagName = FlagName;
        FlagEvents[i].EventName = EventName;
        FlagEvents[i].aEventActor = aEventActor;
        FlagEvents[i].aEventInstigator = aEventInstigator;
        break;
    }
}

// ---------------------------------------------------------------------------------
// FirstFrame
//
// Changes:
// - Create methods StartMission() and TravelToMap()
// - Display mission start text in the root window
// ---------------------------------------------------------------------------------
function FirstFrame()
{
    local name FlagName;
    local ScriptedPawn aPawn;
    local int i;
    
    Flags.DeleteFlag('PlayerTraveling', FLAG_Bool);

    // Check to see which NPCs should be dead from prevous missions
    foreach AllActors(class'ScriptedPawn', aPawn)
    {
        if(!aPawn.bImportant)
            continue;
        
        FlagName = StringToName(aPawn.BindName $ "_Dead");
        
        if(Flags.GetBool(FlagName))
            aPawn.Destroy();
    }

    // Print the mission startup text only once per map
    FlagName = StringToName("M" $ Caps(DXInfo.MapName) $ "_StartupText");
    
    if(!Flags.GetBool(FlagName) && DXInfo.StartupMessage[0] != "")
    {
        for(i = 0; i < ArrayCount(DXInfo.StartupMessage); i++)
            ModernRootWindow(Player.RootWindow).StartDisplay.AddMessage(DXInfo.StartupMessage[i]);
        
        ModernRootWindow(Player.RootWindow).StartDisplay.StartMessage();
        Flags.SetBool(flagName, True);
    }
        
    FlagName = StringToName("M" $ DXInfo.MissionNumber $ "MissionStart");
    
    if(!Flags.GetBool(FlagName))
    {
        // Remove completed Primary goals and all Secondary goals
        Player.ResetGoals();

        // Remove any Conversation History.
        Player.ResetConversationHistory();

        // Set this flag so we only get in here once per mission.
        Flags.SetBool(FlagName, True);
        
        StartMission();
    }

    TravelToMap();
}

// ------------------------
// Runs once per map travel
// ------------------------
function TravelToMap() {}

// ----------------------
// Runs once per map load
// ----------------------
function InitMap() {}

// ---------------------------
// Runs once per mission start
// ---------------------------
function StartMission() {}

// ---------------------------
// Converts a string to a name
// ---------------------------
function name StringToName(string Input)
{
    if(GetPlayer() == None)
        return '';

    return GetPlayer().RootWindow.StringToName(Input);
}

// --------------------
// Replaces a substring
// --------------------
static function string ReplaceString(coerce string Text, coerce string Replace, coerce string With)
{
    local int i;
    local string Output;

    i = InStr(Text, Replace);
    while (i != -1) {
        Output = Output $ Left(Text, i) $ With;
        Text = Mid(Text, i + Len(Replace));
        i = InStr(Text, Replace);
    }
    Output = Output $ Text;
    return Output;
}

// --------------------
// Gets the player pawn
// --------------------
function HumanExtended GetPlayer()
{
    if(Player == None)
        Player = DeusExPlayer(GetPlayerPawn());

    if(aPlayerExtended == None)
        aPlayerExtended = HumanExtended(Player);

    return aPlayerExtended;
}

// ------------------------------
// Execute steps to set up a test
// ------------------------------
function DoTest(name TestName);

// --------------------
// Plays a sound effect
// --------------------
function PlayTransientSound(name ActorTag, coerce string SoundName, float Volume, float Pitch)
{
    local Actor aActor;
    local Sound LoadedSound;

    if(SoundName == "")
        return;

    aActor = GetActor(ActorTag);

    if(aActor == None)
        return;
                
    LoadedSound = Sound(DynamicLoadObject(SoundName, class'Sound'));

    if(LoadedSound == None)
        return;

    aActor.PlaySound(LoadedSound, , Volume, True, , Pitch);
}

// ---------------------
// Plays a musical track
// ---------------------
function PlayMusic(coerce string MusicName, optional byte Section, optional byte Track, optional EMusicTransition Transition)
{
    local Music MusicData;

    if(MusicName == "")
        return;

    MusicData = Music(DynamicLoadObject(MusicName, class'Music'));

    if(MusicData == None)
        return;

    Player.ClientSetMusic(MusicData, Section, Track, Transition);
}

// ---------------
// Stops the music
// ---------------
function StopMusic()
{
    Player.ClientSetMusic(None, 0, 255, MTRAN_FastFade);
}

// -------------------
// Sets the brightness
// -------------------
function SetBrightness(float To)
{
    ModernRootWindow(GetPlayer().RootWindow).SetBrightness(To);
}

// ---------------------
// Fades the view in/out
// ---------------------
function Fade(float To, float Time)
{
    ModernRootWindow(GetPlayer().RootWindow).Fade(To, Time);
}

// --------------------------
// Gives an items to an actor
// --------------------------
function GiveItem(class<Inventory> ItemClass, name ReceiverTag, optional int Amount)
{
    local Pawn aPawn; 
    local Inventory aItem;
    local int i;

    Amount = Max(1, Amount);
    aPawn = Pawn(GetActor(ReceiverTag));
    aItem = Spawn(ItemClass);
    
    if(aItem.IsA('DeusExAmmo'))
    {
        DeusExAmmo(aItem).AmmoAmount = Amount;
        aItem.Frob(aPawn, None);
    }
    else
    {
        for(i = 0; i < Amount; i++)
        {
            aItem.Frob(aPawn, None);
            
            if(i < Amount - 1)
            {
                aItem.Destroy();
                aItem = Spawn(ItemClass);
            }
        }
    }
}

// -----------------
// Triggers an actor
// -----------------
function TriggerActor(name ActorName)
{
    local Actor aActor;

    foreach AllActors(class'Actor', aActor, ActorName)
    {
        aActor.Trigger(Self, Player);
    }
}

// -----------------
// Untriggers an actor
// -----------------
function UntriggerActor(name ActorName)
{
    local Actor aActor;

    foreach AllActors(class'Actor', aActor, ActorName)
    {
        aActor.Untrigger(Self, Player);
    }
}

// ---------------------------
// Sets the alliance of a pawn
// ---------------------------
function SetPawnAlliance(name PawnTag, name Alliance)
{
    local ScriptedPawn aPawn;

    foreach AllActors(class'ScriptedPawn', aPawn, PawnTag)
    {
        aPawn.SetAlliance(Alliance);
    }
}

// ----------------------------
// Sets the alliance of a group
// ----------------------------
function SetAllianceAlly(name AllianceName, name NewAlly, optional float AllyLevel, optional bool bPermanent, optional bool bHonorPermanence)
{
    local ScriptedPawn aPawn;
    
    if(AllianceName == '')
        return;

    foreach AllActors(class'ScriptedPawn', aPawn)
    {
        if(aPawn.Alliance != AllianceName)
            continue;

        aPawn.ChangeAlly(NewAlly, AllyLevel, bPermanent, bHonorPermanence);
    }
}

// --------------------------
// Changes the ally of a pawn
// --------------------------
function SetPawnAlly(name PawnTag, name NewAlly, optional float AllyLevel, optional bool bPermanent, optional bool bHonorPermanence)
{
    local ScriptedPawn aPawn;

    if(PawnTag == '')
        return;

    foreach AllActors(class'ScriptedPawn', aPawn, PawnTag)
    {
        aPawn.ChangeAlly(NewAlly, AllyLevel, bPermanent, bHonorPermanence);
    }
}

// ----------------------------
// Changes the outfit of a pawn
// ----------------------------
function ChangePawnOutfit(name ActorTag, name OutfitName)
{
    local ScriptedPawnExtended aPawn;

    if(ActorTag == 'Player')
    {
        GetPlayer().ChangeOutfit(string(OutfitName));
    }
    else
    {
        foreach AllActors(class'ScriptedPawnExtended', aPawn, ActorTag)
        {
            aPawn.ChangeOutfit(string(OutfitName));
        }
    }
}

// ---------------------------------
// Gets a reference to a pawn by tag
// ---------------------------------
function ScriptedPawnExtended GetPawn(name ActorTag)
{
    return ScriptedPawnExtended(GetActor(ActorTag));
}

// ------------------------------------------
// Checks if the player is carrying something
// ------------------------------------------
function bool IsPlayerCarrying(name Item)
{
    // Decorations
    if(
        GetPlayer().CarriedDecoration != None &&
        (
            GetPlayer().CarriedDecoration.IsA(Item) || 
            GetPlayer().CarriedDecoration.Tag == Item
        )
    )
        return True;

    return False;
}

// -------------------------------------
// Checks if an actor is in conversation
// -------------------------------------
function bool IsPawnConversing(name PawnTag)
{
    local Actor aPawn;

    if(PawnTag == '')
        return False;

    aPawn = GetActor(PawnTag);

    if(aPawn == None || !aPawn.IsA('Pawn'))
        return False;

    return aPawn.IsInState('Conversation') || aPawn.IsInState('FirstPersonConversation');
}

// --------------------------------
// Checks if an actor is in a state
// --------------------------------
function bool IsActorInState(name ActorTag, name StateName)
{
    local Actor aActor;

    aActor = GetActor(ActorTag);

    if(aActor == None)
        return False;

    return aActor.IsInState(StateName);
}

// ---------------------
// Sets an actor's state
// ---------------------
function SetActorState(name ActorTag, name StateName, optional name LabelName)
{
    local Actor aActor;

    if(ActorTag == '')
        return;

    if(ActorTag == 'Player')
    {
        if(LabelName != '')
            GetPlayer().GoToState(StateName, LabelName);
        else
            GetPlayer().GoToState(StateName);
    }
    else
    {
        foreach AllActors(class'Actor', aActor, ActorTag)
        {
            if(LabelName != '')
                aActor.GoToState(StateName, LabelName);
            else
                aActor.GoToState(StateName);
        }
    }
}

// -----------------------------------
// Gets a reference to an actor by tag
// -----------------------------------
function Actor GetActor(name ActorTag)
{
    local Actor aActor;

    if(ActorTag == 'Player')
        return Player;

    foreach AllActors(class'Actor', aActor, ActorTag)
        return aActor;

    return None;
}

// -------------------------------------
// Gets a reference to an actor by event
// -------------------------------------
function Actor GetActorByEvent(name ActorEvent)
{
    local Actor aActor;

    foreach AllActors(class'Actor', aActor)
        if(aActor.Event == ActorEvent)
            return aActor;

    return None;
}

// -------------
// Opens a Mover
// -------------
function OpenDoor(name TagName)
{
    local Mover aMover;

    if(TagName == '')
        return;

    foreach AllActors(class'Mover', aMover, TagName)
    {
        aMover.DoOpen();
    }
}

// --------------
// Closes a Mover
// --------------
function CloseDoor(name TagName)
{
    local Mover aMover;

    if(TagName == '')
        return;
    
    foreach AllActors(class'Mover', aMover, TagName)
    {
        aMover.DoClose();
    }
}

// ----------------------------------
// Gets the current KeyNum of a mover
// ----------------------------------
function int GetMoverKeyNum(name TagName)
{
    local Mover aMover;

    if(TagName == '')
        return -1;

    foreach AllActors(class'Mover', aMover, TagName)
    {
        return aMover.KeyNum;
    }

    return -1;
}

// --------------------------
// Sets the KeyNum of a mover
// --------------------------
function SetMoverKeyNum(name TagName, int KeyNum)
{
    local Mover aMover;

    if(TagName == '')
        return; 

    foreach AllActors(class'Mover', aMover, TagName)
    {
        if(MultiMover(aMover) != None)
        {
            MultiMover(aMover).SetSeq(KeyNum);
        }
        else if(ElevatorMover(aMover) != None)
        {
            ElevatorMover(aMover).SetSeq(KeyNum);
        }
        else
        {
            aMover.InterpolateTo(KeyNum, 0);
            aMover.KeyNum = KeyNum;
        }
    }
}

// -------------------------
// Checks if a mover is open
// -------------------------
function bool IsDoorOpen(name TagName)
{
    local Mover aMover;

    if(TagName == '')
        return False;

    foreach AllActors(class'Mover', aMover, TagName)
    {
        return aMover.bOpening || aMover.KeyNum > 0;
    }

    return False;
}

// ---------------------------------
// Checks if a DeusExMover is locked
// ---------------------------------
function bool IsDoorLocked(name TagName)
{
    local DeusExMover aMover;
    
    if(TagName == '')
        return False;

    foreach AllActors(class'DeusExMover', aMover, TagName)
    {
        return aMover.bLocked;
    }

    return False;
}

// ------------------------------------
// Sets whether a DeusExMover is locked
// ------------------------------------
function SetDoorLocked(name DoorTag, bool bLocked)
{
    local DeusExMover aMover;

    if(DoorTag == '')
        return;

    foreach AllActors(class'DeusExMover', aMover, DoorTag)
    {
        aMover.bLocked = bLocked;
    }
}

// ---------------------------------------
// Sets the light color values on an actor
// ---------------------------------------
function SetLightColor(name ActorTag, byte Brightness, byte Hue, byte Saturation)
{
    local Actor aActor;

    if(ActorTag == '')
        return;

    foreach AllActors(class'Actor', aActor, ActorTag)
    {
        aActor.LightBrightness = Brightness;
        aActor.LightHue = Hue;
        aActor.LightSaturation = Saturation;
    }
}

// ------------------------------------
// Sets the collision flags of an actor
// ------------------------------------
function SetActorCollision(name ActorTag, bool bColActors, optional bool bBlockActors, optional bool bBlockPlayers)
{
    local Actor aActor;

    if(ActorTag == '')
        return;

    foreach AllActors(class'Actor', aActor, ActorTag)
    {
        aActor.SetCollision(bColActors, bBlockActors, bBlockPlayers);
    }
}

// ----------------------------------
// Changes the event name of an actor
// ----------------------------------
function SetActorEvent(name ActorTag, name EventName)
{
    local Actor aActor;

    foreach AllActors(class'Actor', aActor, ActorTag)
    {
        aActor.Event = EventName;
    }
}

// -----------------
// Destroys an actor
// -----------------
function DestroyActor(name ActorTag)
{
    local Actor aActor;

    foreach AllActors(class'Actor', aActor, ActorTag)
    {
        aActor.Destroy();
    }
}

// --------------------
// Shows/hides an actor
// --------------------
function SetActorHidden(name ActorTag, bool bSetHidden)
{
    local Actor aActor;

    if(ActorTag == '')
        return;

    foreach AllActors(class'Actor', aActor, ActorTag)
        aActor.bHidden = bSetHidden;
}

// -------------------------
// Gets the amount of actors
// -------------------------
function int CountActors(name ActorTag)
{
    local int i;
    local Actor aActor;

    foreach AllActors(class'Actor', aActor, ActorTag)
        i++;

    return i;
}

// ----------------------------------------------------------
// Moves an actor to a new location relative to another actor
// ----------------------------------------------------------
function MoveActorSeamlessly(name TravellerTag, name FromTag, name ToTag)
{
    local Actor aTraveller, aFrom, aTo;

    aTraveller = GetActor(TravellerTag);

    if(aTraveller == None)
        return;

    aFrom = GetActor(FromTag);
    aTo = GetActor(ToTag);

    if(aFrom == None || aTo == None)
        return;

    aTraveller.SetLocation(aTo.Location + (aTraveller.Location - aFrom.Location));
}

// ---------------------------------------------------------------------
// Moves an actor to a location and tries to place it nearby if it fails
// ---------------------------------------------------------------------
function MoveActor(name TravellerTag, name DestinationTag, optional int Distance)
{
    local Actor aTraveller, aDestination;
    local Rotator HelpRotator;
    local int HelpDistance;
    local int LoopCount;

    aTraveller = GetActor(TravellerTag);
    aDestination = GetActor(DestinationTag);

    if(aTraveller == None || aDestination == None)
        return;

    HelpRotator = aDestination.Rotation;
    HelpDistance = Max(16, Distance);

    if(!aTraveller.SetLocation(aDestination.Location))
    {
        while(LoopCount < 100 && !aTraveller.SetLocation(aDestination.Location + Normal(Vector(HelpRotator)) * HelpDistance))
        {
            HelpRotator.Yaw += 8192;
            HelpDistance += 16;
            LoopCount++;
        }
    }
    
    aTraveller.SetRotation(aDestination.Rotation);
    aTraveller.DesiredRotation = aDestination.Rotation;

    if(aTraveller.IsA('DeusExPlayer'))
        DeusExPlayer(aTraveller).ViewRotation = aDestination.Rotation;
}

// ---------------------------------------------------------------
// Moves an actor to a radially offset position from another actor
// ---------------------------------------------------------------
function MoveActorRadial(name TravellerTag, name DestinationTag, int Distance, int MinYaw, int MaxYaw, int Spread)
{
    local Actor aTraveller, aDestination;
    local Rotator HelpRotator;
    local int HelpDistance, LoopCount;

    // Get references to traveller and destination
    aTraveller = GetActor(TravellerTag);
    aDestination = GetActor(DestinationTag);

    if(aTraveller == None || aDestination == None)
        return;
    
    if(Spread < 16)
        Spread = 16;

    if(Distance < 32)
        Distance = 32;

    // Init the help rotator
    HelpRotator = aDestination.Rotation;

    if(aDestination.IsA('DeusExPlayer'))
    {
        HelpRotator = DeusExPlayer(aDestination).ViewRotation;
        HelpRotator.Pitch = 0;
    }

    // Make yaw limits relative to target
    MinYaw = HelpRotator.Yaw + MinYaw;
    MaxYaw = HelpRotator.Yaw + MaxYaw;

    // Clamp rotator to lower limit
    HelpRotator.Yaw = MinYaw;

    // Init the help distance
    HelpDistance = Distance;

    // Keep trying to place the traveller around the destination
    while(LoopCount < 100 && !aTraveller.SetLocation(aDestination.Location + Normal(Vector(HelpRotator)) * HelpDistance))
    {
        HelpRotator.Yaw += Spread;

        if(HelpRotator.Yaw > MaxYaw)
            HelpRotator.Yaw = MinYaw;

        HelpDistance += 16;
        LoopCount++;
    }
   
    // Make the traveller face the destination
    aTraveller.DesiredRotation = Rotator(aDestination.Location - aTraveller.Location);
    aTraveller.SetRotation(aTraveller.DesiredRotation);

    if(aTraveller.IsA('DeusExPlayer'))
        DeusExPlayer(aTraveller).ViewRotation = aTraveller.DesiredRotation;
}

// ----------------------------------------
// Moves an actor in front of another actor
// ----------------------------------------
function MoveActorInFrontOf(name TravellerTag, name DestinationTag, int Distance, optional int Range, optional int Spread)
{
    local int MinYaw, MaxYaw;

    if(Range < 8192)
        Range = 8192;

    MinYaw = -(Range / 2);
    MaxYaw = (Range / 2);
    
    MoveActorRadial(TravellerTag, DestinationTag, Distance, MinYaw, MaxYaw, Spread); 
}

// -----------------------------------
// Moves an actor behind another actor
// -----------------------------------
function MoveActorBehind(name TravellerTag, name DestinationTag, int Distance, optional int Range, optional int Spread)
{
    local int MinYaw, MaxYaw;

    if(Range < 8192)
        Range = 8192;

    MinYaw = 32768 - (Range / 2);
    MaxYaw = 32768 + (Range / 2);
    
    MoveActorRadial(TravellerTag, DestinationTag, Distance, MinYaw, MaxYaw, Spread); 
}

// ------------------------------------
// Moves an actor towards another actor
// ------------------------------------
function MoveActorTowards(name TravellerTag, name DestinationTag, optional int MinDistance)
{
    local Actor aTraveller, aDestination;
    local Vector Direction;
    local int HelpDistance;
    local int LoopCount;

    aTraveller = GetActor(TravellerTag);
    aDestination = GetActor(DestinationTag);

    if(aTraveller == None || aDestination == None)
        return;

    Direction = Normal(aTraveller.Location - aDestination.Location); 
    HelpDistance = Max(16, MinDistance);

    while(LoopCount < 100 && !aTraveller.SetLocation(aDestination.Location + Direction * HelpDistance))
    {
        HelpDistance += 16;
        LoopCount++;
    }
    
    aTraveller.DesiredRotation = Rotator(-Direction);
    aTraveller.SetRotation(aTraveller.DesiredRotation);
}

// --------------------------------------------------------------
// Sets the exact location of an actor (this can potentially fail)
// --------------------------------------------------------------
function SetActorLocation(name ActorTag, Vector NewLocation)
{
    local Actor aActor;

    foreach AllActors(class'Actor', aActor, ActorTag)
    {
        aActor.SetLocation(NewLocation);
    }
}

// ------------------------------------------------
// Prints a message to the log and the player's HUD
// ------------------------------------------------
function Print(string Message)
{
    DeusExPlayer(GetPlayerPawn()).ClientMessage(Message);
    Log(Message);
}

// ------------------------------
// Plays an animation on an actor
// ------------------------------
function PlayAnimation(name ActorTag, name Sequence, optional float Rate, optional float TweenTime)
{
    local Actor aActor;

    if(ActorTag == '')
        return;

    foreach AllActors(class'Actor', aActor, ActorTag)
    {
        if(Rate > 0 && TweenTime > 0)
            aActor.PlayAnim(Sequence, Rate, TweenTime);
        
        else if(Rate > 0)
            aActor.PlayAnim(Sequence, Rate);
        
        else if(TweenTime > 0)
            aActor.PlayAnim(Sequence, , TweenTime);

        else
            aActor.PlayAnim(Sequence);
    }
}

// ----------------------------------------
// Starts an actor on an interpolation path
// ----------------------------------------
function SendActorOnPath(name ActorTag, name PathTag)
{
    local InterpolationPoint aPoint;
    local Actor aActor;

    aActor = GetActor(ActorTag);

    if(aActor == None || aActor.IsA('InterpolationPoint'))
    {
        Print("Failed to send actor \"" $ ActorTag $ "\" on path");
        return;
    }
   
    aActor.Event = PathTag;

    foreach AllActors(class'InterpolationPoint', aPoint, PathTag)
    {
        if(aPoint.Position != 1)
            continue;
        
        aActor.SetCollision(False, False, False);
        aActor.bCollideWorld = False;
        aActor.Target = aPoint;
        aActor.SetPhysics(PHYS_Interpolating);
        aActor.PhysRate = 1.0;
        aActor.PhysAlpha = 0.0;
        aActor.bInterpolating = True;
        aActor.bStasis = False;
        aActor.GoToState('Interpolating');
        break;
    }
}

// ------------------------------
// Makes a pawn drop their weapon
// ------------------------------
function MakePawnDropWeapon(name PawnTag)
{
    local ScriptedPawn aPawn;

    foreach AllActors(class'ScriptedPawn', aPawn, PawnTag)
    {
        aPawn.bKeepWeaponDrawn = False;
        aPawn.DropWeapon();
    }
}

// -----------------------------
// Makes a pawn look at an actor
// -----------------------------
function MakePawnLookAt(name PawnTag, name TargetTag)
{
    local Actor aTarget;
    local ScriptedPawn aPawn;
    
    aTarget = GetActor(TargetTag);

    if(aTarget == None)
        return;
   
    if(PawnTag == 'Player')
    {
        GetPlayer().LookAtActor(aTarget, True, True, True, 0, 1.0);
    }
    else
    {
        aPawn = ScriptedPawn(GetActor(PawnTag));
        
        if(aPawn == None)
            return;

        if(aPawn.bSitting)
        {
            if(aPawn.SeatActor != None)
                aPawn.LookAtActor(aTarget, True, True, True, 0, 1.0, aPawn.SeatActor.Rotation.Yaw + 49152, 5461);
            else
                aPawn.LookAtActor(aTarget, False, True, True, 0, 1.0);
        }
        else
        {
            aPawn.LookAtActor(aTarget, True, True, True, 0, 1.0);
        }
    }
}

// ---------------------------
// Makes an actor face another
// ---------------------------
function TurnActorTowards(name ActorTag, name TargetTag, optional bool bInstant)
{
    local Actor aActor, aTarget;
    local Vector TargetLocation;

    aActor = GetActor(ActorTag);
    aTarget = GetActor(TargetTag);

    if(aActor == None || aTarget == None)
        return;

    TargetLocation = aTarget.Location;
    TargetLocation.Z = aActor.Location.Z;

    aActor.DesiredRotation = Rotator(TargetLocation - aActor.Location);

    if(bInstant)
        aActor.SetRotation(aActor.DesiredRotation);
}

// ---------------------------------------------------------
// Checks whether a pawn is currently following these orders
// ---------------------------------------------------------
function bool IsPawnFollowingOrders(name PawnTag, name Orders, optional name OrderTag)
{
    local ScriptedPawn aPawn;

    aPawn = GetPawn(PawnTag);

    if(aPawn == None)
        return False;

    return aPawn.Orders == Orders && (OrderTag == '' || aPawn.OrderTag == OrderTag);
}

// ---------------------------
// Makes a pawn frob something
// ---------------------------
function MakePawnFrob(name PawnTag, name FrobTag)
{
    OrderPawn(PawnTag, 'Frobbing', FrobTag);
}

// ----------------------------------------
// Makes a pawn sit on a seat, no fallbacks
// ----------------------------------------
function MakePawnSit(name PawnTag, name SeatTag)
{
    local ScriptedPawn aPawn;
    local Seat aSeat;

    aPawn = GetPawn(PawnTag);
    aSeat = Seat(GetActor(SeatTag));

    if(aPawn == None || aSeat == None)
        return;

    aPawn.SetOrders('Sitting', SeatTag, True);
    aPawn.SeatHack = aSeat;
    aPawn.bUseFirstSeatOnly = True;
    aPawn.bSeatHackUsed = True;
    aPawn.bSitAnywhere = True;
}

// ------------------------
// Assigns orders to a pawn
// ------------------------
function OrderPawn(name PawnTag, name Orders, optional name OrderTag)
{
    local ScriptedPawn aPawn;

    foreach AllActors(class'ScriptedPawn', aPawn, PawnTag)
    {
        aPawn.SetOrders(Orders, OrderTag, True);
    }
}

// ----------------------------------------------------------------
// Assigns orders to a pawn, but waits for the current state to end
// ----------------------------------------------------------------
function OrderPawnQueued(name PawnTag, name Orders, optional name OrderTag)
{
    local ScriptedPawn aPawn;

    foreach AllActors(class'ScriptedPawn', aPawn, PawnTag)
    {
        aPawn.SetOrders(Orders, OrderTag, False);
    }
}

// ---------------------------------------------
// Checks whether a pawn reached its destination
// ---------------------------------------------
function bool IsPawnStandingAt(name PawnTag, name ActorTag)
{
    if(!IsActorInState(PawnTag, 'Standing'))
        return False;

    return AreActorsOverlapping(PawnTag, ActorTag);
}

// -----------------------------------------------------------------------
// Checks whether an actor is within the collision radius of another actor
// -----------------------------------------------------------------------
function bool AreActorsOverlapping(name Actor1Tag, name Actor2Tag, optional float Radius)
{
    local Actor aActor1, aActor2;

    if(Actor1Tag == '' || Actor2Tag == '')
        return False;

    if(Actor1Tag == 'Player')
    {
        aActor1 = GetPlayer();
        
        foreach AllActors(class'Actor', aActor2, Actor2Tag)
        {
            if(
                aActor1.IsOverlapping(aActor2) &&
                (
                    Radius == 0.0 ||
                    VSize(aActor1.Location - aActor2.Location) <= Radius
                )
            )
                return True;
        }
    }
    else if(Actor2Tag == 'Player')
    {
        aActor2 = GetPlayer();
        
        foreach AllActors(class'Actor', aActor1, Actor1Tag)
        {
            if(
                aActor1.IsOverlapping(aActor2) &&
                (
                    Radius == 0.0 ||
                    VSize(aActor1.Location - aActor2.Location) <= Radius
                )
            )
                return True;
        }
    }
    else
    {
        foreach AllActors(class'Actor', aActor1, Actor1Tag)
        {
            foreach AllActors(class'Actor', aActor2, Actor2Tag)
            {
                if(
                    aActor1.IsOverlapping(aActor2) &&
                    (
                        Radius == 0.0 ||
                        VSize(aActor1.Location - aActor2.Location) <= Radius
                    )
                )
                    return True;
            }
        }
    }

    return False;
}

// ----------------------------------
// Gets the distance between 2 actors
// ----------------------------------
function float GetActorDistance(name Actor1Tag, name Actor2Tag)
{
    local Actor aActor1, aActor2;

    aActor1 = GetActor(Actor1Tag);
    aActor2 = GetActor(Actor2Tag);

    if(aActor1 == None || aActor2 == None)
        return 0.0;

    return VSize(aActor1.Location - aActor2.Location);
}

// --------------------------------------------
// Sets an arbitrary property value on an actor
// --------------------------------------------
function SetActorProperty(name ActorTag, string Key, string Value)
{
    local Actor aActor;

    if(ActorTag == '')
        return;

    foreach AllActors(class'Actor', aActor, ActorTag)
    {
        aActor.SetPropertyText(Key, Value);
    }
}

// -------------------------------
// Starts an InfoLink conversation
// -------------------------------
function StartInfoLink(name ConversationName)
{
    GetPlayer().StartDataLinkTransmission(string(ConversationName));
}

// ---------------------
// Starts a conversation
// ---------------------
function bool StartConversation(
    coerce string BindName,
    name ConversationName,
    optional bool bAvoidState,
    optional bool bForcePlay,
    optional name DesiredSpeakingAnim
)
{
    local Actor aActor;
    local ScriptedPawnExtended aPawn;

    if(BindName == "" || ConversationName == '')
        return False;

    foreach AllActors(class'Actor', aActor)
    {
        if(aActor.BindName != BindName)
            continue;

        aPawn = ScriptedPawnExtended(aActor);

        if(aPawn != None && DesiredSpeakingAnim != '')
            aPawn.DesiredSpeakingAnim = DesiredSpeakingAnim; 

        return GetPlayer().StartConversationByName(ConversationName, aActor, bAvoidState, bForcePlay);
    }

    return False;
}

// -----------------------------------
// Checks if a conversation has played
// -----------------------------------
function bool IsConversationDone(name ConversationName)
{
    return Flags.GetBool(StringToName(ConversationName $ "_Played")) || Flags.GetBool(StringToName(ConversationName $ "_Interrupted"));
}

// ---------------
// Loads a new map
// ---------------
function LoadMap(coerce string MapName, optional name OffsetTag)
{
    local HumanExtended aPlayer;

    aPlayer = GetPlayer();

    if(aPlayer == None)
        return;
    
    if(OffsetTag != '')
        aPlayer.StoreTravelOffset(GetActor(OffsetTag));

    DeusExRootWindow(aPlayer.RootWindow).ClearWindowStack();

    Level.Game.SendPlayer(aPlayer, MapName);
}

// ----------------------
// Possesses another pawn
// ----------------------
function PossessPawn(name PawnTag)
{
    local HumanExtended aPawn;

    if(PawnTag == '')
        return;

    foreach AllActors(class'HumanExtended', aPawn, PawnTag)
    {
        aPawn.Possess();
    }
}

// -------------------------------------
// Changes into another player character
// -------------------------------------
function ChangePlayerClass(class<HumanExtended> PlayerClass)
{
    local HumanExtended aNewPlayer, aOldPlayer;
    local Vector PlayerLocation;
    local Rotator PlayerRotation, PlayerViewRotation;

    aOldPlayer = GetPlayer();
    PlayerLocation = aOldPlayer.Location;
    PlayerRotation = aOldPlayer.Rotation;
    PlayerViewRotation = aOldPlayer.ViewRotation;
    
    aNewPlayer = Spawn(PlayerClass);
    
    if(aNewPlayer == None)
        return;

    aOldPlayer.SetLocation(PlayerLocation - Normal(Vector(PlayerRotation)) * 128);

    aNewPlayer.SetLocation(PlayerLocation);
    aNewPlayer.DesiredRotation = PlayerRotation;
    aNewPlayer.SetRotation(PlayerRotation);
    aNewPlayer.ViewRotation = PlayerViewRotation;

    aNewPlayer.Possess();

    // TODO: Find out why this doesn't work, and destroy the old player
}

// -------------------------------------------
// Resets the player to default playable state
// -------------------------------------------
function ResetPlayer()
{
    local HumanExtended aPlayer;

    aPlayer = GetPlayer();

    if(aPlayer == None)
        return;

    aPlayer.bCollideWorld = True;
    aPlayer.bHidden = False;
    aPlayer.SetCollision(True, True, True);
    aPlayer.GoToState('PlayerWalking');
    aPlayer.bDetectable = True;
    aPlayer.BaseEyeHeight = aPlayer.Default.BaseEyeHeight;
    aPlayer.ShowHUD(True);
    aPlayer.Visibility = 1;
        
    if(aPlayer.Shadow == None)
        aPlayer.CreateShadow();
}

// ----------------------
// Clears the event queue
// ----------------------
function ClearEventQueue()
{
    local int i;

    for(i = 0; i < ArrayCount(EventQueue); i++)
    {
        EventQueue[i].EventName = '';
        EventQueue[i].aEventActor = None;
        EventQueue[i].aEventInstigator = None;
    }
}

// ---------------------------------------------------
// Executes an event in state code, clearing the queue
// ---------------------------------------------------
function DoEvent(name NextEventName, optional Actor aNextEventActor, optional Pawn aNextEventInstigator)
{
    ClearEventQueue();

    EventQueue[0].EventName = NextEventName;
    EventQueue[0].aEventActor = aNextEventActor;
    EventQueue[0].aEventInstigator = aNextEventInstigator;

    GoToState(GetStateName(), NextEventName);
}

// ----------------------------------------------
// Queues up an event to be executed in state code
// ----------------------------------------------
function QueueEvent(name NextEventName, optional Actor aNextEventActor, optional Pawn aNextEventInstigator)
{
    local int i;

    // No events queued up, just execute
    if(EventQueue[0].EventName == '')
    {
        EventQueue[0].EventName = NextEventName;
        EventQueue[0].aEventActor = aNextEventActor;
        EventQueue[0].aEventInstigator = aNextEventInstigator;

        GoToState(GetStateName(), NextEventName);
    }

    // There are events in the queue, add to it
    else
    {
        for(i = 0; i < ArrayCount(EventQueue); i++)
        {
            if(EventQueue[i].EventName != '')
                continue;

            EventQueue[i].EventName = NextEventName;
            EventQueue[i].aEventActor = aNextEventActor;
            EventQueue[i].aEventInstigator = aNextEventInstigator;
            
            break;
        }
    }
}

// -----------------------------------------
// Ends an event, returning to default label
// -----------------------------------------
function EndEvent()
{
    local int i;

    // Pop the queue
    EventQueue[0].EventName = '';
    EventQueue[0].aEventActor = None;
    EventQueue[0].aEventInstigator = None;

    for(i = 1; i < ArrayCount(EventQueue); i++)
    {
        if(EventQueue[i].EventName == '')
            break;

        EventQueue[i - 1].EventName = EventQueue[i].EventName;
        EventQueue[i - 1].aEventActor = EventQueue[i].aEventActor;
        EventQueue[i - 1].aEventInstigator = EventQueue[i].aEventInstigator;
    
        EventQueue[i].EventName = '';
        EventQueue[i].aEventActor = None;
        EventQueue[i].aEventInstigator = None;
    }

    // Execute the next queued event
    if(EventQueue[0].EventName != '')
        GoToState(GetStateName(), EventQueue[0].EventName);

    // Go to the default label
    else
        GoToState(GetStateName(), 'End');
}

auto state EventManager {

End:
    Sleep(0.0);

}

// END UE1
