class AdamJensenAugmentationManager extends AugmentationManager;

defaultproperties
{
     AugLocs(0)=(NumSlots=1,KeyBase=4)
     AugLocs(1)=(NumSlots=1,KeyBase=7)
     AugLocs(2)=(NumSlots=3,KeyBase=8)
     AugLocs(3)=(NumSlots=1,KeyBase=5)
     AugLocs(4)=(NumSlots=1,KeyBase=6)
     AugLocs(5)=(NumSlots=2,KeyBase=2)
     AugLocs(6)=(NumSlots=3,KeyBase=11)
     AugClasses(0)=Class'DeusEx.AugSpeed'
     AugClasses(1)=Class'DeusEx.AugTarget'
     AugClasses(2)=Class'DeusEx.AugCloak'
     AugClasses(3)=Class'DeusEx.AugBallistic'
     AugClasses(4)=Class'DeusEx.AugRadarTrans'
     AugClasses(5)=Class'DeusEx.AugShield'
     AugClasses(6)=Class'DeusEx.AugEnviro'
     AugClasses(7)=Class'DeusEx.AugEMP'
     AugClasses(8)=Class'DeusEx.AugCombat'
     AugClasses(9)=Class'DeusEx.AugHealing'
     AugClasses(10)=Class'DeusEx.AugStealth'
     AugClasses(11)=Class'DeusEx.AugIFF'
     AugClasses(12)=Class'DeusEx.AugLight'
     AugClasses(13)=Class'DeusEx.AugMuscle'
     AugClasses(14)=Class'DeusEx.AugVision'
     AugClasses(15)=Class'DeusEx.AugDrone'
     AugClasses(16)=Class'DeusEx.AugDefense'
     AugClasses(17)=Class'DeusEx.AugAqualung'
     AugClasses(18)=Class'DeusEx.AugDatalink'
     AugClasses(19)=Class'DeusEx.AugHeartLung'
     AugClasses(20)=Class'DeusEx.AugPower'
     DefaultAugs(0)=Class'DeusEx.AugLight'
     DefaultAugs(1)=Class'DeusEx.AugIFF'
     DefaultAugs(2)=Class'DeusEx.AugDatalink'
     AugLocationFull="You can't add any more augmentations to that location!"
     NoAugInSlot="There is no augmentation in that slot"
     bHidden=True
     bTravel=True
}
