class ModernHUDTileWindow extends TileWindow;

var DeusExPlayer Player;
var ModernRootWindow Root;
var ModernHUD HUD;

// Position/size of background
var int backgroundWidth;
var int backgroundHeight;
var int backgroundPosX;
var int backgroundPosY;

// Default Colors
var Color colBackground;
var Color colBorder;
var Color colHeaderText;
var Color colText;

var int     minHeight;
var int     topMargin;
var int     bottomMargin;

// BEGIN UE1

event InitWindow()
{
    Super.InitWindow();
    
    // Get a pointer to the player
    Player = DeusExPlayer(DeusExRootWindow(GetRootWindow()).parentPawn);

    // Get a pointer to the root window
    Root = ModernRootWindow(GetRootWindow());
    HUD = ModernHUD(Root.HUD);
}

event DrawWindow(GC gc)
{
    HUD.DrawLightDialog(gc, 0, 0, Width, Height);
}

function RefreshHUDDisplay(float DeltaTime) {}

// END UE1

defaultproperties
    MinHeight=40
    TopMargin=13
    BottomMargin=10
}
