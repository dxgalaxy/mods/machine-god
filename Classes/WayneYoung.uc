class WayneYoung extends ScriptedPawnMale;

#exec TEXTURE IMPORT NAME=WayneYoung_Head FILE=Textures\Skins\WayneYoung_Head.pcx GROUP=Skins FLAGS=2

// Voice: ElevenLabs Jamahal 

defaultproperties
{
    BindName="WayneYoung"
    FamiliarName="Wayne Young"
    UnfamiliarName="Activist"
    Archetype=AT_Military
    WalkingSpeed=0.300000
    GroundSpeed=200
    bImportant=True
    bInvincible=True
    bFollowPersistently=True
    Mesh=LodMesh'DeusExCharacters.GM_Jumpsuit'
    MultiSkins(0)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(1)=Texture'MachineGod.Skins.TacticalPants3'
    MultiSkins(2)=Texture'MachineGod.Skins.ArmorSweater3_MaleJumpsuit'
    MultiSkins(3)=Texture'MachineGod.Skins.WayneYoung_Head'
    MultiSkins(4)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(5)=Texture'DeusExItems.Skins.GrayMaskTex'
    MultiSkins(6)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(7)=Texture'DeusExItems.Skins.PinkMaskTex'
    Texture=Texture'DeusExItems.Skins.PinkMaskTex'
}
