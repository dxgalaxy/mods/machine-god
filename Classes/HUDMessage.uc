class HUDMessage extends HUDBaseWindow;

var TextWindow Label;
var Font FontLabel;
var float RelativeSizeX;
var float RelativeSizeY;
var float RelativePositionX;
var float RelativePositionY;

// BEGIN UE1

event InitWindow()
{
    Super.InitWindow();

    FontLabel = Font(DynamicLoadObject("DXFonts.MainMenuTrueType", class'Font'));

    Label = TextWindow(NewChild(Class'TextWindow'));
    Label.SetFont(FontLabel);
    Label.SetTextAlignments(HALIGN_Center, VALIGN_Center);

    StyleChanged();
}

function SetRelativePosition(float X, float Y)
{
    RelativePositionX = X;
    RelativePositionY = Y;
}

function SetRelativeSize(float X, float Y)
{
    RelativeSizeX = X;
    RelativeSizeY = Y;
}

function SetValue(string Text)
{
    if(Label == None)
        return;
    
    Label.SetText(Text);
    
    AskParentForReconfigure();
}

event ParentRequestedPreferredSize(bool bWidthSpecified, out float PrefWidth, bool bHeightSpecified, out float PrefHeight)
{
    PrefHeight = Player.RootWindow.Height;
    PrefWidth = Player.RootWindow.Width;
}

function ConfigurationChanged()
{
    local float PrefX, PrefY, PrefWidth, PrefHeight;
    
    Super.ConfigurationChanged();

    PrefHeight = Player.RootWindow.Height * RelativeSizeY;
    PrefWidth = Player.RootWindow.Width * RelativeSizeX;
    PrefX = Player.RootWindow.Width * RelativePositionX;
    PrefY = Player.RootWindow.Height * RelativePositionY;

    //ConfigureChild(0, 0, Player.RootWindow.Width, Player.RootWindow.Height);
    if(Label != None)
        Label.ConfigureChild(PrefX, PrefY, PrefWidth, PrefHeight); 
}

event StyleChanged()
{
    Super.StyleChanged();

    if(Label != None)
        Label.SetTextColor(ColText);
}

// END UE1

defaultproperties
{
     FontLabel=Font'DeusExUI.FontMenuTitle'
     RelativeSizeX=1.0
     RelativeSizeY=1.0
     RelativePositionX=1.0
     RelativePositionY=1.0
}
