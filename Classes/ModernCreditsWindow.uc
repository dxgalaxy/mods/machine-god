class ModernCreditsWindow extends DeusExBaseWindow;

struct Role
{
    var string Heading;
    var string Body[100];
    var int NumBodyLines;
};

struct Logo
{
    var Texture ImageTexture;
    var int Width, Height;
};

// Various settings
var string MusicName;
var bool bLoadIntro, bAnimateText, bUseCorona;
var float DelayTimer, CoronaDuration;
var Texture CoronaTexture;

// Splash text
var localized string SplashText;
var bool bDrawSplashText;
var float SplashTextTimer, SplashTextDuration;
var Color SplashTextColor;
var Font SplashTextFont;
var int SplashTextLineHeight;

// Banner
var bool bDrawBanner;
var Texture BannerTexture;
var float BannerDuration, BannerTimer;
var int BannerHeight, BannerWidth; 

// Roles
var bool bDrawRoles, bPlayedRoleSound;
var float RoleDuration, RoleDelay, CurrentRoleDuration, RoleDurationModifier, RoleTimer, RoleSoundPitch, RoleSoundVolume;
var Role Roles[100];
var Color RoleHeadingColor, RoleBodyColor;
var Font RoleHeadingFont, RoleBodyFont;
var int NumRoles, CurrentRole, RoleHeadingLineHeight, RoleBodyLineHeight;
var string RoleSoundName;
var Sound RoleSound;

// Logos
var bool bDrawLogos;
var float LogoTimer, LogoDuration;
var Logo Logos[10];
var int NumLogos, LogoPadding;

// End message
var bool bDrawEndMessage;
var string EndMessage;
var Color EndMessageColor;
var Font EndMessageFont;
var int EndMessageLineHeight;
var float EndMessageDelay;

event InitWindow()
{
    Super.InitWindow();

    SetWindowAlignments(HALIGN_Full, VALIGN_Full);
    
    Root.ShowCursor(False);

    bTickEnabled = True;

    CoronaTexture = Texture(DynamicLoadObject("Effects.Corona.Corona_D", class'Texture'));
    
    if(RoleSoundName != "")
        RoleSound = Sound(DynamicLoadObject(RoleSoundName, class'Sound'));

    StartMusic();
}

event ParentRequestedPreferredSize(bool bWidthSpecified, out float PrefWidth, bool bHeightSpecified, out float PrefHeight)
{
    PrefHeight = Root.Height;
    PrefWidth = Root.Width;
}

function Tick(float DeltaTime)
{
    bDrawSplashText = False;
    bDrawBanner = False;
    bDrawRoles = False;
    bDrawLogos = False;
    bDrawEndMessage = False;

    if(DelayTimer > 0)
    {
        DelayTimer -= DeltaTime;
    }
    else if(SplashTextTimer < SplashTextDuration && SplashText != "")
    {
        bDrawSplashText = True;
        SplashTextTimer += DeltaTime;
    }
    else if(BannerTimer < BannerDuration && BannerTexture != None)
    {
        bDrawBanner = True;
        BannerTimer += DeltaTime;
    }
    else if(CurrentRole < NumRoles)
    {
        bDrawRoles = True;
        RoleTimer += DeltaTime;
        CurrentRoleDuration = RoleDuration + RoleDurationModifier * Roles[CurrentRole].NumBodyLines;

        if(!bPlayedRoleSound && RoleSound != None)
        {
            PlaySound(RoleSound, RoleSoundVolume, RoleSoundPitch);
            bPlayedRoleSound = True;   
        }

        if(RoleTimer >= CurrentRoleDuration)
        {
            CurrentRole++;
            DelayTimer = RoleDelay; 
            RoleTimer = 0;
            bPlayedRoleSound = False;
        }
    }
    else if(NumLogos > 0 && LogoTimer < LogoDuration)
    {
        bDrawLogos = True;
        LogoTimer += DeltaTime;
    
        if(LogoTimer >= LogoDuration)
            DelayTimer = EndMessageDelay;
    }
    else if(EndMessage != "")
    {
        bDrawEndMessage = True;
    }
}

function float GetTextX(optional bool bReverse)
{
    local float Delta, Min, Max, StartX, EndX;

    if(!bAnimateText)
        return 0;

    Min = 0.5;
    Max = CurrentRoleDuration - Min;
     
    if(bReverse)
    {
        StartX = -Root.Width;
        EndX = Root.Width;
    }
    else
    {
        StartX = Root.Width;
        EndX = -Root.Width;
    }

    if(RoleTimer <= Min)
    {
        Delta = RoleTimer / Min; 
        return Smerp(Delta, StartX, 0);
    }
   
    if(RoleTimer >= Max)
    {
        Delta = (RoleTimer - Max) / Min;   
        return Smerp(Delta, 0, EndX);
    }
    
    return 0;
}

function DrawWindow(GC gc)
{
    local int i, TotalRoleHeight, TotalLogoWidth, LogoX;
    local float CoronaWidth, CoronaHeight, CurrentRoleHeadingLineHeight;
    
    gc.SetTileColorRGB(0, 0, 0);
    gc.DrawPattern(0, 0, Root.Width, Root.Height, 0, 0, Texture'Solid');
    gc.SetTileColorRGB(255, 255, 255);

    if(DelayTimer > 0)
        return;

    gc.EnableSmoothing(True);

    if(bDrawSplashText)
    {
        gc.SetFont(SplashTextFont);
        gc.SetAlignments(HALIGN_Center, VALIGN_Center);
        gc.SetTextColor(SplashTextColor);
        gc.DrawText(0, (Height / 2) - (SplashTextLineHeight / 2), Width, SplashTextLineHeight, SplashText);
    }
    else if(bDrawBanner)
    {
        gc.DrawTexture((Root.Width / 2) - (BannerWidth / 2), (Root.Height / 2) - (BannerHeight / 2), BannerWidth, BannerHeight, 0, 0, BannerTexture);
    }
    else if(bDrawRoles)
    {
        TotalRoleHeight = Roles[CurrentRole].NumBodyLines * RoleBodyLineHeight + RoleHeadingLineHeight;
       
        gc.SetAlignments(HALIGN_Center, VALIGN_Center);
        gc.SetFont(RoleHeadingFont);
        gc.SetTextColor(RoleHeadingColor);
        
        // Draw the heading with corona effect
        if(bUseCorona)
        {
            // At first, shrink in height, expand in width
            CoronaHeight = Smerp(FMin(1.0, RoleTimer / CoronaDuration), RoleHeadingLineHeight * 2, 0);
            CoronaWidth = Smerp(FMin(1.0, RoleTimer / CoronaDuration), 0, 1024);
            
            // At first, expand heading in height halfway through the corona animation, and twice as fast
            if(RoleTimer > CoronaDuration / 2 && RoleTimer < CurrentRoleDuration - CoronaDuration)
                CurrentRoleHeadingLineHeight = Smerp(FMin(1.0, ((RoleTimer - CoronaDuration / 2) / CoronaDuration) * 2), 0, RoleHeadingLineHeight);
            // At last, shrink heading in height twice as fast as the corona animation
            else
                CurrentRoleHeadingLineHeight = Smerp(FMin(1.0, ((RoleTimer - (CurrentRoleDuration - CoronaDuration)) / CoronaDuration) * 2), RoleHeadingLineHeight, 0);
            
            // Draw heading
            gc.DrawText(
                0,
                Height / 2 - TotalRoleHeight / 2 + (RoleHeadingLineHeight - CurrentRoleHeadingLineHeight) / 2,
                Width,
                CurrentRoleHeadingLineHeight,
                Roles[CurrentRole].Heading
            );
           
            // Draw corona
            if(CoronaWidth < 1024)
            {
                gc.SetTileColor(RoleHeadingColor);
                gc.DrawStretchedTexture(
                    Width / 2 - CoronaWidth / 2,
                    Height / 2 - TotalRoleHeight / 2 + (RoleHeadingLineHeight - CoronaHeight) / 2,
                    CoronaWidth,
                    CoronaHeight,
                    0,
                    0,
                    64,
                    64,
                    CoronaTexture
                );
                gc.SetTileColorRGB(255, 255, 255);
            }
        }
        // Draw the heading normally
        else
        {
            gc.DrawText(GetTextX(), Height / 2 - TotalRoleHeight / 2, Width, RoleHeadingLineHeight, Roles[CurrentRole].Heading);
        }

        // Draw the roles
        gc.SetFont(RoleBodyFont);
        gc.SetTextColor(RoleBodyColor);
       
        for(i = 0; i < Roles[CurrentRole].NumBodyLines; i++)
        {
            // If we're using the corona effect, offset the enter/exit direction
            if(bUseCorona)
                gc.DrawText(GetTextX(i % 2 == 1), Height / 2 - TotalRoleHeight / 2 + RoleHeadingLineHeight + i * RoleBodyLineHeight, Width, RoleBodyLineHeight, Roles[CurrentRole].Body[i]);
            else
                gc.DrawText(GetTextX(i % 2 == 0), Height / 2 - TotalRoleHeight / 2 + RoleHeadingLineHeight + i * RoleBodyLineHeight, Width, RoleBodyLineHeight, Roles[CurrentRole].Body[i]);
        }
    }
    else if(bDrawLogos)
    {
        TotalLogoWidth = (NumLogos - 1) * LogoPadding;

        for(i = 0; i < NumLogos; i++)
            TotalLogoWidth += Logos[i].Width;

        LogoX = (Root.Width / 2) - (TotalLogoWidth / 2);

        for(i = 0; i < NumLogos; i++)
        {
            gc.DrawTexture(LogoX, (Root.Height / 2) - (Logos[i].Height / 2), Logos[i].Width, Logos[i].Height, 0, 0, Logos[i].ImageTexture);

            LogoX += Logos[i].Width + LogoPadding;
        }
    }
    else if(bDrawEndMessage)
    {
        gc.SetFont(EndMessageFont);
        gc.SetAlignments(HALIGN_Center, VALIGN_Center);
        gc.SetTextColor(EndMessageColor);
        gc.DrawText(0, (Height / 2) - (EndMessageLineHeight / 2), Width, EndMessageLineHeight, EndMessage);
    }
}

function AddRole(string Heading)
{
    local int i;

    i = NumRoles;

    Roles[i].Heading = Heading;
    NumRoles++;
}

function AddRoleBody(string Body)
{
    local int i;

    i = NumRoles - 1;

    Roles[i].Body[Roles[i].NumBodyLines] = Body;
    Roles[i].NumBodyLines++;
}

function SetSplashText(string NewText, float NewDuration)
{
    SplashText = NewText;
    SplashTextDuration = NewDuration;
}

function SetBanner(Texture NewBannerTexture, int Width, int Height)
{
    BannerTexture = NewBannerTexture;
    BannerWidth = Width;
    BannerHeight = Height;
}

function SetLoadIntro(bool bNewLoadIntro)
{
    bLoadIntro = bNewLoadIntro;
}

function AddLogo(Texture ImageTexture, int Width, int Height)
{
    Logos[NumLogos].ImageTexture = ImageTexture;
    Logos[NumLogos].Width = Width;
    Logos[NumLogos].Height = Height;
    NumLogos++;
}

function StartMusic()
{
    local Music CreditsMusic;

    if(MusicName == "")
        return;

    CreditsMusic = Music(DynamicLoadObject(MusicName, class'Music'));

    if(CreditsMusic == None)
        return;

    Player.ClientSetMusic(CreditsMusic, 0, 255, MTRAN_FastFade);
}

defaultproperties
{
    MusicName="Credits_Music.Credits_Music"
    bAnimateText=True
    bUseCorona=True
    CoronaDuration=0.5

    SplashTextDuration=2.0
    SplashTextFont=Font'FontHeading1'
    SplashTextLineHeight=50
    SplashTextColor=(R=255,G=255,B=255)

    BannerDuration=5.0
    
    RoleHeadingColor=(R=0,G=193,B=255)
    RoleHeadingFont=Font'FontHeading1'
    RoleBodyColor=(R=255,G=255,B=255)
    RoleBodyFont=Font'FontHeading2'
    RoleDuration=5.0
    RoleDurationModifier=0.2
    RoleHeadingLineHeight=50
    RoleBodyLineHeight=40
    RoleDelay=0.5
    RoleSoundName="DeusExSounds.Generic.WindGust1"
    RoleSoundVolume=1.0
    RoleSoundPitch=2.5

    LogoDuration=5.0
    LogoPadding=20

    EndMessage="Thank you for playing!"
    EndMessageDelay=0.5
    EndMessageFont=Font'FontHeading1'
    EndMessageLineHeight=50
    EndMessageColor=(R=255,G=255,B=255)
}
