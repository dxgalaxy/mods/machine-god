#!/bin/bash

SRC="Head_F"
DST="HeliosAgentFemale_Head"
DPI="192"

flatpak run org.inkscape.Inkscape --export-id="$DST;Background" --actions="unhide-all;export-id-only;export-area-page;export-type:png;export-dpi:$DPI;export-filename:$HOME/Repos/dxgalaxy-mods-machine-god/Textures/Skins/$DST.png;export-do" "$HOME/Repos/dxgalaxy-mods-machine-god/Textures/Skins/Source/$SRC.svg"
