class AugTargeted extends Augmentation;

var float mpAugValue, mpEnergyDrain, HighlightTimer;
var ScriptedPawn aHighlightPawn, aTargetPawn;
var AugTargetedWindow Display;
var bool bWasActive;

// BEGIN UE1

function bool IsTargeting()
{
    return aTargetPawn != None;
}

function bool IsRelevant(ScriptedPawn aPawn)
{
    return False;
}

function ScriptedPawn TracePawn(float Distance)
{
    local Vector HitLocation, HitNormal, Position, Line;
    local ScriptedPawn aHitPawn;

    Position = Player.Location;
    Position.Z += Player.BaseEyeHeight;
    Line = Vector(Player.ViewRotation) * Distance;

    return ScriptedPawn(Player.Trace(HitLocation, HitNormal, Position+Line, Position, True));
}

function float GetTargetDelay()
{
    return LevelValues[CurrentLevel] * 5.0;
}

function float GetProgress()
{
    if(aHighlightPawn == None)
        return 0.0;

    return HighlightTimer / GetTargetDelay();
}

simulated function float GetEnergyRate()
{
    if(!IsTargeting())
        return 0;

    return EnergyRate * LevelValues[CurrentLevel];
}

state Active
{
    function Tick(float DeltaTime)
    {
        local ScriptedPawn aNewHighlightPawn;
        local bool bIrrelevantTarget;

        // Only do anything if we're not already targeting someone
        if(aTargetPawn == None)
        {
            // Make sure the display is visible
            if(Display == None)
                Display = AugTargetedWindow(DeusExRootWindow(Player.RootWindow).HUD.NewChild(class'AugTargetedWindow'));
            
            Display.Show();

            // Scan for a target
            aNewHighlightPawn = TracePawn(1024);

            // If the target is irrelevant, unset it 
            if(aNewHighlightPawn != None && !IsRelevant(aNewHighlightPawn))
            {
                aNewHighlightPawn = None;
                bIrrelevantTarget = True;
            }

            // If the target changed, reset the timer
            if(aNewHighlightPawn != aHighlightPawn)
                HighlightTimer = 0; 

            aHighlightPawn = aNewHighlightPawn;
           
            // Increment the timer
            if(aHighlightPawn != None)
                HighlightTimer += DeltaTime;

            // Let the display know how we're doing
            Display.aHighlight = aHighlightPawn;
            Display.Progress = GetProgress();
            Display.bIrrelevantTarget = bIrrelevantTarget;

            // If the progress is complete, turn our highlight into a target
            if(GetProgress() >= 1.0)
            {
                aTargetPawn = aHighlightPawn;
                aHighlightPawn = None;

                Display.Hide();
            
                GoToState('Active', 'BeginTarget');
                bWasActive = True;
            }
        } 
    }

    function BeginState()
    {
        aTargetPawn = None;
        aHighlightPawn = None;
        HighlightTimer = 0;
        bWasActive = False;
    }
}

function Deactivate()
{
    Super.Deactivate();
        
    if(Display != None)
        Display.Hide();

    aTargetPawn = None;
}

// END UE1

defaultproperties
{
    LevelValues(0)=1.000000
    LevelValues(1)=0.830000
    LevelValues(2)=0.660000
    LevelValues(3)=0.500000
}
