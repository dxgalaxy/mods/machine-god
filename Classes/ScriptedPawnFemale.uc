class ScriptedPawnFemale extends ScriptedPawnToon;

defaultproperties
{
     BindName="Woman"
     FamiliarName="Woman"
     UnfamiliarName="Woman"
     bIsFemale=True
     WalkingSpeed=0.320000
     BaseAssHeight=-18.000000
     WalkAnimMult=0.650000
     GroundSpeed=120.000000
     Mesh=LodMesh'DeusExCharacters.GFM_TShirtPants'
     CollisionRadius=20.000000
     CollisionHeight=43.000000
     BaseEyeHeight=38.000000
     Mass=150.000000
     WalkAnimMult=0.750000
     
     Die=Sound'DeusExSounds.Player.FemaleDeath'
     HitSound1=Sound'DeusExSounds.Player.FemalePainMedium'
     HitSound2=Sound'DeusExSounds.Player.FemalePainLarge'
}
