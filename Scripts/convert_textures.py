import os, glob, time, subprocess

printed_hello = False

# Set up directories
script_dir = os.path.dirname(os.path.abspath(__file__))
root_dir = os.path.dirname(script_dir)
textures_dir = os.path.abspath(root_dir + '/Textures')
bright_path = os.path.join(script_dir, 'bright.exe')

# Runs a subprocess
def run(*args):
    subprocess.run(args, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    #subprocess.run(args)
    
# Processes a texture file with bright.exe
def bright(png_file_path):
    tga_file_path = png_file_path.replace('.png', '.tga')
    pcx_file_path = png_file_path.replace('.png', '.pcx')
   
    # First we have to convert the .png to .tga and flip it upside down
    # Why? Only Erik de Neve knows
    run('magick', png_file_path, '-flip', tga_file_path)
    
    # Change to the directory of the file we want to convert
    cwd = os.getcwd()
    os.chdir(os.path.dirname(tga_file_path))

    # Run bright.exe
    run('wine', bright_path, os.path.basename(tga_file_path), os.path.basename(pcx_file_path), '-pinkmask', '-o', '-mask', '0.7') 
    
    # Remove the .tga file
    os.remove(tga_file_path)

    # Return to the dir we came from
    os.chdir(cwd)

# Loop .png files
for png_file_path in glob.glob(os.path.abspath(textures_dir + '/**/*.png'), recursive=True):
    if png_file_path.endswith('_Mask.png'):
        continue

    # If a .pcx file exists, and it's newer than the .png, skip this one
    pcx_file_path = png_file_path.replace('.png', '.pcx')

    if os.path.isfile(pcx_file_path):
        png_file_mtime = os.path.getmtime(png_file_path)
        pcx_file_mtime = os.path.getmtime(pcx_file_path)
   
        if png_file_mtime < pcx_file_mtime:
            continue
    
    if not printed_hello:
        print('')
        print('=======================================')
        print('Converting textures')
        print('=======================================')
        print('')
        printed_hello = True

    print('> ' + png_file_path.replace('.png', '').replace(textures_dir, ''))

    # Apply the mask, if it exists
    mask_file_path = png_file_path.replace('.png', '_Mask.png')

    if os.path.isfile(mask_file_path):
        run('magick', mask_file_path, '+dither', '-colors', '2', mask_file_path)
        run('magick', 'composite', mask_file_path, png_file_path, png_file_path)

    # Run it through bright.exe to palettise and make pink the mask colour
    bright(png_file_path)
