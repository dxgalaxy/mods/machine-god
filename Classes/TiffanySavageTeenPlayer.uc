class TiffanySavageTeenPlayer extends TiffanySavagePlayer;

#exec OBJ LOAD FILE=Effects

// BEGIN UE1

state PlayerStandingWithHUD
{
    function PlayerTick(float DeltaTime)
    {
        Super.PlayerTick(DeltaTime);

        bCrosshairVisible = False;
        ModernHUD(ModernRootWindow(RootWindow).HUD).SetMinimal(True);
    }
}

state ParalyzedWithHUD
{
    function PlayerTick(float DeltaTime)
    {
        Super.PlayerTick(DeltaTime);

        bCrosshairVisible = False;
        ModernHUD(ModernRootWindow(RootWindow).HUD).SetMinimal(True);
    }
}

state PlayerWalking
{
    function PlayerTick(float DeltaTime)
    {
        Super.PlayerTick(DeltaTime);

        bCrosshairVisible = False;
        ModernHUD(ModernRootWindow(RootWindow).HUD).SetMinimal(True);
    }
}

// Stubbed functions
function TakeDamage(int Damage, Pawn aInstigatedBy, Vector HitLocation, Vector Momentum, name DamageType) {}
exec function ActivateAugmentation(int Num) {}
exec function ActivateBelt(int Num) {}
exec function ShowGoalsWindow() {}
exec function ShowLogsWindow() {}
exec function ShowAugmentationAddWindow() {}
exec function ShowQuotesWindow() {}
exec function ShowRGBDialog() {}
exec function ShowInventoryWindow() {}
exec function ShowSkillsWindow() {}
exec function ShowHealthWindow() {}
exec function ShowImagesWindow() {}
exec function ShowConversationsWindow() {}
exec function ShowAugmentationsWindow() {}
exec function NextWeapon() {}
exec function PrevWeapon() {}
exec function NextBeltItem() {}
exec function PrevBeltItem() {}

// END UE1

defaultproperties
{
    Mesh=LodMesh'DeusExCharacters.GFM_TShirtPants'
    Mass=80.000000
    Fatness=126
    Buoyancy=85.000000
    MultiSkins(0)=WetTexture'Effects.UserInterface.DrunkFX'
    MultiSkins(1)=Texture'PinkMaskTex'
    MultiSkins(2)=WetTexture'Effects.UserInterface.DrunkFX'
    MultiSkins(3)=Texture'GrayMaskTex'
    MultiSkins(4)=Texture'BlackMaskTex'
    MultiSkins(5)=None
    MultiSkins(6)=WetTexture'Effects.UserInterface.DrunkFX'
    MultiSkins(7)=WetTexture'Effects.UserInterface.DrunkFX'
}
