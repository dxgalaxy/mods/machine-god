class AugPossession extends AugTargeted;

var float TransitionAlpha;
var ScriptedPawnFemale aDummyPawn;
var int i;
       
// Cached values
var Inventory StartInHand;
var Texture StartMultiSkins[8];
var Rotator StartRotation, EndRotation, TargetRotation;
var name StartAlliance;
var Mesh StartMesh;
var Vector StartLocation, EndLocation, TargetLocation;

// BEGIN UE1

function bool IsRelevant(ScriptedPawn aPawn)
{
    return (
        aPawn != None &&
        !aPawn.IsA('Animal') && !aPawn.IsA('Robot') &&
        !aPawn.bInvincible
    );
}

function float GetTargetDelay()
{
    return 2.0;
}

function TransferInventory(Pawn aFrom, Pawn aTo)
{
    if(aFrom == None || aTo == None)
        return;

    aTo.Inventory = aFrom.Inventory;
    aFrom.Inventory = None;

    if(aTo == Player && aFrom.Weapon != None)
    {
        aFrom.Weapon.SetOwner(Player);
        Player.PutInHand(aFrom.Weapon);

        aFrom.Weapon = None;
    }
    else if(aFrom == Player && aTo.IsA('ScriptedPawn') && Player.InHand != None && Player.InHand.IsA('Weapon'))
    {
        Player.InHand.SetOwner(aTo);
        ScriptedPawn(aTo).SetWeapon(Weapon(Player.InHand));
        Player.PutInHand(None);
    }
    
    DeusExRootWindow(Player.RootWindow).HUD.Belt.RefreshHUDDisplay(1.0); 
}

state Active
{
    function Tick(float DeltaTime)
    {
        local float Beta, CurFOV;
        local Rotator DiffRot, CurRot;
        local Vector DiffLoc, CurLoc;

        Super.Tick(DeltaTime);

        if(aTargetPawn == None || TransitionAlpha >= 1.0)
            return;

        // Half a second transition
        TransitionAlpha += DeltaTime * 2;

        Beta = TransitionAlpha * TransitionAlpha;
    
        DiffRot = TargetRotation - StartRotation;

        if (DiffRot.Pitch >= 32768)
            DiffRot.Pitch = DiffRot.Pitch - 65536;
        else if (DiffRot.Pitch <= -32768)
            DiffRot.Pitch = DiffRot.Pitch + 65536;

        if (DiffRot.Yaw >= 32768)
            DiffRot.Yaw = DiffRot.Yaw - 65536;
        else if (DiffRot.Yaw <= -32768)
            DiffRot.Yaw = DiffRot.Yaw + 65536;

        if (DiffRot.Roll >= 32768)
            DiffRot.Roll = DiffRot.Roll - 65536;
        else if (DiffRot.Roll <= -32768)
            DiffRot.Roll = DiffRot.Roll + 65536;
        
        DiffLoc = TargetLocation - StartLocation; 
        
        CurRot = StartRotation + Beta * DiffRot;
        CurLoc = StartLocation + Beta * DiffLoc;
       
        // Widen the FOV until 80% of the transition time, then narrow back
        if(TransitionAlpha > 0.8)
            CurFOV = Lerp((TransitionAlpha - 0.8) / 0.2, 150 * 0.8, Player.Default.DesiredFOV);
        else
            CurFOV = Smerp(TransitionAlpha, Player.Default.DesiredFOV, 150);
        
        Player.SetFOVAngle(CurFOV);
        Player.DesiredFOV = CurFOV;

        Player.ViewRotation = CurRot;
        Player.SetLocation(CurLoc);
    }

BeginTarget:
    Player.PlaySound(Sound'CloakUp', SLOT_Interact, 0.85, , 768, 1.0);

    // Store the current values
    StartLocation = Player.Location;
    StartRotation = Player.Rotation;
    StartAlliance = Player.Alliance;
    StartInHand = Player.InHand;
    TargetLocation = aTargetPawn.Location;
    TargetRotation = aTargetPawn.Rotation;

    // Transition
    TransitionAlpha = 0.0;
    Sleep(0.5);
    Player.DesiredFOV = Player.Default.DesiredFOV;

    // Hide the target pawn
    aTargetPawn.LeaveWorld();

    // Steal location/rotation
    Player.SetLocation(TargetLocation);
    Player.ViewRotation = TargetRotation;
    Player.Velocity = Vect(0, 0, 0);

    // Steal mesh and textures
    StartMesh = Player.Mesh;
    Player.Mesh = aTargetPawn.Mesh;
    
    for(i = 0; i < 8; i++)
    {
        StartMultiSkins[i] = Player.MultiSkins[i];
        Player.MultiSkins[i] = aTargetPawn.MultiSkins[i];
    }

    // Steal various other vars
    Player.Alliance = aTargetPawn.Alliance;
    Player.BaseEyeHeight = aTargetPawn.BaseEyeHeight;
    Player.GroundSpeed = aTargetPawn.GroundSpeed;
    Player.UnfamiliarName = aTargetPawn.UnfamiliarName;
    Player.FamiliarName = aTargetPawn.FamiliarName;
    Player.BindName = aTargetPawn.BindName;
    Player.Tag = aTargetPawn.Tag;
    Player.SetCollisionSize(aTargetPawn.CollisionRadius, aTargetPawn.CollisionHeight);
        
    TiffanySavagePlayer(Player).SetFemale(aTargetPawn.bIsFemale);

    // Create a dummy
    aDummyPawn = Spawn(class'ScriptedPawnFemale');
    aDummyPawn.bInitialized = True;
    aDummyPawn.bImportant = True;
    aDummyPawn.Orders = 'Standing';
    aDummyPawn.Mesh = StartMesh;

    for(i = 0; i < 8; i++)
    {
        aDummyPawn.MultiSkins[i] = StartMultiSkins[i];
    }

    aDummyPawn.CreateOutline();

    aDummyPawn.FamiliarName = "Tiffany Savage";
    aDummyPawn.UnfamiliarName = "Tiffany Savage";
    aDummyPawn.BindName = "PlayerDummy";
    aDummyPawn.Alliance = StartAlliance;
    aDummyPawn.SetLocation(StartLocation);
    aDummyPawn.SetRotation(StartRotation);
    aDummyPawn.DesiredRotation = StartRotation;

    aDummyPawn.bReactFutz = False;
    aDummyPawn.bReactPresence = False;
    aDummyPawn.bReactLoudNoise = False;
    aDummyPawn.bReactAlarm = False;
    aDummyPawn.bReactShot = False;
    aDummyPawn.bReactCarcass = False;
    aDummyPawn.bReactDistress = False;
    aDummyPawn.bReactProjectiles = False;

    aDummyPawn.bFearHacking = False;
    aDummyPawn.bFearWeapon = False;
    aDummyPawn.bFearShot = False;
    aDummyPawn.bFearInjury = False;
    aDummyPawn.bFearIndirectInjury = False;
    aDummyPawn.bFearCarcass = False;
    aDummyPawn.bFearDistress = False;
    aDummyPawn.bFearAlarm = False;
    aDummyPawn.bFearProjectiles = False;

    aDummyPawn.bHateHacking = False;
    aDummyPawn.bHateWeapon = False;
    aDummyPawn.bHateShot = False;
    aDummyPawn.bHateInjury = False;
    aDummyPawn.bHateIndirectInjury = False;
    aDummyPawn.bHateCarcass = False;
    aDummyPawn.bHateDistress = False;
    
    aDummyPawn.Health = Player.Health;
    aDummyPawn.HealthTorso = Player.HealthTorso;
    aDummyPawn.HealthHead = Player.HealthHead;
    aDummyPawn.HealthLegLeft = Player.HealthLegLeft;
    aDummyPawn.HealthLegRight = Player.HealthLegRight;
    aDummyPawn.HealthArmLeft = Player.HealthArmLeft;
    aDummyPawn.HealthArmRight = Player.HealthArmRight;
    aDummyPawn.GenerateTotalHealth();
    
    // Transfer the inventories
    TransferInventory(Player, aDummyPawn); 
    TransferInventory(aTargetPawn, Player);

    // Set the eye height next frame
    Sleep(0);
    Player.BaseEyeHeight = aTargetPawn.BaseEyeHeight;
}

function Deactivate()
{
    Player.PlaySound(Sound'CloakDown', SLOT_Interact, 0.85, , 768, 1.0);
 
    if(bWasActive)
    {
        // Store end position and rotation
        EndLocation = Player.Location;
        EndRotation = Player.Rotation;

        // Reset MultiSkins
        for(i = 0; i < 8; i++)
        {
            Player.MultiSkins[i] = StartMultiSkins[i];
        }
        
        // Reset vars back to their stored/default values
        if(aDummyPawn != None)
        {
            Player.SetLocation(aDummyPawn.Location);
            Player.SetRotation(aDummyPawn.Rotation);
            Player.ViewRotation = aDummyPawn.Rotation;
        }
        else
        {
            Player.SetLocation(StartLocation);
            Player.SetRotation(StartRotation);
            Player.ViewRotation = StartRotation;
        }

        Player.Alliance = StartAlliance;
        Player.Mesh = StartMesh;
        Player.SetInHandPending(StartInHand);
        Player.BaseEyeHeight = Player.Default.BaseEyeHeight;
        Player.SetCollisionSize(Player.Default.CollisionRadius, Player.Default.CollisionHeight);
        Player.GroundSpeed = Player.Default.GroundSpeed;
        Player.UnfamiliarName = Player.Default.UnfamiliarName;
        Player.FamiliarName = Player.Default.FamiliarName;
        Player.BindName = Player.Default.BindName;
        Player.Velocity = Vect(0, 0, 0);
        Player.Tag = Player.Default.Tag;
        
        TiffanySavagePlayer(Player).SetFemale(True);

        // Restore the target pawn
        if(aTargetPawn != None)
        {
            // Transfer the inventory back
            TransferInventory(Player, aTargetPawn);
            
            // Show the target pawn
            aTargetPawn.EnterWorld();

            // Set the location and rotation to where the player moved
            aTargetPawn.SetLocation(EndLocation);
            aTargetPawn.SetRotation(EndRotation);
            aTargetPawn.DesiredRotation = EndRotation;

            // Knock out the target pawn
            aTargetPawn.HealthTorso = 0;
            aTargetPawn.Health = 0;
            aTargetPawn.bStunned = True;
            aTargetPawn.TakeDamage(1, Player, aTargetPawn.Location, Vect(0, 0, 0), 'KnockedOut');
        }
        
        // Transfer the inventory back
        Player.DropItem();

        TransferInventory(aDummyPawn, Player); 

        // Dummy died, this kills the player
        if(Player.FlagBase.GetBool('PlayerDummy_Dead'))
        {
            Player.Health = 0;
            Player.HealthTorso = 0; 
            Player.HealthHead = 0;
            Player.HealthLegLeft = 0; 
            Player.HealthLegRight = 0;
            Player.HealthArmLeft = 0;
            Player.HealthArmRight = 0;
        }
        // Also apply health values from the dummy
        else if(aDummyPawn != None)
        {
            Player.Health = aDummyPawn.Health;
            Player.HealthTorso = aDummyPawn.HealthTorso;
            Player.HealthHead = aDummyPawn.HealthHead;
            Player.HealthLegLeft = aDummyPawn.HealthLegLeft;
            Player.HealthLegRight = aDummyPawn.HealthLegRight;
            Player.HealthArmLeft = aDummyPawn.HealthArmLeft;
            Player.HealthArmRight = aDummyPawn.HealthArmRight;
            
            aDummyPawn.Destroy();
        }
        
        Player.GenerateTotalHealth();
       
        // Death
        if(Player.Health <= 0)
        {
            Player.Health = -100;
            Player.GoToState('Dying');
            Player.bHidden = True;
        }
    }

    Super.Deactivate();
}

// END UE1

defaultproperties
{
    EnergyRate=300.000000
    Icon=Texture'MachineGod.UI.AugIconPossession'
    smallIcon=Texture'MachineGod.UI.AugIconPossession_Small'
    AugmentationName="Possession"
    Description="Aiming at a human target while activated, a cloud of nanites temporarily infect the host, allowing an agent to assume full muscular control. The mental strain of this possession renders the target unconscious upon release.|n|nTECH ONE: Power drain is normal.|n|nTECH TWO: Power drain is reduced slightly.|n|nTECH THREE: Power drain is reduced moderately.|n|nTECH FOUR: Power drain is reduced significantly."
    MPInfo=""
    AugmentationLocation=LOC_Cranial
    MPConflictSlot=6
}
