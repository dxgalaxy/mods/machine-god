class VialHenosia extends DeusExPickup;

// BEGIN UE1

#exec TEXTURE IMPORT NAME=LargeIconVialHenosia FILE=Textures\UI\LargeIconVialHenosia.pcx GROUP=UI
#exec TEXTURE IMPORT NAME=BeltIconVialHenosia FILE=Textures\UI\BeltIconVialHenosia.pcx GROUP=UI

var DeusExPlayer aPlayer;
var float DesiredFOV;

state Activated
{
    function Activate() {}

Begin:
    aPlayer = DeusExPlayer(Owner);
    aPlayer.AugmentationSystem.DeactivateAll();
    
    DesiredFOV = aPlayer.DesiredFOV;
    aPlayer.ShowHUD(False);
    aPlayer.DesiredFOV = 160;
    aPlayer.ServerSetSlomo(0.0); 
    aPlayer.bBehindView = True;
    aPlayer.AmbientSound = Sound'Ambient.Ambient.Underwater';
    aPlayer.SoundRadius = 255;
    aPlayer.SoundVolume = 128;
    aPlayer.SoundPitch = 32;
    aPlayer.Sprite = FireTexture(DynamicLoadObject("Effects.Electricity.wepn_nesword", class'FireTexture'));

    Sleep(4.0);
   
    aPlayer.Sprite = None;
    aPlayer.ServerSetSlomo(1.0); 
    aPlayer.DesiredFOV = DesiredFOV;
    aPlayer.ShowHUD(True);
    aPlayer.Gasp();
    aPlayer.AmbientSound = None;
    aPlayer.bBehindView = False;

    UseOnce();

}

// END UE1

defaultproperties
{
    Buoyancy=3.000000
    CollisionHeight=4.890000
    CollisionRadius=2.200000
    Description="A psychoactive substance that temporarily pushes the user's brainwave frequency above 80hz."
    Icon=Texture'MachineGod.UI.BeltIconVialHenosia'
    ItemArticle="a"
    ItemName="Henosia Vial"
    LandSound=Sound'DeusExSounds.Generic.GlassHit1'
    Mass=2.000000
    MaxCopies=10
    Mesh=LodMesh'DeusExItems.VialAmbrosia'
    MultiSkins(1)=FireTexture'Effects.Electricity.Nano_SFX_A'
    PickupViewMesh=LodMesh'DeusExItems.VialAmbrosia'
    PlayerViewMesh=LodMesh'DeusExItems.VialAmbrosia'
    PlayerViewOffset=(X=30.000000,Z=-12.000000)
    ThirdPersonMesh=LodMesh'DeusExItems.VialAmbrosia'
    bActivatable=True
    bCanHaveMultipleCopies=True
    beltDescription="HENOSIA"
    largeIcon=Texture'MachineGod.UI.LargeIconVialHenosia'
    largeIconHeight=44
    largeIconWidth=18
}
