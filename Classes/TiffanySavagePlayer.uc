class TiffanySavagePlayer extends MachineGodPlayer;

#exec TEXTURE IMPORT NAME=TiffanySavage_Head FILE=Textures\Skins\TiffanySavage_Head.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TiffanySavage_HeadHelios FILE=Textures\Skins\TiffanySavage_HeadHelios.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TiffanySavage_Coat FILE=Textures\Skins\TiffanySavage_Coat.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TiffanySavage_ShirtFront FILE=Textures\Skins\TiffanySavage_ShirtFront.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TiffanySavage_Legs FILE=Textures\Skins\TiffanySavage_Legs.pcx GROUP=Skins

#exec TEXTURE IMPORT NAME=TiffanySavage_VatTorso FILE=Textures\Skins\TiffanySavage_VatTorso.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TiffanySavage_VatHip FILE=Textures\Skins\TiffanySavage_VatHip.pcx GROUP=Skins

#exec TEXTURE IMPORT NAME=TiffanySavage_Outfit3_ShirtFront FILE=Textures\Skins\TiffanySavage_Outfit3_ShirtFront.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TiffanySavage_Outfit3_Coat FILE=Textures\Skins\TiffanySavage_Outfit3_Coat.pcx GROUP=Skins

#exec TEXTURE IMPORT NAME=TiffanySavage_Outfit4_Dress FILE=Textures\Skins\TiffanySavage_Outfit4_Dress.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TiffanySavage_Outfit4_Legs FILE=Textures\Skins\TiffanySavage_Outfit4_Legs.pcx GROUP=Skins

#exec TEXTURE IMPORT NAME=TiffanySavage_Outfit5_Torso FILE=Textures\Skins\TiffanySavage_Outfit5_Torso.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TiffanySavage_Outfit5_Legs FILE=Textures\Skins\TiffanySavage_Outfit5_Legs.pcx GROUP=Skins

#exec TEXTURE IMPORT NAME=TiffanySavage_Outfit6_ShirtFront FILE=Textures\Skins\TiffanySavage_Outfit6_ShirtFront.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TiffanySavage_Outfit6_Coat FILE=Textures\Skins\TiffanySavage_Outfit6_Coat.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TiffanySavage_Outfit6_Legs FILE=Textures\Skins\TiffanySavage_Outfit6_Legs.pcx GROUP=Skins

var Texture GlitchSprite;
var float GlitchInterval;
var float GlitchTimer;
var float TransitionTimer;
var float TransitionDuration;

// BEGIN UE1 

// -----------------
// We're in the game
// -----------------
event TravelPostAccept()
{
    Super.TravelPostAccept(); 

    // Set up default outfit
    if(FlagBase.GetBool('Player_Chose_Outfit1'))
    {
        ChangeOutfit("Outfit1");
    }
    else if(FlagBase.GetBool('Player_Chose_Outfit2'))
    {
        ChangeOutfit("Outfit2");
    }
    else if(FlagBase.GetBool('Player_Chose_Outfit3'))
    {
        ChangeOutfit("Outfit3");
    }
    else if(FlagBase.GetBool('Player_Chose_Outfit4'))
    {
        ChangeOutfit("Outfit4");
    }
    else if(FlagBase.GetBool('Player_Chose_Outfit5'))
    {
        ChangeOutfit("Outfit5");
    }
    else if(FlagBase.GetBool('Player_Chose_Outfit6'))
    {
        ChangeOutfit("Outfit6");
    }
    else
    {
        ChangeOutfit("Default");
    }
    
    // Transition
    if(FlagBase.GetBool('TransitionIn'))
    {
        GoToState('TransitionIn');
        FlagBase.SetBool('TransitionIn', False,, 0);
    }
}

// ----------------
// Console commands
// ----------------
exec function AddAug(string AugName)
{
    AugName = Caps(AugName);

    switch(AugName)
    {
        case "TELEPATHY":
            AugmentationSystem.GivePlayerAugmentation(class'MachineGod.AugTelepathy');
            break;
        
        case "POSSESSION":
            AugmentationSystem.GivePlayerAugmentation(class'MachineGod.AugPossession');
            break;
    }
}

// -------------------------------------
// Gender swaps Tiffany (for possession)
// -------------------------------------
function SetFemale(bool bNewIsFemale)
{
    bIsFemale = bNewIsFemale;

    if(bIsFemale)
    {
        EyePain = Sound'MachineGod.Player.EyePain';
        Cough = Sound'MachineGod.Player.Cough';
        Drown = Sound'MachineGod.Player.Drown';
        WaterDie = Sound'MachineGod.Player.WaterDeath';
        GaspSound = Sound'MachineGod.Player.Gasp';
        JumpSound = Sound'MachineGod.Player.Jump';
        HitSound1 = Sound'MachineGod.Player.PainSmall';
        HitSound2 = Sound'MachineGod.Player.PainMedium';
        HitSound3 = Sound'MachineGod.Player.PainSmall';
        Land = Sound'MachineGod.Player.Land';
        Die = Sound'MachineGod.Player.Death';
    }
    else
    {
        EyePain = Sound'DeusExSounds.Player.MaleEyePain';
        Cough = Sound'DeusExSounds.Player.MaleCough';
        Drown = Sound'DeusExSounds.Player.MaleDrown';
        WaterDie = Sound'DeusExSounds.Player.MaleWaterDeath';
        GaspSound = Sound'DeusExSounds.Player.MaleGasp';
        JumpSound = Sound'DeusExSounds.Player.MaleJump';
        HitSound1 = Sound'DeusExSounds.Player.MalePainSmall';
        HitSound2 = Sound'DeusExSounds.Player.MalePainMedium';
        HitSound3 = Sound'DeusExSounds.Player.MalePainSmall';
        Land = Sound'DeusExSounds.Player.MaleLand';
        Die = Sound'DeusExSounds.Player.MaleDeath';
    }
}

// ---------------------------------------
// Checks whether we're possessing someone
// ---------------------------------------
function bool IsPossessing()
{
    local Augmentation Aug;   
    
    Aug = AugmentationSystem.FindAugmentation(class'AugPossession');

    return Aug != None && Aug.bIsActive;
}

// -------------------------------------------
// Perform extra checks before starting convos
// -------------------------------------------
function bool StartConversation(
    Actor InvokeActor, 
    EInvokeMethod InvokeMethod, 
    optional Conversation Con,
    optional bool bAvoidState,
    optional bool bForcePlay
    )
{
    // Don't go into convos while possessing (the energy drain would prevent it anyway)
    if(IsPossessing() || IsSpaced())
        return False;
    
    return Super.StartConversation(InvokeActor, InvokeMethod, Con, bAvoidState, bForcePlay);
}

// ---------------------------------
// Check if we're done transitioning
// ---------------------------------
function bool IsDoneTransitioning()
{
    return TransitionTimer >= TransitionDuration;
}

// --------------------
// State: Transition in
// --------------------
state TransitionIn
{
    event PlayerTick(float DeltaTime)
    {
        GlitchTimer += DeltaTime;
        TransitionTimer += DeltaTime;
        ShowHUD(False);
       
        if(GlitchTimer >= GlitchInterval)
        {
            GlitchTimer = 0;
            
            if (Sprite == None)
            {
                Sprite = GlitchSprite;
            }
            else
            {
                Sprite = None;
            }
        }
        
        GlitchInterval = Smerp(TransitionTimer / TransitionDuration, 0.2, 0.001);
        DesiredFOV = Smerp(TransitionTimer / TransitionDuration, 170, Default.DesiredFOV);
        SetFOVAngle(DesiredFOV);

        if(TransitionTimer >= TransitionDuration)
        {
            Sprite = None;
            DesiredFOV = Default.DesiredFOV;
            ShowHUD(True);
            GoToState('PlayerWalking');
        }
    }

Begin:
    AugmentationSystem.DeactivateAll();
    GlitchTimer = 0;
    TransitionDuration = 2.0;
    TransitionTimer = 0;
    SetFOVAngle(170);
    DesiredFOV = 170;
    Sleep(0.0);
    PlaySound(Sound'MachineGod.Transient.TransitionIn', SLOT_Pain);
}

// ---------------------
// State: Transition out
// ---------------------
state TransitionOut
{
    event PlayerTick(float DeltaTime)
    {
        GlitchTimer += DeltaTime;
        TransitionTimer += DeltaTime;
        ShowHUD(False);
       
        if(GlitchTimer >= GlitchInterval)
        {
            GlitchTimer = 0;
            
            if (Sprite == None)
            {
                Sprite = GlitchSprite;
            }
            else
            {
                Sprite = None;
            }
        }
        
        GlitchInterval = Smerp(TransitionTimer / TransitionDuration, 0.001, 0.2);
        DesiredFOV = Smerp(TransitionTimer / TransitionDuration, Default.DesiredFOV, 170);
        SetFOVAngle(DesiredFOV);

        if(TransitionTimer >= TransitionDuration)
        {
            Sprite = GlitchSprite;
            DesiredFOV = 170;
        }
    }

    function BeginState()
    {
        AugmentationSystem.DeactivateAll();
        GlitchTimer = 0;
        TransitionDuration = 2.0;
        TransitionTimer = 0;
        SetFOVAngle(Default.DesiredFOV);
        DesiredFOV = Default.DesiredFOV;
        PlaySound(Sound'MachineGod.Transient.TransitionOut', SLOT_Pain);
    }
}

// END UE1

defaultproperties
{
    TruePlayerName="Tiffany Savage"
    FamiliarName="Tiffany Savage"
    UnfamiliarName="Tiffany Savage"
    bIsFemale=True
    BaseEyeHeight=38.000000
    Mesh=LodMesh'DeusExCharacters.GFM_Trench'
    AugmentationManagerClass=class'TiffanySavageAugmentationManager'
    MultiSkins(0)=Texture'MachineGod.Skins.TiffanySavage_Head'
    MultiSkins(1)=Texture'MachineGod.Skins.TiffanySavage_Coat'
    MultiSkins(2)=Texture'MachineGod.Skins.TiffanySavage_Legs'
    MultiSkins(3)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(4)=Texture'MachineGod.Skins.TiffanySavage_ShirtFront'
    MultiSkins(5)=Texture'MachineGod.Skins.TiffanySavage_Coat'
    MultiSkins(6)=Texture'DeusExItems.Skins.GrayMaskTex'
    MultiSkins(7)=Texture'DeusExItems.Skins.BlackMaskTex'
    CollisionHeight=43.000000
   
    EyePain=Sound'MachineGod.Player.EyePain'
    Cough=Sound'MachineGod.Player.Cough'
    Drown=Sound'MachineGod.Player.Drown'
    WaterDie=Sound'MachineGod.Player.WaterDeath'
    GaspSound=Sound'MachineGod.Player.Gasp'
    JumpSound=Sound'MachineGod.Player.Jump'
    HitSound1=Sound'MachineGod.Player.PainSmall'
    HitSound2=Sound'MachineGod.Player.PainMedium'
    HitSound3=Sound'MachineGod.Player.PainSmall'
    Land=Sound'MachineGod.Player.Land'
    Die=Sound'MachineGod.Player.Death'
    
    GlitchSprite=FireTexture'Effects.Wepn_Prod_FX'
    TransitionDuration=2.0
    
    Outfits(0)=(Name="Office",Mesh=LodMesh'DeusExCharacters.GFM_SuitSkirt',Tex1=Texture'MachineGod.Skins.TiffanySavage_Head',Tex2=None,Tex3=Texture'MachineGod.Skins.Legs_Female_Skin2',Tex4=Texture'MachineGod.Skins.OfficeDress2_Female_Skin2',Tex5=Texture'MachineGod.Skins.OfficeDress2_Female_Skin2',Tex6=Texture'MachineGod.Skins.Frames5',Tex7=Texture'MachineGod.Skins.Lenses2')
    Outfits(1)=(Name="Vat",Mesh=LodMesh'DeusExCharacters.GFM_Dress',Tex0=Texture'DeusExItems.Skins.PinkMaskTex',Tex1=Texture'MachineGod.Skins.Legs_Female_Skin2',Tex2=Texture'MachineGod.Skins.TiffanySavage_VatHip',Tex3=Texture'MachineGod.Skins.TiffanySavage_VatTorso',Tex4=Texture'DeusExItems.Skins.PinkMaskTex',Tex5=Texture'DeusExItems.Skins.PinkMaskTex',Tex6=Texture'DeusExItems.Skins.PinkMaskTex',Tex7=Texture'MachineGod.Skins.TiffanySavage_Head')
    Outfits(2)=(Name="Outfit2",Mesh=LodMesh'DeusExCharacters.GFM_TShirtPants',Tex0=Texture'MachineGod.Skins.TiffanySavage_Head',Tex1=Texture'DeusExItems.Skins.PinkMaskTex',Tex2=Texture'DeusExItems.Skins.PinkMaskTex',Tex3=Texture'DeusExItems.Skins.GrayMaskTex',Tex4=Texture'DeusExItems.Skins.BlackMaskTex',Tex5=Texture'DeusExItems.Skins.PinkMaskTex',Tex6=Texture'MachineGod.Skins.TacticalPants4',Tex7=Texture'MachineGod.Skins.TacticalShirt4_Female')
    Outfits(3)=(Name="Outfit3",Tex1=Texture'MachineGod.Skins.TiffanySavage_Outfit3_Coat',Tex2=Texture'MachineGod.Skins.TacticalPants1',Tex3=Texture'DeusExItems.Skins.GrayMaskTex',Tex4=Texture'MachineGod.Skins.TiffanySavage_Outfit3_ShirtFront',Tex5=Texture'MachineGod.Skins.TiffanySavage_Outfit3_Coat',Tex6=Texture'MachineGod.Skins.Frames4',Tex7=Texture'MachineGod.Skins.Lenses4')
    Outfits(4)=(Name="Outfit4",Mesh=LodMesh'DeusExCharacters.GFM_SuitSkirt',Tex1=Texture'DeusExItems.Skins.PinkMaskTex',Tex2=Texture'DeusExItems.Skins.GrayMaskTex',Tex3=Texture'MachineGod.Skins.TiffanySavage_Outfit4_Legs',Tex4=Texture'TiffanySavage_Outfit4_Dress',Tex5=Texture'DeusExItems.Skins.PinkMaskTex',Tex6=Texture'MachineGod.Skins.Frames3',Tex7=Texture'MachineGod.Skins.Lenses2')
    Outfits(5)=(Name="Outfit5",Mesh=LodMesh'DeusExCharacters.GFM_TShirtPants',Tex0=Texture'MachineGod.Skins.TiffanySavage_Head',Tex1=Texture'DeusExItems.Skins.PinkMaskTex',Tex2=Texture'DeusExItems.Skins.PinkMaskTex',Tex3=Texture'DeusExItems.Skins.GrayMaskTex',Tex4=Texture'DeusExItems.Skins.BlackMaskTex',Tex5=Texture'DeusExItems.Skins.PinkMaskTex',Tex6=Texture'MachineGod.Skins.TiffanySavage_Outfit5_Legs',Tex7=Texture'MachineGod.Skins.TiffanySavage_Outfit5_Torso')
    Outfits(6)=(Name="Outfit6",Tex1=Texture'MachineGod.Skins.TiffanySavage_Outfit6_Coat',Tex2=Texture'MachineGod.Skins.TiffanySavage_Outfit6_Legs',Tex3=Texture'DeusExItems.Skins.GrayMaskTex',Tex4=Texture'MachineGod.Skins.TiffanySavage_Outfit6_ShirtFront',Tex5=Texture'DeusExItems.Skins.PinkMaskTex',Tex6=Texture'DeusExItems.Skins.GrayMaskTex',Tex7=Texture'DeusExItems.Skins.BlackMaskTex')
    Portraits(0)=Texture'MachineGod.UI.PlayerPortrait1'
    Portraits(1)=Texture'MachineGod.UI.PlayerPortrait2'
    Portraits(2)=Texture'MachineGod.UI.PlayerPortrait3'
    Portraits(3)=Texture'MachineGod.UI.PlayerPortrait4'
    Portraits(4)=Texture'MachineGod.UI.PlayerPortrait5'
}
