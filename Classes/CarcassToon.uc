class CarcassToon extends CarcassExtended;

var ActorOutline aOutline;

// BEGIN UE1

function PostBeginPlay()
{
    CreateOutline();
    
    Super.PostBeginPlay();
}

function CreateOutline()
{
    // The inverted models are a jumbled mess, so this doesn't work (yet)
    return;

    if(aOutline == None)
    {
        aOutline = Spawn(class'MachineGod.ActorOutline');
        aOutline.SetOwner(Self);
    }
    else
    {
        aOutline.ApplyMesh();
    }
}

function Restore()
{
    Super.Restore();

    CreateOutline();
}

function InitFor(Actor aOther)
{
    Super.InitFor(aOther);

    CreateOutline();
}

// END UE1
