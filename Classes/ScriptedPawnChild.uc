class ScriptedPawnChild extends ScriptedPawnToon;

defaultproperties
{
    BindName="Child"
    FamiliarName="Child"
    UnfamiliarName="Child"
    WalkingSpeed=0.300000
    BaseEyeHeight=26.000000
    BaseAssHeight=-23.000000
    WalkingSpeed=0.256000
    WalkAnimMult=1.150000
    GroundSpeed=150.000000
    HealthHead=50
    HealthTorso=50
    HealthLegLeft=50
    HealthLegRight=50
    HealthArmLeft=50
    HealthArmRight=50
    Mesh=LodMesh'DeusExCharacters.GMK_DressShirt'
    Mass=80.000000
    Buoyancy=85.000000
    CollisionRadius=17.000000
    CollisionHeight=32.000000
    
    HitSound1=Sound'ChildPainMedium'
    HitSound2=Sound'ChildPainLarge'
    Die=Sound'ChildDeath'
}
