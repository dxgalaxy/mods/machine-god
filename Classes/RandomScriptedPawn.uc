class RandomScriptedPawn extends ScriptedPawnToon;

enum EBodyType
{
    BODY_Undefined,
    BODY_Normal,
    BODY_Fat,
    BODY_Skinny
};

enum EGenerationStatus
{
    GS_Waiting,
    GS_Success,
    GS_Fail
};

// Adjustable parameters
var() name StyleTag;                // Clothing style
var() int SkinTone;                 // Skin tone, 0 means any 
var() name Age;                     // Age
var() EBodyType BodyType;           // Body type

// Internal variables
var Texture PinkMask;
var Texture GrayMask;
var Texture BlackMask;
var EGenerationStatus GenerationStatus;

// BEGIN UE1

// ---------
// Init pawn
// ---------
function InitializePawn()
{
    if(!bInitialized && GenerationStatus == GS_Waiting)
        StartGeneration();

    if(Health <= 0)
        Died(Self, '', Self.Location + Normal(Vector(Self.Rotation)) * 16);

    Super.InitializePawn();
}

// ----------------
// Start generation
// ----------------
function StartGeneration()
{
    local string GenericName;

    GenerationStatus = GS_Waiting;
    
    if(StyleTag == '')
        StyleTag = 'Normal';
    
    Generate();
   
    Archetype = AT_Civilian;

    switch(StyleTag)
    {
        case 'Guerilla':
            Archetype = AT_Military;
            GenericName = "Thug";
            break;

        case 'Military':
            Archetype = AT_Military;
            GenericName = "Soldier";
            break;
        
        case 'Police':
            Archetype = AT_Military;
            GenericName = "Police officer";
            break;
        
        case 'Science':
            GenericName = "Scientist";
            break;
        
        case 'Medical':
            GenericName = "Medical worker";
            break;
    
        case 'Tech':
            GenericName = "Technician";
            break;
    }
   
    if(GenericName != "")
    {
        if(UnfamiliarName == Default.UnfamiliarName)
            UnfamiliarName = GenericName;

        if(FamiliarName == Default.FamiliarName)
            FamiliarName = GenericName;
    }

    ApplyScale();
    ApplyArchetype();
    CreateOutline();
}

// ----------------------
// Applies a random scale
// ----------------------
function ApplyScale()
{
    local float Min, Max;

    Min = 0.94;
    Max = 1.06;
    
    // Asians are a bit shorter
    if(SkinTone == 2)
    {
        Min = 0.9;
        Max = 1.0;
    }

    // Africans are a bit taller
    else if(SkinTone == 4)
    {
        Min = 1.0;
        Max = 1.1;
    }

    DrawScale = Min + (FRand() * (Max - Min)); 

    SetCollisionSize(CollisionRadius * DrawScale, CollisionHeight * DrawScale);
}

// -----------------------
// Generates the character
// -----------------------
function Generate() {}

// -------------------------------------------
// Gets a random texture from the skin library
// -------------------------------------------
function Texture GetRandomTexture(name Gender, name Category)
{
    local Texture Result;
  
    if(SkinTone < 1)
        SkinTone = Rand(4) + 1;
    
    Result = class'SkinLibrary'.static.GetRandomTexture(StyleTag, Gender, Category, SkinTone, Age);

    if(Result == Texture'Engine.DefaultTexture')
        GenerationStatus = GS_Fail;

    return Result;
}

// END UE1

defaultproperties
{
    // Default textures
    MultiSkins(0)=Texture'Engine.DefaultTexture'
    MultiSkins(1)=Texture'Engine.DefaultTexture'
    MultiSkins(2)=Texture'Engine.DefaultTexture'
    MultiSkins(3)=Texture'Engine.DefaultTexture'
    MultiSkins(4)=Texture'Engine.DefaultTexture'
    MultiSkins(5)=Texture'Engine.DefaultTexture'
    MultiSkins(6)=Texture'Engine.DefaultTexture'
    MultiSkins(7)=Texture'Engine.DefaultTexture'

    // Masks
    PinkMask=Texture'DeusExItems.Skins.PinkMaskTex'
    GrayMask=Texture'DeusExItems.Skins.GrayMaskTex'
    BlackMask=Texture'DeusExItems.Skins.BlackMaskTex'
}
