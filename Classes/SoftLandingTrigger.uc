class SoftLandingTrigger extends Trigger;

var Pawn aPawn;

function Touch(Actor aOther)
{
    if(!IsRelevant(aOther))
        return;

    aPawn = Pawn(aOther);

    if(aPawn == None)
        return;

    aPawn.ReducedDamageType = 'All';

    GoToState('Landing');
}

state Landing
{
Begin:
    while(aPawn != None && aPawn.Physics == PHYS_Falling)
        Sleep(0.0);

    aPawn.ReducedDamageType = '';
    aPawn = None;

    if(bTriggerOnceOnly)
        Destroy();
}
