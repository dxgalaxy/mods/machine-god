//=============================================================================
// Mission18 - Streets (Simulation), Tijuana, Mexico
//=============================================================================
class Mission18Streets extends Mission18;

var Actor aActor;
var ScriptedPawn aScriptedPawn;
var float ExitTimer;

// BEGIN UE1

function TravelToMap()
{
    Flags.SetBool('Player_Met_Hypnos', True,, 18); 

    if(Flags.GetBool('Mike_Woke'))
        MoveActor('Mike', 'MikeWokePosition');
}

function InitMap()
{
    Super.InitMap();

    Flags.SetBool('Player_Met_Hypnos', True);
    GetActor('DariosBeer').bOwned = True;
    GetActor('HectorsBeer').bOwned = True;
    OpenDoor('Jackson2Lid2');
}

function Timer()
{
    local bool bForceMikeConvo;
    local HypnosEnforcer aHypnosEnforcer;
    local DeusExMover aDeusExMover;
    
    Super.Timer();

    // Purge mode
    if(!IsInState('Rebooting') && Flags.GetBool('Purge_Mode'))
    {
        // Turn any actor in the player's vicinity into enforcers
        foreach Player.RadiusActors(class'ScriptedPawn', aScriptedPawn, 1024)
        {
            if(
                aScriptedPawn.IsA('Animal') ||
                aScriptedPawn.IsA('Hypnos') ||
                aScriptedPawn.IsA('HypnosEnforcer') ||
                aScriptedPawn.IsA('LarryWinger') ||
                aScriptedPawn.IsA('Mike') ||
                aScriptedPawn.IsA('AntonJackson')
            )
                continue;

            aHypnosEnforcer = Spawn(class'MachineGod.HypnosEnforcer');
            aHypnosEnforcer.Absorb(aScriptedPawn);
        }
    }

    // Force Mike's woke convo, if
    // 1. He's woke, but we haven't played the convo yet
    // 2. ...AND we're not in the middle of a conversation or transition. 
    bForceMikeConvo = Flags.GetBool('Mike_Woke') && !Flags.GetBool('WokeMike_Played');
    bForceMikeConvo = bForceMikeConvo && !Player.InConversation() && !Player.IsInState('TransitionIn') && !Player.IsInState('TransitionOut'); 

    if(bForceMikeConvo)
    {
        foreach AllActors(class'ScriptedPawn', aScriptedPawn, 'Mike')
        {
            aScriptedPawn.Frob(Player, Player.Inventory);
        }
    }

    // Start the train once the panel is unlocked
    foreach AllActors(class'DeusExMover', aDeusExMover, 'TrainPanel')
    {
        if(!aDeusExMover.bLocked && !IsInState('Exiting'))
        {
            GoToState('Exiting');
        }
    }

    // Check for dead Jacksons
    if(
        Flags.GetBool('Jackson1_Dead') &&
        Flags.GetBool('Jackson3_Dead') &&
        Flags.GetBool('Jackson4_Dead') &&
        Flags.GetBool('Jackson5_Dead') &&
        Flags.GetBool('Jackson6_Dead')
    )
    {
        Flags.SetBool('Other_Jacksons_Dead', True);
        Flags.SetBool('Player_Killed_Jacksons', True,, 0);
    }

    // Set a convenience flag for Jackson's quest
    if(Flags.GetBool('Other_Jacksons_Dead') || Flags.GetBool('Player_Spoke_To_Jackson5'))
        Flags.SetBool('Player_Resolved_Jackson2_Quest', True);
}

// -----------------------------------------------------------
// Makes sure Mike and Anton follow the player into the sewers
// -----------------------------------------------------------
function bool TeleportFollowersToSewers()
{
    local ScriptedPawn aAnton;
    local ScriptedPawn aMike;
    local PatrolPoint aPoint;
    local bool bSuccess;

    bSuccess = True;
    aAnton = GetPawn('Anton');
    aMike = GetPawn('Mike');

    if(
        aAnton != None &&
        Flags.GetBool('AntonJackson_Following_Player') &&
        VSize(aAnton.Location - Player.Location) > 2048
    )
    {
        aPoint = PatrolPoint(GetActor('AntonSewerSpawnPoint'));
        
        if(aPoint == None || !aAnton.SetLocation(aPoint.Location))
        {
            bSuccess = False;
        }
        else
        {
            aAnton.DesiredRotation = aPoint.Rotation;
            aAnton.SetPhysics(PHYS_Falling);
        }
    }

    if(
        aMike != None &&
        Flags.GetBool('Mike_Following_Player') &&
        VSize(aMike.Location - Player.Location) > 2048
    )
    {
        aPoint = PatrolPoint(GetActor('MikeSewerSpawnPoint'));
        
        if(aPoint == None || !aMike.SetLocation(aPoint.Location))
        {
            bSuccess = False;
        }
        else
        {
            aMike.DesiredRotation = aPoint.Rotation;
            aMike.SetPhysics(PHYS_Falling);
        }
    }

    return bSuccess;
}

// -------
// Exiting
// -------
state Exiting
{
    function Tick(float DeltaTime)
    {
        ExitTimer += DeltaTime;
        
        if(ExitTimer > 2.0)
        {
            Player.DesiredFOV = Lerp((ExitTimer - 2.0) / 4.0, Player.Default.DesiredFOV, 140); 
            Player.SetFOVAngle(Player.DesiredFOV);
        }
    }

Begin:
    Flags.SetBool('ClearInventory', True,, 0);
    Player.SetInHandPending(None);

    Sleep(1.0);

    foreach AllActors(class'Actor', aActor, 'StartTrain')
    {
        aActor.Trigger(Player, Player);
        break;
    }

    Sleep(1.2);
            
    Player.ShowHUD(False);

    foreach AllActors(class'Actor', aActor, 'Train')
        aActor.bHidden = True;
    
    foreach AllActors(class'Actor', aActor, 'TrainPanel')
        aActor.bHidden = True;
    
    foreach AllActors(class'Actor', aActor, 'TrainDoorsLeft')
        aActor.bHidden = True;
    
    foreach AllActors(class'Actor', aActor, 'TrainDoorsRight')
        aActor.bHidden = True;
    
    foreach AllActors(class'Actor', aActor, 'TrainDoorButtons')
        aActor.bHidden = True;
}

// Events
state EventManager
{

MikeFollow:
    OrderPawn('Mike', 'Following');
    Flags.SetBool('Mike_Following_Player', True);
    
    EndEvent();

SitJackson2Down:
    OrderPawn('Jackson2', 'Sitting');
    Sleep(1.0);
    CloseDoor('Jackson2Lid2');
    
    EndEvent();

Jackson5Alert:
    SetPawnAlly('Jackson5', 'Player', -1, True);
    
    MoveActorBehind('Jackson3', 'Player', 128, 8192, 2048);
    MoveActorBehind('Jackson4', 'Player', 128, 8192, 2048);

    EndEvent();

Jackson5Neutral:
    SetPawnAlly('Jackson3', 'Player', 0);
    OrderPawn('Jackson3', 'GoingTo', 'Jackson3Point');
    
    SetPawnAlly('Jackson4', 'Player', 0);
    OrderPawn('Jackson4', 'GoingTo', 'Jackson4Point');
    
    SetPawnAlly('Jackson5', 'Player', 0);
    
    SetPawnAlly('Jackson6', 'Player', 0);
    
    SetPawnAlly('NSFBot1', 'Player', 0);
    OrderPawn('NSFBot1', 'Patrolling', 'BotPatrol1');
    
    SetPawnAlly('NSFBot2', 'Player', 0);
    OrderPawn('NSFBot2', 'Patrolling', 'BotPatrol3');
    
    EndEvent();

AntonFollow:
    Flags.SetBool('AntonJackson_Following_Player', True);
    SetActorCollision('AntonNSFFollowTrigger', True);
    SetActorCollision('MikeExitConvoTrigger', True);
    SetActorCollision('AntonExitConvoTrigger', True);
    
    EndEvent();

PlayerReachedNSFSewers:
    while(!TeleportFollowersToSewers())
        Sleep(0.5);

    EndEvent();

}

// -----
// Tests
// -----
function DoTest(name TestName)
{
    switch(TestName)
    {
        case 'NSF':
            Flags.SetBool('Player_Met_Hypnos', True);
            Flags.SetBool('Mike_Following_Player', True);
            Flags.SetBool('Mike_Woke', True);
            Flags.SetBool('MeetMike_Played', True);
            Flags.SetBool('WokeMike_Played', True);
            ChangePawnOutfit('Player', 'Default');
            ChangePawnOutfit('Mike', 'Default');
            MoveActor('Player', 'NSF');
            MoveActor('Mike', 'NSF');
            OrderPawn('Mike', 'Following');
            break;

        case 'Purge':
            DoTest('NSF');
            OpenDoor('AntonGate');
            MoveActor('Player', 'Jackson2Spot');
            DoEvent('Jackson5Neutral');
            Flags.SetBool('MeetJackson2_Played', True);
            Flags.SetBool('MeetJackson5_Played', True);
            Flags.SetBool('Player_Spoke_To_Jackson5', True);
            Flags.SetBool('Player_Got_Jackson2_Quest', True);
            Flags.SetBool('Player_Resolved_Jackson2_Quest', True);
            Flags.SetBool('Player_Completed_Jackson2_Quest', True);
            Flags.SetBool('AntonJackson_Following_Player', True);
            Flags.SetBool('Purge_Mode', True);
            Flags.SetBool('MeetAntonFreed_Played', True);
            Flags.SetBool('MeetAntonTrapped_Played', True);
            OrderPawn('Anton', 'Following');
            SetActorCollision('AntonNSFFollowTrigger', True);
            SetActorCollision('MikeSewersFollowTrigger', True);
            SetActorCollision('AntonSewersFollowTrigger', True);
            break;

        case 'Exit':
            DoTest('Purge');
            MoveActor('Player', 'SubwayHall');
            MoveActor('Mike', 'SubwayHall');
            MoveActor('Anton', 'SubwayHall');
            OrderPawn('Mike', 'RunningTo', 'MikeExitConvoPoint');
            OrderPawn('Anton', 'RunningTo', 'AntonExitConvoPoint');
            break;
    }
}

// END UE1
