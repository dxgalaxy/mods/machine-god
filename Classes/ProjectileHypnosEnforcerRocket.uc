class ProjectileHypnosEnforcerRocket extends RocketMini;

simulated function SpawnRocketEffects() {}

defaultproperties
{
     SpawnSound=Sound'DeusExSounds.Robot.RobotFireRocket'
     Mesh=LodMesh'DeusExItems.FireComet'
     DrawScale=4.0
     MultiSkins(0)=FireTexture'Effects.Electricity.Nano_SFX'
     Style=STY_Translucent
}
