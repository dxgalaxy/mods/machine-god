class RandomFemale extends RandomScriptedPawn;

// BEGIN UE1

// -----------------------
// Generates the character
// -----------------------
function Generate()
{
    // Practical attire
    if(StyleTag == 'Tech' || StyleTag == 'Bum' || StyleTag == 'Police' || StyleTag == 'Military' || StyleTag == 'Guerilla')
    {
        GenerateGFMTShirtPants();
    }
    // Scientists
    else if(StyleTag == 'Science')
    {
        GenerateGFMTrench();
    }
    // Doctors and nurses
    else if(StyleTag == 'Medical')
    {
        switch(Rand(2))
        {
            case 0:
                GenerateGFMTShirtPants();
                break;
            case 1:
                GenerateGFMTrench();
                break;
        }
    }
    // Formally dressed ladies
    else if(StyleTag == 'Formal')
    {
        GenerateGFMSuitSkirt();
    }
    // Everyone else
    else
    {
        switch(Rand(3))
        {
            case 0:
                GenerateGFMDress();
                break;
            
            case 1:
                GenerateGFMTrench();
                break;
            
            case 2:
                GenerateGFMTShirtPants();
                break;
        }
    }
}

// ---------------
// GFM_TShirtPants
// NOTE: UV mapping on slot 1 (hair sides) is so terrible we're omitting it
// ---------------
function GenerateGFMTShirtPants()
{
    Mesh = LodMesh'DeusExCharacters.GFM_TShirtPants';

    // Head
    MultiSkins[1] = PinkMask;
    MultiSkins[2] = PinkMask;
    MultiSkins[0] = GetRandomTexture('Female', 'Heads');

    // Ponytail
    if(Rand(2) > 0)
        MultiSkins[2] = MultiSkins[0];

    // Glasses
    if(Rand(2) > 0)
    {
        MultiSkins[3] = GetRandomTexture('Common', 'Frames');
        MultiSkins[4] = GetRandomTexture('Common', 'Lenses');   
    }
    else
    {
        MultiSkins[3] = GrayMask;
        MultiSkins[4] = BlackMask;
    }

    // Torso
    MultiSkins[7] = GetRandomTexture('Female', 'Torsos');
   
    // Legs
    MultiSkins[6] = GetRandomTexture('Common', 'Pants');
}

// ----------------------
// GFM_Trench
// ----------------------
function GenerateGFMTrench()
{
    Mesh = LodMesh'DeusExCharacters.GFM_Trench';

    // Head
    MultiSkins[0] = GetRandomTexture('Female', 'Heads');
    
    // Glasses
    if(Rand(2) > 0)
    {
        MultiSkins[6] = GetRandomTexture('Common', 'Frames');   
        MultiSkins[7] = GetRandomTexture('Common', 'Lenses');   
    }
    else
    {
        MultiSkins[6] = GrayMask;
        MultiSkins[7] = BlackMask;
    }
    
    // Shirt front
    MultiSkins[4] = GetRandomTexture('Female', 'ShirtFronts');

    // Coat
    MultiSkins[1] = GetRandomTexture('Female', 'Coats');
    MultiSkins[5] = MultiSkins[1];

    // Legs
    MultiSkins[2] = GetRandomTexture('Female', 'ClothedLegs');
}

// -------------
// GFM_SuitSkirt
// -------------
function GenerateGFMSuitSkirt()
{
    if(BodyType == BODY_Undefined)
    {
        if(Rand(2) > 0)
            BodyType = BODY_Fat;
        else
            BodyType = BODY_Normal;
    }

    if(BodyType == BODY_Fat)
        Mesh = LodMesh'DeusExCharacters.GFM_SuitSkirt_F';
    else
        Mesh = LodMesh'DeusExCharacters.GFM_SuitSkirt';

    // Head
    MultiSkins[0] = GetRandomTexture('Female', 'Heads');
    
    // Bun
    if(Rand(2) > 0)
        MultiSkins[1] = MultiSkins[0];
    else
        MultiSkins[1] = PinkMask;

    // Glasses
    if(Rand(2) > 0)
    {
        MultiSkins[6] = GetRandomTexture('Common', 'Frames');   
        MultiSkins[7] = GetRandomTexture('Common', 'Lenses');   
    }
    else
    {
        MultiSkins[6] = GrayMask;
        MultiSkins[7] = BlackMask;
    }

    // Dress
    MultiSkins[4] = GetRandomTexture('Female', 'Dresses');
    MultiSkins[5] = MultiSkins[4];

    // Legs
    MultiSkins[3] = GetRandomTexture('Female', 'BareLegs');
}

// -------------
// GFM_Dress
// -------------
function GenerateGFMDress()
{
    Mesh = LodMesh'DeusExCharacters.GFM_Dress';

    // Head
    MultiSkins[5] = PinkMask;
    MultiSkins[6] = PinkMask;
    MultiSkins[7] = GetRandomTexture('Female', 'Heads');

    // Ponytail
    if(Rand(2) > 0)
        MultiSkins[6] = MultiSkins[7];

    // Torso
    MultiSkins[3] = GetRandomTexture('Female', 'Torsos');
   
    // Skirt
    MultiSkins[2] = GetRandomTexture('Female', 'Skirts');
    MultiSkins[4] = MultiSkins[2];

    // Legs
    MultiSkins[1] = GetRandomTexture('Female', 'BareLegs');
}

// END UE1

defaultproperties
{
    Mesh=LodMesh'DeusExCharacters.GFM_TShirtPants'
    CollisionHeight=44.5
    HitSound1=Sound'DeusExSounds.Player.FemalePainSmall'
    HitSound2=Sound'DeusExSounds.Player.FemalePainMedium'
    Die=Sound'DeusExSounds.Player.FemaleDeath'
    BaseEyeHeight=38.000000
    BaseAssHeight=-18.000000
    bIsFemale=True
    WalkingSpeed=0.320000
    WalkAnimMult=0.650000
    BindName="RandomFemale"
    FamiliarName="Woman"
    UnfamiliarName="Woman"
}
