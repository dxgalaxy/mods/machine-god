class ActorAnimator extends KeyPoint;

enum EAnimationType
{
    ANIM_Rotate,
    ANIM_PingPong
};

var() float AnimationSpeed;
var() Vector AnimationDelta;
var() EAnimationType AnimationType;

var Actor Actors[100];
var int NumActors;
var float AnimationTimer;
var float Lifetime;

function PostBeginPlay()
{
    local Actor aActor;

    if(Event == '')
        return;

    foreach AllActors(class'Actor', aActor, Event)
    {
        Actors[NumActors] = aActor;
        NumActors++;
    }
}

function Tick(float DeltaTime)
{
    local int i;
    local Rotator NewRotation;

    Lifetime += DeltaTime;

    for(i = 0; i < NumActors; i++)
    {
        // If this actor is falling, it means it's been picked up,
        // so we don't need to animate it anymore
        if(Actors[i] != None && Actors[i].Physics == PHYS_Falling)
            Actors[i] = None;

        if(Actors[i] == None)
            continue;

        switch(AnimationType)
        {
            case ANIM_Rotate:
                NewRotation = Actors[i].Rotation + Rotator(AnimationDelta) * DeltaTime * AnimationSpeed;
                
                if(NewRotation.Pitch > 65536 || NewRotation.Pitch < -65536)
                    NewRotation.Pitch = 0;

                if(NewRotation.Yaw > 65536 || NewRotation.Yaw < -65536)
                    NewRotation.Yaw = 0;
                
                if(NewRotation.Roll > 65536 || NewRotation.Roll < -65536)
                    NewRotation.Roll = 0;
                
                Actors[i].SetRotation(NewRotation);
                break;
            
            case ANIM_PingPong:
                Actors[i].SetLocation(Actors[i].Location + (AnimationDelta / 256) * Sin(Lifetime * AnimationSpeed));                 
                break;
        }
    }
}

defaultproperties
{
    bStatic=False,
    AnimationDelta=(X=0.0,Y=0.0,Z.0)
    AnimationSpeed=1.0
}
