//=============================================================================
// Mission20 - Hong Kong - Triad Compound
//=============================================================================
class Mission20TriadCompound extends Mission20;

// BEGIN UE1

function InitMap()
{
    local ScriptedPawnKnowledge aKnowledge;

    // Knowledge
    foreach AllActors(class'ScriptedPawnKnowledge', aKnowledge)
    {
        aKnowledge.AddObject('TongsOfficeKeypad', "for Tracer Tong's office door");
        aKnowledge.AddSubject('TongsOfficeKeypad', 'ScientistMale');
        aKnowledge.AddSubject('TongsOfficeKeypad', 'ScientistFemale');
    }
    
    // Doors
    if(!Flags.GetBool('TongsLabLockdown'))
        OpenDoor('LabBlastDoor');

    // Events
    BindEventToFlag('ScientistsGoToComputers', 'OverhearScientists_Played');
}

function Tick(float DeltaTime)
{
    local bool bShouldOpenTongsOfficeDoor;
    local DeusExWeapon aWep;

    Super.Tick(DeltaTime);

    if(GetPlayer() == None || Flags == None)
        return;

    if(
        Flags.GetBool('Player_Agreed_To_Merge_With_Helios') &&
        Flags.GetBool('MeetTracerTong_Played')
    )
    {
        bShouldOpenTongsOfficeDoor = !GetPlayer().HasA('DeusExWeapon');
            
        foreach AllActors(class'DeusExWeapon', aWep)
        {
            if(aWep.Region.Zone.Tag != 'TongsOfficeSafeZone')
                continue;
            
            bShouldOpenTongsOfficeDoor = False;
            break;
        }

        if(bShouldOpenTongsOfficeDoor && !IsDoorOpen('TongsOfficeDoor'))
            OpenDoor('TongsOfficeDoor');
        else if(!bShouldOpenTongsOfficeDoor && IsDoorOpen('TongsOfficeDoor'))
            CloseDoor('TongsOfficeDoor');
    }
}

function RemoveLooseWeapons()
{
    local DeusExWeapon aWep;

    foreach AlLActors(class'DeusExWeapon', aWep)
    {
        if(aWep.Owner != None && aWep.Owner.IsA('Pawn'))
            continue;

        aWep.Destroy();
    }
}

function SpawnTearGas()
{
    local TearGas aGas;
    local Actor aPoint;

    GetActor('GasSound').SoundVolume = 190;

    foreach AllActors(class'Actor', aPoint, 'TongsOfficeGasPoint')
    {
        aGas = Spawn(class'TearGas', None,, aPoint.Location);
        
        if(aGas == None)
            continue;
        
        aGas.Velocity = Vector(aPoint.Rotation);
        aGas.Acceleration = Vect(0,0,0);
        aGas.DrawScale = FRand() * 0.5 + 2.0;
        aGas.LifeSpan = FRand() * 10 + 30;
        aGas.bFloating = False;
        aGas.Instigator = Player;
    }
}

auto state EventManager
{
   
ElevatorToggleRoof:
    CloseDoor('ElevatorDoorGroundOuter');
    CloseDoor('ElevatorDoorGroundInner');
    
    if(IsDoorOpen('ElevatorDoorRoofInner'))
    {
        CloseDoor('ElevatorDoorRoofInner');
        Sleep(1.0);
        CloseDoor('ElevatorDoorRoofOuter');
    }
    else
    {
        OpenDoor('ElevatorDoorRoofOuter');
        Sleep(1.0);
        OpenDoor('ElevatorDoorRoofInner');
    }

    EndEvent();

ElevatorToggleGround:
    CloseDoor('ElevatorDoorRoofOuter');
    CloseDoor('ElevatorDoorRoofInner');
    
    if(IsDoorOpen('ElevatorDoorGroundInner'))
    {
        CloseDoor('ElevatorDoorGroundInner');
        Sleep(1.0);
        CloseDoor('ElevatorDoorGroundOuter');
    }
    else
    {
        OpenDoor('ElevatorDoorGroundOuter');
        Sleep(1.0);
        OpenDoor('ElevatorDoorGroundInner');
    }

    EndEvent();

ElevatorUp:
    // Only start if the player is at the ground floor elevator
    if(GetActorDistance('Player', 'ElevatorRoof') > GetActorDistance('Player', 'ElevatorGround'))
    {
        CloseDoor('ElevatorDoorGroundOuter');
        Sleep(1.0);
        CloseDoor('ElevatorDoorGroundInner');
        Sleep(1.0);
    
        // Only actually go up, if the player is still inside the elevator
        if(GetActorDistance('Player', 'ElevatorGround') < 128)
        {
            MoveActorSeamlessly('Player', 'ElevatorGround', 'ElevatorRoof');

            GetPlayer().PlaySound(Sound'Elevator2Start');
            Sleep(1.0);
            GetPlayer().AmbientSound = Sound'Elevator2Move';
            Sleep(4.0);
            GetPlayer().AmbientSound = None;
            GetPlayer().PlaySound(Sound'Elevator2Stop');
            Sleep(1.0);

            OpenDoor('ElevatorDoorRoofInner');
            Sleep(1.0);
            OpenDoor('ElevatorDoorRoofOuter');
        }
    }

    EndEvent();

ElevatorDown:
    if(GetActorDistance('Player', 'ElevatorGround') > GetActorDistance('Player', 'ElevatorRoof'))
    {
        CloseDoor('ElevatorDoorRoofOuter');
        Sleep(1.0);
        CloseDoor('ElevatorDoorRoofInner');
        Sleep(1.0);

        if(GetActorDistance('Player', 'ElevatorRoof') < 128)
        {
            MoveActorSeamlessly('Player', 'ElevatorRoof', 'ElevatorGround');
            
            GetPlayer().PlaySound(Sound'Elevator2Start');
            Sleep(1.0);
            GetPlayer().AmbientSound = Sound'Elevator2Move';
            Sleep(4.0);
            GetPlayer().AmbientSound = None;
            GetPlayer().PlaySound(Sound'Elevator2Stop');
            Sleep(1.0);

            OpenDoor('ElevatorDoorGroundInner');
            Sleep(1.0);
            OpenDoor('ElevatorDoorGroundOuter');
        }
    }

    EndEvent();

ScientistsGoToComputers:
    OrderPawn('ScientistMale1', 'Sitting', 'ScientistMale1Chair');
    OrderPawn('ScientistFemale1', 'Sitting', 'ScientistFemale1Chair');

    EndEvent();

MeetHeliosEnded:
    if(Flags.GetBool('Player_Agreed_To_Merge_With_Helios'))
        SetAllianceAlly('Triad', 'Player', 1, True);

    EndEvent();

PlayerEnteredTongsOffice:
    if(!Flags.GetBool('MeetTracerTong_Played') && Flags.GetBool('Player_Agreed_To_Merge_With_Helios'))
        GoTo('PlayerDownloadedBlueprint');
        
    EndEvent();

PlayerDownloadedBlueprint:
    Sleep(1.0);
    
    if(Player.ActiveComputer != None)
        Player.CloseThisComputer(Player.ActiveComputer);

    CloseDoor('TongsOfficeDoor');
    Flags.SetBool('TongsLabLockdown', True);
    CloseDoor('LabBlastDoor');
    
    Sleep(1.0);
    
    TriggerActor('ShowTracerTong');

    Sleep(1.0);

    if(Player.ActiveComputer != None)
        Player.CloseThisComputer(Player.ActiveComputer);
    
    StartConversation('TracerTong', 'MeetTracerTong');

    EndEvent();

MeetTracerTongEnded:
    RemoveLooseWeapons();
    TriggerActor('HideTracerTong');
    OpenDoor('NaniteInterfacingDoor');
    SetAllianceAlly('Triad', 'Player', 1, True);

    EndEvent();

ReleaseGas:
    TriggerActor('HideTracerTong');
   
    Sleep(1.0);

    SpawnTearGas();

    Sleep(3.0);

    StartConversation('JCDenton', 'TiffanyGasBark');

    Sleep(2.0);

    ModernRootWindow(Player.RootWindow).Blink(1, 0, 1, 1.0);

    GetPlayer().DrugEffectTimer = 5.0;

    Sleep(1.0);
    
    ModernRootWindow(Player.RootWindow).Fade(0, 3.0);

    Sleep(4.0);

    PlayTransientSound('Player', "DeusExSounds.Generic.BodyThud", 1, 1);

    Sleep(2.0);

    LoadMap("20_Dream");

    EndEvent();

PlayerEnteredNaniteInterfacing:
    CloseDoor('NaniteInterfacingDoor');

    Sleep(3.0);

    LoadMap("90_Ending_Helios", 'NaniteInterfacingTravelOffset'); 

    EndEvent();

}

// END UE1
