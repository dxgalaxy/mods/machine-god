class ModernHUDInfoLinkDisplay extends HUDInfoLinkDisplay;

var ModernRootWindow Root;
var ModernHUD HUD;
var Texture NewTexBackgrounds[9];
var Texture NewTexBorders[9];
var int MarginInner, MarginLeft, MarginRight, MarginTop, MarginBottom, PortraitSize, TextWidth;
var bool bSpeakerIsPlayer, bIconVisible;
var string CurrentText, ActualText;
var float CursorTimer, CharactersPerSecond;

// BEGIN UE1

// --------------
// Initialisation
// --------------
event InitWindow()
{
    Super.InitWindow();

    SetSize(MarginLeft + TextWidth + MarginInner + PortraitSize + MarginRight, MarginTop + PortraitSize + MarginBottom);
   
    bIconVisible = True;
    
    WinName.Hide();
    WinLine.Hide();

    WinText.Hide();
    WinPortrait.Hide();
    WinStatic.Hide();
}

// ----------------------------
// Sets the icon visible/hidden
// ----------------------------
function ShowDataLinkIcon(bool bShow)
{
    bIconVisible = True;

    WinPortrait.SetBackgroundStretching(True);

    WinPortrait.SetBackground(Texture'DataLinkIcon');
    WinPortrait.SetBackgroundStyle(DSTY_Masked);
    WinPortrait.Show(bShow);
    WinPortrait.SetPos(Width - MarginRight - PortraitSize, MarginTop);

    bPortraitVisible = False;

    WinStatic.Hide();
    WinLine.Hide();
}

// ------------------------
// Displays the spoken text
// ------------------------
function DisplayText(string NewText)
{
    bIconVisible = False;
    CurrentText = NewText;
    ActualText = "";
    bTickEnabled = True;

    if(bSpeakerIsPlayer)
    {
        WinText.Hide();
        WinPortrait.Hide();
        WinStatic.Hide();

        if(ModernRootWindow(GetRootWindow()).HUD.ConWindow == None)
            ModernRootWindow(GetRootWindow()).HUD.CreateConWindowFirst();
        
        ModernRootWindow(GetRootWindow()).HUD.ConWindow.Show();
        ModernRootWindow(GetRootWindow()).HUD.ConWindow.DisplayText(NewText, GetPlayerPawn());
    }
    else
    {
        WinPortrait.Show();
        WinStatic.Show();
        
        WinPortrait.SetPos(Width - MarginRight - PortraitSize, MarginTop);
        WinPortrait.SetSize(PortraitSize, PortraitSize);
        
        WinStatic.SetPos(Width - MarginRight - PortraitSize, MarginTop);
        WinStatic.SetSize(PortraitSize, PortraitSize);
        
        if(ModernRootWindow(GetRootWindow()).HUD.ConWindow != None)
            ModernRootWindow(GetRootWindow()).HUD.ConWindow.Destroy();
    }
}

// Configuration changed
event ConfigurationChanged()
{
    WinPortrait.SetPos(Width - MarginRight - PortraitSize, MarginTop);
    WinPortrait.SetSize(PortraitSize, PortraitSize);
    
    WinStatic.SetPos(Width - MarginRight - PortraitSize, MarginTop);
    WinStatic.SetSize(PortraitSize, PortraitSize);
}

// -------------------------------------------------------------
// Sets the current speaker
//
// Changes:
// - Look up the pawn in the map and use their assigned portrait
// -------------------------------------------------------------
function SetSpeaker(string BindName, string DisplayName)
{
    local ScriptedPawnExtended aPawn;
        
    bSpeakerIsPlayer = False;

    // If this is the player speaking, remove the portrait
    if(BindName == "JCDenton")
    {
        bSpeakerIsPlayer = True;
        SpeakerPortrait = None;
        bPortraitVisible = False; 
        return;
    }
        
    // See if we can find an actor with a portrait
    foreach Player.AllActors(class'ScriptedPawnExtended', aPawn)
    {
        if(aPawn.BindName != BindName)
            continue;
   
        SpeakerPortrait = aPawn.PortraitTexture;
        return;
    }

    // Lookup failed, use the old method
    Super.SetSpeaker(BindName, DisplayName);
}

function ShowPortrait()
{
    WinPortrait.SetBackground(SpeakerPortrait);
    WinPortrait.SetBackgroundStyle(DSTY_Normal);
    WinPortrait.Show(True);

    bPortraitVisible = True;
    
    WinStatic.Show(True);
}

// -----------------------------------------------------
// Draws this window
//
// Changes:
// - Draw text live instead of relying on ComputerWindow
// - Added shadow background
// -----------------------------------------------------
function DrawWindow(GC gc)
{
    if(Root == None)
        Root = ModernRootWindow(GetRootWindow());
    
    if(HUD == None)
        HUD = ModernHUD(Root.HUD);

    if(CurrentText != "" && !bSpeakerIsPlayer)
        HUD.DrawShadow(gc, 0, 0, Width, Height, 1, 1);

    Super.DrawWindow(gc);

    if(CurrentText != "" && !bSpeakerIsPlayer)
        DrawText(gc);
}

// ----
// Tick
// ----
event Tick(float DeltaTime)
{
    if(
        bIsVisible &&
        Root != None &&
        Root.HUD != None &&
        Root.HUD.bIsVisible &&
        Len(ActualText) < Len(CurrentText)
    )
    {
        CursorTimer += DeltaTime;
        
        if(CursorTimer >= 1.0 / CharactersPerSecond)
        {
            ActualText = Left(CurrentText, Len(ActualText) + 1);
            CursorTimer = 0;
        }
    }
}

// --------------
// Draws the text
// --------------
function DrawText(GC gc)
{
    local float ActualTextWidth, ActualTextHeight;

    gc.SetFont(FontText);
    gc.SetAlignments(HALIGN_Left, VALIGN_Bottom);
    gc.EnableWordWrap(True);
    gc.SetTextColor(ColText);

    gc.GetTextExtent(TextWidth, ActualTextWidth, ActualTextHeight, ActualText);
    
    gc.DrawText(MarginLeft, MarginTop, TextWidth, Min(ActualTextHeight, PortraitSize), ActualText);
}

// ------------------------------------------
// Draws the background
//
// Changes:
// - Adopted style from HUDSharedBorderWindow
// ------------------------------------------
function DrawBackground(GC gc)
{
    if(!bSpeakerIsPlayer && !bIconVisible)
    {
        gc.SetStyle(BackgroundDrawStyle);
        gc.SetTileColor(ColBackground);
        gc.DrawBorders(0, 0, Width, Height, 0, 0, 0, 0, NewTexBackgrounds);
    }
}

// ------------------------------------------
// Draws the border
//
// Changes:
// - Adopted style from HUDSharedBorderWindow
// ------------------------------------------
function DrawBorder(GC gc)
{
    if(bDrawBorder && !bSpeakerIsPlayer && !bIconVisible)
    {
        gc.SetStyle(BorderDrawStyle);
        gc.SetTileColor(ColBorder);
        gc.DrawBorders(0, 0, Width, Height, 0, 0, 0, 0, NewTexBorders);
    }
}

// END UE1

defaultproperties
{
    bTickEnabled=True
    CharactersPerSecond=40
    MarginInner=6
    MarginLeft=18
    MarginRight=14
    MarginTop=20
    MarginBottom=20
    PortraitSize=96
    IncomingTransmission=""
    FontText=Font'FontLocation'
    TextWidth=380
    NewTexBackgrounds(0)=Texture'DeusExUI.UserInterface.HUDWindowBackground_TL'
    NewTexBackgrounds(1)=Texture'DeusExUI.UserInterface.HUDWindowBackground_TR'
    NewTexBackgrounds(2)=Texture'DeusExUI.UserInterface.HUDWindowBackground_BL'
    NewTexBackgrounds(3)=Texture'DeusExUI.UserInterface.HUDWindowBackground_BR'
    NewTexBackgrounds(4)=Texture'DeusExUI.UserInterface.HUDWindowBackground_Left'
    NewTexBackgrounds(5)=Texture'DeusExUI.UserInterface.HUDWindowBackground_Right'
    NewTexBackgrounds(6)=Texture'DeusExUI.UserInterface.HUDWindowBackground_Top'
    NewTexBackgrounds(7)=Texture'DeusExUI.UserInterface.HUDWindowBackground_Bottom'
    NewTexBackgrounds(8)=Texture'DeusExUI.UserInterface.HUDWindowBackground_Center'
    NewTexBorders(0)=Texture'DeusExUI.UserInterface.HUDWindowBorder_TL'
    NewTexBorders(1)=Texture'DeusExUI.UserInterface.HUDWindowBorder_TR'
    NewTexBorders(2)=Texture'DeusExUI.UserInterface.HUDWindowBorder_BL'
    NewTexBorders(3)=Texture'DeusExUI.UserInterface.HUDWindowBorder_BR'
    NewTexBorders(4)=Texture'DeusExUI.UserInterface.HUDWindowBorder_Left'
    NewTexBorders(5)=Texture'DeusExUI.UserInterface.HUDWindowBorder_Right'
    NewTexBorders(6)=Texture'DeusExUI.UserInterface.HUDWindowBorder_Top'
    NewTexBorders(7)=Texture'DeusExUI.UserInterface.HUDWindowBorder_Bottom'
    NewTexBorders(8)=Texture'DeusExUI.UserInterface.HUDWindowBorder_Center'
}
