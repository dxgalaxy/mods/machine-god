class BarrelHenosia extends Containers;

defaultproperties
{
     HitPoints=30
     bInvincible=True
     bFlammable=False
     ItemName="Henosia Storage Container"
     bBlockSight=True
     MultiSkins(1)=FireTexture'Effects.Electricity.Nano_SFX_A'
     Mesh=LodMesh'DeusExDeco.BarrelAmbrosia'
     CollisionRadius=16.000000
     CollisionHeight=28.770000
     LightType=LT_Steady
     LightEffect=LE_WateryShimmer
     LightBrightness=96
     LightHue=140
     LightRadius=4
     Mass=80.000000
     Buoyancy=90.000000
}
