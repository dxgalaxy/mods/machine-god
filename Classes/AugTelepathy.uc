class AugTelepathy extends AugTargeted;

var ScriptedPawnKnowledge aKnowledgeList[100];
var int NumKnowledge;

// BEGIN UE1

function bool HasKnowledge(ScriptedPawn aPawn)
{
    local ScriptedPawnKnowledge aKnowledge;
    local int i;

    if(NumKnowledge < 1)
    {
        foreach AllActors(class'ScriptedPawnKnowledge', aKnowledge)
        {
            aKnowledgeList[NumKnowledge] = aKnowledge;
            NumKnowledge++;
        }
    }

    for(i = 0; i < NumKnowledge; i++)
    {
        if(aKnowledgeList[i].HasPawn(aPawn))
            return True;
    }

    return False;
}

function bool IsRelevant(ScriptedPawn aPawn)
{
    return (
        aPawn != None &&
        !aPawn.IsA('Animal') && !aPawn.IsA('Robot') &&
        HasKnowledge(aPawn)
    );
}

function string GetPawnName(ScriptedPawn aPawn)
{
    if(aPawn.FamiliarName != "" && aPawn.FamiliarName != aPawn.UnfamiliarName)
        return aPawn.FamiliarName $ " the " $ class'Shivs'.static.StrToLower(aPawn.UnfamiliarName);
    else if(aPawn.UnfamiliarName != "")
        return "the " $ class'Shivs'.static.StrToLower(aPawn.UnfamiliarName);
    else if(aPawn.BindName != "")
        return aPawn.BindName;
    else
        return string(aPawn.Tag);
}

function AddNotes()
{
    local string Notes[100], Note;
    local name EntryIds[100];
    local ScriptedPawnKnowledge aKnowledge;
    local int NumNotes, i;

    if(aTargetPawn == None)
        return;
    
    Note = "Knowledge from " $ GetPawnName(aTargetPawn) $ ":|n";

    foreach AllActors(class'ScriptedPawnKnowledge', aKnowledge)
    {
        NumNotes = aKnowledge.GetNotesFromPawn(aTargetPawn, Notes, EntryIds);

        for(i = 0; i < NumNotes; i++)
        {
            Player.FlagBase.SetBool(Player.RootWindow.StringToName("Player_Knows_About_" $ EntryIds[i]), True);

            Note = Note $ "|n- " $ Notes[i];
        }
    }
    
    Player.AddNote(Note, False, True);
}

state Active
{

BeginTarget:
    AddNotes();
    
    Deactivate();
}

simulated function float GetEnergyRate()
{
    return EnergyRate * LevelValues[CurrentLevel];
}

defaultproperties
{
    EnergyRate=300.000000
    Icon=Texture'MachineGod.UI.AugIconTelepathy'
    smallIcon=Texture'MachineGod.UI.AugIconTelepathy_Small'
    AugmentationName="Telepathy"
    Description="Aiming at a human target while activated, neuroelectrical signals are transferred wirelessly and decoded, allowing an agent to read various thoughts of the target.|n|nTECH ONE: Simple thoughts can be read, but with significant noise.|n|nTECH TWO: Noise is reduced.|n|nTECH THREE: More detailed thoughts can be decoded.|n|nTECH FOUR: An agent is a flawless mind reader."
    MPInfo=""
    LevelValues(0)=1.000000
    LevelValues(1)=1.000000
    LevelValues(2)=1.000000
    LevelValues(3)=1.000000
    AugmentationLocation=LOC_Cranial
    MPConflictSlot=6
}

// END UE1
