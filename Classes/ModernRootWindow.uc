// ==========================================
// ModernRootWindow
//
// Changes:
// - Fade in/out
// - New HUD
// - Generalised shadows
// - Mission start display moved from HUD
// ==========================================
class ModernRootWindow extends DeusExRootWindow;

var float Brightness;
var float FadeFrom, FadeTo, FadeToQueued, FadeTimer, FadeTime;
var ModernHUDMissionStartTextDisplay StartDisplay;

// BEGIN UE1

// ---------------------
// Initialise subwindows
// ---------------------
event InitWindow()
{
    local Console TheConsole;
    
    Brightness = 0;
    FadeFrom = 0;
    FadeTo = 1;
    FadeToQueued = -1;
    FadeTime = 0.5;

    SetFont(Font'TechMedium');
    SetDefaultCursor(Texture'DefaultCursor');
    SetDefaultEditCursor(Texture'DefaultTextCursor');
    SetDefaultMovementCursors(Texture'DefaultMoveCursor', Texture'DefaultHMoveCursor', Texture'DefaultVMoveCursor', Texture'DefaultTLMoveCursor', Texture'DefaultTRMoveCursor');

    winCount = 0;

    actorDisplay = ActorDisplayWindow(NewChild(Class'ActorDisplayWindow'));
    actorDisplay.SetWindowAlignments(HALIGN_Full, VALIGN_Full);

    HUD = ModernHUD(NewChild(Class'ModernHUD'));
    HUD.UpdateSettings(DeusExPlayer(parentPawn));
    HUD.SetWindowAlignments(HALIGN_Full, VALIGN_Full, 0, 0);

    scopeView = DeusExScopeView(NewChild(Class'DeusExScopeView', False));
    scopeView.SetWindowAlignments(HALIGN_Full, VALIGN_Full, 0, 0);
    scopeView.Lower();

    StartDisplay = ModernHUDMissionStartTextDisplay(NewChild(Class'ModernHUDMissionStartTextDisplay', False));

    SetDefaultCursor(Texture'DeusExCursor1', Texture'DeusExCursor1_Shadow');

    ConditionalBindMultiplayerKeys();
}

// -------------------
// Hijack menu screens
// -------------------
function DeusExBaseWindow InvokeMenuScreen(class<DeusExBaseWindow> NewScreenClass, optional bool bNoPause)
{
    switch(NewScreenClass)
    {
        case class'MenuScreenNewGame':
            NewScreenClass = class'ModernMenuScreenNewGame';
            break;
    }

    return Super.InvokeMenuScreen(NewScreenClass, bNoPause);
}

// ----------------------------
// Draw the fade in/out texture
// ----------------------------
event DrawWindow(GC gc)
{
    Super.DrawWindow(gc);
  
    if(Brightness <= 0.0)
    {
        gc.SetTileColorRGB(0, 0, 0);
        gc.SetStyle(DSTY_Normal);
        gc.DrawPattern(0, 0, Width, Height, 0, 0, Texture'Solid');
    }
    else if(Brightness < 1.0)
    {
        gc.SetTileColorRGB(0, 0, 0);
        gc.SetStyle(DSTY_Modulated);
        gc.DrawStretchedTexture(0, 0, Width, Height, 0, 32 + Lerp(1.0 - Brightness, 0, 96), 1, 1, Texture'MenuBrightnessGradient');
    }
}

// ------------------------------------------
// Process the fade in/out internal variables
// ------------------------------------------
event Tick(float DeltaTime)
{
    local float FadeDelta;

    if(DeltaTime > 1)
        return;

    if(FadeTimer < FadeTime)
    {
        FadeTimer += DeltaTime;
        FadeDelta = FadeTimer / FadeTime;
        
        Brightness = Lerp(FadeDelta, FadeFrom, FadeTo);
    }
    else if(FadeToQueued != -1)
    {
        FadeTo = FadeToQueued;
        FadeTimer = 0;
        FadeToQueued = -1;
    }
}

// -------------------
// Sets the brightness
// -------------------
function SetBrightness(float To)
{
    Brightness = To;
    FadeTo = To;
    FadeFrom = To;
    FadeTime = 0;
    FadeTimer = 0;
}

// ----------------------------------
// Starts an asynchronous fade in/out
// ----------------------------------
function Fade(float To, float Time)
{
    FadeFrom = Min(1.0, Max(0.0, Brightness));
    FadeTo = To;
    FadeTime = Time;
    FadeTimer = 0;
}

// ---------------------------------------
// Fades between three brightness settings
// ---------------------------------------
function Blink(float A, float B, float C, float Time)
{
    FadeFrom = Min(1.0, Max(0.0, A));
    FadeTo = B;
    FadeToQueued = C;
    FadeTime = Time / 2;
    FadeTimer = 0;
}

// END UE1

defaultproperties
{
    bTickEnabled=True
}
