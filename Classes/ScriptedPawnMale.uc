class ScriptedPawnMale extends ScriptedPawnToon;

defaultproperties
{
     BindName="Man"
     FamiliarName="Man"
     UnfamiliarName="Man"
     WalkingSpeed=0.213333
     BaseAssHeight=-23.000000
     WalkAnimMult=1.050000
     GroundSpeed=180.000000
     Mesh=LodMesh'DeusExCharacters.GM_DressShirt'
     CollisionRadius=20.000000
     CollisionHeight=47.500000
     BaseEyeHeight=40.000000
     Mass=150.000000
     WalkAnimMult=0.750000
     
     Die=Sound'DeusExSounds.Player.MaleDeath'
     HitSound1=Sound'DeusExSounds.Player.MalePainSmall'
     HitSound2=Sound'DeusExSounds.Player.MalePainMedium'
}
