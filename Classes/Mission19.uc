//=============================================================================
// Mission18 - Illuminati, Tijuana, Mexico
//=============================================================================
class Mission19 expands MissionScriptExtended;

#exec OBJ LOAD FILE=MoverSFX

var VTOL aVTOL;
var int CapsuleDelay;
var float TweenDelta;

// BEGIN UE1
function StartMission()
{
    GetPlayer().DeleteAllGoals();
}

// ---------------------------------
// Executed every time the map loads
// ---------------------------------
function InitStateMachine()
{
    local ZoneInfo aZoneInfo;
    local ScriptedPawnKnowledge aKnowledge;

    Super.InitStateMachine();

    if(LocalURL == "19_TIJUANA_ILLUMINATI")
    {
        // Clear the white fog
        foreach AllActors(class'ZoneInfo', aZoneInfo, 'StartZone')
        {
            aZoneInfo.ViewFog = Vect(0, 0, 0);
        }
            
        // Set Tiffany's outfit
        if(!Flags.GetBool('Player_Chose_Outfit'))
            TiffanySavagePlayer(Player).ChangeOutfit("Vat");

        // Cache the VTOL
        aVTOL = VTOL(GetActor('VTOL'));
    }

    // Knowledge
    foreach AllActors(class'ScriptedPawnKnowledge', aKnowledge)
    {
        aKnowledge.AddObject('AlexLabComputer', "on Alex Jacobson's desk");
        aKnowledge.AddSubject('AlexLabComputer', 'AlexJacobson');
        
        aKnowledge.AddObject('LabAugCanister', "in the laboratory safe");
        aKnowledge.AddSubject('LabAugCanister', 'AlexJacobson');
        aKnowledge.AddSubject('LabAugCanister', 'ScienceMale');
        aKnowledge.AddSubject('LabAugCanister', 'ScienceFemale');

        aKnowledge.AddObject('AlexApartmentKeypad', "for Alex Jacobson's apartment door");
        aKnowledge.AddSubject('AlexApartmentKeypad', 'AlexJacobson');
    
        aKnowledge.AddObject('MorgansHiddenDoorKeypad', "for Morgan Everett's hidden door");
        aKnowledge.AddSubject('MorgansHiddenDoorKeypad', 'MorganEverett');
        aKnowledge.AddSubject('MorgansHiddenDoorKeypad', 'JuliaMontes');
    }
}

// -------------------------
// Executed on player travel
// -------------------------
function FirstFrame()
{
    local Mover aMover;

    Super.FirstFrame();
   
    if(LocalURL == "19_TIJUANA_ILLUMINATI")
    {
        // Open the vat
        foreach AllActors(class'Mover', aMover, 'TiffanyVatRing')
        {
            if(aMover.KeyNum > 0)
                continue;

            aMover.DoOpen();
        }
    }
}

// ---------------
// Closes a portal
// ---------------
function ClosePortal(name Portal, name Direction)
{
    local WarpZoneInfo aFrom, aTo;
    
    // Get the "from" ZoneInfo actor
    aFrom = WarpZoneInfo(GetActor(StringToName(Portal $ "Portal" $ Direction)));

    if(aFrom == None)
        return;

    // Get the "to" ZoneInfo actor
    if(aFrom.OtherSideURL != "")
        aTo = WarpZoneInfo(GetActor(StringToName(aFrom.OtherSideURL)));

    aFrom.OtherSideURL = "";
    aFrom.ForceGenerate();

    if(aTo == None)
        return;

    aTo.OtherSideURL = "";
    aTo.ForceGenerate();
}

// -----------------------------------------
// Connects a specific portal to another one
// -----------------------------------------
function ConnectPortal(name From, name FromDirection, name To)
{
    local WarpZoneInfo aFrom, aTo;
    local name ToDirection;

    switch(FromDirection)
    {
        case 'North':
            ToDirection = 'South';
            break;
        
        case 'South':
            ToDirection = 'North';
            break;
        
        case 'East':
            ToDirection = 'West';
            break;
        
        case 'West':
            ToDirection = 'East';
            break;
    }

    // Get the "from" ZoneInfo actor
    aFrom = WarpZoneInfo(GetActor(StringToName(From $ "Portal" $ FromDirection)));
    
    if(aFrom == None)
        Player.ClientMessage("ERROR: " $ From $ " " $ FromDirection $ " zone not found");
    
    // Get the "to" ZoneInfo actor
    aTo = WarpZoneInfo(GetActor(StringToName(To $ "Portal" $ ToDirection)));
    
    if(aTo == None)
        Player.ClientMessage("ERROR: " $ To $ " " $ ToDirection $ " zone not found");
  
    // Connect the two
    if(aFrom != None && aTo != None)
    {
        aFrom.OtherSideURL = To $ "Portal" $ ToDirection;
        aTo.OtherSideURL = From $ "Portal" $ FromDirection;
        
        aFrom.ForceGenerate();
        aTo.ForceGenerate();
    }
}

// -----------------------------------------------
// Connects all portals in a portal hub to another
// -----------------------------------------------
function ConnectPortalHub(name From, name To)
{
    ConnectPortal(From, 'North', To); 
    ConnectPortal(From, 'South', To); 
    ConnectPortal(From, 'East', To); 
    ConnectPortal(From, 'West', To); 
}

// ---------------------
// Executes every second
// ---------------------
function Timer()
{
    local Augmentation AugPossession, AugTelepathy;
    local Button1 aButton;

    Super.Timer();

    if(LocalURL == "19_TIJUANA_CAPSULE")
    {
        if(Flags.GetName('NextMap') != '')
        {
            if(CapsuleDelay >= Flags.GetInt('NumReboots'))
            {
                Flags.SetBool('TransitionIn', True,, 0);
                Level.Game.SendPlayer(Player, string(Flags.GetName('NextMap')));
            }

            CapsuleDelay++;
        }
    }
    else if(LocalURL == "19_TIJUANA_ILLUMINATI")
    {
        AugPossession = Player.AugmentationSystem.FindAugmentation(class'AugPossession');
        AugTelepathy = Player.AugmentationSystem.FindAugmentation(class'AugTelepathy');

        if(AugPossession != None && AugPossession.bHasIt)
        {
            Flags.SetBool('Player_Picked_Aug', True,, 20);
            Flags.SetBool('Player_Picked_AugPossession', True,, -1);
        }
        else if(AugTelepathy != None && AugTelepathy.bHasIt)
        {
            Flags.SetBool('Player_Picked_Aug', True,, 20);
            Flags.SetBool('Player_Picked_AugTelepathy', True,, -1);
        }

        foreach AllActors(class'Button1', aButton, 'PortalButton')
        {
            if(Flags.GetBool('Player_Has_Portal_Access'))
                aButton.bHidden = False;
            else
                aButton.bHidden = True;
        }

        if((Flags.GetBool('OverhearJuliaAlex_Played') || Flags.GetBool('MeetAlexJacbson_Played')) && !Flags.GetBool('Julia_Went_To_Aug_Container'))
        {
            Flags.setBool('Julia_Went_To_Aug_Container', True);
            OrderPawn('JuliaMontes', 'GoingTo', 'JuliaLabContainerPoint');
        }
    }
}

// ------------------------
// Changes Tiffany's outfit
// ------------------------
function ChangeOutfit(name OutfitName)
{
    local Actor aActor;

    // Set up flags
    Flags.SetBool('Player_Chose_Outfit', True,, 20);
    Flags.DeleteFlag('Player_Chose_Outfit1', FLAG_Bool);  
    Flags.DeleteFlag('Player_Chose_Outfit2', FLAG_Bool);  
    Flags.DeleteFlag('Player_Chose_Outfit3', FLAG_Bool);  
    Flags.DeleteFlag('Player_Chose_Outfit4', FLAG_Bool);  
    Flags.DeleteFlag('Player_Chose_Outfit5', FLAG_Bool);  
    Flags.DeleteFlag('Player_Chose_Outfit6', FLAG_Bool);  
    Flags.SetBool(StringToName("Player_Chose_" $ OutfitName), True,, -1);
    
    // Change the appearance and play a sound
    TiffanySavagePlayer(Player).ChangeOutfit(string(OutfitName));
    TiffanySavagePlayer(Player).PlaySound(Sound'DeusExSounds.Weapons.CrowbarSelect', SLOT_Pain);
    
    // Hide the chosen outfit decoration and button
    foreach AllActors(class'Actor', aActor)
    {
        if(aActor.Tag == 'OutfitDeco' || aActor.Event == 'TriggerChangeOutfit')
            aActor.bHidden = aActor.Event == OutfitName || aActor.Tag == OutfitName;
    }
}

// ----------------------------
// Shows the demo splash screen
// ----------------------------
function ShowDemoSplash()
{
    local ModernDemoSplashWindow SplashWindow;
    
    SplashWindow = ModernDemoSplashWindow(DeusExRootWindow(Player.RootWindow).NewChild(class'ModernDemoSplashWindow'));
    SplashWindow.SetImage(Texture'MachineGod.UI.Title');
    SplashWindow.SetMessage("Thank you very very much for playing our demo!|n|nCheck out machine-god.dxgalaxy.org for more info");
    SplashWindow.Show();
}

auto state EventManager
{
JuliaRunningToCell:
    OrderPawn('JuliaMontes', 'RunningTo', 'JuliaCellPoint');

    EndEvent();

JuliaArrivedAtCell:
    OpenDoor('CellDoor');
    
    EndEvent();

JuliaRunningToPrisonPortal:
    OrderPawn('JuliaMontes', 'RunningTo', 'JuliaPrisonPortalPoint');

    EndEvent();

ArriveAtApartments:
    ClosePortal('Apartments', 'North');
    MoveActor('JuliaMontes', 'ApartmentsPortalNorth');
    OrderPawn('JuliaMontes', 'GoingTo', 'JuliasRoomJuliaPoint'); 

    EndEvent();

ChangeOutfit:
    ChangeOutfit(EventQueue[0].aEventActor.Tag);

    EndEvent();

JuliaMontesPostOutfitConvoDone:
    OrderPawn('JuliaMontes', 'RunningTo', 'MorgansRoomJuliaPoint');
    CloseDoor('JuliaApartmentDoor');
    
    EndEvent();

MeetMorganEverettConvoDone:
    OrderPawn('JuliaMontes', 'RunningTo', 'JuliaApartmentsPortalPoint');
    ConnectPortal('Apartments', 'East', 'Lab');
    DestroyActor('MikeInVat');

    EndEvent();

ArriveAtLab:
    ClosePortal('Lab', 'West');
    MoveActor('JuliaMontes', 'LabPortalWest');
    OrderPawn('JuliaMontes', 'GoingTo', 'JuliaLabAlexPoint');
    CloseDoor('MorganApartmentDoor');
    MoveActor('MorganEverett', 'MorganOfficePoint');
    TriggerActor('TurnOffMorgansRoomHologram');

    EndEvent();

ScientistFemale1Sitting:
    OrderPawn('ScientistFemale1', 'Sitting');

    EndEvent();

JuliaMontesPostPickedAugConvoEnded:
    OrderPawn('JuliaMontes', 'Sitting');
    Flags.SetBool('Player_Has_Portal_Access', True);

    EndEvent();

ElevatorGoToHelipad:
    CloseDoor('MorgansElevatorDoor');
    Sleep(1.0);
    MoveActorSeamlessly('Player', 'MorgansElevator', 'HelipadElevator');
    Player.PlaySound(Sound'MoverSFX.Elevator.Elevator2Start', SLOT_Misc);
    Sleep(1.0);
    Player.AmbientSound = Sound'MoverSFX.Elevator.Elevator2Move';
    Sleep(4.0);
    Player.AmbientSound = None;
    Player.PlaySound(Sound'MoverSFX.Elevator.Elevator2Stop', SLOT_Misc);
    Sleep(1.0);
    OpenDoor('HelipadElevatorDoor');
    SetActorEvent('HelipadElevatorDoorButton', 'HelipadElevatorDoor');
    
    EndEvent();

MikeGoingToRide:
    OrderPawn('Mike', 'GoingTo', 'MikeRidePoint');
    SetActorHidden('StartRideButton', False);
    
    EndEvent();

StartRide:
    Sleep(0.5); 
    DestroyActor('Mike');
   
    aVTOL.GoToState('Liftoff');

    Sleep(24);

    LoadMap("20_HongKong_NorthPoint"); 
    
    EndEvent();

}

function DoTest(name TestName)
{
    switch(TestName)
    {
        case 'PostOutfit':
            Flags.SetBool('MeetJuliaMontes_Played',  True);
            Flags.SetBool('JuliaMontesPortalBark_Played', True);
            Flags.SetBool('JuliaMontesPreOutfitBark_Played', True);
            Flags.SetBool('Player_Chose_Outfit', True);
            Flags.SetBool('Player_Chose_Outfit1', True);
            GetPlayer().ChangeOutfit("Outfit1");
            MoveActor('JuliaMontes', 'JuliasRoomJuliaPoint');
            MoveActor('Player', 'JuliasRoomPlayerPoint');
            SetActorCollision('ArriveAtApartments', False);
            OpenDoor('JuliasRoomDoor');
            break;

        case 'MeetMorgan':
            DoTest('PostOutfit');
            OpenDoor('MorgansRoomDoor');
            Flags.SetBool('JuliaMontesPostOutfit_Played', True);
            MoveActor('Player', 'MorgansRoomPlayerPoint', 128);
            MoveActor('JuliaMontes', 'MorgansRoomJuliaPoint', 128);
            break;

        case 'Lab':
            DoTest('MeetMorgan');
            Flags.SetBool('MeetMorganEverett_Played', True);
            MoveActor('Player', 'Lab');
            DoEvent('ArriveAtLab');
            break;
    
        case 'Office':
            DoTest('Lab');
            Flags.SetBool('Player_Has_Portal_Access', True);
            MoveActor('MorganEverett', 'MorganOfficePoint');
            MoveActor('JuliaMontes', 'MorgansRoomJuliaPoint', 128);
            Flags.SetBool('Player_Picked_Aug', True);
            Flags.SetBool('Player_Picked_AugPossession', True);
            Flags.SetBool('JuliaMontesPostPickedAug_Played', True);
            MoveActor('Player', 'MorgansOffice');
            break;

        case 'Exit':
            DoTest('Office');
            MoveActor('Player', 'Helipad');
            break;
    }
}

// END UE1
