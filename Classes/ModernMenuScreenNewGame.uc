class ModernMenuScreenNewGame extends MenuScreenNewGame;

event InitWindow()
{
    local HumanExtended aPlayer;
    local int i;

    Super.InitWindow();

    aPlayer = HumanExtended(ModernRootWindow(Root).ParentPawn);

    if(aPlayer == None)
        return;

    for(i = 0; i < ArrayCount(aPlayer.Portraits); i++)
    {
        TexPortraits[i] = aPlayer.Portraits[i];
    }
    
    BtnPortrait.SetBackground(TexPortraits[0]);
}
