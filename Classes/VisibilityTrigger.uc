//=============================================================================
// VisibilityTrigger - Toggles an actor's visibility on/off
//=============================================================================
class VisibilityTrigger extends Trigger;

var() bool bTransition;
var() bool bVisible;
var() bool bInitiallyHidden;
var() float TransitionDuration;
var(Sound) Sound TransientSound;

var float TransitionTimer, TransitionToggleTimer;
var bool bActorsVisible;
var Actor aActors[100];
var float ActorValues[100];
var int NumActors;

// BEGIN UE1

function PostBeginPlay()
{
    local Actor aActor;
    local int i;

    if(Event != '')
    {
        foreach AllActors(class'Actor', aActor, Event)
        {
            aActors[NumActors] = aActor;
            
            if(aActor.IsA('Mover'))
                ActorValues[NumActors] = aActor.Location.Z;

            else if(aActor.IsA('Light'))
                ActorValues[NumActors] = aActor.LightBrightness;

            NumActors++;
        }
    }

    if(bInitiallyHidden)
        for(i = 0; i < NumActors; i++)
            SetActorVisible(i, False);

    Super.PostBeginPlay();
}

function Touch(Actor aOther)
{
    if(!IsRelevant(aOther) || DeusExPlayer(aOther) == None)
        return;

    Trigger(aOther, DeusExPlayer(aOther));
}

function SetActorVisible(int i, bool bNewVisible)
{
    local Vector Loc;

    if(aActors[i] == None)
        return;

    // Shove movers into the void
    if(aActors[i].IsA('Mover'))
    {
        Loc = aActors[i].Location;

        if(!bNewVisible)
            Loc.Z = 20000 + (FRand() * 10000);
        else
            Loc.Z = ActorValues[i];
        
        aActors[i].SetLocation(Loc);
    }
    // Set lights to 0 brightness
    else if(aActors[i].IsA('Light'))
    {
        if(!bNewVisible)
            aActors[i].LightBrightness = 0;
        else
            aActors[i].LightBrightness = ActorValues[i];
    }
    // Everything else uses the bHidden flag
    else
    {
        aActors[i].bHidden = !bNewVisible;
    }
}

function Trigger(Actor Other, Pawn Instigator)
{
    local int i;
    
    bActorsVisible = bInitiallyHidden;

    if(TransientSound != None)
        PlaySound(TransientSound, SLOT_Misc);

    if(!bTransition)
    {
        for(i = 0; i < NumActors; i++)
            SetActorVisible(i, bVisible);
    }
    else
    {
        for(i = 0; i < NumActors; i++)
            SetActorVisible(i, !bVisible);
        
        TransitionTimer = TransitionDuration;
    }

    Super.Trigger(Other, Instigator);
}

function Tick(float DeltaTime)
{
    local int i;

    Super.Tick(DeltaTime);

    if(TransitionTimer <= 0 || Event == '')
        return;

    TransitionTimer -= DeltaTime;
    TransitionToggleTimer -= DeltaTime;

    if(TransitionTimer <= 0)
    {
        bActorsVisible = bVisible;
    }
    else if(TransitionToggleTimer <= 0) 
    {
        bActorsVisible = !bActorsVisible;
        
        if(bVisible)
            TransitionToggleTimer = TransitionTimer * 0.1;
        else
            TransitionToggleTimer = (TransitionDuration - TransitionTimer) * 0.1;
    }
   
    for(i = 0; i < NumActors; i++)
        SetActorVisible(i, bActorsVisible);
}

// END UE1

defaultproperties
{
    TransitionDuration=1.0
    bVisible=True
    bInitiallyHidden=True
}
