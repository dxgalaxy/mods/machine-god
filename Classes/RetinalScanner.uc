class RetinalScanner extends AuthenticationDevice;

defaultproperties
{
     ItemName="Retinal Scanner"
     Mesh=LodMesh'DeusExDeco.RetinalScanner'
     CollisionRadius=10.000000
     CollisionHeight=11.430000
     Mass=30.000000
     Buoyancy=40.000000
}
