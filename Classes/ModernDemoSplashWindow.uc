class ModernDemoSplashWindow extends Window;

var DeusExRootWindow Root;

var Texture ImageTexture;
var float ImageHeight, ImageWidth;

var string MessageText;
var TextWindow MessageWindow;
var Font MessageFont;
var int MessageMaxWidth, MessageMargin;
var Color MessageColor;

event InitWindow()
{
    Super.InitWindow();

    SetWindowAlignments(HALIGN_Full, VALIGN_Full);
    
    MessageFont = Font(DynamicLoadObject("DXFonts.MainMenuTrueType", class'Font'));
    
    MessageWindow = TextWindow(NewChild(Class'TextWindow'));
    MessageWindow.SetFont(MessageFont);
    MessageWindow.SetTextAlignments(HALIGN_Center, VALIGN_Top);
    MessageWindow.Show();

    Root = DeusExRootWindow(GetRootWindow());
}

function SetImage(Texture Image)
{
    ImageTexture = Image;
}

function SetMessage(string Message, optional int MaxWidth, optional int Margin)
{
    MessageText = Message;

    if(MaxWidth > 0)
        MessageMaxWidth = MaxWidth;
    
    if(Margin > 0)
        MessageMargin = Margin;

    MessageWindow.SetTextColor(MessageColor);
    MessageWindow.SetText(MessageText);
   
    UpdateMessageWindow();

    AskParentForReconfigure();
}

function UpdateMessageWindow()
{
    local float W, H, X, Y;

    W = Min(Root.Width, MessageMaxWidth);
    X = (Root.Width / 2)  - (W / 2); 
    Y = (Root.Height / 2) + (ImageHeight / 2) + MessageMargin;
    H = Root.Height - Y;

    MessageWindow.ConfigureChild(X, Y, W, H); 
}

function ConfigurationChanged()
{
    Super.ConfigurationChanged();

    UpdateMessageWindow();
}

event ParentRequestedPreferredSize(bool bWidthSpecified, out float PrefWidth, bool bHeightSpecified, out float PrefHeight)
{
    PrefHeight = Root.Height;
    PrefWidth = Root.Width;
}

function DrawWindow(GC gc)
{
    gc.SetStyle(DSTY_Masked);

    if(ImageTexture != None)
        gc.DrawTexture((Root.Width / 2) - (ImageWidth / 2), (Root.Height / 2) - (ImageHeight / 2), ImageWidth, ImageHeight, 0, 0, ImageTexture);
}

defaultproperties
{
    ImageHeight=256
    ImageWidth=1024
    MessageMaxWidth=800
    MessageMargin=40
    MessageColor=(R=255,G=255,B=255)
}
