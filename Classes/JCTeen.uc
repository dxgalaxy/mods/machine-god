class JCTeen extends ScriptedPawnMale;

#exec OBJ LOAD FILE=Effects

// Voice: ElevenLabs Teen JC
//
// Audacity:
// - Reverb, wet only
// - Delay
//   - Delay per echo: -12
//   - Delay time: 0.3
//   - Pitch change: -1.0
//   - Number of echoes: 2
// - Limiter
//   - +4dB input gain

function SupportActor(Actor aStandingActor) {}
function TakeDamage(int Damage, Pawn aInstigatedBy, Vector HitLocation, Vector Momentum, name DamageType) {}

defaultproperties
{
    bInvincible=True
    UnfamiliarName="Tantalus"
    FamiliarName="Tantalus"
    BindName="JCTeen"
    Mesh=LodMesh'DeusExCharacters.GM_DressShirt_S'
    MultiSkins(0)=WetTexture'Effects.UserInterface.DrunkFX'
    MultiSkins(1)=Texture'PinkMaskTex'
    MultiSkins(2)=Texture'PinkMaskTex'
    MultiSkins(3)=WetTexture'Effects.UserInterface.DrunkFX'
    MultiSkins(4)=None
    MultiSkins(5)=WetTexture'Effects.UserInterface.DrunkFX'
    MultiSkins(6)=Texture'GrayMaskTex'
    MultiSkins(7)=Texture'BlackMaskTex'
    OutlineTexture=FireTexture'Effects.Electricity.wepn_nesword'
}
