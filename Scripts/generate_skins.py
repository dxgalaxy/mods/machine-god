from generate_skins_functions import *

print_hello()

# ----------------------------------------
# Head_F
# ----------------------------------------
set_svg('Head_F')

# Major characters 
export_id('CarlaBrown_Head', 2);
export_id('ErinTodd_Head', 2);
export_id('HeliosAgentFemale_Head', 2);
export_id('Iris_Head', 2);
export_id('JuliaMontes_Head', 2);
export_id('SandraRenton_Head', 2);
export_id('TiffanySavage_Head', 2);

# Generic characters
set_group('Female', 'Heads')

for s in ['1', '2', '3', '4']: 
    set_skin_tone(s)
    set_age('Young')
    export_id('Head_Female_Young1_Skin' + s, 2)
    export_id('Head_Female_Young2_Skin' + s, 2)
    set_age('Old')
    export_id('Head_Female_Old1_Skin' + s, 2)

# ----------------------------------------
# Head_M
# ----------------------------------------
set_svg('Head_M')

# Major characters
export_id('AdamJensen_Head', 2)
export_id('AlexJacobson_Head', 2)
export_id('AntonJackson_Head', 2)
export_id('DeckerParkes_Head', 2)
export_id('ElCoyote_Head', 2)
export_id('GarySavage_Head', 2)
export_id('GordonQuick_Head', 2)
export_id('HeliosAgentMale_Head', 2)
export_id('JCHelios_Head', 2)
export_id('LarryWinger_Head', 2)
export_id('Mike_Head', 2)
export_id('MorganEverett_Head', 2)
export_id('PaulDenton_Head', 2)
export_id('TimBaker_Head', 2)
export_id('TracerTong_Head', 2)
export_id('TriadBrute_Head', 2)
export_id('WayneYoung_Head', 2)

# Generic characters
set_group('Male', 'Heads')

for s in ['1', '2', '3', '4']: 
    set_skin_tone(s)
    set_age('Young')
    export_id('Head_Male_Young1_Skin' + s, 2)
    export_id('Head_Male_Young2_Skin' + s, 2)
    set_age('Old')
    export_id('Head_Male_Old1_Skin' + s, 2)

unset_group()

# ----------------------------------------
# Head_K
# ----------------------------------------
set_svg('Head_K')
set_base_objects('background', 'skin-shadow', 'skin-highlight', 'skin-blush', 'skin-texture', 'teeth', 'lines-base')
set_group('Child', 'Heads')

for s in ['1', '2', '3', '4']:
    set_skin_tone(s)

    for v in ['1']:
        # Black kids get a short afro
        if v == '1' and s == '4':
            set_objects('hair-4-base', 'hair-4-texture', 'hair-4-eyebrows')
        else:
            set_objects('hair-' + v + '-' + s, 'hair-' + v + '-base', 'hair-' + v + '-rough', 'hair-' + v + '-fine', 'hair-' + v + '-highlight', 'hair-' + v + '-eyebrows')
            
        add_objects('skin-' + s, 'mouth-' + s, 'eyes-' + s)
        export('Head_Child' + v + '_Skin' + s, 2) 

# ----------------------------------------
# Torso_F
# ----------------------------------------
set_svg('Torso_F')
set_base_objects('skin-shadow', 'skin-highlight', 'skin-texture', 'skin-lines')
set_group('Female', 'Torsos')

for s in ['1', '2', '3', '4']:
    set_skin_tone(s)

    for t, ts in {'Sleeveless': ['Normal','Bum'], 'TShirt': 'Normal', 'TankTop': ['Normal', 'Bum', 'Tech'], 'GunHolster': 'Guerilla', 'BulletProofVest': 'Police', 'CropTop': 'Party'}.items():
        set_tag_set(ts)

        set_objects('skin-' + s, t + '-' + s, t + '-lines', t + '-shading', t + '-decoration', t + '-texture')
        export(t + s + '_Female_Skin' + s)

for v in ['1', '2', '3', '4']:
    set_tags('Military', 'Guerilla')
    set_objects('tacticalshirt-lines', 'tacticalshirt-texture', 'tacticalshirt-shading', 'tacticalshirt-' + v)
    export('TacticalShirt' + v + '_Female')

unset_group()

set_objects('skin-1', 'sleeveless-sandrarenton', 'sleeveless-lines', 'sleeveless-shading', 'sleeveless-texture')
export('SandraRenton_Torso')

set_objects('skin-3', 'mecharms', 'gunholster-juliamontes', 'gunholster-lines', 'gunholster-shading', 'gunholster-texture')
export('JuliaMontes_Torso')

set_objects(
    'skin-2',
    'offshouldertop-shadow', 'offshouldertop-tiffanysavage-outfit5', 'offshouldertop-shading', 'offshouldertop-lines',
    'croptop-1', 'croptop-shading', 'croptop-lines'
)
export('TiffanySavage_Outfit5_Torso')
    
# ----------------------------------------
# Torso_M
# ----------------------------------------
set_svg('Torso_M')
set_base_objects('skin-shadow', 'skin-highlight', 'skin-texture', 'skin-lines')
    
# Nude
for s in ['1', '2', '3', '4']:
    set_objects('skin-' + s)
    export('Torso_Male_Skin' + s) 

# Skin typed clothes
for s in ['1', '2', '3', '4']:
    set_skin_tone(s)

    # Suit
    set_group('Male', 'SuitTorsos')
    set_tags('Formal')
    
    for t in ['Jacket']:
        set_objects('skin-' + s, t + '-suit-' + s, t + '-suit-lines', t + '-suit-shading', t + '-suit-texture', t + '-suit-decoration')
        set_mask_objects(t + '-suit-mask')
        export(t + s + '_MaleSuit_Skin' + s)
        
    # Jumpsuit shirts
    set_group('Male', 'JumpsuitTorsos')
    
    for t, ts in {'TankTop': ['Guerilla', 'Tech'], 'GunHolster': 'Guerilla', 'Sweater': ['Military', 'Guerilla'], 'BulletProofVest': 'Police'}.items():
        set_tag_set(ts)

        set_objects('skin-' + s, 'skin-lines', 'skin-highlight', 'skin-shadow')
        add_composed_objects(t, s, [ 'lines', 'shading', 'texture', 'decoration' ], 'jumpsuit')
        export(t + s + '_MaleJumpsuit_Skin' + s)
       
    # Fat shirts
    set_group('Male', 'FatTorsos')
    
    for t, ts in {'TankTop': ['Normal', 'Bum'], 'DressShirt': 'Formal'}.items():
        set_tag_set(ts)

        set_objects('skin-' + s, t + '-fat-' + s, t + '-fat-lines', t + '-fat-shading', t + '-fat-texture', t + '-fat-decoration')
        export(t + s + '_MaleFat_Skin' + s)
        
    # Generic shirts
    set_group('Male', 'Torsos')
    
    for t, ts in {'TankTop': ['Normal', 'Bum'], 'DressShirt': 'Normal'}.items():
        set_tag_set(ts)
        
        set_objects('skin-' + s, t + '-' + s, t + '-lines', t + '-shading', t + '-texture')
        export(t + s + '_Male_Skin' + s)

# Skin agnostic shirts
unset_skin_tone()
    
set_group('Male', 'JumpsuitTorsos')

for v in ['1', '2', '3', '4']:
    for t, ts in {'Armor+Sweater': ['Military', 'Guerilla']}.items():
        set_tag_set(ts)

        set_composed_objects(t, v, [ 'lines', 'shading', 'texture', 'decoration' ], 'jumpsuit')
        export(t + v + '_MaleJumpsuit')

unset_group()

set_objects('skin-1',
    'opendressshirt-skinny-alexjacobson', 'opendressshirt-skinny-lines', 'opendressshirt-skinny-shading', 'opendressshirt-skinny-texture',
    'tshirt-texture', 'tshirt-lines', 'tshirt-shading', 'tshirt-alexjacobson')
export('AlexJacobson_Torso')

set_objects('skin-heliosagent', 'jacket-suit-heliosagent', 'jacket-suit-lines', 'jacket-suit-shading', 'jacket-suit-texture', 'jacket-suit-decoration')
set_mask_objects('jacket-suit-mask')
export('HeliosAgentMale_Torso')

# ----------------------------------------
# Torso_K
# ----------------------------------------
set_svg('Torso_K')
set_base_objects('skin-shadow', 'skin-highlight', 'skin-texture', 'skin-lines')
set_group('Child', 'Torsos')

for s in ['1', '2', '3', '4']:
    set_skin_tone(s)

    for t, ts in {'TankTop': ['Normal','Bum']}.items():
        set_tag_set(ts)

        set_objects('skin-' + s, t + '-' + s, t + '-lines', t + '-shading', t + '-texture')
        export(t + s + '_Child_Skin' + s)
    
# ----------------------------------------
# Pants
# ----------------------------------------
set_svg('Pants')
set_base_objects('background')
set_group('Common', 'Pants')

# Skin agnostic pants
unset_skin_tone()

for t, ts in {'Jeans': ['Normal', 'Science'], 'TacticalPants': ['Guerilla', 'Military', 'Police']}.items():
    set_tag_set(ts)

    for v in ['1', '2', '3', '4']:
        set_objects(t + '-shoes-base', t + '-shoes', t + '-belt', t + '-decoration', t + '-seams', t + '-shading', t + '-texture', t + '-lines', t + '-' + v)
        export(t + v)

set_tags('Tech')
set_objects('techpants-shoes-base', 'techpants-shoes', 'techpants-decoration', 'techpants-seams', 'techpants-shading', 'techpants-texture', 'techpants-lines', 'techpants-fabric')
export('TechPants')
    
# Skin specific pants
for s in ['1', '2', '3', '4']:
    set_skin_tone(s)
    
    for t, ts in {'TornJeans': 'Bum', 'SuitPants': 'Formal'}.items():
        set_tag_set(ts)

        set_objects(t + '-shoes', t + '-belt', t + '-decoration', t + '-seams', t + '-shading', t + '-texture', t + '-lines', t + '-' + s)
        export(t + s + '_Skin' + s)

unset_group()
unset_skin_tone()

set_objects('suitpants-shoes', 'suitpants-belt', 'suitpants-seams', 'suitpants-shading', 'suitpants-texture', 'suitpants-lines', 'suitpants-heliosagent')
export('HeliosAgentMale_Legs')

set_objects('jeans-shoes', 'jeans-belt', 'jeans-decoration-mike', 'jeans-seams', 'jeans-shading', 'jeans-texture', 'jeans-lines', 'jeans-1')
export('Mike_Legs')

set_objects('fashionpants-tiffanysavage-outfit5', 'fashionpants-seams', 'fashionpants-texture', 'fashionpants-lines', 'fashionpants-shading', 'fashionpants-shoes') 

# ----------------------------------------
# Coat_M
# ----------------------------------------
set_svg('Coat_M')
set_base_objects('background')

# Normal guys
set_group('Male', 'Coats')

for t, ts in {'PlainCoat': {1:'Science',2:'Bum'}, 'LeatherJacket': 'Normal'}.items():
    for v in ['1', '2']:
        set_tag_set(ts, v)

        set_objects(t + '-lines', t + '-shading', t + '-texture', t + '-decoration', t + '-' + v)
        set_mask_objects(t + '-mask')
        export(t + v + '_Male')
    
# Fat guys
set_group('Male', 'FatCoats')

for t, ts in {'PlainCoat': {1:'Science',2:'Bum'}, 'LeatherJacket': 'Normal', 'SuitJacket': 'Formal'}.items():
    for v in ['1', '2']:
        set_tag_set(ts, v)
        
        set_objects(t + '-fat-lines', t + '-fat-shading', t + '-fat-texture', t + '-fat-decoration', t + '-fat-' + v)
        set_mask_objects(t + '-fat-mask')
        export(t + v + '_MaleFat')
  
unset_group()

set_composed_objects('suitjacket-fat', 'morganeverett', ['lines', 'texture', 'shading'])
set_mask_objects('suitjacket-fat-mask')
export('MorganEverett_Coat')

# ----------------------------------------
# Coat_F
# ----------------------------------------
set_svg('Coat_F')
set_base_objects('background')
set_group('Female', 'Coats')

set_tags('Normal')

for t, ts in {'PlainCoat': {1: 'Science', 2: 'Normal', 3: 'Normal'}, 'SegmentedCoat': {1: 'Normal', 2: 'Normal', 3: 'Party'}, 'LeatherJacket': 'Normal'}.items():
    for v in ['1', '2', '3']:
        set_tag_set(ts, v)

        set_objects(t + '-texture', t + '-lines', t + '-shading', t + '-decoration', t + '-' + v)
        export(t + v + '_Female')

unset_group()

set_objects('plaincoat-texture', 'plaincoat-lines', 'plaincoat-shading', 'plaincoat-decoration', 'plaincoat-tiffanysavage-outfit3')
export('TiffanySavage_Outfit3_Coat')

set_objects('plaincoat-texture', 'plaincoat-lines', 'plaincoat-shading', 'plaincoat-decoration', 'plaincoat-heliosagent')
set_mask_objects('mask-nocollar', 'mask-shorttail')
export('HeliosAgentFemale_Coat')

# ----------------------------------------
# Dress_F
# ----------------------------------------
set_svg('Dress_F')
set_base_objects('background', 'skin-highlight', 'skin-shadow', 'skin-lines', 'skin-texture')
set_group('Female', 'Dresses')

for s in ['1', '2', '3', '4']:
    set_skin_tone(s)

    for t, ts in {'OfficeDress': ['Formal', 'Normal'] }.items():
        set_tag_set(ts)

        set_objects('skin-' + s, t + '-' + s, t + '-lines', t + '-texture', t + '-shading')
        export(t + s + '_Female_Skin' + s)

unset_group()
unset_skin_tone()

# TODO: Outfit4
#set_objects()

# ----------------------------------------
# Legs_M
# ----------------------------------------
set_svg('Legs_M')
set_base_objects('background')

for s in ['1', '2', '3', '4']:
    set_skin_tone(s)

    set_objects('legs-decoration', 'legs-texture', 'legs-highlight', 'legs-shadow', 'legs-lines', 'legs-' + s)
    export('Legs_Male_Skin' + s) 
    
# ----------------------------------------
# Legs_F
# ----------------------------------------
set_svg('Legs_F')
set_base_objects('background')

# Skin specific
for s in ['1', '2', '3', '4']:
    set_skin_tone(s)

    # Legs
    set_group('Female', 'BareLegs')
    set_tags('Normal', 'Formal')
    set_objects('legs-decoration', 'legs-texture', 'legs-shoes', 'legs-highlight', 'legs-shadow', 'legs-lines', 'legs-' + s)
    export('Legs_Female_Skin' + s) 

    # Clothes
    set_group('Female', 'ClothedLegs')
    
    for t, ts in {'Hotpants': 'Party'}.items():
        set_tag_set(ts)
        
        set_objects('legs-decoration', 'legs-texture', 'legs-shoes', 'legs-highlight', 'legs-shadow', 'legs-lines', 'legs-' + s)
        add_objects(t + '-' + s, t + '-lines', t + '-texture', t + '-seams', t + '-shading', t + '-seams', t + '-shoes', t + '-belt')
        export(t + s + '_Female_Skin' + s)

# Skin agnostic
unset_skin_tone()
set_group('Female', 'ClothedLegs')

for v in ['1', '2', '3', '4']:
    for t, ts in {'SkinnyJeans': ['Normal', 'Science']}.items():
        set_tag_set(ts)
        
        set_objects(t + '-' + v, t + '-lines', t + '-texture', t + '-seams', t + '-shading', t + '-seams', t + '-shoes', t + '-belt')
        export(t + v + '_Female')

unset_group()
set_objects('suitpants-belt', 'suitpants-shoes', 'suitpants-seams', 'suitpants-texture', 'suitpants-shading', 'suitpants-lines', 'suitpants-heliosagent')
export('HeliosAgentFemale_Legs')

# ----------------------------------------
# ShirtFront_F
# ----------------------------------------
set_svg('ShirtFront_F')
set_group('Female', 'ShirtFronts')
set_base_objects('skin-lines', 'skin-shading', 'skin-texture')

for t, ts in {'TShirt': ['Normal', 'Science']}.items():
    set_tag_set(ts)

    for s in ['1', '2', '3', '4']:
        set_skin_tone(s)
        set_objects('skin-' + s)
        add_composed_objects(t, s, ['base', 'lines', 'texture', 'shading'])
        export(t + 'Front' + s + '_Female_Skin' + s)

unset_group()

set_objects('bulletproofvest-base', 'bulletproofvest-lines', 'bulletproofvest-shading', 'bulletproofvest-base', 'bulletproofvest-texture', 'bulletproofvest-tiffanysavage-outfit3')
export('TiffanySavage_Outfit3_ShirtFront')

set_objects('skin-2', 'tshirt-texture', 'tshirt-shading', 'texture-lines', 'tshirt-tiffanysavage-outfit6')
export('TiffanySavage_Outfit6_ShirtFront')

set_objects()

# ----------------------------------------
# Skirt_F
# ----------------------------------------
set_svg('Skirt_F')
set_group('Female', 'Skirts')
set_base_objects('shading', 'texture', 'lines')

for t, ts in {'Skirt': 'Normal'}.items():
    set_tag_set(ts)

    for v in ['1', '2', '3', '4']:
        set_objects(t + '-shading', t + '-base', t + '-lines', t + '-' + v)
        export(t + v + '_Female')

# ----------------------------------------
# ShirtFront_M
# ----------------------------------------
set_svg('ShirtFront_M')
set_group('Male', 'ShirtFronts')

# Skin specific
for t, ts in {'TShirt': 'Normal', 'TornTShirt': 'Bum'}.items():
    set_tag_set(ts)

    for s in ['1', '2', '3', '4']:
        set_skin_tone(s)
        set_objects('skin-shading', 'skin-' + s)
        add_composed_objects(t, s, ['lines', 'texture', 'shading'])
        export(t + 'Front' + s + '_Male_Skin' + s)
    
# Skin agnostic
unset_skin_tone()

for t, ts in {'DressShirt': 'Normal', 'DressShirt+Tie': 'Science' }.items():
    set_tag_set(ts)
    
    for v in ['1', '2', '3', '4']:
        set_composed_objects(t, v, ['lines', 'texture', 'shading'])
        export(t + 'Front' + v + '_Male')

unset_group()

set_composed_objects('dressshirt+tie', 'garysavage', ['lines', 'texture', 'shading'])
export('GarySavage_ShirtFront')

set_composed_objects('dressshirt+vest+tie', 'morganeverett', ['lines', 'texture', 'shading'])
export('MorganEverett_ShirtFront')

set_composed_objects('tshirt', 'mike', ['lines', 'texture', 'shading'])
add_objects('skin-shading', 'skin-3')
export('Mike_ShirtFront')

set_objects('skin-helios', 'skin-lines', 'skin-shading', 'jchelios')
export('JCHelios_ShirtFront')

# ---------------------------------------- 
# Glasses
# ---------------------------------------- 
# Common lenses
set_svg('Lenses')
set_group('Common', 'Lenses')
set_base_objects('mask')

for v in ['1', '2']:
    set_objects('lenses-' + v)
    export('Lenses' + v)

# Bespoke lenses
unset_group()

for v in ['4']:
    set_objects('lenses-' + v)
    export('Lenses' + v)

# Common frames
set_svg('Frames')
set_group('Common', 'Frames')
set_base_objects('background')

for v in ['1', '5']:
    set_objects('frames-' + v)
    export('Frames' + v)

# Bespoke frames
unset_group()

for v in ['2', '3', '4']:
    set_objects('frames-' + v)
    export('Frames' + v)

# ---------------------------------------- 
# Helmet
# ---------------------------------------- 
set_svg('Helmet')
set_group('Male', 'Helmets')
set_base_objects('background')

# Goggles
for t in ['Goggles']:
    set_objects('goggles-base')
    set_mask_objects('goggles-mask')
    export('Goggles');

    for v in ['1', '2']:
        set_objects('goggles-' + v)
        export('Goggles_' + v)

# ---------------------------------------- 
# Library
# ---------------------------------------- 
build_library()
