//=============================================================================
// Mission17 - Slums, Tijuana, Mexico
//=============================================================================
class Mission17 extends MissionScriptExtended;

// BEGIN UE1

function Timer()
{
    Super.Timer();

    CheckOfficerLuis();
    CheckPoliceHarassingJuliaMontes();
}

// -----------------------------------------------------
// Check on the officer Luis quest
// -----------------------------------------------------
function CheckOfficerLuis()
{
    local ComputerPersonal aComputer;
    local ScriptedPawn aPawn;

    // Add sending evidence option to computers if the player found the image
    if(Flags.GetBool('Player_Picked_Up_Evidence'))
    {
        foreach AllActors(class'ComputerPersonal', aComputer, 'BorderComputers')
        {
            aComputer.SpecialOptions[0].bTriggerOnceOnly = True;
            aComputer.SpecialOptions[0].Text = "Send evidence to sergeant";
            aComputer.SpecialOptions[0].TriggerEvent = 'UploadLuisEvidence';
        }
    }
    
    // Set an extra convenience flag and change Luis' alliance
    if(
        (Flags.GetBool('Player_Sent_Evidence') || Flags.GetBool('Luis_Dead')) &&
        !Flags.GetBool('Job_Resolved')
    )
    {
        Flags.SetBool('Job_Resolved', True,, 18);
        
        foreach AllActors(class'ScriptedPawn', aPawn, 'Luis')
        {
            aPawn.ChangeAlly('Player', 1, True);
            aPawn.SetOrders('Standing', '', True);
        }
    }
    
    // Set a permanent flag
    if(Flags.GetBool('Luis_Dead'))
    {
        Flags.SetBool('Player_Killed_Luis', True,, 0);
    }
}

// ------------------------------------------------------
// Determine what to make the police do with Julia Montes
// ------------------------------------------------------
function CheckPoliceHarassingJuliaMontes()
{
    local Trigger T;
    local DeusExCarcass C;
    local ScriptedPawn P;
    local int DeadPoliceCount;
        
    // Check if the police are dead
    DeadPoliceCount = 0;

    foreach AllActors(class'DeusExCarcass', C, 'PoliceHarassingJuliaMontes')
    {
        DeadPoliceCount++;

        // Did the player kill the police?
        if(C.KillerBindName == "JCDenton")
        {
            Flags.SetBool('Player_Killed_PoliceHarassingJuliaMontes', True,, 0); 
        }
    }

    if(DeadPoliceCount > 1)
    {
        Flags.SetBool('PoliceHarassingJuliaMontes_Resolved', True,, 18);
        return;
    }
        
    // If it's already been determined, cancel the check
    if(Flags.GetBool('PoliceHarassingJuliaMontes_Resolved'))
    {
        return;
    }
   
    // The conversation has not yet played, and nobody died, cancel
    if(!Flags.GetBool('OverhearJuliaPolice_Played') && DeadPoliceCount == 0)
    {
        return;
    }
    
    // Police leave JuliaMontes alone
    if(Flags.GetBool('Player_Convinced_PoliceHarassingJuliaMontes'))
    {
        foreach AllActors(class'Trigger', T, 'PoliceLeaveJuliaMontes')
        {
            T.Trigger(Self, Player);
        }

        Flags.SetBool('Police_Left_JuliaMontes', True,, 18);
        Flags.SetBool('PoliceHarassingJuliaMontes_Resolved', True,, 18);
        return;
    }
    
    // The player is busy, cancel
    if(Player.InConversation())
    {
        return;
    }
    
    // Police attack JuliaMontes 
    foreach AllActors(class'Trigger', T, 'PoliceAttackJuliaMontes')
    {
        T.Trigger(Self, Player);
    }

    Flags.SetBool('Police_Attacked_JuliaMontes', True,, 18);
}

auto state EventManager
{

LuisGoToCliff:
    Sleep(1.0);
    
    MakePawnDropWeapon('Luis');

    Sleep(1.0);

    OrderPawn('Luis', 'GoingTo', 'LuisCliffPoint');

    EndEvent();

MapExit:
    ModernRootWindow(GetPlayer().RootWindow).Fade(0, 3.0);

    Sleep(3.0);

    LoadMap("18_Tijuana_Office"); 

    EndEvent();

}

// END UE1
