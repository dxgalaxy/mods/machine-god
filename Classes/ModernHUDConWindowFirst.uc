// ========================================
// ModernHUDConWindowFirst
//
// Changes:
// - Removed name
// - Same colour text for players and NPCs
// - Black translucent background
// - Removed border
// - Larger font
// - Direct drawing of text instead of TextWindow
// ========================================
class ModernHUDConWindowFirst extends HUDConWindowFirst;

var ModernRootWindow Root;
var ModernHUD HUD;
var bool bStackable;
var float VPadding, HPadding;
var string CurrentText;

// BEGIN UE1

function CreateNameWindow()
{
    // Create the name window, but hide it, since it's being referenced externally elsewhere
    nameWindow = TextWindow(LowerConWindow.NewChild(Class'TextWindow'));
    nameWindow.Hide();
}

function DisplayText(string Text, Actor SpeakingActor)
{
    CurrentText = Text;

    AskParentForReconfigure();
}

event ParentRequestedPreferredSize(bool bWidthSpecified, out float PreferredWidth, bool bHeightSpecified, out float PreferredHeight)
{
    local GC gc;

    gc = GetGC();

    gc.SetAlignments(HALIGN_Center, VALIGN_Center);
    gc.SetFont(Font'FontLocation');
    gc.GetTextExtent(700, PreferredWidth, PreferredHeight, CurrentText);
    
    PreferredHeight += VPadding * 2; 
    PreferredWidth += HPadding * 2; 
    
    ReleaseGC(gc);
}

event DrawWindow(GC gc)
{
    if(Root == None)
        Root = ModernRootWindow(GetRootWindow());
    
    if(HUD == None)
        HUD = ModernHUD(Root.HUD);

    HUD.DrawLightDialog(gc, 0, 0, Width, Height);
       
    gc.SetTextColor(HUD.ColText);
    gc.SetAlignments(HALIGN_Center, VALIGN_Center);
    gc.SetFont(Font'FontLocation');
    gc.DrawText(HPadding, VPadding, Width - HPadding * 2, Height - VPadding * 2, CurrentText);
}

// END UE1

defaultproperties
{
    bStackable=True
    VPadding=8
    HPadding=12
}

