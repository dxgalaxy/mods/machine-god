### Phase 1

Vandenberg and Tijuana, iteration 1


#### Vandenberg

- [x] Create barracks for staff
- [x] Optional convos with scientists
- [x] Agents to bypass, violently or sneakily
- [x] Convo with Gary and JC, introduce the premise of the story
- [x] Introduce Miguel


#### Tijuana: Slums

- [x] Create hilly environment, dodgy houses made from sheet metal
- [x] Meet Miguel's mother through a confrontation with the police
- [x] Solicit the help of local gangs to enter the city
- [x] Some NPCs for environmental storytelling
- [x] Create transition scene to sim


#### Tijuana: Office

- [x] Create corpo environment, a stab at Embracer
- [x] Convos with CEO, ultimately just to mock him
- [x] Introduce Hypnos
- [x] Have a creative way to break out
- [x] Escape sequence onto streets


#### Tijuana: Streets

- [x] Meet Miguel in an assigned role, help him out of it
- [x] Meet Anton Jackson, an old friend of Miguel's, who can help the player escape
- [x] Populate the streets
- [x] Escape sequence out of sim, alone


#### Tijuana: Illuminati

- [x] Meet Hypnos and try to convince it to release the others
- [x] Meet Julia, who apologises to the player and takes her to Morgan
- [x] Allow the player to change outfit
- [x] Convos with Morgan and Gary, introducing the premise of the next chapter
- [x] Obtain a new aug, telepathy or possession
- [x] Rooftop convo with Miguel
- [x] Map exit


### Phase 2

Vandenberg and Tijuana, iteration 2


#### General

- [x] Set flag triggers
    - [x] Helped family
    - [x] Killed Luis
    - [x] Stole Julia's gear
    - [x] Helped Anton
    - [x] Killed Jacksons
- [x] Make pink the mask color for most textures
    - [x] Make sure previously black masked textures are updated
- [x] Update portraits in InfoLink messages
    - [x] Gary Savage
- [x] Revamp all character skins in toon style
- [x] Set up automated flow for generating skins
    - [x] Integrate Mike's textures
    - [x] Integrate Anton Jackson's textures
    - [x] Integrate JC/Helios
    - [x] Integrate MIB/WIB
- [x] Improve telepathy aug
    - [x] Get rid of word salad, just the facts
    - [x] Create a "secrets" manager that determines who knows what
    - [x] Use outline to highlight relevant characters
- [x] Improve possession aug
    - [x] Use outline to highlight relevant characters
    - [x] Fix dying after deactivation
- [x] Remove the "outfit changed" prompt
- [x] Fix leather jackets having black coat tails
- [x] Fix jumpsuits having black helmets
- [x] Fix pink masks on suits
- [x] Change "Mike" to "Miguel" after slums
- [x] Launch demo
    - [x] Make website
    - [x] Package LLC's launcher
    - [x] Final playthroughs
    - [x] Make announcement


#### Vandenberg

- [x] Improve barracks design
- [x] Entirely new path to lab
- [x] Fix Carla's legs


#### Tijuana: Slums

- [x] More people
- [x] More pickups
- [x] Mini-missions
- [x] Fix BSP errors in the road
- [x] Fix police killing each other
- [x] Add goals and skill points
- [x] Fix police being too hard for Julia to kill
- [x] Fix Helios TV
- [x] Exclude balaclavas from random humans
- [x] Exclude funny frames and sunglasses from random humans
- [x] Remove couch table from El Coyote's tipi
- [x] Fix black lockers in the border area
- [x] Create nodes for the PCs at the border area
- [x] Add light to border area exit
- [x] Fix hovering house
- [x] Update Luis photos
- [x] Fix blocked tunnel
- [x] Generalise Luis' "fired" trigger


#### Tijuana: Office

- [x] Expand building interior and remove glass walls  
- [x] Escape sequence way longer  
- [x] Fix inventory still present on start
- [x] Remove \*ugh\* at beginning of MeetHypnos convo
- [x] Remove skybox
- [x] Fix vents all moving at the same time
- [x] Fix transitioning sound
- [x] Add goals and skill points
- [x] Fix stairs loop collision
- [x] Fix intersecting doors
- [x] Enable killing Larry Winger


#### Tijuana: Streets

- [x] More interesting layout in NSF compound
- [x] More difficult to find manhole
- [x] Mini quest to unlock Anton's cage
- [x] Escape through sewer better layout
- [x] Fix manhole bug
- [x] Fix Mike's hiding bark
- [x] Fix exit convo not firing
- [x] Implement new voice lines for random NPCs
- [x] Better startup sound for the train
- [x] Add goals and skill points
- [x] Fix corrupt sound in VA
- [x] Fix exit convo not starting
- [x] Fix vat goal appearing too soon
- [x] Implement new voice lines for Anton Jackson
- [x] Fix bar floor tiles
- [x] Fix subtitle: "if can keep me safe"
- [x] Increase enforcer radius


#### Tijuana: Illuminati

- [x] More employees
- [x] Make portals omnidirectional
- [x] Larger scale
- [x] Add technicians to the helipad
- [x] Make VTOL
- [x] Julia's apartment
    - [x] Environmental storytelling
- [x] Alex' apartment
    - [x] Computer, gadgets
    - [x] Gain access by reading his mind
- [x] Morgan's office
- [x] Morgan's apartment
- [x] Fix Morgan's legs
- [x] Non-sucky skybox
- [x] New outfits
- [x] Add goals and skill points
- [x] Fix Julia going the wrong way after portal to apartments
- [x] Fix player losing inventory after possessing
- [x] Fix Morgan's question about Anton reappearing
- [x] Give the secretary a few lines
- [x] Fix apartments -> lab portal


### Phase 3

Hong Kong, Tiffany's mission


#### Bugs

- [x] Fix crash when using ModernConCamera
- [ ] Fix players keeping health values on new game (?)


#### Chapter 1

- [x] Update El Coyote skin
- [x] Update Julia Montes skin
- [x] Account for player killing Jackson1
- [x] Improve visibility of Vandenberg parking garage
- [x] Keep Larry's liquor mission after breaking the faucet
- [x] Improve transition to sim
- [x] Fix OverhearMorganGary not playing properly
- [x] Fix inventory not getting entirely removed when going to office
- [x] Fix frobbing Alex first breaking flow
- [x] Make outfits disappear upon frob
- [x] Replace Helios TV in slums
- [x] Fix wooden planks in the abandoned house
- [x] Add meelee weapon to Vandenberg
- [x] Fix Julia last bark being played too soon
- [x] Add tech bot to Illuminati nano lab
- [x] Change FullscreenColorBits to 32
- [ ] Add random barks to slums
- [ ] Add random barks to lab


#### General

- [x] Clear mission objectives between missions
- [x] Fix double pink entries in indexed colour palettes generated by bright.exe
- [x] Fix hologram effect
- [x] Create inventory icons for Henosia vials
- [x] Update generic faces with new design improvements
- [ ] Fix possession aug misplacing player
- [x] Fix 3 line dialog being cut off in the subtitles
- [ ] Make more clothes for women
- [ ] Fix skin tones in torso textures


#### Intro dream sequence

- [x] Turn player into a kid
- [x] Add transition to awake state


#### Hong Kong: North Street

- [x] Tavern 
- [x] Exit: MTR
- [x] Exit: Triad compound
- [x] Simpler Helios screen
- [x] Hide Gordon Quick better
- [x] Hide the drug dealer better
- [x] Decker Parkes rescue mission
- [x] Iris skin
- [x] Decker skin
- [x] Erin skin
- [x] Wayne skin
- [x] Gordon skin
- [x] Make someone talk about wanting to go to Wan Chai
- [x] Fix ambient light
- [x] Shorter intro infolink
- [x] Give players more of a chance to hear the triad soldier convo
- [x] Get Miguel back in the story
- [x] Create male+female triad soldiers
- [x] Create intro sequence
- [x] Complete the hideout
- [x] Random voice lines for people in tea restaurant
- [x] Fix Decker's seat
- [x] Fix Decker pressing fingerprint scanner
- [x] Shop signs
- [~] Voice acting  
  - [x] BartenderGiveQuest  
  - [x] BartenderShop  
  - [x] BumMale1Barks  
  - [x] BumFemale1Barks  
  - [x] BumFemale2Barks  
  - [ ] GordonQuickBarks  
  - [x] MeetBartender  
  - [x] MeetDrugDealer  
  - [~] MeetGordonQuick  
  - [x] OverhearBums  
  - [x] OverhearDeckerBartender  
  - [x] OverhearTriadSoldiersStreet  
  - [ ] RandomFemale  
  - [ ] RandomMale  


#### Hong Kong: Triad Compound

- [x] Evidence that they killed the PM when he refused to cooperate
- [x] Evidence and reason that they're developing Henosia
- [x] Prevent player from leaving Tong's office until they dropped their weapons
- [x] Skybox
- [x] Exterior guards
- [x] Better construction chute
- [x] Loot in empty rooms
- [x] More excuses to use the new augs, lockpicks and multitools
- [x] Finish the kitchen
- [x] Make Paul Denton skin
- [~] Make Tracer Tong skin
- [x] Turn lions into dragons
- [x] Find out why boxes don't stack under the vents
- [x] Fix Paul room vent cover UVs
- [x] Fix guard bath vent opening the wrong way
- [x] Fix dining hall door UVs
- [x] Add ways for players to discover Tong's office code
- [x] Improve MeetTracerTong bit
- [ ] Voice acting  
  - [x] OverhearTriadSoldiersExterior  
  - [x] OverhearTriadSoldiersGarden  
  - [ ] TriadSoldierMaleBarks  
  - [ ] TriadSoldierFemaleBarks  
  - [x] OverhearScientists  
  - [ ] ScientistFemaleBarks  
  - [ ] ScientistMaleBarks  
  - [x] MeetTracerTong


#### Gassed dream sequence

- [x] JC as teenager
- [x] Platforming puzzle to get basketball down from a tree
- [x] Transition to Helios and credits


### Phase 4

Hong Kong, Adam's hub

#### Side quests

- [ ] Sabotage triad scramblers
- [ ] Autonomous MTR subplot
  - [ ] Turns out to be an AI
  - [ ] Purpose is to enforce good behaviour


#### Hong Kong: Mong Kok

- [ ] Exit: MTR
- [ ] Exit: NSF hideout
- [ ] Exit: Night club "Vongott"
- [ ] Exit: Temple street?
- [ ] Adam and Iris' flat
- [~] Fence
- [~] Pharmacy


#### Hong Kong: NSF Hideout

- [ ] Include comment about Anton Jackson
- [ ] NSF hideout as a brothel


### Phase X

The final development stage


#### General

- [ ] Dialogue cleanup
