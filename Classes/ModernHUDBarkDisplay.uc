// ========================================
// ModernHUDBarkDisplay
//
// Changes:
// - Centered text
// ========================================
class ModernHUDBarkDisplay extends HUDBarkDisplay;

// BEGIN UE1

function AddBark(string Text, float NewDisplayTime, Actor SpeakingActor)
{
    local HUDBarkDisplayItem NewBark;

    if(TrimSpaces(Text) != "")
    {
        NewBark = HUDBarkDisplayItem(WinBarks.NewChild(Class'HUDBarkDisplayItem'));
        NewBark.WinName.SetTextAlignments(HALIGN_Center, VALIGN_Top); 
        NewBark.WinBark.SetTextAlignments(HALIGN_Center, VALIGN_Top); 
        NewBark.SetBarkSpeech(Text, NewDisplayTime, SpeakingActor);

        BarkCount++;

        Show();

        AskParentForReconfigure();
    }
}

// END UE1
