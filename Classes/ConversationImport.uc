class ConversationImport expands Object abstract;

#exec CONVERSATION IMPORT FILE=Conversations\Mission16.con
#exec CONVERSATION IMPORT FILE=Conversations\Mission16_Dream.con
#exec CONVERSATION IMPORT FILE=Conversations\Mission17.con
#exec CONVERSATION IMPORT FILE=Conversations\Mission18.con
#exec CONVERSATION IMPORT FILE=Conversations\Mission18_RandomBarks.con
#exec CONVERSATION IMPORT FILE=Conversations\Mission19.con
#exec CONVERSATION IMPORT FILE=Conversations\Mission20.con
#exec CONVERSATION IMPORT FILE=Conversations\Mission90.con
