class JCHeliosHead extends ScriptedPawn;

// Voice processing in Audacity:
// - Helios voice
// - Duplicate track
//     - Set one to -20% pitch
//     - Set the other to +20% pitch
//     - Mix them back into one
// - Apply reverb
//     - Room size: 100%
//     - Reverberance: 40%
//     - Tone low: 100%
//     - Tone high: 50%

// BEGIN UE1

#exec MESH IMPORT MESH=JCHeliosHead ANIVFILE=Models\JCHeliosHead_a.3d DATAFILE=Models\JCHeliosHead_d.3d X=0 Y=0 Z=0 LODSTYLE=10 LODFRAME=0 
#exec MESH ORIGIN MESH=JCHeliosHead X=0 Y=0 Z=0 YAW=-64 PITCH=0 ROLL=0

#exec MESHMAP NEW MESHMAP=JCHeliosHead MESH=JCHeliosHead
#exec MESHMAP SCALE MESHMAP=JCHeliosHead X=0.003125 Y=0.003125 Z=0.003125

// END UE1

// BEGIN UE2

#exec MESH IMPORT MESH=JCHeliosHead ANIVFILE=Models\JCHeliosHead_UNR_a.3d DATAFILE=Models\JCHeliosHead_UNR_d.3d X=0 Y=0 Z=0 LODSTYLE=10 LODFRAME=0 
#exec MESH ORIGIN MESH=JCHeliosHead X=0 Y=0 Z=0 YAW=-64 PITCH=0 ROLL=0

#exec MESHMAP NEW MESHMAP=JCHeliosHead MESH=JCHeliosHead
#exec MESHMAP SCALE MESHMAP=JCHeliosHead X=0.1 Y=0.1 Z=0.2

// END UE2

#exec TEXTURE IMPORT NAME=JCHelios_FloatingHead FILE=Textures\Skins\JCHelios_FloatingHead.pcx GROUP=Decorations FLAGS=2
#exec MESHMAP SETTEXTURE MESHMAP=JCHeliosHead NUM=0 TEXTURE=JCHelios_FloatingHead

#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=ALL                   STARTFRAME=0    NUMFRAMES=110   RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=Still                 STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=MouthClosed           STARTFRAME=99   NUMFRAMES=1     RATE=10 
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=MouthA                STARTFRAME=100  NUMFRAMES=1     RATE=10 
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=MouthE                STARTFRAME=101  NUMFRAMES=1     RATE=10 
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=MouthF                STARTFRAME=102  NUMFRAMES=1     RATE=10 
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=MouthM                STARTFRAME=103  NUMFRAMES=1     RATE=10 
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=MouthO                STARTFRAME=104  NUMFRAMES=1     RATE=10 
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=MouthT                STARTFRAME=105  NUMFRAMES=1     RATE=10 
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=MouthU                STARTFRAME=106  NUMFRAMES=1     RATE=10 
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=Walk                  STARTFRAME=0    NUMFRAMES=1     RATE=10 
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=Run                   STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=CrouchWalk            STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=Ducking
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=Crouch                STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=Ducking
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=Stand                 STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=Ducking
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=CrouchShoot           STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=Ducking
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=BreatheLight          STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=Waiting
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=DeathFront            STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=DeathBack             STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=HitHead               STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=TakeHit
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=HitTorso              STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=TakeHit
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=HitArmLeft            STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=TakeHit
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=HitLegLeft            STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=TakeHit
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=HitArmRight           STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=TakeHit
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=HitLegRight           STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=TakeHit
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=HitHeadBack           STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=TakeHit
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=HitTorsoBack          STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=TakeHit
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=Blink                 STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=Shoot                 STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=Gesture
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=Attack                STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=Gesture
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=HeadUp                STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=HeadDown              STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=HeadLeft              STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=HeadRight             STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=RunShoot              STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=GestureLeft           STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=GestureRight          STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=GestureBoth           STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=RubEyesStart          STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=RubEyes               STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=RubEyesStop           STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=SitBegin              STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=SitStill              STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=SitStand              STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=Pickup                STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=Jump                  STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=Land                  STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=Landing
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=Panic                 STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=PushButton            STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=Gesture
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=ReloadBegin           STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=Gesture
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=Reload                STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=Gesture
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=ReloadEnd             STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=Gesture
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=Tread                 STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=TreadShoot            STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=WaterHitTorso         STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=TakeHit
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=WaterHitTorsoBack     STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=TakeHit
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=WaterDeath            STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=CowerBegin            STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=CowerStill            STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=CowerEnd              STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=AttackSide            STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=Strafe                STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=Walk2H                STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=Strafe2H              STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=RunShoot2H            STARTFRAME=0    NUMFRAMES=1     RATE=10
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=Shoot2H               STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=Gesture
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=BreatheLight2H        STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=Waiting
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=Idle1                 STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=Waiting
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=Idle12H               STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=Waiting
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=Shocked               STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=TakeHit
#exec MESH SEQUENCE MESH=JCHeliosHead SEQ=SitBreathe            STARTFRAME=0    NUMFRAMES=1     RATE=10     GROUP=Waiting

defaultproperties
{
    FamiliarName="Helios"
    UnfamiliarName="Helios"
    BindName="Helios"
    bUnlit=True
    Physics=PHYS_Flying
    Style=STY_Translucent
    Mesh=LodMesh'MachineGod.JCHeliosHead'
    MultiSkins(0)=Texture'MachineGod.JCHelios_FloatingHead'
    CollisionHeight=128
    CollisionRadius=40
    DrawScale=8
    DrawType=DT_Mesh
    Orders='Standing'
    BaseEyeHeight=0
}
