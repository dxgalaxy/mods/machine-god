class Shivs extends Object;

static function string StrToLower(string Text)
{
    local int IndexChar;

    for(IndexChar = 0; IndexChar < Len(Text); IndexChar++)
        if(Mid(Text, IndexChar, 1) >= "A" && Mid(Text, IndexChar, 1) <= "Z")
            Text = Left(Text, IndexChar) $ Chr(Asc(Mid(Text, IndexChar, 1)) + 32) $ Mid(Text, IndexChar + 1);

    return Text;
}

static function string SplitStr(coerce string Text, coerce string SplitStr, optional bool bOmitSplitStr)
{
    local int i;

    i = InStr(Text, SplitStr);

    if(i < 0)
        return Text;

    if(!bOmitSplitStr)
        return Mid(Text, i);

    if(bOmitSplitStr)
        return Mid(Text, i + Len(SplitStr));
}

static function string ReplaceStr(coerce string Text, coerce string Replace, coerce string With)
{
    local int i;
    local string Output;

    i = InStr(Text, Replace);
    while (i != -1) {
        Output = Output $ Left(Text, i) $ With;
        Text = Mid(Text, i + Len(Replace));
        i = InStr(Text, Replace);
    }
    Output = Output $ Text;
    return Output;
}

static function JoinStrArray(string StringArray[100], out string Result, optional string Delimiter, optional bool bIgnoreBlanks)
{
    local int i;

    Result = "";

    for(i = 0; i < ArrayCount(StringArray); i++)
    {
        if(StringArray[i] == "" && bIgnoreBlanks)
            continue;

        if(Result != "")
            Result = Result $ Delimiter;

        Result = Result $ StringArray[i];
    }
}

static function StrToArray(coerce string InStr, out string Parts[100], string Delimiter)
{
    local string Part, Glyph;
    local int i, NumParts;

    for(i = 0; i < Len(InStr) + 1; i++)
    {
        Glyph = Mid(InStr, i, 1);

        if(Glyph == Delimiter || i == Len(InStr))
        {
            Parts[NumParts] = Part;
            NumParts++;
            Part = "";
        }
        else
        {
            Part = Part $ Glyph;
        }
    }
}
