class TriadBrute extends ScriptedPawnMale;

// Voice processing in Audacity:
// - Smuggler voice
// - Amplify with -4dB
// - Duplicate track
//     - Set one to -10% pitch
//     - Set the other to -20% pitch
//     - Mix them back into one
// - Apply phaser with default values
// - Apply tremolo with 40hz
// - Apply limiter with 4dB input gain

#exec TEXTURE IMPORT NAME=TriadBrute_Head FILE=Textures\Skins\TriadBrute_Head.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TriadBrute_Torso FILE=Textures\Skins\TriadBrute_Torso.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TriadBrute_Helmet FILE=Textures\Skins\TriadBrute_Helmet.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=TriadBrute_Legs FILE=Textures\Skins\TriadBrute_Legs.pcx GROUP=Skins

defaultproperties
{
     BarkBindName="TriadBrute"
     BaseEyeHeight=58.000000
     BindName="TriadBrute"
     FamiliarName="Private Military"
     UnfamiliarName="Private Military"
     WalkingSpeed=0.300000
     bInvincible=False
     BaseAssHeight=-23.000000
     WalkAnimMult=1.050000
     GroundSpeed=200.000000
     Texture=None
     DrawScale=1.2
     Mesh=LodMesh'DeusExCharacters.GM_DressShirt_B'
     MultiSkins(0)=Texture'MachineGod.Skins.TriadBrute_Torso'
     MultiSkins(1)=Texture'MachineGod.Skins.TriadBrute_Legs'
     MultiSkins(2)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(3)=Texture'MachineGod.Skins.TriadBrute_Head'
     MultiSkins(4)=Texture'MachineGod.Skins.TriadBrute_Helmet'
     MultiSkins(5)=Texture'DeusExItems.Skins.GrayMaskTex'
     MultiSkins(6)=Texture'DeusExItems.Skins.PinkMaskTex'
     MultiSkins(7)=Texture'DeusExItems.Skins.BlackMaskTex'
     Texture=Texture'DeusExCharacters.Skins.VisorTex1'
     CollisionRadius=20.000000
     CollisionHeight=57.000000
     Health=200
}
