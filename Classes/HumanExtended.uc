class HumanExtended extends Human;

// Outfits
struct PlayerOutfit
{
    var string Name;
    var Mesh Mesh;
    var Texture Tex0;
    var Texture Tex1;
    var Texture Tex2;
    var Texture Tex3;
    var Texture Tex4;
    var Texture Tex5;
    var Texture Tex6;
    var Texture Tex7;
};

var PlayerOutfit Outfits[10];
var travel string CurrentOutfit;
var bool bIgnoreAllShowMenu, bIgnoreAllInput;
var Texture Portraits[5];
var class<AugmentationManager> AugmentationManagerClass;
var Sound EyePain, Cough, HitSound3, Drown, WaterDie, GaspSound; 

// BEGIN UE1

// --------------------------------
// Gets whether input is restricted
// --------------------------------
function bool RestrictInput()
{
    return bIgnoreAllInput || Super.RestrictInput();
}

// -----------
// Post travel
// -----------
function TravelPostAccept()
{
    local Vector LocDiff, NewLoc;

    Super.TravelPostAccept();
    
    if(
        FlagBase.CheckFlag('TravelLocationOffset', FLAG_Vector) &&
        FlagBase.CheckFlag('TravelRotationOffset', FLAG_Rotator) &&
        FlagBase.CheckFlag('TravelViewRotationOffset', FLAG_Rotator)
    )
    {
        // Position the player relative to the offset,
        // accounting for the offset being rotated
        LocDiff = FlagBase.GetVector('TravelLocationOffset');
        NewLoc = Location;
        NewLoc += Vector(Rotation) * LocDiff.X;  
        NewLoc += Vector(Rotation + Rot(0, 16384, 0)) * LocDiff.Y;  
        NewLoc -= Vector(Rotation + Rot(16384, 0, 0)) * LocDiff.Z;  
        SetLocation(NewLoc);

        DesiredRotation = Rotation + FlagBase.GetRotator('TravelRotationOffset');
        SetRotation(DesiredRotation);
        ViewRotation = DesiredRotation + FlagBase.GetRotator('TravelViewRotationOffset');
        
        FlagBase.DeleteFlag('TravelLocationOffset', FLAG_Vector);
        FlagBase.DeleteFlag('TravelRotationOffset', FLAG_Rotator);
        FlagBase.DeleteFlag('TravelViewRotationOffset', FLAG_Rotator);
    }
}

// -------------------
// Store travel offset
// -------------------
function StoreTravelOffset(Actor aOffset)
{
    if(aOffset == None)
        return;
   
    FlagBase.SetRotator('TravelRotationOffset', Rotation - aOffset.Rotation,, 0);
    FlagBase.SetRotator('TravelViewRotationOffset', ViewRotation - Rotation,, 0);
    FlagBase.SetVector('TravelLocationOffset', aOffset.Location - Location,, 0);
}

// -----------------
// Shows the credits
// -----------------
function ShowCredits(optional bool bLoadIntro)
{
    local DeusExRootWindow Root;
    local ModernCreditsWindow WinCredits;

    Root = DeusExRootWindow(RootWindow);

    if(Root == None)
        return;

    WinCredits = ModernCreditsWindow(Root.InvokeMenuScreen(class'ModernCreditsWindow', bLoadIntro));
    WinCredits.SetLoadIntro(bLoadIntro);
}

// ----------------------------------
// Shows the credits with splash text
// ----------------------------------
function ShowCreditsWithSplashText(string Text, optional float Duration, optional bool bLoadIntro)
{
    local DeusExRootWindow Root;
    local ModernCreditsWindow WinCredits;

    Root = DeusExRootWindow(RootWindow);

    if(Root == None)
        return;

    WinCredits = ModernCreditsWindow(Root.InvokeMenuScreen(class'ModernCreditsWindow', bLoadIntro));
    WinCredits.SetLoadIntro(bLoadIntro);
    WinCredits.SplashText = Text;

    if(Duration > 0.0)
        WinCredits.SplashTextDuration = Duration;
}

// -------------------
// Shows the main menu
// -------------------
exec function ShowMainMenu()
{
    if(bIgnoreAllShowMenu || bIgnoreAllInput)
        return;

    Super.ShowMainMenu();
}

// ---------------------------------------------
// Override the vanilla aug manager with our own
// ---------------------------------------------
function InitializeSubSystems()
{
    // Spawn the BarkManager
    if (BarkManager == None)
        BarkManager = Spawn(class'BarkManager', Self);

    // Spawn the Color Manager
    CreateColorThemeManager();
    ThemeManager.SetOwner(self);
    
    // Install the augmentation system if not found
    if(TiffanySavageAugmentationManager(AugmentationSystem) == None)
    {
        AugmentationSystem = Spawn(AugmentationManagerClass, Self);
        AugmentationSystem.CreateAugmentations(Self);
        AugmentationSystem.AddDefaultAugmentations();
        AugmentationSystem.SetOwner(Self);       
    }
    else
    {
        AugmentationSystem.SetPlayer(Self);
        AugmentationSystem.SetOwner(Self);
    }
    
    // Install the skill system if not found
    if (SkillSystem == None)
    {
        SkillSystem = Spawn(class'SkillManager', Self);
        SkillSystem.CreateSkills(Self);
    }
    else
    {
        SkillSystem.SetPlayer(Self);
    }
    
    if ((Level.Netmode == NM_Standalone) || (!bBeltIsMPInventory))
    {
        // Give the player a keyring
        CreateKeyRing();
    }
}

// ----------------------------------------------------------------
// Fixed this function, so it actually returns a texture group name
// ----------------------------------------------------------------
function name GetWallMaterial(out vector wallNormal)
{
    local Vector StartTrace, EndTrace, HitLocation, HitNormal;
    local Actor aTarget;
    local int TexFlags;
    local Name TexName, TexGroup;

    StartTrace = Location + Vect(0, 0, 1.0) * BaseEyeHeight;
    EndTrace = StartTrace + (Vector(Rotation) * CollisionRadius * 1.5);

    foreach TraceTexture(class'Actor', aTarget, TexName, TexGroup, TexFlags, HitLocation, HitNormal, EndTrace, StartTrace)
    {
        if(aTarget == Level || aTarget.IsA('Mover'))
            break;
    }

    WallNormal = HitNormal;

    return TexGroup;
}

// ---------------------------------
// Clears out the player's inventory
// ---------------------------------
exec function ClearInventory()
{
    local Inventory aItem, aNextItem;

    aItem = Inventory;

    while(aItem != None)
    {
        aNextItem = aItem.Inventory;
        DeleteInventory(aItem);
        aItem.Destroy();
        aItem = aNextItem;
    }

    Inventory = None;
}

// ---------------------------------------------------------
// Starts an infolink convo with a custom DataLinkPlay class
// ---------------------------------------------------------
function bool StartDataLinkTransmission(
    String datalinkName, 
    Optional DataLinkTrigger datalinkTrigger)
{
    local Conversation activeDataLink;
    local bool bDataLinkPlaySpawned;

    // Don't allow DataLinks to start if we're in PlayersOnly mode
    if ( Level.bPlayersOnly )
        return False;

    activeDataLink = GetActiveDataLink(datalinkName);

    if ( activeDataLink != None )
    {
        // Search to see if there's an active DataLinkPlay object 
        // before creating one

        if ( dataLinkPlay == None )
        {
            datalinkPlay = Spawn(class'ModernDataLinkPlay');
            bDataLinkPlaySpawned = True;
        }

        // Call SetConversation(), which returns 
        if (datalinkPlay.SetConversation(activeDataLink))
        {
            datalinkPlay.SetTrigger(datalinkTrigger);

            if (datalinkPlay.StartConversation(Self))
            {
                return True;
            }
            else
            {
                // Datalink must already be playing, or in queue
                if (bDataLinkPlaySpawned)
                {
                    datalinkPlay.Destroy();
                    datalinkPlay = None;
                }
                
                return False;
            }
        }
        else
        {
            // Datalink must already be playing, or in queue
            if (bDataLinkPlaySpawned)
            {
                datalinkPlay.Destroy();
                datalinkPlay = None;
            }
            return False;
        }
    }
    else
    {
        return False;
    }
}
// -------------------------------------------------
// Starts a conversation with a custom ConPlay class
// -------------------------------------------------
function bool StartConversation(
    Actor invokeActor, 
    EInvokeMethod invokeMethod, 
    optional Conversation con,
    optional bool bAvoidState,
    optional bool bForcePlay
    )
{
    local DeusExRootWindow root;

    root = DeusExRootWindow(rootWindow);

    // First check to see the actor has any conversations or if for some
    // other reason we're unable to start a conversation (typically if 
    // we're alread in a conversation or there's a UI screen visible)

    if ((!bForcePlay) && ((invokeActor.conListItems == None) || (!CanStartConversation())))
        return False;

    // Make sure the other actor can converse
    if ((!bForcePlay) && ((ScriptedPawn(invokeActor) != None) && (!ScriptedPawn(invokeActor).CanConverse())))
        return False;

    // If we have a conversation passed in, use it.  Otherwise check to see
    // if the passed in actor actually has a valid conversation that can be
    // started.

    if ( con == None )
        con = GetActiveConversation(invokeActor, invokeMethod);

    // If we have a conversation, put the actor into "Conversation Mode".
    // Otherwise just return false.
    //
    // TODO: Scan through the conversation and put *ALL* actors involved
    //       in the conversation into the "Conversation" state??

    if ( con != None )
    {
        // Check to see if this conversation is already playing.  If so,
        // then don't start it again.  This prevents a multi-bark conversation
        // from being abused.
        if ((conPlay != None) && (conPlay.con == con))
            return False;

        // Now check to see if there's a conversation playing that is owned
        // by the InvokeActor *and* the player has a speaking part *and*
        // it's a first-person convo, in which case we want to abort here.
        if (((conPlay != None) && (conPlay.invokeActor == invokeActor)) && 
            (conPlay.con.bFirstPerson) &&
            (conPlay.con.IsSpeakingActor(Self)))
            return False;

        // Check if the person we're trying to start the conversation 
        // with is a Foe and this is a Third-Person conversation.  
        // If so, ABORT!
        if ((!bForcePlay) && ((!con.bFirstPerson) && (ScriptedPawn(invokeActor) != None) && (ScriptedPawn(invokeActor).GetPawnAllianceType(Self) == ALLIANCE_Hostile)))
            return False;

        // If the player is involved in this conversation, make sure the 
        // scriptedpawn even WANTS to converse with the player.
        //
        // I have put a hack in here, if "con.bCanBeInterrupted" 
        // (which is no longer used as intended) is set, then don't 
        // call the ScriptedPawn::CanConverseWithPlayer() function

        if ((!bForcePlay) && ((con.IsSpeakingActor(Self)) && (!con.bCanBeInterrupted) && (ScriptedPawn(invokeActor) != None) && (!ScriptedPawn(invokeActor).CanConverseWithPlayer(Self))))
            return False;

        // Hack alert!  If this is a Bark conversation (as denoted by the 
        // conversation name, since we don't have a field in ConEdit), 
        // then force this conversation to be first-person
        if (Left(con.conName, Len(con.conOwnerName) + 5) == (con.conOwnerName $ "_Bark"))
            con.bFirstPerson = True;
        // Make sure the player isn't ducking.  If the player can't rise
        // to start a third-person conversation (blocked by geometry) then 
        // immediately abort the conversation, as this can create all 
        // sorts of complications (such as the player standing through
        // geometry!!)

        if ((!con.bFirstPerson) && (ResetBasedPawnSize() == False))
            return False;

        // If ConPlay exists, end the current conversation playing
        if (conPlay != None)
        {
            // If we're already playing a third-person conversation, don't interrupt with
            // another *radius* induced conversation (frobbing is okay, though).
            if ((conPlay.con != None) && (conPlay.con.bFirstPerson) && (invokeMethod == IM_Radius))
                return False;

            conPlay.InterruptConversation();
            conPlay.TerminateConversation();
        }

        // If this is a first-person conversation _and_ a DataLink is already
        // playing, then abort.  We don't want to give the user any more 
        // distractions while a DL is playing, since they're pretty important.
        if ( dataLinkPlay != None )
        {
            if (con.bFirstPerson)
                return False;
            else
                dataLinkPlay.AbortAndSaveHistory();
        }

        // Found an active conversation, so start it
        conPlay = Spawn(class'ModernConPlay');
        conPlay.SetStartActor(invokeActor);
        conPlay.SetConversation(con);
        conPlay.SetForcePlay(bForcePlay);
        conPlay.SetInitialRadius(VSize(Location - invokeActor.Location));

        // If this conversation was invoked with IM_Named, then save away
        // the current radius so we don't abort until we get outside 
        // of this radius + 100.
        if ((invokeMethod == IM_Named) || (invokeMethod == IM_Frob))
        {
            conPlay.SetOriginalRadius(con.radiusDistance);
            con.radiusDistance = VSize(invokeActor.Location - Location);
        }

        // If the invoking actor is a ScriptedPawn, then force this person 
        // into the conversation state
        if ((!bForcePlay) && (ScriptedPawn(invokeActor) != None ))
            ScriptedPawn(invokeActor).EnterConversationState(con.bFirstPerson, bAvoidState);

        // Do the same if this is a DeusExDecoration
        if ((!bForcePlay) && (DeusExDecoration(invokeActor) != None ))
            DeusExDecoration(invokeActor).EnterConversationState(con.bFirstPerson, bAvoidState);

        // If this is a third-person convo, we're pretty much going to 
        // pause the game.  If this is a first-person convo, then just 
        // keep on going..
        //
        // If this is a third-person convo *AND* 'bForcePlay' == True, 
        // then use first-person mode, as we're playing an intro/endgame
        // sequence and we can't have the player in the convo state (bad bad bad!)

        if ((!con.bFirstPerson) && (!bForcePlay))
        {
            // Turn towards the invoke actor
            GotoState('Conversation');
            
            TurnTowardInstant(InvokeActor);
        }
        else
        {
            if (!conPlay.StartConversation(Self, invokeActor, bForcePlay))
            {
                AbortConversation(True);
            }
        }

        return True;
    }
    else
    {
        return False;
    }
}

// ----------------------------------------------------------------
// Neuter this function, its only purpose is to break conversations
// ----------------------------------------------------------------
function bool CheckActorDistances();

// --------------
// Changes outfit
// --------------
function ChangeOutfit(string Outfit)
{
    local int i;
    local bool bFound;

    CurrentOutfit = Outfit;

    for(i = 0; i < ArrayCount(Outfits); i++)
    {
        if(Outfits[i].Name != Outfit)
            continue;
        
        if(Outfits[i].Mesh == None)
            Mesh = Default.Mesh;
        else
            Mesh = Outfits[i].Mesh;
      
        if(Outfits[i].Tex0 == None)
            MultiSkins[0] = Default.MultiSkins[0]; 
        else
            MultiSkins[0] = Outfits[i].Tex0;
        
        if(Outfits[i].Tex1 == None)
            MultiSkins[1] = Default.MultiSkins[1]; 
        else
            MultiSkins[1] = Outfits[i].Tex1;
        
        if(Outfits[i].Tex2 == None)
            MultiSkins[2] = Default.MultiSkins[2]; 
        else
            MultiSkins[2] = Outfits[i].Tex2;
        
        if(Outfits[i].Tex3 == None)
            MultiSkins[3] = Default.MultiSkins[3]; 
        else
            MultiSkins[3] = Outfits[i].Tex3;
        
        if(Outfits[i].Tex4 == None)
            MultiSkins[4] = Default.MultiSkins[4]; 
        else
            MultiSkins[4] = Outfits[i].Tex4;
        
        if(Outfits[i].Tex5 == None)
            MultiSkins[5] = Default.MultiSkins[5]; 
        else
            MultiSkins[5] = Outfits[i].Tex5;
        
        if(Outfits[i].Tex6 == None)
            MultiSkins[6] = Default.MultiSkins[6]; 
        else
            MultiSkins[6] = Outfits[i].Tex6;
        
        if(Outfits[i].Tex7 == None)
            MultiSkins[7] = Default.MultiSkins[7]; 
        else
            MultiSkins[7] = Outfits[i].Tex7;

        bFound = True;
        break;
    }

    if(!bFound)
    {
        CurrentOutfit = "";
        Mesh = Default.Mesh;
            
        for(i = 0; i < ArrayCount(MultiSkins); i++)
        {
            MultiSkins[i] = Default.MultiSkins[i];
        }
    }
}

// ------------------------------------------------------------
// Checks whether a location is in front of us or another actor
// ------------------------------------------------------------
function bool IsInFront(Vector Loc, optional Actor aActor)
{
    local float DotProduct;
    local Rotator ActorRotation;

    if(aActor == None)
        aActor = Self;

    if(aActor.IsA('DeusExPlayer'))
        ActorRotation = DeusExPlayer(aActor).ViewRotation;
    else
        ActorRotation = aActor.Rotation;

    DotProduct = (aActor.Location - Loc) dot Vector(ActorRotation);

    return DotProduct < 0.0;
}

// ------------------------------------------
// Checks if we're currently using a computer
// ------------------------------------------
function bool IsUsingComputer()
{
    return ActiveComputer != None && ActiveComputer.IsInState('On');
}

// ------------------------
// Plays the dying sequence
// ------------------------
function PlayDying(name damageType, vector hitLoc)
{
    local Vector X, Y, Z;
    local float dotp;
    
    GetAxes(Rotation, X, Y, Z);
    dotp = (Location - HitLoc) dot X;

    if (Region.Zone.bWaterZone)
    {
        PlayAnim('WaterDeath',,0.1);
    }
    else
    {
        // die from the correct side
        if (dotp < 0.0)     // shot from the front, fall back
            PlayAnim('DeathBack',,0.1);
        else                // shot from the back, fall front
            PlayAnim('DeathFront',,0.1);
    }

    if ((damageType == 'Stunned') || (damageType == 'KnockedOut') ||
        (damageType == 'Poison') || (damageType == 'PoisonEffect'))
    {
        if (bIsFemale)
            PlaySound(Sound'FemaleUnconscious', SLOT_Pain,,,, RandomPitch());
        else
            PlaySound(Sound'MaleUnconscious', SLOT_Pain,,,, RandomPitch());
    }
    else
    {
        PlayDyingSound();
    }
}

// ------------------------------------------
// Checks whether we're standing on the floor
// ------------------------------------------
function bool IsOnFloor()
{
    local Vector EndTrace, HitLocation, HitNormal;
    local Actor aTarget;

    EndTrace = Location - CollisionHeight * 2 * Vect(0, 0, 1);

    foreach TraceActors(class'Actor', aTarget, HitLocation, HitNormal, EndTrace)
    {
        if(aTarget == Level || aTarget.IsA('Mover'))
            return True;
    }

    return False;
}

// -----------
// Drug effect
// -----------
simulated function DrugEffects(float DeltaTime)
{
    local float Mult, FOV;
    local Rotator Rot;
    local DeusExRootWindow Root;

    Root = DeusExRootWindow(RootWindow);

    // Random wandering and swaying when drugged
    if(DrugEffectTimer > 0)
    {
        if(
            Root != None && 
            Root.HUD != None &&
            Root.HUD.Background == None
        )
        {
            Root.HUD.SetBackground(Texture'DrunkFX');
            Root.HUD.SetBackgroundSmoothing(True);
            Root.HUD.SetBackgroundStretching(True);
            Root.HUD.SetBackgroundStyle(DSTY_Modulated);
        }

        Mult = FClamp(DrugEffectTimer / 10.0, 0.0, 3.0);
        Rot.Pitch = 1024.0 * Cos(Level.TimeSeconds * Mult) * DeltaTime * Mult;
        Rot.Yaw = 1024.0 * Sin(Level.TimeSeconds * Mult) * DeltaTime * Mult;
        Rot.Roll = 0;

        Rot.Pitch = FClamp(Rot.Pitch, -4096, 4096);
        Rot.Yaw = FClamp(Rot.Yaw, -4096, 4096);
        Rot.Roll = 0;

        ViewRotation += Rot;

        if(IsOnFloor())
            Velocity = Velocity * (1.0 + Sin(Level.TimeSeconds * Mult) / 2);

        if(Level.NetMode == NM_Standalone)
        {
            FOV = Default.DesiredFOV - drugEffectTimer + Rand(2);
            FOV = FClamp(FOV, 30, Default.DesiredFOV);
            DesiredFOV = FOV;
        }
        else
        {
            DesiredFOV = Default.DesiredFOV;
        }
        
        DrugEffectTimer -= DeltaTime;
        
        if(DrugEffectTimer < 0)
            DrugEffectTimer = 0;
    }
    else
    {
        if(
            Root != None &&
            Root.HUD != None &&
            Root.HUD.Background != None
        )
        {
            Root.HUD.SetBackground(None);
            Root.HUD.SetBackgroundStyle(DSTY_Normal);
            DesiredFOV = Default.DesiredFOV;
        }
    }
}

// -------------------------------------------
// Calculates the camera position and rotation
//
// Changes:
// - Call a revised positioning algorithm
// -------------------------------------------
event PlayerCalcView( out actor ViewActor, out vector CameraLocation, out rotator CameraRotation )
{
    local float NewFOV;

    NewFOV = FOVAngle;

    // check for spy drone and freeze player's view
    if (bSpyDroneActive)
    {
        if (aDrone != None)
        {
            // First-person view.
            CameraLocation = Location;
            CameraLocation.Z += EyeHeight;
            CameraLocation += WalkBob;
            return;
        }
    }

    // Check if we're in first-person view or third-person.  If we're in first-person then
    // we'll just render the normal camera view.  Otherwise we want to place the camera
    // as directed by the conPlay.cameraInfo object.

    if ( bBehindView && (!InConversation()) )
    {
        Super.PlayerCalcView(ViewActor, CameraLocation, CameraRotation);
        return;
    }

    if ( (!InConversation()) || ( conPlay.GetDisplayMode() == DM_FirstPerson ) )
    {
        // First-person view.
        ViewActor = Self;
        CameraRotation = ViewRotation;
        CameraLocation = Location;
        CameraLocation.Z += EyeHeight;
        CameraLocation += WalkBob;
        return;
    }

    // Allow the ConPlay object to calculate the camera position and 
    // rotation for us (in other words, take this sloppy routine and 
    // hide it elsewhere).

    if(ModernConPlay(conPlay).CalculateCameraPosition(ViewActor, CameraLocation, CameraRotation, NewFOV) == False)
        Super.PlayerCalcView(ViewActor, CameraLocation, CameraRotation);

    DesiredFOV = NewFOV;
    SetFOVAngle(NewFOV);
}

// ----------------------------------------------------
// Console: Identifies the actor in front of the player
// ----------------------------------------------------
exec function Identify()
{
    local Actor aHitActor;
    local Vector HitLocation, HitNormal;
    local Vector Position, Line;

    if(FrobTarget != None)
    {
        aHitActor = FrobTarget;
    }
    else
    {
        Position = Location;
        Position.Z += BaseEyeHeight;
        Line = Vector(ViewRotation) * 4000;

        aHitActor = Trace(HitLocation, HitNormal, Position + Line, Position, True);
    }

    if(aHitActor == None)
        return;

    ClientMessage(aHitActor.Name);
}

// ---------------------
// Console: Roll credits
// ---------------------
exec function RollCredits()
{
    ShowCredits(False);
}

// ------------------------------------
// Console: Possess another player pawn
// ------------------------------------
exec function PossessPawn(name PawnTag)
{
    local HumanExtended aPawn;

    if(PawnTag == '')
        return;

    foreach AllActors(class'HumanExtended', aPawn, PawnTag)
    {
        aPawn.Possess();
    }
}

// --------------------------------------
// Console: Set pawns' alliance to player
// --------------------------------------
exec function Ally(name PawnTag, int Value)
{
    local ScriptedPawn aPawn;

    if(Caps(PawnTag) == "ALL")
        PawnTag = '';

    foreach AllActors(class'ScriptedPawn', aPawn, PawnTag)
        aPawn.ChangeAlly('Player', Value, True, False);
}

// --------------------
// Console: Give orders
// --------------------
exec function Order(name PawnTag, name Orders, optional name OrderTag)
{
    local ScriptedPawn aPawn;

    if(PawnTag == '' || Orders == '')
        return;

    foreach AllActors(class'ScriptedPawn', aPawn, PawnTag)
    {
        aPawn.SetOrders(Orders, OrderTag, True);
    }
}

// ------------------------
// Console: Test a scenario
// ------------------------
exec function Test(name TestName)
{
    local MissionScriptExtended aMissionScript;

    foreach AllActors(class'MissionScriptExtended', aMissionScript)
        aMissionScript.DoTest(TestName);
}

// ---------------------------
// Console: Move an actor here
// ---------------------------
exec function SummonExisting(name ActorTag)
{
    local Actor aActor;

    foreach AllActors(class'Actor', aActor, ActorTag)
    {
        aActor.SetLocation(Location + (CollisionRadius + aActor.CollisionRadius + 30) * Vector(Rotation) + Vect(0 , 0, 1) * 15);
    }

}

// -----------------
// Console: Teleport
// -----------------
exec function TP(name TagName)
{
    local Actor aActor;

    foreach AllActors(class'Actor', aActor, TagName)
    {
        SetLocation(aActor.Location);
        SetRotation(aActor.Rotation);
        ViewRotation = aActor.Rotation;
    }
}

// ------------------------------
// Console: Give the player a key
// ------------------------------
exec function GimmeKey(name KeyID)
{
    local NanoKey aKey;

    aKey = Spawn(class'NanoKey');
    aKey.Description = string(KeyID);
    aKey.KeyID = KeyID;
    aKey.GiveTo(Self);
}

// -------------------------
// Console: Trigger anything
// -------------------------
exec function DoTrigger(name TagName)
{
    local Actor aActor;

    foreach AllActors(class'Actor', aActor, TagName)
    {
        aActor.Trigger(Self, Self);
    }
}

// ----------------------
// Console: Echo a string
// ----------------------
exec function Echo(string Message)
{
    ClientMessage(Message);
}

// --------------------------------------------------
// Console: Runs the travel code in the MissionScript
// --------------------------------------------------
exec function DoTravel()
{
    local MissionScriptExtended aMissionScript;

    foreach AllActors(class'MissionScriptExtended', aMissionScript)
        aMissionScript.TravelToMap();
}

// -------------------------------------------
// Console: Send an event to the MissionScript
// -------------------------------------------
exec function DoEvent(name EventName)
{
    local MissionScriptExtended aMissionScript;

    foreach AllActors(class'MissionScriptExtended', aMissionScript)
        aMissionScript.DoEvent(EventName, Self, Self);
}

// -------------------
// Console: Set a flag
// -------------------
exec function SetFlag(name FlagName, bool bValue)
{
    FlagBase.SetBool(FlagName, bValue);
}

// --------------------------
// Stubs that prevent crashes
// --------------------------
function FindGoodView();
function PlayerMove(Float DeltaTime);
function Rise();
function Dodge(eDodgeDir DodgeMove);

// -------------------------
// State: Paralysed with HUD
// -------------------------
state ParalyzedWithHUD extends Paralyzed
{
    event PlayerTick(float DeltaTime)
    {
        UpdateInHand();
        ShowHUD(True);
        ViewFlash(deltaTime);
    }
}

// ----------------------------------------------
// State: Standing (no items, no walking, no HUD)
// ----------------------------------------------
state PlayerStanding extends PlayerWalking
{
    function BeginState()
    {
        ShowHUD(False);
        PutInHand(None);
        DropDecoration();
        bIsCrouching = False;
        bCrouchOn = False;
        bWasCrouchOn = False;
        Velocity = Vect(0, 0, 0); 
        Acceleration = Vect(0, 0, 0);
    }
    
    function ProcessMove(float DeltaTime, Vector NewAccel, EDodgeDir DodgeMove, Rotator DeltaRot)
    {
        NewAccel.X = 0;
        NewAccel.Y = 0;
        NewAccel.Z = 0;

        Super.ProcessMove(DeltaTime, NewAccel, DodgeMove, DeltaRot);
    }
}

// ------------------------
// State: Standing with HUD
// ------------------------
state PlayerStandingWithHUD extends PlayerStanding
{
    function BeginState()
    {
        Super.BeginState();
        ShowHUD(True);
    }
}

// ---------------------------------------------
// Checks if the player has an item of this type
// ---------------------------------------------
function bool HasA(name InventoryClassName)
{
    local Inventory Inv;

    for(Inv = Inventory; Inv != None; Inv = Inv.Inventory)   
        if(Inv.IsA(InventoryClassName))
            return True;
    
    return False;
} 

// ---------------------------
// Fix player sounds for dying
// ---------------------------
function PlayDyingSound()
{
    if (Region.Zone.bWaterZone)
        PlaySound(WaterDie, SLOT_Pain,,,, RandomPitch());
    else
        PlaySound(Die, SLOT_Pain,,,, RandomPitch());
}

// -----------------------------
// Fix player sounds for gasping
// -----------------------------
function Gasp()
{
    PlaySound(GaspSound);
}

// ----------------------------
// Fix player sounds for damage
// ----------------------------
function PlayTakeHitSound(int Damage, name damageType, int Mult)
{
    local float rnd;

    if ( Level.TimeSeconds - LastPainSound < FRand() + 0.5)
        return;

    LastPainSound = Level.TimeSeconds;

    if (Region.Zone.bWaterZone)
    {
        if (damageType == 'Drowned')
        {
            if (FRand() < 0.8)
                PlaySound(Drown, SLOT_Pain, FMax(Mult * TransientSoundVolume, Mult * 2.0),,, RandomPitch());
        }
        else
            PlaySound(HitSound1, SLOT_Pain, FMax(Mult * TransientSoundVolume, Mult * 2.0),,, RandomPitch());
    }
    else
    {
        // Body hit sound for multiplayer only
        if (((damageType=='Shot') || (damageType=='AutoShot'))  && ( Level.NetMode != NM_Standalone ))
        {
            PlaySound(sound'BodyHit', SLOT_Pain, FMax(Mult * TransientSoundVolume, Mult * 2.0),,, RandomPitch());
        }

        if ((damageType == 'TearGas') || (damageType == 'HalonGas'))
            PlaySound(EyePain, SLOT_Pain, FMax(Mult * TransientSoundVolume, Mult * 2.0),,, RandomPitch());
        else if (damageType == 'PoisonGas')
            PlaySound(Cough, SLOT_Pain, FMax(Mult * TransientSoundVolume, Mult * 2.0),,, RandomPitch());
        else
        {
            rnd = FRand();
            if (rnd < 0.33)
                PlaySound(HitSound1, SLOT_Pain, FMax(Mult * TransientSoundVolume, Mult * 2.0),,, RandomPitch());
            else if (rnd < 0.66)
                PlaySound(HitSound2, SLOT_Pain, FMax(Mult * TransientSoundVolume, Mult * 2.0),,, RandomPitch());
            else
                PlaySound(HitSound3, SLOT_Pain, FMax(Mult * TransientSoundVolume, Mult * 2.0),,, RandomPitch());
        }
        AISendEvent('LoudNoise', EAITYPE_Audio, FMax(Mult * TransientSoundVolume, Mult * 2.0));
    }
}

// ----------------------------------
// Instantly turns towards a location
// ----------------------------------
function TurnToInstant(Vector Target)
{
    local Vector Direction;

    Target.Z = Location.Z;
    Direction = Target - Location;

    DesiredRotation = Rotator(Direction);
    SetRotation(DesiredRotation);
}

// --------------------------------
// Instantly turns towards an actor
// --------------------------------
function TurnTowardInstant(Actor Target)
{
    TurnToInstant(Target.Location);
}

// END UE1

defaultproperties
{
    AugmentationManagerClass=class'AugmentationManager'
    BindName="JCDenton"
    CarcassType=class'CarcassExtended'
    
    Portraits(0)=Texture'DeusExUI.UserInterface.MenuNewGameJCDenton_1'
    Portraits(1)=Texture'DeusExUI.UserInterface.MenuNewGameJCDenton_2'
    Portraits(2)=Texture'DeusExUI.UserInterface.MenuNewGameJCDenton_3'
    Portraits(3)=Texture'DeusExUI.UserInterface.MenuNewGameJCDenton_4'
    Portraits(4)=Texture'DeusExUI.UserInterface.MenuNewGameJCDenton_5'
    
    EyePain=Sound'DeusExSounds.Player.MaleEyePain'
    Cough=Sound'DeusExSounds.Player.MaleCough'
    HitSound1=Sound'DeusExSounds.Player.MalePainSmall'
    HitSound2=Sound'DeusExSounds.Player.MalePainMedium'
    HitSound3=Sound'DeusExSounds.Player.MalePainLarge'
    Drown=Sound'DeusExSounds.Player.MaleDrown'
    WaterDie=Sound'DeusExSounds.Player.MaleWaterDeath'
    GaspSound=Sound'DeusExSounds.Player.MaleGasp'
    JumpSound=Sound'DeusExSounds.Player.MaleJump'
    HitSound1=Sound'DeusExSounds.Player.MalePainSmall'
    HitSound2=Sound'DeusExSounds.Player.MalePainMedium'
    Land=Sound'DeusExSounds.Player.MaleLand'
    Die=Sound'DeusExSounds.Player.MaleDeath'
}
