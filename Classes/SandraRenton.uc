class SandraRenton extends ScriptedPawnFemale;

#exec TEXTURE IMPORT NAME=SandraRenton_Head FILE=Textures\Skins\SandraRenton_Head.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=SandraRenton_Torso FILE=Textures\Skins\SandraRenton_Torso.pcx GROUP=Skins

defaultproperties
{
     bImportant=True
     Mesh=LodMesh'DeusExCharacters.GFM_TShirtPants'
     MultiSkins(0)=Texture'MachineGod.Skins.SandraRenton_Head'
     MultiSkins(1)=Texture'MachineGod.Skins.SandraRenton_Head'
     MultiSkins(2)=Texture'MachineGod.Skins.SandraRenton_Head'
     MultiSkins(3)=Texture'DeusExItems.Skins.GrayMaskTex'
     MultiSkins(4)=Texture'DeusExItems.Skins.BlackMaskTex'
     MultiSkins(5)=None
     MultiSkins(6)=Texture'MachineGod.Skins.Jeans2'
     MultiSkins(7)=Texture'MachineGod.Skins.SandraRenton_Torso'
     BindName="SandraRenton"
     FamiliarName="Sandra Renton"
     UnfamiliarName="Sandra Renton"
}
