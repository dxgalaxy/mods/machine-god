class RandomChild extends RandomScriptedPawn;

// BEGIN UE1

// -----------------------
// Generates the character
// -----------------------
function Generate()
{
    GenerateGMKDressShirt();
}

// ---------------
// GMK_DressShirt
// ---------------
function GenerateGMKDressShirt()
{
    if(BodyType == BODY_Fat)
        Mesh = LodMesh'DeusExCharacters.GMK_DressShirt_F';
    else
        Mesh = LodMesh'DeusExCharacters.GMK_DressShirt';

    // Head
    MultiSkins[0] = GetRandomTexture('Child', 'Heads');
    MultiSkins[4] = PinkMask;

    // Glasses
    if(Rand(2) > 0)
    {
        MultiSkins[6] = GetRandomTexture('Common', 'Frames');   
        MultiSkins[7] = GetRandomTexture('Common', 'Lenses');   
    }
    else
    {
        MultiSkins[6] = GrayMask;
        MultiSkins[7] = BlackMask;
    }

    // Torso
    MultiSkins[1] = GetRandomTexture('Child', 'Torsos');
    MultiSkins[5] = PinkMask;
   
    // Legs
    MultiSkins[2] = GetRandomTexture('Common', 'Pants');
}

// END UE1

defaultproperties
{
    BindName="RandomChild"
    UnfamiliarName="Child"
    FamiliarName="Child"
    Mesh=LodMesh'DeusExCharacters.GMK_DressShirt'
    WalkingSpeed=0.256000
    WalkAnimMult=1.150000
    GroundSpeed=150.000000
    BaseEyeHeight=26.000000
    HealthHead=50
    HealthTorso=50
    HealthLegLeft=50
    HealthLegRight=50
    HealthArmLeft=50
    HealthArmRight=50
    CollisionRadius=17.000000
    CollisionHeight=32.500000
    Mass=80.000000
    Buoyancy=85.000000
}
