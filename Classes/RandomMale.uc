class RandomMale extends RandomScriptedPawn;

// BEGIN UE1

// -----------------------
// Generates the character
// -----------------------
function Generate()
{
    // Military, guerilla, police and technicians
    if(StyleTag == 'Guerilla' || StyleTag == 'Military' || StyleTag == 'Police' || StyleTag == 'Tech')
    {
        GenerateGMJumpsuit();
    }
    // Scientists
    else if(StyleTag == 'Science')
    {
        GenerateGMTrench();
    }
    // Doctors and nurses
    else if(StyleTag == 'Medical')
    {
        switch(Rand(2))
        {
            case 0:
                GenerateGMDressShirt();
                break;
            case 1:
                GenerateGMTrench();
                break;
        }
    }
    // Formally dressed guys
    else if(StyleTag == 'Formal')
    {
        GenerateGMSuit();
    }
    // Everyone else
    else
    {
        if(BodyType == BODY_Skinny)
        {
            GenerateGMDressShirt();
        }
        else
        {
            switch(Rand(2))
            {
                case 0:
                    GenerateGMDressShirt();
                    break;

                case 1:
                    GenerateGMTrench();
                    break;
            }
        }
    }
}

// ---------------
// GM_Suit
// 0: Head
// 1: Legs
// 2: ???
// 3: Torso
// 4: Skirt
// 5: Frames
// 6: Lenses
// 7: Hat and ponytail
// ---------------
function GenerateGMSuit()
{
    Mesh = LodMesh'DeusExCharacters.GM_Suit';

    // Head
    MultiSkins[0] = GetRandomTexture('Male', 'Heads');

    // Glasses
    if(Rand(2) > 0)
    {
        MultiSkins[5] = GetRandomTexture('Common', 'Frames');   
        MultiSkins[6] = GetRandomTexture('Common', 'Lenses');   
    }
    else
    {
        MultiSkins[5] = GrayMask;
        MultiSkins[6] = BlackMask;
    }

    // TODO: Hat and ponytail
    MultiSkins[7] = PinkMask;

    // Torso
    MultiSkins[3] = GetRandomTexture('Male', 'SuitTorsos');
    MultiSkins[4] = MultiSkins[3];
   
    // Legs
    MultiSkins[1] = GetRandomTexture('Common', 'Pants');
}

// ---------------
// GM_DressShirt
// 0: Head
// 1: Shade back
// 2: Shade front
// 3: Legs
// 4: ???
// 5: Torso
// 6: Frames
// 7: Lenses
// ---------------
function GenerateGMDressShirt()
{
    if(BodyType == BODY_Undefined)
    {
        switch(Rand(3))
        {
            case 0:
                BodyType = BODY_Fat;
                break;

            case 1:
                BodyType = BODY_Normal;
                break;
            
            case 2:
                BodyType = BODY_Skinny;
                break;
        }
    }

    if(BodyType == BODY_Fat)
        Mesh = LodMesh'DeusExCharacters.GM_DressShirt_F';

    else if(BodyType == BODY_Skinny)
        Mesh = LodMesh'DeusExCharacters.GM_DressShirt_S';

    else
        Mesh = LodMesh'DeusExCharacters.GM_DressShirt';

    // Head
    MultiSkins[0] = GetRandomTexture('Male', 'Heads');
    MultiSkins[1] = PinkMask;
    MultiSkins[2] = PinkMask;

    // Glasses
    if(Rand(2) > 0)
    {
        MultiSkins[6] = GetRandomTexture('Common', 'Frames');   
        MultiSkins[7] = GetRandomTexture('Common', 'Lenses');   
    }
    else
    {
        MultiSkins[6] = GrayMask;
        MultiSkins[7] = BlackMask;
    }

    // Torso
    if(BodyType == BODY_Fat)
        MultiSkins[5] = GetRandomTexture('Male', 'FatTorsos');
    else
        MultiSkins[5] = GetRandomTexture('Male', 'Torsos');
   
    // Legs
    MultiSkins[3] = GetRandomTexture('Common', 'Pants');
}

// ----------------------
// GM_Trench
//
// 0: Head
// 1: Torso 
// 2: Legs
// 3: ???
// 4: Shirt
// 5: Coattail and collar
// 6: Frames
// 7: Lenses
// ----------------------
function GenerateGMTrench()
{
    if(BodyType == BODY_Undefined)
    {
        if(Rand(2) > 0)
            BodyType = BODY_Fat;
        else
            BodyType = BODY_Normal;
    }

    if(BodyType == BODY_Fat)
        Mesh = LodMesh'DeusExCharacters.GM_Trench_F';
    else
        Mesh = LodMesh'DeusExCharacters.GM_Trench';

    // Head
    MultiSkins[0] = GetRandomTexture('Male', 'Heads');
    
    // Glasses
    if(Rand(2) > 0)
    {
        MultiSkins[6] = GetRandomTexture('Common', 'Frames');   
        MultiSkins[7] = GetRandomTexture('Common', 'Lenses');   
    }
    else
    {
        MultiSkins[6] = GrayMask;
        MultiSkins[7] = BlackMask;
    }

    // Coat
    MultiSkins[4] = GetRandomTexture('Male', 'ShirtFronts');

    if(BodyType == BODY_Fat)
        MultiSkins[1] = GetRandomTexture('Male', 'FatCoats');
    else
        MultiSkins[1] = GetRandomTexture('Male', 'Coats');
    
    MultiSkins[5] = MultiSkins[1];

    // Pants
    MultiSkins[2] = GetRandomTexture('Common', 'Pants');
}

// -----------
// GM_Jumpsuit
// 0: ???
// 1: Legs
// 2: Torso
// 3: Head
// 4: Mask
// 5: Gogggles
// 6: Helmet
// 7: Visor
// -----------
function GenerateGMJumpsuit()
{
    Mesh = LodMesh'DeusExCharacters.GM_Jumpsuit';
 
    // Head
    MultiSkins[3] = GetRandomTexture('Male', 'Heads');

    // Mask
    MultiSkins[4] = PinkMask;

    // Goggles
    MultiSkins[5] = PinkMask;

    // Torso
    MultiSkins[2] = GetRandomTexture('Male', 'JumpsuitTorsos');

    // Pants
    MultiSkins[1] = GetRandomTexture('Common', 'Pants');

    // Helmet & visor
    MultiSkins[6] = PinkMask;
    MultiSkins[7] = PinkMask;
    Texture = PinkMask;
}

// END UE1

defaultproperties
{
    Mesh=LodMesh'DeusExCharacters.GM_DressShirt'
    CollisionHeight=47.500000
    HitSound1=Sound'DeusExSounds.Player.MalePainSmall'
    HitSound2=Sound'DeusExSounds.Player.MalePainMedium'
    Die=Sound'DeusExSounds.Player.MaleDeath'
    WalkingSpeed=0.320000
    BaseAssHeight=-23.000000
    WalkingSpeed=0.320000
    WalkAnimMult=0.650000
    BindName="RandomMale"
    FamiliarName="Man"
    UnfamiliarName="Man"
}
