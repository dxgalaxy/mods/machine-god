// ========================================
// ModernHUDLogDisplay
//
// Changes:
// - New font
// - Right alignment
// - Window based on text size
// - Killed duplicates
// - Axed icon
// ========================================
class ModernHUDLogDisplay extends HUDLogDisplay;

var ModernRootWindow Root;
var ModernHUD HUD;
var string LastLogMessageString;

// BEGIN UE1

// ----------------------
// Creates the log window
// ----------------------
function CreateControls()
{
    WinLog = TextLogWindow(NewChild(class'TextLogWindow'));
    WinLog.SetTextAlignments(HALIGN_Right, VALIGN_Top);
    WinLog.SetTextMargins(0, 0);
    WinLog.SetFont(Font'FontLocation');
    WinLog.SetLines(MinLogLines, MaxLogLines);
}

// ------------------------
// Positions the log window
// ------------------------
function ConfigurationChanged()
{
    local float W, H;

    WinLog.QueryPreferredSize(W, H);
    WinLog.ConfigureChild(0, 0, W, H);
}

// ----------------------------------------------------------
// Makes this window the same size as the log window + margin
// ----------------------------------------------------------
event ParentRequestedPreferredSize(bool bWidthSpecified, out float PreferredWidth, bool bHeightSpecified, out float PreferredHeight)
{
    WinLog.QueryPreferredSize(PreferredWidth, PreferredHeight);
}

// -----------------------------------------
// Adds a log message, skipping if duplicate
// -----------------------------------------
function AddLog(coerce string NewLog, Color LineCol)
{
    if(
        NewLog == "" ||
        (
            NewLog == LastLogMessageString &&
            LastLogMsg < DisplayTime 
        )
    )
        return;
    
    if(Root == None)
        Root = ModernRootWindow(GetRootWindow());
    
    if(HUD == None)
        HUD = ModernHUD(Root.HUD);

    LastLogMessageString = NewLog;

    Super.AddLog(NewLog, HUD.ColText);
}

// -----------------
// Stubbed functions
// -----------------
function SetIcon(Texture NewIcon) {}
function DrawWindow(GC gc){}

// END UE1

defaultproperties
{
    DisplayTime=3.000000
    MinLogLines=1
    MaxLogLines=10
}
