class DynamicTextureController extends Info;

var() Texture ScriptedTexture, DisplayTexture;
var() float U, V, X, Y, UL, VL, XL, YL;
var() bool bMasked;

simulated function BeginPlay()
{
    if(ScriptedTexture != None)
        ScriptedTexture(ScriptedTexture).NotifyActor = Self;
}

simulated function Destroyed()
{
    if(ScriptedTexture != None)
        ScriptedTexture(ScriptedTexture).NotifyActor = None;
}

simulated event RenderTexture(ScriptedTexture Tex)
{
    Tex.DrawTile(X, Y, XL, YL, U, V, UL, VL, DisplayTexture, bMasked);
}

defaultproperties
{
    bAlwaysRelevant=True
    bNoDelete=True
    bStatic=False
    RemoteRole=ROLE_SimulatedProxy
}
