class Iris extends ScriptedPawnFemale;

// Voice: ElevenLabs Jenn

#exec TEXTURE IMPORT NAME=Iris_Head FILE=Textures\Skins\Iris_Head.pcx GROUP=Skins FLAGS=2
#exec TEXTURE IMPORT NAME=Iris_Torso FILE=Textures\Skins\Iris_Torso.pcx GROUP=Skins

defaultproperties
{
    ArcheType=AT_Military
    BindName="Iris"
    FamiliarName="Iris"
    GroundSpeed=200.000000
    Mesh=LodMesh'DeusExCharacters.GFM_TShirtPants'
    MultiSkins(0)=Texture'MachineGod.Skins.Iris_Head'
    MultiSkins(1)=Texture'MachineGod.Skins.Iris_Head'
    MultiSkins(2)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(3)=Texture'DeusExItems.Skins.GrayMaskTex'
    MultiSkins(4)=Texture'DeusExItems.Skins.BlackMaskTex'
    MultiSkins(5)=None
    MultiSkins(6)=Texture'MachineGod.Skins.TacticalPants4'
    MultiSkins(7)=Texture'MachineGod.Skins.Iris_Torso'
    UnfamiliarName="Some badass"
    bImportant=True
    bInvincible=True
    CombatStyle=1
    bAimForHead=True
    BaseAccuracy=-0.1
    CollisionHeight=46
    DrawScale=1.08
}
