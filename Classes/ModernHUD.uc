class ModernHUD extends DeusExHUD;

var DeusExRootWindow Root;
var DeusExPlayer Player;
var bool bMinimal;
var bool bDrawBorder;
var EDrawStyle BorderDrawStyle;
var EDrawStyle BackgroundDrawStyle;
var Texture TexShadows[9], TexHeavyBackgrounds[9], TexHeavyBorders[9];
var Color ColBlack, ColWhite, ColBackground, ColBorder, ColText, ColTextShadow, ColHeaderText;

// BEGIN UE1

event InitWindow()
{
    // Get a pointer to the root window
    Root = DeusExRootWindow(GetRootWindow());

    // Get a pointer to the player
    Player = DeusExPlayer(Root.ParentPawn);

    SetFont(Font'FontBody');
    SetSensitivity(False);

    Ammo            = HUDAmmoDisplay(NewChild(Class'HUDAmmoDisplay'));
    Hit             = HUDHitDisplay(NewChild(Class'HUDHitDisplay'));
    Cross           = Crosshair(NewChild(Class'Crosshair'));
    Belt            = HUDObjectBelt(NewChild(Class'HUDObjectBelt'));
    ActiveItems     = HUDActiveItemsDisplay(NewChild(Class'HUDActiveItemsDisplay'));
    DamageDisplay   = DamageHUDDisplay(NewChild(Class'DamageHUDDisplay'));
    Compass         = ModernHUDCompassDisplay(NewChild(Class'ModernHUDCompassDisplay'));
    HMS             = HUDMultiSkills(NewChild(Class'HUDMultiSkills'));

    // Create the InformationWindow
    Info = ModernHUDInformationDisplay(NewChild(Class'ModernHUDInformationDisplay', False));

    // Create the log window
    MsgLog = ModernHUDLogDisplay(NewChild(Class'ModernHUDLogDisplay', False));
    MsgLog.SetLogTimeout(player.GetLogTimeout());

    FrobDisplay = ModernFrobDisplayWindow(NewChild(Class'ModernFrobDisplayWindow'));
    FrobDisplay.SetWindowAlignments(HALIGN_Full, VALIGN_Full);

    AugDisplay  = AugmentationDisplayWindow(NewChild(Class'AugmentationDisplayWindow'));
    AugDisplay.SetWindowAlignments(HALIGN_Full, VALIGN_Full);

    StartDisplay = HUDMissionStartTextDisplay(NewChild(Class'HUDMissionStartTextDisplay', False));

    // Bark display
    BarkDisplay = ModernHUDBarkDisplay(NewChild(Class'ModernHUDBarkDisplay', False));

    // Received Items Display
    ReceivedItems = HUDReceivedDisplay(NewChild(Class'HUDReceivedDisplay', False));

    StyleChanged();
}

function UpdateSettings(DeusExPlayer aPlayer)
{
    if(bMinimal)
    {
        Belt.SetVisibility(False);
        Hit.SetVisibility(False);
        Ammo.SetVisibility(False);
        ActiveItems.SetVisibility(False);
        DamageDisplay.SetVisibility(False);
        Compass.SetVisibility(False);
        Cross.SetCrosshair(False);
    }
    else
    {
        Belt.SetVisibility(aPlayer.bObjectBeltVisible);
        Hit.SetVisibility(aPlayer.bHitDisplayVisible);
        Ammo.SetVisibility(aPlayer.bAmmoDisplayVisible);
        ActiveItems.SetVisibility(aPlayer.bAugDisplayVisible);
        DamageDisplay.SetVisibility(aPlayer.bHitDisplayVisible);
        Compass.SetVisibility(aPlayer.bCompassVisible);
        Cross.SetCrosshair(aPlayer.bCrosshairVisible);
    }
}

function SetMinimal(bool bOn)
{
    bMinimal = bOn;

    UpdateSettings(Player);
}

function HUDConWindowFirst CreateConWindowFirst()
{
    local DeusExRootWindow Root;

    Root = DeusExRootWindow(GetRootWindow());

    ConWindow = ModernHUDConWindowFirst(NewChild(Class'ModernHUDConWindowFirst', False));
    ConWindow.AskParentForReconfigure();

    return ConWindow;
}

function HUDInfoLinkDisplay CreateInfoLinkWindow()
{
    if(InfoLink != None)
        return None;

    InfoLink = ModernHUDInfoLinkDisplay(NewChild(class'ModernHUDInfoLinkDisplay'));

    if(MsgLog != None)
        MsgLog.Hide();

    InfoLink.AskParentForReconfigure();

    return InfoLink;
}

function DestroyInfoLinkWindow()
{
    Super.DestroyInfoLinkWindow();

    if(ConWindow != None)
        ConWindow.Destroy();
}

function ConfigurationChanged()
{
    local float qWidth, qHeight;
    local float compassWidth, compassHeight;
    local float beltWidth, beltHeight;
    local float ammoWidth, ammoHeight;
    local float hitWidth, hitHeight;
    local float infoX, infoY, infoTop, infoBottom;
    local float infoWidth, infoHeight, maxInfoWidth, maxInfoHeight;
    local float itemsWidth, itemsHeight;
    local float damageWidth, damageHeight;
    local float conHeight;
    local float barkHeight;
    local float recWidth, recHeight, recPosY;

    if (ammo != None)
    {
        if (ammo.IsVisible())
        {
            ammo.QueryPreferredSize(ammoWidth, ammoHeight);
            ammo.ConfigureChild(width - ammoWidth, height-ammoHeight, ammoWidth, ammoHeight);
        }
        else
        {
            ammoWidth  = 0;
            ammoHeight = 0;
        }
    }

    // Hit display: Top left
    if(Hit != None && Hit.IsVisible())
    {
        Hit.QueryPreferredSize(HitWidth, HitHeight);
        Hit.ConfigureChild(0, 0, HitWidth, HitHeight);
    }
    
    // Damage display: Left side, under the hit display
    if(DamageDisplay != None)
    {
        DamageDisplay.QueryPreferredSize(DamageWidth, DamageHeight);
        DamageDisplay.ConfigureChild(0, HitHeight, DamageWidth, DamageHeight);
    }

    // Log: Just below the middle right
    if(MsgLog != None)
    {
        MsgLog.QueryPreferredSize(qWidth, qHeight);
        MsgLog.ConfigureChild(Width - qWidth - 40, (Height / 2) + 100, qWidth, qHeight);
    }
    
    // Crosshair: Center
    if(Cross != None)
    {
        Cross.QueryPreferredSize(qWidth, qHeight);
        Cross.ConfigureChild((width-qWidth)*0.5+0.5, (height-qHeight)*0.5+0.5, qWidth, qHeight);
    }

    // Belt: Bottom center
    if(Belt != None)
    {
        Belt.QueryPreferredSize(beltWidth, beltHeight);
        Belt.ConfigureChild(width / 2 - beltWidth / 2, height - beltHeight, beltWidth, beltHeight);

        InfoBottom = height - beltHeight;
    }
    else
    {
        InfoBottom = height;
    }
    
    // Compass: Top center
    if(Compass != None)
    {
        Compass.QueryPreferredSize(CompassWidth, CompassHeight);
        Compass.ConfigureChild((Width / 2) - (CompassWidth / 2), 0, CompassWidth, CompassHeight);
    }
    
    // Active Items/augmentations: Top right
    if(ActiveItems != None)
    {
        ActiveItems.QueryPreferredSize(itemsWidth, itemsHeight);
        ActiveItems.ConfigureChild(Width - ItemsWidth, 0, ItemsWidth, ItemsHeight);
    }

    // InfoLink: Top right
    if (infolink != None)
    {
        infolink.QueryPreferredSize(qWidth, qHeight);

        infolink.ConfigureChild(width - qWidth - 20, 20, qWidth, qHeight);
    }

    // First-person conversation window

    if (conWindow != None)
    {
        conWindow.QueryPreferredSize(qWidth, ConHeight);

        // Stick it above the belt
        conWindow.ConfigureChild(
            (width / 2) - (qwidth / 2), (infoBottom - conHeight) - 20, 
            qWidth, conHeight);
    }

    // Bark Display.  Position where first-person convo window would
    // go, or above it if the first-person convo is visible
    if (barkDisplay != None)
    {
        qWidth = Min(width - 100, 800);
        barkHeight = barkDisplay.QueryPreferredHeight(qWidth);

        barkDisplay.ConfigureChild(
            (width / 2) - (qwidth / 2), (infoBottom - barkHeight - conHeight) - 20, 
            qWidth, barkHeight);
    }
    

    // Received Items display
    // 
    // Stick below the crosshair, but above any bark/convo windows that might 
    // be visible.

    if (receivedItems != None)
    {
        receivedItems.QueryPreferredSize(recWidth, recHeight);

        recPosY = (height / 2) + 20;

        if ((barkDisplay != None) && (barkDisplay.IsVisible()))
            recPosY -= barkHeight;
        if ((conWindow != None) && (conWindow.IsVisible()))
            recPosY -= conHeight;

        receivedItems.ConfigureChild(
            (width / 2) - (recWidth / 2), recPosY,
            recWidth, recHeight);
    }

    // Display the timer above the object belt if it's visible

    if (timer != None)
    {
        timer.QueryPreferredSize(qWidth, qHeight);

        if ((belt != None) && (belt.IsVisible()))
            timer.ConfigureChild(width-qWidth, height-qHeight-beltHeight-10, qWidth, qHeight);
        else
            timer.ConfigureChild(width-qWidth, height-qHeight, qWidth, qHeight);
    }

    // Mission Start Text
    if (startDisplay != None)
    {
        // Stick this baby right in the middle of the screen.
        startDisplay.QueryPreferredSize(qWidth, qHeight);
        startDisplay.ConfigureChild(
            (width / 2) - (qWidth / 2), (height / 2) - (qHeight / 2) - 75,
            qWidth, qHeight);
    }

    // Display the Info Window sandwiched between all the other windows.  :)

    if ((info != None) && (info.IsVisible(False)))
    {
        // Must redo these formulas
        maxInfoWidth  = Min(width - 170, 800);
        maxInfoHeight = (infoBottom - infoTop) - 20;

        info.QueryPreferredSize(infoWidth, infoHeight);

        if (infoWidth > maxInfoWidth)
        {
            infoHeight = info.QueryPreferredHeight(maxInfoWidth);
            infoWidth  = maxInfoWidth;
        }

        infoX = (width / 2) - (infoWidth / 2);
        infoY = infoTop + (((infoBottom - infoTop) / 2) - (infoHeight / 2)) + 10;

        info.ConfigureChild(infoX, infoY, infoWidth, infoHeight);
    }

    StyleChanged();
}

event StyleChanged()
{
    local ColorTheme Theme;
    
    Theme = Player.ThemeManager.GetCurrentHUDColorTheme();

    ColBackground = Theme.GetColorFromName('HUDColor_Background');
    ColBorder = Theme.GetColorFromName('HUDColor_Borders');
    ColText = Theme.GetColorFromName('HUDColor_NormalText');
    ColHeaderText = Theme.GetColorFromName('HUDColor_HeaderText');
    ColTextShadow = ColBlack;

    bDrawBorder = Player.GetHUDBordersVisible();

    if(Player.GetHUDBorderTranslucency())
        BorderDrawStyle = DSTY_Translucent;
    
    else
        BorderDrawStyle = DSTY_Masked;

    if(Player.GetHUDBackgroundTranslucency())
        BackgroundDrawStyle = DSTY_Translucent;
    else
        BackgroundDrawStyle = DSTY_Masked;
}

// --------------
// Draws a shadow
// --------------
function DrawShadow(GC gc, float X, float Y, float W, float H, float Opacity, float Blur)
{
    local int SrcMargin, SrcCornerMargin, DstMargin;

    gc.SetStyle(DSTY_Modulated);

    SrcMargin = Opacity * 32;
    DstMargin = Blur * 16;

    // Offset the corners
    // TODO: Turn this into a formula
    if(SrcMargin <= 4)
        SrcCornerMargin = 5;
    
    else if(SrcMargin <= 8)
        SrcCornerMargin = 3;
    
    else if(SrcMargin <= 16)
        SrcCornerMargin = 1;

    // This bananas sorcery is just slicing up the shadow background textures to simulate opacity control
    // Parameters:              DestX                   DestY               DestWidth           DestHeight          SrcX                                    SrcY                                SrcWidth        SrcHeight
    
    // Top left
    gc.DrawStretchedTexture(    X,                      Y,                  DstMargin,          DstMargin,          SrcCornerMargin,                        SrcCornerMargin,                    SrcMargin,      SrcMargin,      Texture'WindowShadow_TL');
    
    // Top
    gc.DrawStretchedTexture(    X + DstMargin,          Y,                  W - DstMargin * 2,  DstMargin,          0,                                      0,                                  1,              SrcMargin,      Texture'WindowShadow_Top');
    
    // Top right
    gc.DrawStretchedTexture(    X + W - DstMargin,      Y,                  DstMargin,          DstMargin,          32 - SrcMargin - SrcCornerMargin,       SrcCornerMargin,                    SrcMargin,      SrcMargin,      Texture'WindowShadow_TR');
    
    // Right
    gc.DrawStretchedTexture(    X + W - DstMargin,      Y + DstMargin,      DstMargin,          H - DstMargin * 2,  32 - SrcMargin,                         0,                                  SrcMargin,      1,              Texture'WindowShadow_Right');

    // Bottom right
    gc.DrawStretchedTexture(    X + W - DstMargin,      Y + H - DstMargin,  DstMargin,          DstMargin,          32 - SrcMargin - SrcCornerMargin,       32 - SrcMargin - SrcCornerMargin,   SrcMargin,      SrcMargin,      Texture'WindowShadow_BR');

    // Bottom
    gc.DrawStretchedTexture(    X + DstMargin,          Y + H - DstMargin,  W - DstMargin * 2,  DstMargin,          0,                                      32 - SrcMargin,                     1,              SrcMargin,      Texture'WindowShadow_Bottom');
    
    // Bottom left
    gc.DrawStretchedTexture(    X,                      Y + H - DstMargin,  DstMargin,          DstMargin,          SrcCornerMargin,                        32 - SrcMargin - SrcCornerMargin,   SrcMargin,      SrcMargin,      Texture'WindowShadow_BL');
    
    // Left
    gc.DrawStretchedTexture(    X,                      Y + DstMargin,      DstMargin,          H - DstMargin * 2,  0,                                      0,                                  SrcMargin,      1,              Texture'WindowShadow_Left');
    
    // Center 
    gc.DrawStretchedTexture(    X + DstMargin,          Y + DstMargin,      W - DstMargin * 2,  H - DstMargin * 2,  0,                                      32 - SrcMargin,                     1,              1,              Texture'WindowShadow_Bottom');
    
    gc.SetStyle(DSTY_Normal);
}

// ----------------------
// Draws a "heavy" dialog
// ----------------------
function DrawHeavyDialog(GC gc, float X, float Y, float W, float H, optional bool bMargins)
{ 
    gc.SetStyle(DSTY_Modulated);

    if(bMargins)
        gc.DrawBorders(X - 15, Y - 6, W + 29, H + 12, 0, 0, 0, 0, TexShadows);
    
    else
        gc.DrawBorders(X, Y, W, H, 0, 0, 0, 0, TexShadows);
    
    gc.SetStyle(BackgroundDrawStyle);
    gc.SetTileColor(ColBackground);

    if(bMargins)
        gc.DrawBorders(X - 16, Y - 14, W + 26, H + 26, 0, 0, 0, 0, TexHeavyBackgrounds);

    else
        gc.DrawBorders(X, Y, W, H, 0, 0, 0, 0, TexHeavyBackgrounds);
    
    if(bDrawBorder)
    {
        gc.SetStyle(BorderDrawStyle);
        gc.SetTileColor(ColBorder);
        
        if(bMargins)
            gc.DrawBorders(X - 16, Y - 14, W + 26, H + 26, 0, 0, 0, 0, TexHeavyBorders);

        else
            gc.DrawBorders(X, Y, W, H, 0, 0, 0, 0, TexHeavyBorders);
    } 
    
    gc.SetStyle(DSTY_Normal);
}

// ----------------------
// Draws a "light" dialog
// ----------------------
function DrawLightDialog(GC gc, float X, float Y, float W, float H, optional float HPadding, optional float VPadding)
{
    local float ShadowX, ShadowY, ShadowWidth, ShadowHeight;

    if(HPadding > 0)
    {
        X -= HPadding;
        W += HPadding * 2;
    }

    if(VPadding > 0)
    {
        Y -= VPadding;
        H += VPadding * 2;
    }
    
    gc.SetStyle(DSTY_Modulated);

    ShadowX = X - 6;
    ShadowY = Y - 6;
    ShadowWidth = W + 12;
    ShadowHeight = H + 12;

    if(ShadowWidth < 64)
    {
        ShadowX = X;
        ShadowWidth = W;
    }

    if(ShadowHeight < 64)
    {
        ShadowY = Y;
        ShadowHeight = H;
    }

    gc.DrawBorders(ShadowX, ShadowY, ShadowWidth, ShadowHeight, 0, 0, 0, 0, TexShadows);

    gc.SetStyle(BackgroundDrawStyle);
    gc.SetTileColor(ColBackground);
    
    // Top left
    gc.DrawTexture(X, Y, 4, 4, 0, 0, Texture'HUDKeypadButton_Normal');
    
    // Top
    gc.DrawStretchedTexture(X + 4, Y, W - 4 - 4, 4, 4, 0, 4, 4, Texture'HUDKeypadButton_Normal');
    
    // Top right
    gc.DrawTexture(X + W - 4, Y, 4, 4, 21, 0, Texture'HUDKeypadButton_Normal');
    
    // Right
    gc.DrawStretchedTexture(X + W - 4, Y + 4, 4, H - 4 - 4, 21, 4, 4, 4, Texture'HUDKeypadButton_Normal');
    
    // Bottom right
    gc.DrawTexture(X + W - 4, Y + H - 4, 4, 4, 21, 23, Texture'HUDKeypadButton_Normal');

    // Bottom
    gc.DrawStretchedTexture(X + 4, Y + H - 4, W - 4 - 4, 4, 4, 23, 4, 4, Texture'HUDKeypadButton_Normal');
    
    // Bottom left
    gc.DrawTexture(X, Y + H - 4, 4, 4, 0, 23, Texture'HUDKeypadButton_Normal');
    
    // Left
    gc.DrawStretchedTexture(X, Y + 4, 4, H - 4 - 4, 0, 4, 4, 4, Texture'HUDKeypadButton_Normal');
    
    // Center 
    gc.DrawStretchedTexture(X + 4, Y + 4, W - 4 - 4, H - 4 - 4, 4, 4, 4, 4, Texture'HUDKeypadButton_Normal');
}

// END UE1

defaultproperties
{
    ColBlack=(R=0,G=0,B=0)
    ColWhite=(R=255,G=255,B=255)
    TexShadows(0)=Texture'DeusExUI.UserInterface.WindowShadow_TL'
    TexShadows(1)=Texture'DeusExUI.UserInterface.WindowShadow_TR'
    TexShadows(2)=Texture'DeusExUI.UserInterface.WindowShadow_BL'
    TexShadows(3)=Texture'DeusExUI.UserInterface.WindowShadow_BR'
    TexShadows(4)=Texture'DeusExUI.UserInterface.WindowShadow_Left'
    TexShadows(5)=Texture'DeusExUI.UserInterface.WindowShadow_Right'
    TexShadows(6)=Texture'DeusExUI.UserInterface.WindowShadow_Top'
    TexShadows(7)=Texture'DeusExUI.UserInterface.WindowShadow_Bottom'
    TexShadows(8)=Texture'DeusExUI.UserInterface.WindowShadow_Center'
    TexHeavyBackgrounds(0)=Texture'DeusExUI.UserInterface.HUDWindowBackground_TL'
    TexHeavyBackgrounds(1)=Texture'DeusExUI.UserInterface.HUDWindowBackground_TR'
    TexHeavyBackgrounds(2)=Texture'DeusExUI.UserInterface.HUDWindowBackground_BL'
    TexHeavyBackgrounds(3)=Texture'DeusExUI.UserInterface.HUDWindowBackground_BR'
    TexHeavyBackgrounds(4)=Texture'DeusExUI.UserInterface.HUDWindowBackground_Left'
    TexHeavyBackgrounds(5)=Texture'DeusExUI.UserInterface.HUDWindowBackground_Right'
    TexHeavyBackgrounds(6)=Texture'DeusExUI.UserInterface.HUDWindowBackground_Top'
    TexHeavyBackgrounds(7)=Texture'DeusExUI.UserInterface.HUDWindowBackground_Bottom'
    TexHeavyBackgrounds(8)=Texture'DeusExUI.UserInterface.HUDWindowBackground_Center'
    TexHeavyBorders(0)=Texture'DeusExUI.UserInterface.HUDWindowBorder_TL'
    TexHeavyBorders(1)=Texture'DeusExUI.UserInterface.HUDWindowBorder_TR'
    TexHeavyBorders(2)=Texture'DeusExUI.UserInterface.HUDWindowBorder_BL'
    TexHeavyBorders(3)=Texture'DeusExUI.UserInterface.HUDWindowBorder_BR'
    TexHeavyBorders(4)=Texture'DeusExUI.UserInterface.HUDWindowBorder_Left'
    TexHeavyBorders(5)=Texture'DeusExUI.UserInterface.HUDWindowBorder_Right'
    TexHeavyBorders(6)=Texture'DeusExUI.UserInterface.HUDWindowBorder_Top'
    TexHeavyBorders(7)=Texture'DeusExUI.UserInterface.HUDWindowBorder_Bottom'
    TexHeavyBorders(8)=Texture'DeusExUI.UserInterface.HUDWindowBorder_Center'
}
