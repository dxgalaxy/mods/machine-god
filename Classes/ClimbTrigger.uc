//=============================================================================
// ClimbTrigger
// ---
// Enables the player to climb up walls
//=============================================================================
class ClimbTrigger extends Trigger;

// BEGIN UE1

function Touch(Actor aOther)
{
    if(!IsRelevant(aOther) || DeusExPlayer(aOther) == None)
        return;
    
    DeusExPlayer(aOther).JumpZ = 600;
}

function UnTouch(Actor aOther)
{
    if(!IsRelevant(aOther) || DeusExPlayer(aOther) == None)
        return;

    DeusExPlayer(aOther).JumpZ = DeusExPlayer(aOther).Default.JumpZ;
}

// END UE1
