class HeliosAgentMale extends ScriptedPawnMale;

#exec TEXTURE IMPORT NAME=HeliosAgentMale_Head FILE=Textures\Skins\HeliosAgentMale_Head.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=HeliosAgentMale_Torso FILE=Textures\Skins\HeliosAgentMale_Torso.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=HeliosAgentMale_Legs FILE=Textures\Skins\HeliosAgentMale_Legs.pcx GROUP=Skins

defaultproperties
{
    BindName="AugmentedAgent"
    CloseCombatMult=0.500000
    CollisionHeight=52.250000
    DrawScale=1.100000
    FamiliarName="Augmented Agent"
    GroundSpeed=180.000000
    Health=350
    HealthArmLeft=350
    HealthArmRight=350
    HealthHead=350
    HealthLegLeft=350
    HealthLegRight=350
    HealthTorso=350
    InitialInventory(0)=(Count=1,Inventory=class'DeusEx.WeaponMiniCrossbow')
    InitialInventory(1)=(Count=100,Inventory=class'DeusEx.AmmoDartPoison')
    Mesh=LodMesh'DeusExCharacters.GM_Suit'
    Mesh=LodMesh'DeusExCharacters.GM_Suit'
    MinHealth=0.000000
    MultiSkins(0)=Texture'MachineGod.Skins.HeliosAgentMale_Head'
    MultiSkins(1)=Texture'MachineGod.Skins.HeliosAgentMale_Legs'
    MultiSkins(2)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(3)=Texture'MachineGod.Skins.HeliosAgentMale_Torso'
    MultiSkins(4)=Texture'MachineGod.Skins.HeliosAgentMale_Torso'
    MultiSkins(5)=Texture'DeusExItems.Skins.GrayMaskTex'
    MultiSkins(6)=Texture'DeusExItems.Skins.BlackMaskTex'
    MultiSkins(7)=Texture'DeusExItems.Skins.PinkMaskTex'
    UnfamiliarName="Augmented Agent"
    WalkingSpeed=0.213333
    bExplodeOnDeath=True
}
