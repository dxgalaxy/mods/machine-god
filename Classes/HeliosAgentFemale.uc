class HeliosAgentFemale extends ScriptedPawnFemale;

#exec TEXTURE IMPORT NAME=HeliosAgentFemale_Head FILE=Textures\Skins\HeliosAgentFemale_Head.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=HeliosAgentFemale_Coat FILE=Textures\Skins\HeliosAgentFemale_Coat.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=HeliosAgentFemale_ShirtFront FILE=Textures\Skins\HeliosAgentFemale_ShirtFront.pcx GROUP=Skins
#exec TEXTURE IMPORT NAME=HeliosAgentFemale_Legs FILE=Textures\Skins\HeliosAgentFemale_Legs.pcx GROUP=Skins

defaultproperties
{
    BaseAssHeight=-18.000000
    BindName="AugmentedAgent"
    CloseCombatMult=0.500000
    CollisionHeight=47.299999
    DrawScale=1.100000
    FamiliarName="Augmented Agent"
    GroundSpeed=200.000000
    Health=300
    HealthArmLeft=300
    HealthArmRight=300
    HealthHead=300
    HealthLegLeft=300
    HealthLegRight=300
    HealthTorso=300
    InitialInventory(0)=(Count=1,Inventory=class'DeusEx.WeaponMiniCrossbow')
    InitialInventory(1)=(Count=100,Inventory=class'DeusEx.AmmoDartPoison')
    Mesh=LodMesh'DeusExCharacters.GFM_Trench'
    MinHealth=0.000000
    MultiSkins(0)=Texture'MachineGod.Skins.HeliosAgentFemale_Head'
    MultiSkins(1)=Texture'MachineGod.Skins.HeliosAgentFemale_Coat'
    MultiSkins(2)=Texture'MachineGod.Skins.HeliosAgentFemale_Legs'
    MultiSkins(3)=Texture'DeusExItems.Skins.PinkMaskTex'
    MultiSkins(4)=Texture'MachineGod.Skins.HeliosAgentFemale_ShirtFront'
    MultiSkins(5)=Texture'MachineGod.Skins.HeliosAgentFemale_Coat'
    MultiSkins(6)=Texture'DeusExItems.Skins.GrayMaskTex'
    MultiSkins(7)=Texture'DeusExItems.Skins.BlackMaskTex'
    UnfamiliarName="Augmented Agent"
    WalkAnimMult=0.870000
    WalkingSpeed=0.296000
    bIsFemale=True
    bExplodeOnDeath=True
}
