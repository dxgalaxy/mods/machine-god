class ModernFrobDisplayWindow extends FrobDisplayWindow;
    
// BEGIN UE1

var ModernRootWindow Root;
var ModernHUD HUD;

// ------------------------------------
// Gets the box coordinates of an actor
// ------------------------------------
function GetActorExtents(
    Actor aActor,
    out float TopLeftX,
    out float TopLeftY,
    out float BottomRightX,
    out float BottomRightY
)
{
    local Mover aMover;
    local Vector Min, Max, Corners[8];
    local float X, Y, Radius;
    local int i;

    // If it's a mover, get the bounding box
    aMover = Mover(aActor);

    if(aMover != None)
    {
        aMover.GetBoundingBox(Min, Max, False, aMover.KeyPos[aMover.KeyNum] + aMover.BasePos, aMover.KeyRot[aMover.KeyNum] + aMover.BaseRot);
    }

    // For any other actor, use the collision cylinder
    else
    {
        Radius = aActor.CollisionRadius;

        // ScriptedPawns have a padded radius for local obstacle avoidance
        if(aActor.IsA('ScriptedPawn'))
            Radius = Radius / 2;

        Min = aActor.Location - aActor.CollisionHeight * Vect(0, 0, 1) - Radius * Vect(1, 1, 0);
        Max = aActor.Location + aActor.CollisionHeight * Vect(0, 0, 1) + Radius * Vect(1, 1, 0);
    }

    // Top 1
    Corners[0] = Max;
    
    // Top 2
    Corners[1] = Max;
    Corners[1].X = Min.X;
    
    // Top 3
    Corners[2] = Max;
    Corners[2].X = Min.X;
    Corners[2].Y = Min.Y;
    
    // Top 4
    Corners[3] = Max;
    Corners[3].Y = Min.Y;
    
    // Bottom 1
    Corners[4] = Min;
    
    // Bottom 2
    Corners[5] = Min;
    Corners[5].X = Max.X;
    
    // Bottom 3
    Corners[6] = Min;
    Corners[6].X = Max.X;
    Corners[6].Y = Max.Y;
    
    // Bottom 4
    Corners[7] = Min;
    Corners[7].Y = Max.Y;

    // Convert 3D corners to screen coordinates
    for(i = 0; i < ArrayCount(Corners); i++)
    {
        ConvertVectorToCoordinates(Corners[i], X, Y);
       
        if(TopLeftX == 0 || X < TopLeftX)
            TopLeftX = X;
        
        if(TopLeftY == 0 || Y < TopLeftY)
            TopLeftY = Y;
        
        if(BottomRightX == 0 || X > BottomRightX)
            BottomRightX = X;
        
        if(BottomRightY == 0 || Y > BottomRightY)
            BottomRightY = Y;
    }
}

// -------------------------------------------
// Draws the window
//
// Changes:
// - Display custom mover names
// - Bigger font
// - Softer pulse for the bounding box
// - More padding for the text box
// - More accurate bounding box
// - Icons instead of text wherever applicable
// - Fixed position info box
// -------------------------------------------
function DrawWindow(GC gc)
{
    local Actor             aFrobTarget;
    local float             infoX, infoY, infoW, infoH;
    local string            strInfo;
    local DeusExMover       aDeusExMover;
    local Mover             aMover;
    local HackableDevices   aHackableDevice;
    local Vector            centerLoc, v1, v2;
    local float             boxCX, boxCY, boxTLX, boxTLY, boxBRX, boxBRY, boxW, boxH;
    local float             corner, x, y;
    local int               i, j, k, offset;
    local Color             col;
    local int               numTools;

    if(Player == None)
        return;

    if(Root == None)
        Root = ModernRootWindow(GetRootWindow());
    
    if(HUD == None)
        HUD = ModernHUD(Root.HUD);
        
    aFrobTarget = Player.FrobTarget;
    aDeusExMover = DeusExMover(aFrobTarget);
    aHackableDevice = HackableDevices(aFrobTarget);

    // Stop if there is no frob target, it's not highlighted, or it's an interpolating mover
    if(
        aFrobTarget == None ||
        !Player.IsHighlighted(aFrobTarget) ||
        (aDeusExMover != None && aDeusExMover.bInterpolating)
    )
        return;

    // Move the box in and out based on time
    Offset = 12.0 * ((1.0 + Sin(aFrobTarget.Level.TimeSeconds * 8)) / 2);
    
    // Get the extents
    GetActorExtents(aFrobTarget, boxTLX, boxTLY, boxBRX, boxBRY);

    BoxTLX = FClamp(BoxTLX, Margin, Width - Margin);
    BoxTLY = FClamp(BoxTLY, Margin, Height - Margin);
    BoxBRX = FClamp(BoxBRX, Margin, Width - Margin);
    BoxBRY = FClamp(BoxBRY, Margin, Height - Margin);

    BoxW = BoxBRX - BoxTLX;
    BoxH = BoxBRY - BoxTLY;

    // Scale the corner based on the size of the box
    Corner = FClamp((BoxW + BoxH) * 0.1, 4.0, 40.0);

    // Make sure the box doesn't invert itself
    if(BoxBRX - BoxTLX < Corner)
    {
        BoxTLX -= (Corner + 4);
        BoxBRX += (Corner + 4);
    }

    if(BoxBRY - BoxTLY < Corner)
    {
        BoxTLY -= Corner + 4;
        BoxBRY += Corner + 4;
    }

    // Draw the box
    gc.SetStyle(DSTY_Translucent);
   
    gc.SetTileColor(ColBackground);
    
    gc.DrawBox(BoxTLX - Offset, BoxTLY - Offset, Corner, 1, 0, 0, 1, Texture'Solid');
    gc.DrawBox(BoxTLX - Offset, BoxTLY - Offset, 1, Corner, 0, 0, 1, Texture'Solid');

    gc.DrawBox(BoxBRX - Corner + Offset, BoxTLY - Offset, Corner, 1, 0, 0, 1, Texture'Solid');
    gc.DrawBox(BoxBRX + Offset, BoxTLY - Offset, 1, Corner, 0, 0, 1, Texture'Solid');

    gc.DrawBox(BoxTLX - Offset, BoxBRY + Offset, Corner, 1, 0, 0, 1, Texture'Solid');
    gc.DrawBox(BoxTLX - Offset, BoxBRY - Corner + Offset, 1, Corner, 0, 0, 1, Texture'Solid');

    gc.DrawBox(BoxBRX - Corner + 1 + Offset, BoxBRY + Offset, Corner, 1, 0, 0, 1, Texture'Solid');
    gc.DrawBox(BoxBRX + Offset, BoxBRY - Corner + Offset, 1, Corner, 0, 0, 1, Texture'Solid');

    gc.SetStyle(DSTY_Normal);

    // Mover info
    if(aDeusExMover != None)
    {
        // Get the name
        if(aDeusExMover.FamiliarName != "")
        {
            strInfo = aDeusExMover.FamiliarName;
        }
        else if(aDeusExMover.UnfamiliarName != "")
        {
            strInfo = aDeusExMover.UnfamiliarName;
        }
        else
        {
            strInfo = msgUnlocked;
        }

        gc.SetFont(Font'FontLocation');
        StrInfo = Caps(StrInfo);
        gc.GetTextExtent(0, InfoW, InfoH, StrInfo);

        if(aDeusExMover.bLocked)
            InfoW = Max(InfoW + 32, 192);

        InfoX = BoxTLX + BoxW + 32;
        InfoY = BoxTLY + 32;

        InfoX = Min(InfoX, Width * 0.8 - InfoW);
        InfoY = Max(InfoY, Height * 0.2); 
        
        if(aDeusExMover.bLocked)
        {
            // Draw a container
            HUD.DrawLightDialog(gc, InfoX, InfoY, InfoW, InfoH + 64, 12, 12);
        
            // Draw bars for each value
            gc.SetFont(Font'FontMenuSmall');
            gc.SetTextColor(HUD.ColHeaderText);
            gc.SetHorizontalAlignment(HALIGN_Center);
            gc.SetVerticalAlignment(VALIGN_Center);
            
            // Lock strength
            gc.SetStyle(DSTY_Masked);
            gc.SetTileColor(HUD.ColWhite);
            gc.DrawTexture(InfoX, InfoY + InfoH + 8, 24, 24, 0, 0, Texture'SkillIconLockPicking');
            gc.SetStyle(DSTY_Normal);
            gc.SetTileColor(HUD.ColBackground);
            
            if(aDeusExMover.bPickable)
            {
                // Bar
                gc.DrawBox(InfoX + 32, InfoY + InfoH + 8, InfoW - 64, 24, 0, 0, 1, Texture'Solid');
                gc.DrawPattern(InfoX + 32, InfoY + InfoH + 8, aDeusExMover.LockStrength * (InfoW - 64), 24, 0, 0, Texture'Solid');
                gc.DrawText(InfoX + 32, InfoY + InfoH + 8, InfoW - 64, 24, FormatString(aDeusExMover.LockStrength * 100) $ " %");
              
                // Owned key
                if(aDeusExMover.KeyIDNeeded != '' && Player.KeyRing != None && Player.KeyRing.HasKey(aDeusExMover.KeyIDNeeded))
                {
                    gc.SetTileColor(HUD.ColWhite);
                    gc.SetStyle(DSTY_Masked);
                    gc.DrawTexture(InfoX + InfoW - 30, InfoY + InfoH + 4, 36, 36, 3, 1, Texture'BeltIconNanoKey');
                    gc.SetStyle(DSTY_Normal);
                }
                
                // Required lockpicks
                else
                {
                    NumTools = int((aDeusExMover.LockStrength / Player.SkillSystem.GetSkillLevelValue(class'SkillLockPicking')) + 0.99);

                    gc.SetTileColor(HUD.ColWhite);
                    gc.SetStyle(DSTY_Masked);
                    gc.DrawTexture(InfoX + InfoW - 30, InfoY + InfoH + 4, 36, 36, 0, 0, Texture'BeltIconLockPick');
                    gc.SetStyle(DSTY_Normal);
    
                    gc.SetTextColor(HUD.ColHeaderText);
                    gc.DrawText(InfoX + InfoW - 24, InfoY + InfoH + 10, 10, 10, string(NumTools));
                }
            }
            else
            {
                gc.DrawPattern(InfoX + 32, InfoY + InfoH + 10, 100, 20, 0, 0, Texture'Solid');
                gc.DrawText(InfoX + 32, InfoY + InfoH + 10, InfoW - 64, 20, MsgInf);
            }

            // Door strength
            gc.SetStyle(DSTY_Masked);
            gc.SetTileColor(HUD.ColWhite);
            gc.DrawTexture(InfoX, InfoY + InfoH + 40, 24, 24, 0, 0, Texture'SkillIconDemolition');
            gc.SetStyle(DSTY_Normal);
            gc.SetTileColor(HUD.ColBackground);
            
            if(aDeusExMover.bBreakable)
            {
                // Bar
                gc.DrawBox(InfoX + 32, InfoY + InfoH + 40, InfoW - 64, 24, 0, 0, 1, Texture'Solid');
                gc.DrawPattern(InfoX + 32, InfoY + InfoH + 40, aDeusExMover.DoorStrength * (InfoW - 64), 24, 0, 0, Texture'Solid');
                gc.DrawText(InfoX + 32, InfoY + InfoH + 40, InfoW - 64, 24, FormatString(aDeusExMover.DoorStrength * 100) $ " %");
            }
            else
            {
                gc.DrawPattern(InfoX + 32, InfoY + InfoH + 42, InfoW - 64, 20, 0, 0, Texture'Solid');
                gc.DrawText(InfoX + 32, InfoY + InfoH + 42, InfoW - 64, 20, MsgInf);
            }
        }
        else
        {
            // Draw a container
            HUD.DrawLightDialog(gc, InfoX, InfoY, InfoW, InfoH, 24, 12);
        }

        // Draw the name
        StrInfo = Caps(StrInfo);
        gc.SetFont(Font'FontLocation');
        gc.SetTextColor(HUD.ColText);
        gc.SetHorizontalAlignment(HALIGN_Left);
        gc.SetVerticalAlignment(VALIGN_Center);
        gc.DrawText(InfoX, InfoY, InfoW, InfoH, StrInfo);
    }

    // Hackable device info
    else if(aHackableDevice != None)
    {
        // Get the name of the device
        StrInfo = DeusExDecoration(aFrobTarget).ItemName;
        StrInfo = Caps(StrInfo);

        gc.SetFont(Font'FontLocation');
        gc.GetTextExtent(0, InfoW, InfoH, StrInfo);

        if(aHackableDevice.HackStrength > 0)
            InfoW = Max(InfoW + 32, 192);

        InfoX = BoxTLX + BoxW + 32;
        InfoY = BoxTLY + 32;

        InfoX = Min(InfoX, Width * 0.8 - InfoW);
        InfoY = Max(InfoY, Height * 0.2); 

        if(aHackableDevice.HackStrength > 0)
        {
            // Draw a container
            HUD.DrawLightDialog(gc, InfoX, InfoY, InfoW, InfoH + 32, 12, 12);
        
            // Draw a bar for the hack value
            gc.SetFont(Font'FontMenuSmall');
            gc.SetTextColor(HUD.ColHeaderText);
            gc.SetHorizontalAlignment(HALIGN_Center);
            gc.SetVerticalAlignment(VALIGN_Center);
            gc.SetStyle(DSTY_Masked);
            gc.SetTileColor(HUD.ColWhite);
            gc.DrawTexture(InfoX, InfoY + InfoH + 8, 24, 24, 0, 0, Texture'SkillIconTech');
            gc.SetStyle(DSTY_Normal);
            gc.SetTileColor(HUD.ColBackground);
            
            if(aHackableDevice.bHackable)
            {
                // Bar
                gc.DrawBox(InfoX + 32, InfoY + InfoH + 8, InfoW - 64, 24, 0, 0, 1, Texture'Solid');
                gc.DrawPattern(InfoX + 32, InfoY + InfoH + 8, aHackableDevice.HackStrength * (InfoW - 64), 24, 0, 0, Texture'Solid');
                gc.DrawText(InfoX + 32, InfoY + InfoH + 8, InfoW - 64, 24, FormatString(aHackableDevice.HackStrength * 100) $ " %");
               
                // Required multitools
                NumTools = int((aHackableDevice.HackStrength / Player.SkillSystem.GetSkillLevelValue(class'SkillTech')) + 0.99);

                gc.SetTileColor(HUD.ColWhite);
                gc.SetStyle(DSTY_Masked);
                gc.DrawTexture(InfoX + InfoW - 30, InfoY + InfoH + 4, 36, 36, 0, 0, Texture'BeltIconMultitool');
                gc.SetStyle(DSTY_Normal);
    
                gc.SetTextColor(HUD.ColHeaderText);
                gc.DrawText(InfoX + InfoW - 24, InfoY + InfoH + 10, 10, 10, string(NumTools));
            }
            else
            {
                gc.DrawPattern(InfoX + 32, InfoY + InfoH + 10, 100, 20, 0, 0, Texture'Solid');
                gc.DrawText(InfoX + 32, InfoY + InfoH + 10, InfoW - 64, 20, MsgInf);
            }
            
            gc.SetFont(Font'FontLocation');
        }
        else
        {
            // Draw a container
            HUD.DrawLightDialog(gc, InfoX, InfoY, InfoW, InfoH, 24, 12);
        }
        
        // Draw the name
        gc.SetTextColor(HUD.ColText);
        gc.SetHorizontalAlignment(HALIGN_Left);
        gc.SetVerticalAlignment(VALIGN_Center);
        gc.DrawText(InfoX, InfoY, InfoW, InfoH, StrInfo);
    }

    // Other frobbables
    else if(!aFrobTarget.bStatic && Player.bObjectNames)
    {
        if(aFrobTarget.IsA('Pawn'))
            StrInfo = Player.GetDisplayName(aFrobTarget);
        else if (aFrobTarget.IsA('DeusExCarcass'))
            StrInfo = DeusExCarcass(aFrobTarget).itemName;
        else if (aFrobTarget.IsA('Inventory'))
            StrInfo = Inventory(aFrobTarget).itemName;
        else if (aFrobTarget.IsA('DeusExDecoration'))
            StrInfo = Player.GetDisplayName(aFrobTarget);
        else if (aFrobTarget.IsA('DeusExProjectile'))
            StrInfo = DeusExProjectile(aFrobTarget).itemName;
        else
            StrInfo = "DEFAULT ACTOR NAME - REPORT THIS AS A BUG - " $ aFrobTarget.GetItemName(String(aFrobTarget.Class));

        gc.SetFont(Font'FontLocation');
        StrInfo = Caps(StrInfo);
        gc.GetTextExtent(0, InfoW, InfoH, StrInfo);
        
        InfoX = BoxTLX + BoxW + 32;
        InfoY = BoxTLY + 32;

        InfoX = Min(InfoX, Width * 0.8 - InfoW);
        InfoY = Max(InfoY, Height * 0.2); 

        // Draw a container
        HUD.DrawLightDialog(gc, InfoX, InfoY, InfoW, InfoH, 24, 12);

        // Draw the text
        gc.SetHorizontalAlignment(HALIGN_Center);
        gc.SetVerticalAlignment(VALIGN_Center);
        gc.SetTextColor(HUD.ColText);
        gc.DrawText(InfoX, InfoY, InfoW, InfoH, StrInfo);
    }
}

// END UE1

defaultproperties
{
    MsgLocked="Door"
    MsgUnlocked="Door"
    MsgInf="INF"
}
