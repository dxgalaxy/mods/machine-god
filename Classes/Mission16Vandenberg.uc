class Mission16Vandenberg extends Mission16;

var int i, TotalAgents;

// BEGIN UE1

function TravelToMap()
{
    DoEvent('WakeUp');
}

function InitMap()
{
    local ScriptedPawn aPawn;
    
    // Count agents
    foreach AllActors(class'ScriptedPawn', aPawn)
    {
        if(aPawn.IsA('HeliosAgentMale') || aPawn.IsA('HeliosAgentFemale'))
            TotalAgents++;
    }
}

function Timer()
{
    local ScriptedPawn aPawn;
    local DeusExCarcass aCarcass; 
    local int NumAgents, NumAgentCarcasses;

    Super.Timer();

    // Check for dead/unconscious agents
    if(!Flags.GetBool('Agents_Dead') && !Flags.GetBool('Agents_Unconscious'))
    {
        foreach AllActors(class'ScriptedPawn', aPawn)
        {
            if(aPawn.IsA('HeliosAgentMale') || aPawn.IsA('HeliosAgentFemale'))
                NumAgents++;
        }
       
        foreach AllActors(class'DeusExCarcass', aCarcass)
        {
            if(aCarcass.IsA('HeliosAgentMaleCarcass') || aCarcass.IsA('HeliosAgentFemaleCarcass'))
            {
                NumAgentCarcasses++;
                Flags.SetBool('Agents_Unconscious', True,, 17); 
            }
        }
        
        if(NumAgents + NumAgentCarcasses < TotalAgents)
        {
            Flags.SetBool('Agents_Dead', True,, 17); 
        }
    }
}

auto state EventManager
{
WakeUp:
    StartInfoLink('DL_EnterBarracks');

    Sleep(1);
    
    TriggerActor('WakeUp');

    DestroyActor('TiffanySavageKidPlayer');

    GiveItem(class'WeaponProd', 'Player');
    GiveItem(class'AmmoBattery', 'Player', 8);
    GiveItem(class'WeaponStealthPistol', 'Player');
    GiveItem(class'Ammo10mm', 'Player', 50);
    GiveItem(class'Lockpick', 'Player');
    GiveItem(class'Multitool', 'Player');
    
    Fade(1, 1);

    EndEvent();
}

// END UE1

