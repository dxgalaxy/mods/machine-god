class TextImport expands Object abstract;

// ==========================================
// Mission 16
// ==========================================
// Books
#exec DEUSEXTEXT IMPORT FILE=Text\16_Book_TheArtOfLucidDreaming.txt

// ==========================================
// Mission 17
// ==========================================
// Data cubes
#exec DEUSEXTEXT IMPORT FILE=Text\17_Border_ComputerAccessDataCube.txt
#exec DEUSEXTEXT IMPORT FILE=Text\17_Border_EvidenceDataCube.txt
#exec DEUSEXTEXT IMPORT FILE=Text\17_Slums_CrackDenDataCube.txt
#exec DEUSEXTEXT IMPORT FILE=Text\17_Slums_BorderDoorCodeDataCube.txt

// Border checkpoint emails
#exec DEUSEXTEXT IMPORT FILE=Text\17_Email01.txt
#exec DEUSEXTEXT IMPORT FILE=Text\17_Email02.txt
#exec DEUSEXTEXT IMPORT FILE=Text\17_EmailMenu_anon123.txt
 
// ==========================================
// Mission 18
// ==========================================
// Data cubes
#exec DEUSEXTEXT IMPORT FILE=Text\18_AntonLockDataCube.txt

// Adopter emails
#exec DEUSEXTEXT IMPORT FILE=Text\18_Email01.txt
#exec DEUSEXTEXT IMPORT FILE=Text\18_Email02.txt
#exec DEUSEXTEXT IMPORT FILE=Text\18_Email03.txt
#exec DEUSEXTEXT IMPORT FILE=Text\18_Email04.txt
#exec DEUSEXTEXT IMPORT FILE=Text\18_Email05.txt
#exec DEUSEXTEXT IMPORT FILE=Text\18_Email06.txt
#exec DEUSEXTEXT IMPORT FILE=Text\18_Email07.txt
#exec DEUSEXTEXT IMPORT FILE=Text\18_EmailMenu_jose.txt
#exec DEUSEXTEXT IMPORT FILE=Text\18_EmailMenu_lwinger.txt
#exec DEUSEXTEXT IMPORT FILE=Text\18_EmailMenu_maria.txt
#exec DEUSEXTEXT IMPORT FILE=Text\18_EmailMenu_marina.txt
#exec DEUSEXTEXT IMPORT FILE=Text\18_EmailMenu_stefano.txt

// ==========================================
// Mission 19
// ==========================================
// Data cubes
#exec DEUSEXTEXT IMPORT FILE=Text\19_AntonVatDataCube.txt
#exec DEUSEXTEXT IMPORT FILE=Text\19_LarryVatDataCube.txt
#exec DEUSEXTEXT IMPORT FILE=Text\19_MikeVatDataCube.txt
#exec DEUSEXTEXT IMPORT FILE=Text\19_TiffanyVatDataCube.txt

// ==========================================
// Mission 20
// ==========================================
// North Point
#exec DEUSEXTEXT IMPORT FILE=Text\20_DataCube_CreepyApartment.txt
#exec DEUSEXTEXT IMPORT FILE=Text\20_DataCube_DrugDealer.txt
#exec DEUSEXTEXT IMPORT FILE=Text\20_Newspaper01.txt
#exec DEUSEXTEXT IMPORT FILE=Text\20_Newspaper02.txt

// Triad compond
#exec DEUSEXTEXT IMPORT FILE=Text\20_EmailMenu_TTong.txt
#exec DEUSEXTEXT IMPORT FILE=Text\20_Email_TTong_OfficeCode1.txt
#exec DEUSEXTEXT IMPORT FILE=Text\20_Email_TTong_OfficeCode2.txt
#exec DEUSEXTEXT IMPORT FILE=Text\20_Email_TTong_ReinforcedRoom1.txt
#exec DEUSEXTEXT IMPORT FILE=Text\20_Email_TTong_ReinforcedRoom2.txt
#exec DEUSEXTEXT IMPORT FILE=Text\20_Email_TTong_HenosiaDistribution.txt
#exec DEUSEXTEXT IMPORT FILE=Text\20_DataCube_TracersHiddenRoom.txt
#exec DEUSEXTEXT IMPORT FILE=Text\20_DataCube_TriadDiningHall.txt
#exec DEUSEXTEXT IMPORT FILE=Text\20_EmailMenu_PDenton.txt
#exec DEUSEXTEXT IMPORT FILE=Text\20_Email_PDenton_RoomDecoration1.txt
#exec DEUSEXTEXT IMPORT FILE=Text\20_Email_PDenton_RoomDecoration2.txt
#exec DEUSEXTEXT IMPORT FILE=Text\20_DataCube_PaulsComputer.txt
