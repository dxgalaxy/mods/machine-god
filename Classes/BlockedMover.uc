class BlockedMover extends DeusExMover;

var(Mover) name BlockedBy;

// BEGIN UE1

function bool ShouldFixTag()
{
    return Tag == 'BlockedMover';
}

function PostBeginPlay()
{
    if(ShouldFixTag())
        Tag = 'DeusExMover';

    Super.PostBeginPlay();
}

function DoOpen()
{
    local DeusExMover aMover;

    if(BlockedBy != '')
    {
        foreach AllActors(class'DeusExMover', aMover, BlockedBy)
        {
            if(!aMover.bDestroyed && !aMover.bHidden && !aMover.bOpening)
                return;
        }
    }

    Super.DoOpen();
}

// END UE1
