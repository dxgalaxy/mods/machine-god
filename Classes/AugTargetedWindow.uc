class AugTargetedWindow extends Window;

var float Progress;

var DeusExPlayer Player;
var ScriptedPawn aHighlight;
var bool bIrrelevantTarget;

var Color ColBackground;
var Color ColBorder;
var Color ColText;

// BEGIN UE1

function InitWindow()
{
    SetWindowAlignments(HALIGN_Full, VALIGN_Full);
    Super.InitWindow();

    Player = TiffanySavagePlayer(GetRootWindow().ParentPawn);

    StyleChanged();
}

function DrawWindow(GC gc)
{
    local float             InfoX, InfoY, InfoW, InfoH;
    local Vector            CenterLoc;
    local float             BoxCX, BoxCY, BoxX, BoxY, BoxW, BoxH;
    local int               i;
    local string            Message;

    // Draw the "scanning" message
    if(aHighlight == None)
    {
        if(bIrrelevantTarget)
            Message = "Incompatible target";
        else
            Message = "Scanning...";

        gc.SetFont(Font'FontLocation');
        gc.GetTextExtent(0, InfoW, InfoH, Message);
        
        InfoX = (Width / 2) - (InfoW / 2);
        InfoY = (Height / 2) - InfoH - 20;

        gc.SetTextColor(ColText);
        gc.DrawText(InfoX, InfoY, InfoW, InfoH, Message);
    }

    // Draw a targeting box
    else
    {
        CenterLoc = aHighlight.Location + Vect(0, 0, 1) * aHighlight.BaseEyeHeight;

        ConvertVectorToCoordinates(CenterLoc, BoxCX, BoxCY);

        BoxW = Lerp(Progress, Width, 10);
        BoxH = Lerp(Progress, Height, 10);
        BoxX = BoxCX - (BoxW / 2);
        BoxY = BoxCY - (BoxH / 2);

        // Draw white lines first, then black 
        gc.SetTileColorRGB(0,0,0);

        for (i=1; i>=0; i--)
        {
            // Main box
            gc.DrawBox(BoxX + i, BoxY, BoxW, BoxH, 0, 0, 1, Texture'Solid');
           
            // Top line
            gc.DrawBox(BoxCX + i, 0, 1, BoxY, 0, 0, 1, Texture'Solid');
            
            // Bottom line
            gc.DrawBox(BoxCX + i, BoxY + BoxH, 1, Height - (BoxY + BoxH), 0, 0, 1, Texture'Solid');
            
            // Left line
            gc.DrawBox(0, BoxCY + i, BoxX, 1, 0, 0, 1, Texture'Solid');
            
            // Right line
            gc.DrawBox(BoxX + BoxW, BoxCY + i, Width - (BoxX + BoxW), 1, 0, 0, 1, Texture'Solid');

            gc.SetTileColor(ColText);
        }
    }
}

event StyleChanged()
{
    local ColorTheme theme;

    Theme = Player.ThemeManager.GetCurrentHUDColorTheme();

    ColBackground = Theme.GetColorFromName('HUDColor_Background');
    ColBorder     = Theme.GetColorFromName('HUDColor_Borders');
    ColText       = Theme.GetColorFromName('HUDColor_HeaderText');
}

// END UE1
