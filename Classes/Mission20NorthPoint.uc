// =============================================================================
// Mission20 - Hong Kong - North Point
// =============================================================================
class Mission20NorthPoint extends Mission20;

// BEGIN UE1

// -----------------------------------------------------------------------------
// Initialisation
// -----------------------------------------------------------------------------
function InitMap()
{
    local ScriptedPawnKnowledge aKnowledge;

    // Doors
    OpenDoor('TempleGate');

    // Knowledge
    foreach AllActors(class'ScriptedPawnKnowledge', aKnowledge)
    {
        aKnowledge.AddObject('TriadRoadBlock', "at the North East end of the district");
        aKnowledge.AddSubject('TriadRoadBlock', 'RandomFemale');
        aKnowledge.AddSubject('TriadRoadBlock', 'RandomMale');
        aKnowledge.AddSubject('TriadRoadBlock', 'ArmsDealer');
        aKnowledge.AddSubject('TriadRoadBlock', 'Pharmacist');
        aKnowledge.AddSubject('TriadRoadBlock', 'GordonQuick');
    
        aKnowledge.AddCustomNote('GordonQuick_Location', "Gordon Quick is at the tea restaurant in North Point. He is wearing a bulletproof vest and tech goggles.");
        aKnowledge.AddSubject('GordonQuick_Location', 'DrugDealer');
    
        aKnowledge.AddCustomNote('Bypass_Roadblock', "There is a hole in the wall behind some boxes in the shop at the corner of Kam Ping Street");
        aKnowledge.AddSubject('Bypass_Roadblock', 'RoadblockBum');
    }

    // Auto repair shop
    ToggleAutoRepairPower(Flags.GetBool('Auto_Repair_Power_On'));
    
    // Event bound to flags
    BindEventToFlag('TriadSoldiersStartPatrol', 'OverhearTriadSoldiersStreet_Played');
    BindEventToFlag('DeckerErinWayneDisperse', 'OverhearDeckerErinWayne_Played');
    BindEventToFlag('IrisDisperse', 'MeetIris_Played');
}

// -----------------------------------------------------------------------------
// Ad hoc functions
// -----------------------------------------------------------------------------
function ToggleAutoRepairPower(bool bOn)
{
    local Actor aActor;
    local Lightbulb aLightbulb;
    local HangingShopLight aShopLight;
    local Light aLight;

    Flags.SetBool('Auto_Repair_Power_On', bOn);

    foreach AllActors(class'Actor', aActor, 'AutoRepairLight')
    {
        aLight = Light(aActor);

        if(aLight != None)
        {
            if(bOn)
                aLight.LightBrightness = 32;
            else
                aLight.LightBrightness = 0;
        }
        
        aShopLight = HangingShopLight(aActor);

        if(aShopLight != None)
        {
            if(bOn)
                aShopLight.ScaleGlow = 2;
            else
                aShopLight.ScaleGlow = 0.25;
        }
        
        aLightbulb = Lightbulb(aActor);

        if(aLightbulb != None)
        {
            if(bOn)
                aLightbulb.ScaleGlow = 2;
            else
                aLightbulb.ScaleGlow = 0.25;
        }
    }
}

// -----------------------------------------------------------------------------
// Timer
// -----------------------------------------------------------------------------
function Timer()
{
    Super.Timer();

    SetActorCollision('VTOLInventory', IsDoorOpen('VTOLStash'));
}

// -----------------------------------------------------------------------------
// Sequences
// -----------------------------------------------------------------------------
function UpdateSequence(name Sequence, out name Step)
{
    switch(Sequence)
    {
        // Decker Parkes rescue
        case 'DeckerParkesRescue':
            switch(Step)
            {
                case 'Bark1':
                    if(
                        IsConversationDone('DeckerParkesBark1') &&
                        !IsPawnConversing('DeckerParkes')
                    ) 
                        Step = 'Bark2';
                    
                    else if(!IsPawnConversing('DeckerParkes'))
                        StartConversation('DeckerParkes', 'DeckerParkesBark1', False, True);
                
                    break;

                case 'Bark2':
                    if(
                        IsConversationDone('DeckerParkesBark2') &&
                        !IsPawnConversing('DeckerParkes')
                    ) 
                        Step = 'Bark3';
                    
                    else if(
                        GetActorDistance('Player', 'DeckerParkes') < 256 &&
                        !IsPawnConversing('DeckerParkes')
                    )
                        StartConversation('DeckerParkes', 'DeckerParkesBark2');
    
                    break;

                case 'Bark3':
                    if(
                        IsConversationDone('DeckerParkesBark3') &&
                        !IsPawnConversing('DeckerParkes')
                    ) 
                        Step = 'Meet';
                    
                    else if(
                        Flags.GetBool('Auto_Repair_Power_On') &&
                        !IsPawnConversing('DeckerParkes')
                    )
                        StartConversation('DeckerParkes', 'DeckerParkesBark3');
                    
                    break;
            
                case 'Meet':
                    if(
                        IsConversationDone('MeetDeckerParkes') &&
                        !IsPawnConversing('DeckerParkes')
                    ) 
                        Step = 'RunToGate1';

                    else if(
                        IsDoorOpen('AutoRepairInnerGate') &&
                        !IsPawnConversing('DeckerParkes')
                    )
                        StartConversation('DeckerParkes', 'MeetDeckerParkes');

                    break;

                case 'RunToGate1':
                    if(IsPawnStandingAt('DeckerParkes', 'DeckerParkesGate1Point'))
                        Step = 'FrobGate1';

                    else if(!IsPawnFollowingOrders('DeckerParkes', 'RunningTo', 'DeckerParkesGate1Point'))
                        OrderPawn('DeckerParkes', 'RunningTo', 'DeckerParkesGate1Point');

                    break;

                case 'FrobGate1':
                    if(IsDoorOpen('AutoRepairOuterGate'))
                        Step = 'RunToEscapePoint1';

                    else if(
                        Flags.GetBool('Auto_Repair_Power_On') &&
                        !IsActorInState('DeckerParkes', 'Frobbing')
                    )
                        MakePawnFrob('DeckerParkes', 'AutoRepairOuterGateButtonUp');
           
                    break;

                case 'RunToEscapePoint1':
                    if(
                        GetActorDistance('Player', 'DeckerParkes') < 256 &&
                        AreActorsOverlapping('DeckerParkes', 'DeckerParkesEscapePoint1')
                    )
                        Step = 'RunToEscapePoint2';
            
                    else if(!IsPawnFollowingOrders('DeckerParkes', 'RunningTo', 'DeckerParkesEscapePoint1'))
                        OrderPawn('DeckerParkes', 'RunningTo', 'DeckerParkesEscapePoint1');
                        
                    break;

                case 'RunToEscapePoint2':
                    if(
                        GetActorDistance('Player', 'DeckerParkes') < 256 &&
                        AreActorsOverlapping('DeckerParkes', 'DeckerParkesEscapePoint2')
                    )
                        Step = 'RunToGate2';

                    else if(!IsPawnFollowingOrders('DeckerParkes', 'RunningTo', 'DeckerParkesEscapePoint2'))
                        OrderPawn('DeckerParkes', 'RunningTo', 'DeckerParkesEscapePoint2');
                    
                    break;
                    
                case 'RunToGate2':
                    if(IsPawnStandingAt('DeckerParkes', 'DeckerParkesGate2Point'))
                        Step = 'FrobGate2';
                        
                    else if(!IsPawnFollowingOrders('DeckerParkes', 'RunningTo', 'DeckerParkesGate2Point'))
                        OrderPawn('DeckerParkes', 'RunningTo', 'DeckerParkesGate2Point');
                    
                    break;

                case 'FrobGate2':
                    if(IsDoorOpen('EscapePoint2Gate'))
                        Step = 'RunToTavernPoint';
                        
                    else if(!IsActorInState('DeckerParkes', 'Frobbing'))
                        MakePawnFrob('DeckerParkes', 'EscapePoint2GateButton');
                    
                    break;

                case 'RunToTavernPoint':
                    if(
                        AreActorsOverlapping('DeckerParkes', 'DeckerParkesTavernPoint') &&
                        GetActorDistance('Player', 'DeckerParkes') < 256
                    )
                        Step = 'Bark4';
                        
                    else if(!IsPawnFollowingOrders('DeckerParkes', 'RunningTo', 'DeckerParkesTavernPoint'))
                        OrderPawn('DeckerParkes', 'RunningTo', 'DeckerParkesTavernPoint');
                   
                    break;

                case 'Bark4':
                    if(
                        IsConversationDone('DeckerParkesBark4') &&
                        !IsPawnConversing('DeckerParkes')
                    )
                        Step = 'GoToBartender';

                    else if(!IsPawnConversing('DeckerParkes'))
                        StartConversation('DeckerParkes', 'DeckerParkesBark4');

                    break;

                case 'GoToBartender':
                    if(
                        AreActorsOverlapping('DeckerParkes', 'DeckerParkesBartenderPoint') &&
                        GetActorDistance('Player', 'DeckerParkes') < 256
                    )
                        Step = 'OverhearDeckerBartender';

                    else if(!IsPawnFollowingOrders('DeckerParkes', 'GoingTo', 'DeckerParkesBartenderPoint'))
                        OrderPawn('DeckerParkes', 'GoingTo', 'DeckerParkesBartenderPoint');

                    break;

                case 'OverhearDeckerBartender':
                    if(
                        IsConversationDone('OverhearDeckerBartender') &&
                        !IsPawnConversing('DeckerParkes')
                    )
                    {
                        if(!IsDoorOpen('VendingMachineBase'))
                            Step = 'GoToFingerprintScanner';
                        else
                            Step = 'GoToHideout';
                    }
                    else if(!IsPawnConversing('DeckerParkes'))
                    {
                        StartConversation('DeckerParkes', 'OverhearDeckerBartender');
                    }
                    
                    break;

                case 'GoToFingerprintScanner':
                    if(IsPawnStandingAt('DeckerParkes', 'DeckerParkesFingerprintScannerPoint'))
                        Step = 'FrobFingerprintScannerDoor';
                    
                    else if(!IsPawnFollowingOrders('DeckerParkes', 'GoingTo', 'DeckerParkesFingerprintScannerPoint'))
                        OrderPawn('DeckerParkes', 'GoingTo', 'DeckerParkesFingerprintScannerPoint');

                    break;

                case 'FrobFingerprintScannerDoor':
                    if(IsDoorOpen('VendingMachineFingerprintScannerDoor'))
                        Step = 'FrobFingerprintScanner';
                    
                    else if(!IsPawnFollowingOrders('DeckerParkes', 'Frobbing', 'VendingMachineFingerprintScannerDoor'))
                        MakePawnFrob('DeckerParkes', 'VendingMachineFingerprintScannerDoor');
    
                    break;
                
                case 'FrobFingerprintScanner':
                    if(IsDoorOpen('VendingMachineBase'))
                        Step = 'Bark5';
                    
                    else if(!IsPawnFollowingOrders('DeckerParkes', 'Frobbing', 'VendingMachineFingerprintScanner'))
                        MakePawnFrob('DeckerParkes', 'VendingMachineFingerprintScanner');
                    
                    break;

                case 'Bark5':
                    if(
                        IsConversationDone('DeckerParkesBark5') &&
                        !IsPawnConversing('DeckerParkes')
                    )
                        Step = 'GoToHideout';

                    else if(
                        GetSequenceChangeTime(Sequence) > 1.0 &&
                        GetActorDistance('Player', 'DeckerParkes') < 256 &&
                        !IsPawnConversing('DeckerParkes')
                    )
                    {
                        StartConversation('DeckerParkes', 'DeckerParkesBark5', False, False, 'GestureRight');
                    }

                    break;

                case 'GoToHideout':
                    if(AreActorsOverlapping('DeckerParkes', 'DeckerParkesHideoutPoint'))
                        Step = 'MeetIris';
                    
                    else if(!IsPawnFollowingOrders('DeckerParkes', 'GoingTo', 'DeckerParkesHideoutPoint'))
                        OrderPawn('DeckerParkes', 'GoingTo', 'DeckerParkesHideoutPoint');
                    
                    break;

                case 'MeetIris':
                    if(IsConversationDone('MeetIris'))
                        Step = 'GoChat';

                    break;

                case 'GoChat':
                    if(AreActorsOverlapping('DeckerParkes', 'DeckerParkesChatPoint'))
                        Step = 'End';
                    
                    else if(!IsPawnFollowingOrders('DeckerParkes', 'GoingTo', 'DeckerParkesChatPoint'))
                        OrderPawn('DeckerParkes', 'GoingTo', 'DeckerParkesChatPoint');
                    
                    break;
            }
            
            break;
    }
}

// -----------------------------------------------------------------------------
// Events
// -----------------------------------------------------------------------------
auto state EventManager
{
// -----
// Intro
// -----
IntroSequenceContinue:
    // Hide Mike
    SetActorHidden('Mike', True);

    // Brief Tiffany
    StartInfoLink('DL_MikeTiffanyBriefing');

    Sleep(20);

    // Boom shakalak!
    GetPlayer().ShakeView(11, 64, 8);
    SetLightColor('VTOLLight', 0, 0, 0);
    PlayTransientSound('Player', "DeusExSounds.Generic.MediumExplosion1", 1, 0.5); 

    Sleep(1);

    // Tiffany says WTF
    StartInfoLink('DL_MikeTiffanyTrouble');

    Sleep(1);

    // Flicker lights
    while(GetPlayer().DataLinkPlay != None)
    {
        if(Rand(2) > 0)
            SetLightColor('VTOLLight', 0, 0, 0);
        else 
            SetLightColor('VTOLLight', 64, 148, 127);
    
        Sleep(FRand() * 0.5);
    }

IntroSequenceLanding:
    // Reset VTOL interior
    SetLightColor('VTOLLight', 64, 148, 127);
    GetActor('VTOLAmbiance').AmbientSound = None;
    SetActorCollision('ExitVTOLButton', True, False, True);

    // Start landing
    SetActorState('VTOL', 'Landing');
    TriggerActor('IntroSequenceLanding1');

    Sleep(26);
    
    // Show Mike
    SetActorHidden('Mike', False);

    EndEvent();

ExitVTOL:
    ModernRootWindow(GetPlayer().RootWindow).Fade(0, 0.5);
    GetPlayer().ShowHUD(False);
    
    Sleep(0.5);
   
    PlayTransientSound('Player', "MoverSFX.door.MachineDoor2", 1, 1);

    Sleep(1.0);
    
    PlayTransientSound('Player', "MoverSFX.door.Pneumatic1Close", 1, 1);

    MoveActor('Player', 'VTOLOutsideTeleporter');
   
    if(!Flags.GetBool('MeetMike_Played'))
    {
        StartConversation('Mike', 'MeetMike');
    }
    else
    {
        ModernRootWindow(GetPlayer().RootWindow).Fade(1, 0.5);
        GetPlayer().ShowHUD(True);
    }
    
    EndEvent();

EnterVTOL:
    ModernRootWindow(GetPlayer().RootWindow).Fade(0, 0.5);
    GetPlayer().ShowHUD(False);
    
    Sleep(0.5);
    
    PlayTransientSound('Player', "MoverSFX.door.MachineDoor2", 1, 1);

    Sleep(1.0);
    
    PlayTransientSound('Player', "MoverSFX.door.Pneumatic1Close", 1, 1);

    MoveActor('Player', 'VTOLInsideTeleporter');
    
    ModernRootWindow(GetPlayer().RootWindow).Fade(1, 0.5);
    GetPlayer().ShowHUD(True);
    
    EndEvent();

MeetMikeConvoEnded:
    OrderPawn('Mike', 'Sitting', 'MikeSeat');
    Sleep(2.0);
    StartInfoLink('DL_JuliaMontesTiffanyBriefing');

    EndEvent();

// ----------------
// Auto repair shop
// ----------------
PlayerLandedInAutoRepairShop:
    StartSequence('DeckerParkesRescue', 'Bark1');
    
    EndEvent();

PlayerFlippedAutoRepairSwitch:
    ToggleAutoRepairPower(!Flags.GetBool('Auto_Repair_Power_On'));
  
    EndEvent();

PlayerPressedAutoRepairInnerGateUp:
    if(Flags.GetBool('Auto_Repair_Power_On') && !IsDoorOpen('AutoRepairInnerGate'))
        OpenDoor('AutoRepairInnerGate');

    EndEvent();

PlayerPressedAutoRepairInnerGateDown:
    if(Flags.GetBool('Auto_Repair_Power_On') && IsDoorOpen('AutoRepairInnerGate'))
        CloseDoor('AutoRepairInnerGate');

    EndEvent();

PlayerPressedAutoRepairOuterGateUp:
    if(Flags.GetBool('Auto_Repair_Power_On') && !IsDoorOpen('AutoRepairOuterGate'))
        OpenDoor('AutoRepairOuterGate');

    EndEvent();

PlayerPressedAutoRepairOuterGateDown:
    if(Flags.GetBool('Auto_Repair_Power_On') && IsDoorOpen('AutoRepairOuterGate'))
        CloseDoor('AutoRepairOuterGate');

    EndEvent();

// -------
// General
// -------
PlayerThreatenedTriadBrutes:
    SetPawnAlly('TriadBrute', 'Player', -1);

    EndEvent();

TriadSoldiersStartPatrol:
    OrderPawn('TriadSoldierFemale3', 'Patrolling', 'StreetTriadPatrol1');
    OrderPawn('TriadSoldierMale3', 'Patrolling', 'StreetTriadPatrol3');

    EndEvent();

IrisDisperse:
    OrderPawn('Iris', 'GoingTo', 'IrisPlanPoint');

    EndEvent();

DeckerErinWayneDisperse:
    MakePawnSit('DeckerParkes', 'DeckerParkesSeat');
    OrderPawn('ErinTodd', 'GoingTo', 'ErinToddPoint');
    OrderPawn('WayneYoung', 'GoingTo', 'WayneYoungPoint');

    EndEvent();

PlayerStoleNSFGear:
    if(Flags.GetBool('MeetDeckerParkes_Played'))
        StartConversation('DeckerParkes', 'PlayerStoleNSFGearBark');

    EndEvent();

}

function DoTest(name Test)
{
    switch(Test)
    {
        case 'SaveDecker':
            Flags.SetBool('Player_Got_Decker_Quest', True);
            Flags.SetBool('BartenderGiveQuest_Played', True);
            MoveActor('Player', 'AutoRepairShop');
            break;

        case 'DeckerSafe':
            DoTest('SaveDecker');
            Flags.SetBool('OverhearDeckerBartender_Played', True);
            Flags.SetBool('MeetDeckerParkes_Played', True);
            Flags.SetBool('Auto_Repair_Power_On', True);
            ToggleAutoRepairPower(True);
            MoveActor('DeckerParkes', 'DeckerParkesBartenderPoint');
            MoveActor('Player', 'DeckerParkesBartenderPoint');
            SetCurrentSequenceStep('DeckerParkesRescue', 'GoToFingerprintScanner'); 
            break;

        case 'NSFDisperse':
            DoTest('DeckerSafe');
            MoveActor('DeckerParkes', 'DeckerParkesChatPoint');
            MoveActor('Player', 'DeckerParkesChatPoint');
            Flags.SetBool('MeetIris_Played', True);
            Flags.SetBool('OverhearDeckerErinWayne_Played', True);
            StopSequence('DeckerParkesRescue');
            OrderPawn('Iris', 'GoingTo', 'IrisPlanPoint');
            break;
    }
}

// END UE1
