//=============================================================================
// SeamlessTeleporter - Teleports an actor while keeping relative offsets
//=============================================================================
class SeamlessTeleporter extends Teleporter;

var(Teleporter) bool bRelativeYaw;

// BEGIN UE1

// --------------------------------------
// Accept an actor that has teleported in
// --------------------------------------
simulated function bool Accept(Actor Incoming, Actor Source)
{
    local Rotator NewRot, OldRot;
    local float Magnitude;
    local Vector OldDir, NewLocation;
    local Pawn aPawn;
    local Actor aActor;

    Disable('Touch');
 
    // Offset location relative to the source
    if(Source != None)
        NewLocation = Location + (Incoming.Location - Source.Location);
    else
        NewLocation = Location;

    // Use ViewRotation for players
    NewRot = Incoming.Rotation;

    if(Incoming.IsA('DeusExPlayer'))
        NewRot = DeusExPlayer(Incoming).ViewRotation;
   
    // Change the yaw
    if(bChangesYaw)
    {
        OldRot = NewRot;
        NewRot.Yaw = Rotation.Yaw;
        
        if(Source != None && bRelativeYaw)
            NewRot.Yaw += (32768 + OldRot.Yaw - Source.Rotation.Yaw);
    }

    // Trigger event
    if(Event != '')
    {
        foreach AllActors(class'Actor', aActor, Event)
        {
            aActor.Trigger(Self, Pawn(Incoming));
        }
    }

    if(Pawn(Incoming) != None)
    {
        // Notify enemies
        if(Role == ROLE_Authority)
        {
            aPawn = Level.PawnList;

            While(aPawn != None)
            {
                if(aPawn.Enemy == Incoming)
                    aPawn.LastSeenPos = Incoming.Location; 
                
                aPawn = aPawn.NextPawn;
            }
        }
        
        Pawn(Incoming).SetLocation(NewLocation);
        
        if ( (Role == ROLE_Authority) 
            || (Level.TimeSeconds - LastFired > 0.5) )
        {
            Pawn(Incoming).SetRotation(NewRot);
            Pawn(Incoming).ViewRotation = NewRot;
            LastFired = Level.TimeSeconds;
        }
        Pawn(Incoming).MoveTimer = -1.0;
        Pawn(Incoming).MoveTarget = self;
        PlayTeleportEffect( Incoming, false);
    }
    else
    {
        if(!Incoming.SetLocation(NewLocation))
        {
            Enable('Touch');

            return False;
        }

        if(bChangesYaw)
        {
            if(Incoming.IsA('DeusExPlayer'))
                DeusExPlayer(Incoming).ViewRotation = NewRot;
            else
                Incoming.SetRotation(NewRot);
        }
    }

    Enable('Touch');

    // Handle velocity
    if(bChangesVelocity)
    {
        Incoming.Velocity = TargetVelocity;
    }
    else
    {
        if(bChangesYaw)
        {
            OldDir = Vector(OldRot);
            Magnitude = Incoming.Velocity Dot OldDir;     
            Incoming.Velocity = Incoming.Velocity - Magnitude * OldDir + Magnitude * Vector(Incoming.Rotation);
        } 
        
        if(bReversesX)
            Incoming.Velocity.X *= -1.0;
        
        if(bReversesY)
            Incoming.Velocity.Y *= -1.0;
        
        if(bReversesZ)
            Incoming.Velocity.Z *= -1.0;
    }   

    return True;
}

// ---------------------------------------------------------------
// Touch event, stores offset information if we're traversing maps
// ---------------------------------------------------------------
simulated function Touch(Actor aOther)
{
    local HumanExtended aPlayer;

    aPlayer = HumanExtended(aOther);

    if(
        aPlayer != None &&
        aPlayer.bCanTeleport &&
        !aPlayer.PreTeleport(Self) &&
        (
            InStr(URL, "/" ) >= 0 ||
            InStr( URL, "#" ) >= 0
        ) &&
        Role == ROLE_Authority
    )
    {
        aPlayer.StoreTravelOffset(Self);
    }

    Super.Touch(aOther);
}

// END UE1
